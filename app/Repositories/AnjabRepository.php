<?php

namespace App\Repositories;


use App\Models\TrxAnjab;
use App\Models\TrxSkjUpload;

class AnjabRepository{

	function getDataAnjab($kodeOpd, $jenis, $searchJabatan = null){
		// dd($kodeOpd);
		// dd($kodeOpd);
		$activePeriode                                  = \MojokertokabApp::getActivePeriod();
		// dd($activePeriode);
		$anjab                                          = TrxAnjab::query();
		
		

		$anjab                                          = $anjab->where('m_periode_id', $activePeriode->id);
        if(\MojokertokabUser::getUser()->role == 'PD'){
		    $anjab                                      = $anjab->with('jabatanStruktural')
				->where('jenis_jabatan', $jenis)
				->where(function($query) use ($kodeOpd) {
					return $query->where('user_opd', 'LIKE', $kodeOpd.'%')
					->orWhere('user_opd', '=', null)
					->orWhere('kode_opd', '=', null);
				});
        }
		// dd($anjab);
		if($jenis != NULL){
            $anjab                                      = $anjab->where('jenis_jabatan', $jenis);
		}

    	if($searchJabatan){
            $anjab                                      = $anjab->where('nama', 'like', '%'.$searchJabatan.'%');
    	}
		
		$anjab                                          = $anjab->unitKerja($kodeOpd);
		// dd($anjab->count());
		// Jika kode OPD adalah 28, tambahkan filter untuk hanya mengambil data dengan parent yang tidak null
		if ($kodeOpd === '28') {
			$anjab->whereNotNull('parent');
		}
		// dd($anjab);
		// return $anjab->get();
		// dd($anjab->count());
		
		
		$anjab                                          = $anjab->orderByRaw('ISNULL(eselon_jabatan), eselon_jabatan ASC')->get();
		// dd($anjab);
		return $anjab;
	}

	function getStukturalAnjab($kodeOpd){
		$pejabatStruktural                              = TrxAnjab::query();

    	// $pejabatStruktural = $pejabatStruktural->where('jenis_jabatan', 1)->where(function($query) use($kodeOpd){
    	// 		return $query->where('unit_kerja_eselon2', 'LIKE', $kodeOpd.'%')->orWhere('unit_kerja_eselon3', 'LIKE', $kodeOpd.'%')->orWhere('unit_kerja_eselon3', 'LIKE', $kodeOpd.'%');
    	// 	})->orWhere('kode_jabatan_2', '110001')->orderBy('eselon_jabatan', 'ASC')->get();

    	$pejabatStruktural                           = $pejabatStruktural->where('jenis_jabatan', 1)->where('unit_kerja', 'LIKE', $kodeOpd.'%')->orWhere('kode_jabatan_2', '999999')->orderBy('eselon_jabatan', 'ASC')->get();
		// dd($pejabatStruktural);
    	return $pejabatStruktural;
	}

	function getFungsionalAnjab($kodeOpd){
		$pejabatFungsional = TrxAnjab::query()
			->where('jenis_jabatan', 2)
			->where('unit_kerja', 'LIKE', $kodeOpd.'%')
			->where('parent', 8) // Tambahkan kondisi parent = 8
			->orWhere('kode_jabatan_2', '999999')
			->get();
	
		return $pejabatFungsional;
	}
	

	function getDetailAnjab($id){
		return TrxAnjab::find($id);
	}

	function createAnjabPelaksana($jabatanId, $kodeJabatan, $kodeOpd, $jenisJabatan, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja,  $diklatPenjenjangan, $parent, $user, $unitKerja, $periode){

		$anjab                                    = new TrxAnjab;

		$anjab->m_jabatan_id                      = $jabatanId;
        $anjab->kode_opd                          = $kodeOpd;
        $anjab->jenis_jabatan                     = $jenisJabatan;
        $anjab->eselon_jabatan                    = NULL;
        $anjab->kode_jabatan_2                    = $kodeJabatan;
        $anjab->{'_kode_jabatan'}                 = $kodeJabatan;
        $anjab->diklat_teknis                     = $diklatTeknis;
        $anjab->pengalaman_kerja                  = $pengalamanKerja;
        $anjab->pengetahuan_kerja                 = $pengetahuanKerja;
        $anjab->diklat_penjenjangan                      = $diklatPenjenjangan;
		$anjab->parent                            = $parent;
        $anjab->user                              = $user;
        $anjab->unit_kerja                        = $unitKerja;
        $anjab->m_periode_id                      = $periode;
		// dd($anjab);
        $anjab->save();
	}

	function updateAnjabPelaksana($id, $jabatanId, $kodeJabatan, $kodeOpd, $jenisJabatan, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $parent, $user, $unitKerja, $periode){

		$anjab                                    = TrxAnjab::find($id);

		$anjab->m_jabatan_id                      = $jabatanId;
        $anjab->kode_opd                          = $kodeOpd;
        $anjab->jenis_jabatan                     = $jenisJabatan;
        $anjab->eselon_jabatan                    = NULL;
        $anjab->kode_jabatan_2                    = $kodeJabatan;
        $anjab->{'_kode_jabatan'}                 = $kodeJabatan;
        $anjab->diklat_teknis                     = $diklatTeknis;
        $anjab->pengalaman_kerja                  = $pengalamanKerja;
        $anjab->pengetahuan_kerja                 = $pengetahuanKerja;
        $anjab->parent                            = $parent;
        $anjab->user                              = $user;
        $anjab->unit_kerja                        = $unitKerja;
        $anjab->m_periode_id                      = $periode;

        $anjab->update();
	}

    function createAnjabFungsional($jabatanId, $kodeJabatan, $kodeOpd, $jenisJabatan, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $jenjangfungsional, $diklatpenjenjangan, $kualifikasipendidikan, $parent, $user, $unitKerja, $periode){

		$anjab                                    = new TrxAnjab;

		$anjab->m_jabatan_id                      = $jabatanId;
        $anjab->kode_opd                          = $kodeOpd;
        $anjab->jenis_jabatan                     = $jenisJabatan;
        $anjab->eselon_jabatan                    = NULL;
        $anjab->kode_jabatan_2                    = $kodeJabatan;
        $anjab->{'_kode_jabatan'}                 = $kodeJabatan;
        $anjab->diklat_teknis                     = $diklatTeknis;
        $anjab->pengalaman_kerja                  = $pengalamanKerja;
        $anjab->pengetahuan_kerja                 = $pengetahuanKerja;
		$anjab->m_jabatan_fungsional_jenjang      = $jenjangfungsional;
		$anjab->diklat_penjenjangan               = $diklatpenjenjangan;
		$anjab->kualifikasi_pendidikan            = $kualifikasipendidikan;
        $anjab->parent                            = $parent;
        $anjab->user                              = $user;
        $anjab->unit_kerja                        = $unitKerja;
        $anjab->m_periode_id                      = $periode;

		// dd($anjab); 
		

        $anjab->save();
	}

    function updateAnjabFungsional($id, $jabatanId, $kodeJabatan, $kodeOpd, $jenisJabatan, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $jenjangfungsional, $diklatpenjenjangan, $kualifikasipendidikan, $parent, $user, $unitKerja, $periode){

		$anjab                                    = TrxAnjab::find($id);

		$anjab->m_jabatan_id                      = $jabatanId;
        $anjab->kode_opd                          = $kodeOpd;
        $anjab->jenis_jabatan                     = $jenisJabatan;
        $anjab->eselon_jabatan                    = NULL;
        $anjab->kode_jabatan_2                    = $kodeJabatan;
        $anjab->{'_kode_jabatan'}                 = $kodeJabatan;
        $anjab->diklat_teknis                     = $diklatTeknis;
        $anjab->pengalaman_kerja                  = $pengalamanKerja;
        $anjab->pengetahuan_kerja                 = $pengetahuanKerja;
		$anjab->m_jabatan_fungsional_jenjang                = $jenjangfungsional;
		$anjab->diklat_penjenjangan               = $diklatpenjenjangan;
		$anjab->kualifikasi_pendidikan            = $kualifikasipendidikan;
        $anjab->parent                            = $parent;
        $anjab->user                              = $user;
        $anjab->unit_kerja                        = $unitKerja;
        $anjab->m_periode_id                      = $periode;
		// return $anjab 

        $anjab->update();
	}

	function createAnjabStruktural($jabatanId, $kodeJabatan, $eselon, $kodeOpd, $jenisJabatan, $pangkat, $ikhtisarJabatan, $kualifikasiPendidikan, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $parent, $user, $unitKerja, $periode){

		$anjab                                          = new TrxAnjab;

		$anjab->m_jabatan_id                            = $jabatanId;
        $anjab->kode_opd                          = $kodeOpd;
        $anjab->jenis_jabatan                     = $jenisJabatan;
        $anjab->eselon_jabatan                    = $eselon;
        $anjab->kode_jabatan_2                    = $kodeJabatan;
        $anjab->{'_kode_jabatan'}                 = $kodeJabatan;
        $anjab->ikhtisar_jabatan                  = $ikhtisarJabatan;
        $anjab->pangkat                           = $pangkat;
        $anjab->kualifikasi_pendidikan            = $kualifikasiPendidikan;
        $anjab->diklat_teknis                     = $diklatTeknis;
        $anjab->pengalaman_kerja                  = $pengalamanKerja;
        $anjab->pengetahuan_kerja                 = $pengetahuanKerja;
        $anjab->parent                            = $parent;
        $anjab->user                              = $user;
        $anjab->unit_kerja                        = $unitKerja;
        $anjab->m_periode_id                      = $periode;

        $saved                                    = $anjab->save();
		// dd($anjab);
        return $anjab;

	}

	function updateAnjabStruktural($id, $jabatanId, $kodeJabatan, $eselon, $kodeOpd, $jenisJabatan, $pangkat, $ikhtisarJabatan, $kualifikasiPendidikan, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $parent, $user, $unitKerja, $periode){

		$anjab                                    = TrxAnjab::find($id);

		$anjab->m_jabatan_id                      = $jabatanId;
        $anjab->kode_opd                          = $kodeOpd;
        $anjab->jenis_jabatan                     = $jenisJabatan;
        $anjab->eselon_jabatan                    = $eselon;
        $anjab->kode_jabatan_2                    = $kodeJabatan;
        $anjab->{'_kode_jabatan'}                 = $kodeJabatan;
        $anjab->ikhtisar_jabatan                  = $ikhtisarJabatan;
        $anjab->pangkat                           = $pangkat;
        $anjab->kualifikasi_pendidikan            = $kualifikasiPendidikan;
        $anjab->diklat_teknis                     = $diklatTeknis;
        $anjab->pengalaman_kerja                  = $pengalamanKerja;
        $anjab->pengetahuan_kerja                 = $pengetahuanKerja;
        $anjab->parent                            = $parent;
        $anjab->user                              = $user;
        $anjab->unit_kerja                        = $unitKerja;
        $anjab->m_periode_id                      = $periode;

        $anjab->update();
	}

	function checkAnjabIfExist($periodeId, $kodeJabatan, $unitKerja, $jenjangfungsional){
		$anjab = TrxAnjab::where('m_periode_id', $periodeId)->where('m_jabatan_fungsional_jenjang', $jenjangfungsional)->where('_kode_jabatan', $kodeJabatan)->where('unit_kerja', $unitKerja)->first();
	
		if($anjab){
			return 1;
		}else{
			return 0;
		}
	}

	function verifikasi($jenis, $id, $status, $keterangan){
		$anjab                                          = $this->getDetailAnjab($id);

		switch ($jenis) {
			case 'data-jabatan'                            :
				$anjab->status_data_jabatan                   = $status;
				$anjab->keterangan_data_jabatan               = $keterangan;
				break;
			case 'uraian-tugas'                            :
				$anjab->status_uraian_tugas                   = $status;
				$anjab->keterangan_uraian_tugas               = $keterangan;
				break;
			case 'tanggung-jawab'                          :
				$anjab->status_tanggung_jawab                 = $status;
				$anjab->keterangan_tanggung_jawab             = $keterangan;
				break;
			case 'wewenang'                                :
				$anjab->status_wewenang                       = $status;
				$anjab->keterangan_wewenang                   = $keterangan;
				break;
			case 'korelasi-jabatan'                        :
				$anjab->status_korelasi_jabatan               = $status;
				$anjab->keterangan_korelasi_jabatan           = $keterangan;
				break;
			case 'bahan-kerja'                             :
				$anjab->status_bahan_kerja                    = $status;
				$anjab->keterangan_bahan_kerja                = $keterangan;
				break;
			case 'prestasi-kerja'                          :
				$anjab->status_prestasi_kerja                 = $status;
				$anjab->keterangan_prestasi_kerja             = $keterangan;
				break;
			case 'perangkat'                               :
				$anjab->status_perangkat                      = $status;
				$anjab->keterangan_perangkat                  = $keterangan;
				break;
			case 'hasil-kerja'                             :
				$anjab->status_hasil_kerja                    = $status;
				$anjab->keterangan_hasil_kerja                = $keterangan;
				break;
			case 'kondisi-lingkungan-kerja'                :
				$anjab->status_kondisi_lingkungan_kerja       = $status;
				$anjab->keterangan_kondisi_lingkungan_kerja   = $keterangan;
				break;
			case 'resiko-bahaya'                           :
				$anjab->status_resiko_bahaya                  = $status;
				$anjab->keterangan_resiko_bahaya              = $keterangan;
				break;
			case 'syarat-jabatan'                          :
				$anjab->status_syarat_jabatan                 = $status;
				$anjab->keterangan_syarat_jabatan             = $keterangan;
				break;
			default                                        :
				// code...
				break;
		}

		$anjab->update();

		return $anjab;

		
	}
	function createSkjStruktural($jabatanId, $kodeJabatan, $eselon, $kodeOpd, $jenisJabatan, $pangkat, $ikhtisarJabatan, $kualifikasiPendidikan, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $nama_skj, $file_skj, $parent, $user, $unitKerja, $periode){

		$skj									= new TrxSkjUpload;
		$skj->m_jabatan_id                      = $jabatanId;
        $skj->kode_opd                          = $kodeOpd;
        $skj->jenis_jabatan                     = $jenisJabatan;
        $skj->eselon_jabatan                    = $eselon;
        $skj->kode_jabatan_2                    = $kodeJabatan;
        $skj->{'_kode_jabatan'}                 = $kodeJabatan;
        $skj->ikhtisar_jabatan                  = $ikhtisarJabatan;
        $skj->pangkat                           = $pangkat;
        $skj->kualifikasi_pendidikan            = $kualifikasiPendidikan;
        $skj->diklat_teknis                     = $diklatTeknis;
        $skj->pengalaman_kerja                  = $pengalamanKerja;
        $skj->pengetahuan_kerja                 = $pengetahuanKerja;
		$skj->nama_skj                 			= $nama_skj;
		if($request->hasfile('file_skj')){
			$file = $request->file('file_skj');
			if($file->getClientOriginalExtension() != 'pdf' && $file->getClientOriginalExtension() != 'xls' &&  $file->getClientOriginalExtension() != 'xlsx'){
				return redirect()->back()->with('notify', 'Dokumen harus PDF / Excel');
			}
			$name = 'skj-'.$skj->nama_skj.'-'.$skj->id.'.'.$file->getClientOriginalExtension();
			$file->move('skj/', $name);
			$abk->file_skj = $name;

			// dd($abk);
			$abk->save();
			return redirect()->back()->with('notify', 'Abk berhasil tersimpan');
		}
        $skj->parent                            = $parent;
        $skj->user                              = $user;
        $skj->unit_kerja                        = $unitKerja;
        $skj->m_periode_id                      = $periode;
		dd($skj);

        $saved                                    = $skj->save();

        return $skj;

	}

	function getDataSkj($kodeOpd, $jenis, $searchJabatan = null){
		// dd($kodeOpd);
		$activePeriode                                  = \MojokertokabApp::getActivePeriod();
		// dd($activePeriode);
		$skj                                          = TrxSkjUpload::query();

		$skj                                          = $skj->where('m_periode_id', $activePeriode->id);
        if(\MojokertokabUser::getUser()->role == 'PD'){
		    $skj                                      = $skj->where('jenis_jabatan', $jenis)->where(
                function($query) use ($kodeOpd) {
                  return $query->where('user_opd', 'LIKE', $kodeOpd.'%')
                  ->orWhere('user_opd', '=', null)
                  ->orWhere('kode_opd', '=', null);
                 });
        }
		// dd($skj);
		if($jenis != NULL){
            $skj                                      = $skj->where('jenis_jabatan', $jenis);
		}

    	if($searchJabatan){
            $skj                                      = $skj->where('nama', 'like', '%'.$searchJabatan.'%');
    	}
		$skj                                          = $skj->unitKerja($kodeOpd);
		$skj                                          = $skj->orderByRaw('ISNULL(eselon_jabatan), eselon_jabatan ASC')->get();
		// dd($skj);
		return $skj;
	}

	function getDataVerify($kodeOpd, $jenis)
	{
		$activePeriode                                  = \MojokertokabApp::getActivePeriod();
		$anjab                                          = TrxAnjab::query();
		$anjab                                          = $anjab->where('m_periode_id', $activePeriode->id)->where('status_data_jabatan','verifikasi');
        if(\MojokertokabUser::getUser()->role == 'PD'){
		    $anjab                                      = $anjab->where('jenis_jabatan', $jenis)->where(
                function($query) use ($kodeOpd) {
                  return $query->where('user_opd', 'LIKE', $kodeOpd.'%')
                  ->orWhere('user_opd', '=', null)
                  ->orWhere('kode_opd', '=', null);
                 });
        }
		if($jenis != NULL){
            $anjab                                      = $anjab->where('jenis_jabatan', $jenis);
		}

		$anjab                                          = $anjab->unitKerja($kodeOpd);
		$anjab                                          = $anjab->orderByRaw('ISNULL(eselon_jabatan), eselon_jabatan ASC')->get();
		return $anjab;
	}
}
