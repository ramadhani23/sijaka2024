<?php

namespace App\Repositories;


use App\Models\TrxAnjab;
use App\Models\TrxAnjabAbk;

use App\Http\Resources\Json\Anjab as AnjabResource;

class AbkRepository{
		
	function getAbkByBkpp($kodeBkpp){
		$anjab = TrxAnjab::where('jenis_jabatan', '!=', 2)->where('unit_kerja', 'LIKE', $kodeBkpp.'%')->get();
		
		$anjabResource = AnjabResource::collection($anjab);

		return $anjabResource;
	}

	function getAbkCountVerify($jenis)
	{
		$abk = TrxAnjab::query();
        $abk = $abk->whereHas('dataAbkKhusus', function($q){
            $q->where('status', 'verifikasi');
        })->where('jenis_jabatan', $jenis)
        ->with(['dataAbkKhusus' => function($q){
            $q->where('status', 'verifikasi');
        }])->where('jenis_jabatan', $jenis);
        $unitKerja = \MojokertokabUser::getUser()->option->satker_id;

        $tempKodeOPD = substr($unitKerja, 0,2);

        if ($tempKodeOPD == '01'){
            $unitKerja = substr($unitKerja, 0,6);
        }  elseif ($tempKodeOPD == '28'){
            $unitK = \MojokertokabUser::getUser()->option->satker_id;
            if ($unitK == '2801') {
                $unitKerja = '28';
            } else {
                $unitKerja = \MojokertokabUser::getUser()->option->satker_id;
            }
        } else {
            $unitKerja = $tempKodeOPD;
        }
        $abk = $abk->where('unit_kerja', 'like', $unitKerja .'%')->get();
		return $abk;	
	}

}