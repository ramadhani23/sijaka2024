<?php

namespace App\Repositories;


use App\Models\MUnitKerja;

class UnitKerjaRepository{

	// function getActiveUnitKerja(){
	// 	$unitKerjaActive = MUnitkerja::dinas()->orWhere(function($query){
	// 						    		return $query->badan();
	// 						    	})->orWhere(function($query){
	// 						    		return $query->bagianSetda();
	// 						    	})->orWhere(function($query){
	// 						    		return $query->kecamatan();
	// 						    	})->orWhere(function($query){
	// 						    		return $query->rumahSakit();
	// 						    	})->orWhere(function($query){
	// 						    		return $query->wilker();
	// 						    	})->orWhere(function($query){
	// 						    		return $query->upt();
	// 						    	})->get();
	// 	// $unitKerjaActive = MUnitkerja::dinas()->get();
	// 	return $unitKerjaActive;
	// }

	function getActiveUnitKerja() {
		// Ambil kode_opd dari user yang sedang login
		$kodeopd = \MojokertokabUser::getUser()->kode_opd;

		// Jika kode_opd adalah 28, gunakan scopePuskesmas
		if (substr($kodeopd, 0, 2) === "28") {
			$unitKerjaActive = MUnitkerja::puskesmas()
			->orWhere('kodeunit', '28') // Tambahkan kodeunit '28'
			->get();
			// dd($unitKerjaActive); // Cek apakah hasil query ada datanya
			return $unitKerjaActive;
		}

		// Jika tidak, jalankan query unit kerja seperti biasa
		$unitKerjaActive = MUnitkerja::dinas()
			->orWhere(function($query) {
				$query->badan();
			})->orWhere(function($query) {
				$query->bagianSetda();
			})->orWhere(function($query) {
				$query->kecamatan();
			})->orWhere(function($query) {
				$query->rumahSakit();
			})->orWhere(function($query) {
				$query->wilker();
			})->orWhere(function($query) {
				$query->upt();
			})->get();
			return $unitKerjaActive;

		// dd($unitKerjaActive); // Cek hasil query sebelum dikirim ke view
		// return $unitKerjaActive;

	}
	

	function getActiveUnitKerjaPd(){
		$unitKerjaActive = MUnitkerja::dinas()->orWhere(function($query){
							    		return $query->badan();

							    	})->orWhere(function($query){
							    		return $query->kecamatan();
									})->orWhere(function($query){
							    		return $query->puskesmas();
							    	})->get();
		// $unitKerjaActive = MUnitkerja::dinas()->get();
		return $unitKerjaActive;
	}

	function getUnitKerjaOnKode($kodeUnit){
		return MUnitkerja::where('kodeunit', 'LIKE', $kodeUnit.'%')->get();
	}

	function getEselon3UnitKerja($kodeUnit){
		$unitKerja = MUnitkerja::where('kodeunit', 'LIKE', $kodeUnit.'%')->where(function($query){
			return $query->where('jenis', 'BIDANG')->orWhere('jenis', 'SEKRETARIAT_DINAS');
		})->get();

		return $unitKerja;
	}

	function getEselon4UnitKerja($kodeUnit){
		$unitKerja = MUnitkerja::where('kodeunit', 'LIKE', $kodeUnit.'%')->where(function($query){
			return $query->where('jenis', 'SEKSI')->orWhere('jenis', 'SUB_BAGIAN');
		})->get();

		return $unitKerja;
	}

}
