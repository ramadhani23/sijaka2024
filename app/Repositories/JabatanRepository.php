<?php

namespace App\Repositories;

use App\Models\MJabatanFungsional;
use App\Models\MJabatanFungsionalRumpun;
use App\Models\MJabatanStruktural;
use App\Models\MJabatanPelaksana;
use App\Models\MInstansiPembina;
use App\Models\MJabatanFungsionalInstansiPembina;
use App\Models\MUrusanPemerintahan;

class JabatanRepository{
    //Jabatan Fungsional
	function getJabatanFungsional(){
		$jabatanFungsional = MJabatanFungsional::get();

		return $jabatanFungsional;
	}

	function saveJabatanFungsional($nama, $uraianTugas, $instansiPembina, $rumpun, $urutan, $kategori){
		// return $urutan;
		$jabatanFungsional = new MJabatanFungsional;

		$jabatanFungsional->jabatan = $nama;
		$jabatanFungsional->_kode = '13.'.$instansiPembina.'.'.$rumpun.'.'.sprintf("%04d", $urutan);
		$jabatanFungsional->m_jabatan_fungsional_rumpun_id = $rumpun;
		$jabatanFungsional->m_jabatan_fungsional_instansi_pembina_id = $instansiPembina;
		$jabatanFungsional->tugas = $uraianTugas;
		$jabatanFungsional->kategori = $kategori;
		$jabatanFungsional->urutan = $urutan;
		$jabatanFungsional->save();

		return $jabatanFungsional;
	}

	function getRumpunJF(){
		return MJabatanFungsionalRumpun::all();
	}

	function getInstansiPembinaJF(){
		return MJabatanFungsionalInstansiPembina::all();
	}

    function getUrusanPemerintahan(){
		return MUrusanPemerintahan::all();
	}

	function getRowJabatan($kodeJabatan, $jenis){
		if($jenis == '3'){
			return MJabatanPelaksana::where('_kode', $kodeJabatan)->first();
		}if($jenis == '2'){
			return MJabatanFungsional::where('_kode', $kodeJabatan)->first();
		}elseif($jenis == '1'){
			return MJabatanStruktural::where('_kode', $kodeJabatan)->first();
		}
	}


    //Jabatan Pelaksana
    function getJabatanPelaksana(){
		$jabatanPelaksana = MJabatanPelaksana::whereNotNull('jenjang_pendidikan')->get();

		return $jabatanPelaksana;
	}

    function saveJabatanPelaksana($kualifikasi_pendidikan, $jabatan, $klasifikasi, $instansi_teknis, $jenjang_pendidikan, $kode, $tugas, $kode_bkpp, $dasar_hukum, $jenis, $_kode){
		// dd($instansi_teknis);
		$jabatanPelaksana = new MJabatanPelaksana;

		// $jabatanPelaksana->kode_urusan_pemerintahan   = $kode_urusan_pemerintahan;
		$jabatanPelaksana->kualifikasi_pendidikan     = $kualifikasi_pendidikan;
		$jabatanPelaksana->jabatan                    = $jabatan;
		$jabatanPelaksana->klasifikasi                       = $klasifikasi;
		$jabatanPelaksana->instansi_teknis                       = $instansi_teknis;
		$jabatanPelaksana->jenjang_pendidikan                       = $jenjang_pendidikan;
		$jabatanPelaksana->kode                       = $kode;
		$jabatanPelaksana->tugas_jabatan              = $tugas;
		$jabatanPelaksana->kode_bkpp                  = $kode_bkpp;
		$jabatanPelaksana->dasar_hukum                  = $dasar_hukum;
		$jabatanPelaksana->jenis                      = $jenis;
		$jabatanPelaksana->_kode                      = $_kode;
		// dd($jabatanPelaksana);
		$jabatanPelaksana->save();

		return $jabatanPelaksana;
	}
}
