<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TrxAnjab;
use App\Models\MJabatanPelaksana;

class SyncAnjabKodeJabatan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sijaka:sync-kode-jabatan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $anjab = TrxAnjab::where('jenis_jabatan', '3')->get();

        // foreach ($anjab as $itemAnjab) {
        //     // dd($itemAnjab);
        //     $jabatanPelaksana = MJabatanPelaksana::where('kode_2', $itemAnjab->kode_jabatan_2)->first();
        //     if($jabatanPelaksana){
        //         echo "Jabatan ".$jabatanPelaksana->jabatan."\n";
        //         $itemAnjab->{'_kode_jabatan'} = $jabatanPelaksana->{'_kode'};
        //         $itemAnjab->update();
        //     }
        // }

        $anjab = TrxAnjab::where('jenis_jabatan', '3')->where('_kode_jabatan', NULL)->get();
        foreach ($anjab as $itemAnjab) {
            $jabatanPelaksana = MJabatanPelaksana::where('jabatan', 'LIKE', '%'.$itemAnjab->nama.'%')->first();
            if($jabatanPelaksana){
                echo "Jabatan ".$jabatanPelaksana->jabatan."\n";
                $itemAnjab->{'_kode_jabatan'} = $jabatanPelaksana->{'_kode'};
                $itemAnjab->update();
            }
        }


        return 0;
    }
}
