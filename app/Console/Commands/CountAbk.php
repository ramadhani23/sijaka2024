<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\MUnitKerja;
use App\Repositories\UnitKerjaRepository;
use App\Models\TrxAnjab;
use App\Models\RkpKebutuhanPegawai;

class CountAbk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sijaka:count-abk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Untuk menghitung ABK per Unit Kerja Aktif';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $unitKerjaRepo = new UnitKerjaRepository();

        $unitKerjaActive = $unitKerjaRepo->getActiveUnitKerja();
        $activePeriode = \MojokertokabApp::getActivePeriod();

        foreach ($unitKerjaActive as $itemUnitKerja) {
            echo "Hitung Kebutuhan di ".$itemUnitKerja->unitkerja."\n";
            $anjabStruktural = TrxAnjab::where('unit_kerja', 'LIKE', $itemUnitKerja->kodeunit.'%')->where('jenis_jabatan', '1')->get();
            if($itemUnitKerja->kodeunit == '21'){
                // dd($anjabStruktural);
            }
            
            $kebutuhanPegawaiStruktural = 0;

            foreach($anjabStruktural as $itemAnjab){
                $kebutuhanPegawai = $itemAnjab->total_kebutuhan_pegawai;
                echo "Kebutuhan Pegawai ".@$itemAnjab->jabatan->jabatan." = ".$kebutuhanPegawai." \n";
                $kebutuhanPegawaiStruktural += $kebutuhanPegawai;
            }

            $anjabFungsional = TrxAnjab::where('unit_kerja', 'LIKE', $itemUnitKerja->kodeunit.'%')->where('jenis_jabatan', '2')->get();
            
            $kebutuhanPegawaiFungsional = 0;

            foreach($anjabFungsional as $itemAnjab){
                $kebutuhanPegawai = $itemAnjab->total_kebutuhan_pegawai;
                echo "Kebutuhan Pegawai ".@$itemAnjab->jabatan->jabatan." = ".$kebutuhanPegawai." \n";
                $kebutuhanPegawaiFungsional += $kebutuhanPegawai;
            }

            $anjabPelaksana = TrxAnjab::where('unit_kerja', 'LIKE', $itemUnitKerja->kodeunit.'%')->where(function($query){
                return $query->where('jenis_jabatan', '3');
            })->get();

            $kebutuhanPegawaiPelaksana = 0;
            foreach($anjabPelaksana as $itemAnjab){
                $kebutuhanPegawai = $itemAnjab->total_kebutuhan_pegawai;
                echo "Kebutuhan Pegawai ".@$itemAnjab->jabatan->jabatan." = ".$kebutuhanPegawai." \n";
                $kebutuhanPegawaiPelaksana += $kebutuhanPegawai;
            }


            $rkpKebutuhanPegawai = RkpKebutuhanPegawai::where('kodeunit', $itemUnitKerja->kodeunit)->where('m_periode_id', $activePeriode->id)->first();

            if($rkpKebutuhanPegawai){
                $rkpKebutuhanPegawai->kebutuhan_pegawai_struktural = $kebutuhanPegawaiStruktural;
                $rkpKebutuhanPegawai->kebutuhan_pegawai_fungsional = $kebutuhanPegawaiFungsional;
                $rkpKebutuhanPegawai->kebutuhan_pegawai_pelaksana = $kebutuhanPegawaiPelaksana;
                $rkpKebutuhanPegawai->update();
            }else{
                $rkpKebutuhanPegawai = new RkpKebutuhanPegawai;
                $rkpKebutuhanPegawai->m_periode_id = \MojokertokabApp::getActivePeriod()->id;
                $rkpKebutuhanPegawai->kodeunit = $itemUnitKerja->kodeunit;
                $rkpKebutuhanPegawai->kebutuhan_pegawai_struktural = $kebutuhanPegawaiStruktural;
                $rkpKebutuhanPegawai->kebutuhan_pegawai_fungsional = $kebutuhanPegawaiFungsional;
                $rkpKebutuhanPegawai->kebutuhan_pegawai_pelaksana = $kebutuhanPegawaiPelaksana;
                $rkpKebutuhanPegawai->save();
                
            }
        }

        return 0;
    }
}
