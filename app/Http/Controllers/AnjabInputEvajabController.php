<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabEvajabStruktural;
use App\Models\TrxAnjabEvajabFungsional;
use App\Models\MFaktor;
use App\Models\MLevelFaktor;

use DB;

class AnjabInputEvajabController extends Controller
{
    function input($id){

    	$data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "input Evajab";
    	$data['pages'] = "detail_input_evajab";
    	$anjab = TrxAnjab::where('id', $id)->first();
        

        if($anjab->jenis_jabatan == 1){
            $faktorEvajab = TrxAnjabEvajabStruktural::where('trx_anjab_id', $id)->first();
            $data['faktorEvajab'] = $faktorEvajab;

            $data['faktor1'] = MFaktor::where('id', 1)->first();
            $data['faktor2'] = MFaktor::where('id', 2)->first();
            $data['faktor3'] = MFaktor::where('id', 3)->first();
            $data['faktor4A'] = MFaktor::where('id', 4)->first();
            $data['faktor4B'] = MFaktor::where('id', 5)->first();
            $data['faktor5'] = MFaktor::where('id', 6)->first();
            $data['faktor6'] = MFaktor::where('id', 7)->first();

            $data['level_faktor1'] = MLevelFaktor::where('m_faktor_id', 1)->get();
            $data['level_faktor2'] = MLevelFaktor::where('m_faktor_id', 2)->get();
            $data['level_faktor3'] = MLevelFaktor::where('m_faktor_id', 3)->get();
            $data['level_faktor4A'] = MLevelFaktor::where('m_faktor_id', 4)->get();
            $data['level_faktor4B'] = MLevelFaktor::where('m_faktor_id', 5)->get();
            $data['level_faktor5'] = MLevelFaktor::where('m_faktor_id', 6)->get();
            $data['level_faktor6'] = MLevelFaktor::where('m_faktor_id', 7)->get();

            $data['anjab'] = $anjab;

            return view('evjab.struktural.detail', $data);
        }elseif($anjab->jenis_jabatan == 2 || $anjab->jenis_jabatan == 3){
            $mFaktor = MFaktor::where('jenis', 'FUNGSIONAL')->orderBy('nama', 'ASC')->get();

            $faktorEvajab = TrxAnjabEvajabFungsional::where('trx_anjab_id', $id)->first();

            $faktor1 = MFaktor::where('id', 8)->first();
            $faktor2 = MFaktor::where('id', 9)->first();
            $faktor3 = MFaktor::where('id', 10)->first();
            $faktor4 = MFaktor::where('id', 11)->first();
            $faktor5 = MFaktor::where('id', 12)->first();
            $faktor6 = MFaktor::where('id', 13)->first();
            $faktor7 = MFaktor::where('id', 14)->first();
            $faktor8 = MFaktor::where('id', 15)->first();
            $faktor9 = MFaktor::where('id', 16)->first();

            $levelFaktor1 = MLevelFaktor::where('m_faktor_id', 8)->get();
            $levelFaktor2 = MLevelFaktor::where('m_faktor_id', 9)->get();
            $levelFaktor3 = MLevelFaktor::where('m_faktor_id', 10)->get();
            $levelFaktor4 = MLevelFaktor::where('m_faktor_id', 11)->get();
            $levelFaktor5 = MLevelFaktor::where('m_faktor_id', 12)->get();
            $levelFaktor6 = MLevelFaktor::where('m_faktor_id', 13)->get();
            $levelFaktor7 = MLevelFaktor::where('m_faktor_id', 14)->get();
            $levelFaktor8 = MLevelFaktor::where('m_faktor_id', 15)->get();
            $levelFaktor9 = MLevelFaktor::where('m_faktor_id', 16)->get();

            $data = [
                'faktor1' => $faktor1,
                'faktor2' => $faktor2,
                'faktor3' => $faktor3,
                'faktor4' => $faktor4,
                'faktor5' => $faktor5,
                'faktor6' => $faktor6,
                'faktor7' => $faktor7,
                'faktor8' => $faktor8,
                'faktor9' => $faktor9,
                'levelFaktor1' => $levelFaktor1,
                'levelFaktor2' => $levelFaktor2,
                'levelFaktor3' => $levelFaktor3,
                'levelFaktor4' => $levelFaktor4,
                'levelFaktor5' => $levelFaktor5,
                'levelFaktor6' => $levelFaktor6,
                'levelFaktor7' => $levelFaktor7,
                'levelFaktor8' => $levelFaktor8,
                'levelFaktor9' => $levelFaktor9,
                'anjab' => $anjab,
                'faktorEvajab'  => $faktorEvajab,
                'mFaktor'       => $mFaktor
            ];

            return view('evjab.fungsional.detail', $data);
        }

    	

    }

    function save(Request $request){

        $anjab = TrxAnjab::findOrFail($request->id);
        if($anjab->jenis_jabatan == 1){
            $evajab = new TrxAnjabEvajabStruktural;
            $evajab->faktor1 = $request->level_faktor1;
            $evajab->faktor2 = $request->level_faktor2;
            $evajab->faktor3 = $request->level_faktor3;
            $evajab->faktor4A = $request->level_faktor4A;
            $evajab->faktor4B = $request->level_faktor4B;
            $evajab->faktor5 = $request->level_faktor5;
            $evajab->faktor6 = $request->level_faktor6;
            

            $evajab->text_faktor1 = $request->text_faktor1;
            $evajab->text_faktor2 = $request->text_faktor2;
            $evajab->text_faktor3 = $request->text_faktor3;
            $evajab->text_faktor4A = $request->text_faktor4A;
            $evajab->text_faktor4B = $request->text_faktor4B;
            $evajab->text_faktor5 = $request->text_faktor5;
            $evajab->text_faktor6 = $request->text_faktor6;


            $evajab->trx_anjab_id = $request->id;
        }elseif ($anjab->jenis_jabatan == 2 || $anjab->jenis_jabatan == 3) {
            $evajab = new TrxAnjabEvajabFungsional;
            $evajab->faktor1 = $request->level_faktor1;
            $evajab->faktor2 = $request->level_faktor2;
            $evajab->faktor3 = $request->level_faktor3;
            $evajab->faktor4 = $request->level_faktor4;
            $evajab->faktor5 = $request->level_faktor5;
            $evajab->faktor6 = $request->level_faktor6;
            $evajab->faktor7 = $request->level_faktor7;
            $evajab->faktor8 = $request->level_faktor8;
            $evajab->faktor9 = $request->level_faktor9;

            $evajab->text_faktor1 = $request->text_faktor1;
            $evajab->text_faktor2 = $request->text_faktor2;
            $evajab->text_faktor3 = $request->text_faktor3;
            $evajab->text_faktor4 = $request->text_faktor4;
            $evajab->text_faktor5 = $request->text_faktor5;
            $evajab->text_faktor6 = $request->text_faktor6;
            $evajab->text_faktor7 = $request->text_faktor7;
            $evajab->text_faktor8 = $request->text_faktor8;
            $evajab->text_faktor9 = $request->text_faktor9;

            $evajab->trx_anjab_id = $request->id;

        }

        

        // $evajab->level_faktor1 = $request->level_faktor1;
        // $evajab->ruang_lingkup = $request->ruang_lingkup;
        // $evajab->dampak_program = $request->dampak_program;
        // $evajab->level_faktor2 = $request->level_faktor2;
        // $evajab->pengaturan_organisasi = $request->pengaturan_organisasi;
        // $evajab->level_faktor3 = $request->level_faktor3;
        // $evajab->wewenang_penyeliaan_manajerial = $request->wewenang_penyeliaan_manajerial;
        // $evajab->level_faktor4A = $request->level_faktor4A;
        // $evajab->sifat_hubungan = $request->sifat_hubungan;
        // $evajab->level_faktor4B = $request->level_faktor4B;
        // $evajab->tujuan_hubungan = $request->tujuan_hubungan;
        // $evajab->level_faktor5 = $request->level_faktor5;
        // $evajab->kesulitan_pengarahan = $request->kesulitan_pengarahan;
        // $evajab->level_faktor6 = $request->level_faktor6;
        // $evajab->kondisi_lain = $request->kondisi_lain;
        // $evajab->trx_anjab_id = $request->id;
        // $evajab->created_at = date('Y-m-d H:i:s');
        $saved = $evajab->save();
        if($saved){
            return redirect()->route('anjab.input-evajab',$request->id)->with('notify', 'Faktor Evajab berhasil disimpan !');
        }
        
      
    }

    function update(Request $request){

        $anjab = TrxAnjab::findOrFail($request->id);
        if($anjab->jenis_jabatan == 1){
            $evajab = TrxAnjabEvajab::find($request->id_evajab);

            $evajab->level_faktor1 = $request->level_faktor1;
            $evajab->ruang_lingkup = $request->ruang_lingkup;
            $evajab->dampak_program = $request->dampak_program;
            $evajab->level_faktor2 = $request->level_faktor2;
            $evajab->pengaturan_organisasi = $request->pengaturan_organisasi;
            $evajab->level_faktor3 = $request->level_faktor3;
            $evajab->wewenang_penyeliaan_manajerial = $request->wewenang_penyeliaan_manajerial;
            $evajab->level_faktor4A = $request->level_faktor4A;
            $evajab->sifat_hubungan = $request->sifat_hubungan;
            $evajab->level_faktor4B = $request->level_faktor4B;
            $evajab->tujuan_hubungan = $request->tujuan_hubungan;
            $evajab->level_faktor5 = $request->level_faktor5;
            $evajab->kesulitan_pengarahan = $request->kesulitan_pengarahan;
            $evajab->level_faktor6 = $request->level_faktor6;
            $evajab->kondisi_lain = $request->kondisi_lain;
        }elseif ($anjab->jenis_jabatan == 2 || $anjab->jenis_jabatan == 3) {
            $evajab = TrxAnjabEvajabFungsional::find($request->id_evajab);
            $evajab->faktor1 = $request->level_faktor1;
            $evajab->faktor2 = $request->level_faktor2;
            $evajab->faktor3 = $request->level_faktor3;
            $evajab->faktor4 = $request->level_faktor4;
            $evajab->faktor5 = $request->level_faktor5;
            $evajab->faktor6 = $request->level_faktor6;
            $evajab->faktor7 = $request->level_faktor7;
            $evajab->faktor8 = $request->level_faktor8;
            $evajab->faktor9 = $request->level_faktor9;

            $evajab->text_faktor1 = $request->text_faktor1;
            $evajab->text_faktor2 = $request->text_faktor2;
            $evajab->text_faktor3 = $request->text_faktor3;
            $evajab->text_faktor4 = $request->text_faktor4;
            $evajab->text_faktor5 = $request->text_faktor5;
            $evajab->text_faktor6 = $request->text_faktor6;
            $evajab->text_faktor7 = $request->text_faktor7;
            $evajab->text_faktor8 = $request->text_faktor8;
            $evajab->text_faktor9 = $request->text_faktor9;

            $evajab->trx_anjab_id = $request->id;
        }

        
        $saved = $evajab->update();
        if($saved){
            return redirect()->route('anjab.input-evajab',$request->id)->with('notify', 'Faktor Evajab berhasil di update !');
        }
        // else{
        //     return redirect()->back()->with('alert', 'Faktor Evajab gagal ditambahkan');
        // }

      
    }

    public function getLevelFaktor(Request $request){
        if (isset($request->id)) {
            $levelFaktor = MLevelFaktor::where('id',$request->id)->first();
            $faktor = MFaktor::where('id',$request->faktor_id)->first();
            
            $html ='<div class="form-group row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                        <div class="rounded bg-gray-100 p-5">
                            <b>TINGKAT FAKTOR '.explode('Faktor ', $faktor->nama)[1].' - '.explode('Level ', $levelFaktor->nama)[1].'</b> &#8212; NILAI '.$levelFaktor->nilai.'<br><br>
                            <b>PENJELASAN</b> : <br><br>'.$levelFaktor->uraian.'
                        </div>  
                    </div>  
                </div>';
            echo $html;
           
        }
    }
   
}
