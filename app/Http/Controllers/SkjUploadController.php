<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxSkjUpload;
use App\Models\TrxAnjabAbk;
use App\Models\TrxAbkKhusus;
use App\Models\TrxAnjabUraianJabatan;
use App\Models\MUnitKerja;
use App\Models\User;

use App\Repositories\UnitKerjaRepository;

class SkjUploadController extends Controller
{
    function browse(Request $request, $jenis, UnitKerjaRepository $unitKerjaRepo){
        // return $jenis;
        $data['section'] = "skj";

        $data['page'] = "SKJ Upload";
        $data['pages'] = "skj";

        $data['jenis'] = $jenis;
        // $data['unitKerja'] = MUnitkerja::whereRaw('LENGTH(kodeunit) = 2')->get();
        $data['unitKerja'] = MUnitkerja::whereRaw('LENGTH(kodeunit) = 2')->get();

        $anjab = TrxAnjab::query()->with(['skj'])->get();
        // // // ->with(['skj'])->get();
        // dd($anjab);
        $abk = TrxAnjab::query()->with(['skj'])->get();
        // echo "<pre>";
        // var_dump($anjab);
        // die;
        $unitKerja = 0;

        if($jenis == 'struktural'){
            $data['page_section'] = "skj-struktural";
            // $abk = TrxAnjab::whereDoesntHave('dataAbk')
            //                     ->whereDoesntHave('dataAbkKhusus')
            //                     ->where('jenis_jabatan', '1')
            //                     ->with(['skj']);
            // TrxAnjab::with('skj')->get();

            $skj = TrxAnjab::with('skj')->where('jenis_jabatan', '1');

            if(\MojokertokabUser::getUser()->role == 'PD'){
                
                $unitKerja = \MojokertokabUser::getUser()->option->satker_id;
                $tempKodeOPD = substr($unitKerja, 0,2);
                // echo 'unit='.$unitKerja.'<br> tempkode= '.$tempKodeOPD;
                // die();
                // MojokertokabUser::getUser();

                if ($tempKodeOPD == '01'){
                    $unitKerja = substr($unitKerja, 0,6);
                } elseif($tempKodeOPD == '28'){
                    $unitKerja = \MojokertokabUser::getUser()->option->satker_id;
                    // echo 'unit='.$unitKerja.'<br> tempkode= '.$tempKodeOPD;
                    // die();
                } else {
                    $unitKerja = $tempKodeOPD;
                }

                $skj = $skj->where('unit_kerja', 'like', $unitKerja .'%');
                // $data['anjab'] = $skj;

            }elseif(\MojokertokabUser::getUser()->role == 'ADMIN'){
                $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
                $data['unitKerja'] = $unitKerja;

                $unitKerja        = $request->unit_kerja;
                $jenis_jabatan    = $request->jenis_jabatan;
                if($unitKerja){
                    $skj = $skj->where('unit_kerja', 'like', $unitKerja .'%');
                }
                if($jenis_jabatan){
                    $skj = $skj->where('jenis_jabatan', 'LIKE', $jenis_jabatan.'%');
                }
            }

            if(\MojokertokabUser::getUser()->role == 'PD'|| $request->unit_kerja != NULL){
                $skj = $skj->paginate(50);
            }else{
                $skj = $skj->paginate(50);
            }

            $data['anjab'] = $skj;

        }elseif($jenis == 'fungsional'){
            // return $jenis;
            $data['page_section'] = "skj-fungsional";
            // $anjab = TrxAnjab::whereDoesntHave('dataAbk')
            //                     ->whereDoesntHave('dataAbkKhusus')
            //                     ->where('jenis_jabatan', '2')
            //                     ->get();

            // return $anjab;
            
            // $data['anjab'] = $anjab;
            // return $data;

            $skj = TrxAnjab::with('skj')->where('jenis_jabatan', '2');
            

            if(\MojokertokabUser::getUser()->role == 'PD'){

                // return User::where('option->satker_id', 'LIKE', '38%')->first();
                // return \MojokertokabUser::getUser();
                $unitKerja = \MojokertokabUser::getUser()->option->satker_id;

                $tempKodeOPD = substr($unitKerja, 0,2);

                if ($tempKodeOPD == '01'){
                    $unitKerja = substr($unitKerja, 0,6);
                }  elseif($tempKodeOPD == '28'){
                    $unitKerja = \MojokertokabUser::getUser()->option->satker_id;
                    // echo 'unit='.$unitKerja.'<br> tempkode= '.$tempKodeOPD;
                    // die();
                } else {
                    $unitKerja = $tempKodeOPD;
                }

                $skj = $skj->where('unit_kerja', 'like', $unitKerja .'%');
                // $data['anjab'] = $skj;
                // return $skj;
                // $skj = $skj->whereRaw('LENGTH(unit_kerja) > 2')->where('unit_kerja', 'like', $unitKerja .'%');
            }elseif(\MojokertokabUser::getUser()->role == 'ADMIN'){
                $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
                $data['unitKerja'] = $unitKerja;

                $unitKerja        = $request->unit_kerja;
                $jenis_jabatan    = $request->jenis_jabatan;
                if($unitKerja){
                    $skj = $skj->where('unit_kerja', 'like', $unitKerja .'%');
                }
                if($jenis_jabatan){
                    $skj = $skj->where('jenis_jabatan', 'LIKE', $jenis_jabatan.'%');
                }
                // return $unitKerja;
            }
            
            if(\MojokertokabUser::getUser()->role == 'PD'|| $request->unit_kerja != NULL){
                $skj = $skj->paginate(50);
            }else{
                $skj = $skj->paginate(50);
            }

            // $data['skj'] = $skj;
            $data['anjab'] = $skj;
            // return $skj;
            

        }elseif($jenis == 'pelaksana'){
            $data['page_section'] = "skj-pelaksana";
            // $anjab = TrxAnjab::whereDoesntHave('dataAbk')
            //                     ->whereDoesntHave('dataAbkKhusus')
            //                     ->where('jenis_jabatan', '3')
            //                     ->get();

            // return $anjab;
            $skj = TrxAnjab::where('jenis_jabatan', '3');

             // return $request->unit_kerja;
            //  if ($request->unit_kerja != null){
            //     $data['unit_kerja'] = $request->unit_kerja;
            // }

            // $data['anjab'] = $skj;

            if(\MojokertokabUser::getUser()->role == 'PD'){

                // return User::where('option->satker_id', 'LIKE', '38%')->first();
                // return \MojokertokabUser::getUser();
                $unitKerja = \MojokertokabUser::getUser()->option->satker_id;

                $tempKodeOPD = substr($unitKerja, 0,2);

                if ($tempKodeOPD == '01'){
                    $unitKerja = substr($unitKerja, 0,6);
                } elseif($tempKodeOPD == '28'){
                    $unitKerja = \MojokertokabUser::getUser()->option->satker_id;
                    // echo 'unit='.$unitKerja.'<br> tempkode= '.$tempKodeOPD;
                    // die();
                } else {
                    $unitKerja = $tempKodeOPD;
                }

                $skj = $skj->where('unit_kerja', 'like', $unitKerja .'%');
                // $skj = $skj->whereRaw('LENGTH(unit_kerja) > 2')->where('unit_kerja', 'like', $unitKerja .'%');
            }elseif(\MojokertokabUser::getUser()->role == 'ADMIN'){
                $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
                $data['unitKerja'] = $unitKerja;
                $unitKerja        = $request->unit_kerja;
                $jenis_jabatan    = $request->jenis_jabatan;
                if($unitKerja){
                    $skj = $skj->where('unit_kerja', 'like', $unitKerja .'%');
                }
                if($jenis_jabatan){
                    $skj = $skj->where('jenis_jabatan', 'LIKE', $jenis_jabatan.'%');
                }
            }

            if(\MojokertokabUser::getUser()->role == 'PD'|| $request->unit_kerja != NULL){
                $skj = $skj->paginate(50);
            }else{
                $skj = $skj->paginate(50);
            }
            

            // $data['skj'] = $skj;
            $data['anjab'] = $skj;
            // return $skj;
            
            // dd($data['anjab']);
            // dd($data);

        }
        return view('skj.browse', $data);
    }
 
    function getForm(Request $request){
        // return $request;
    	if($request->aksi == 'add-skj'){
            $currentAbk = TrxAnjabAbk::where('trx_anjab_id', $request->id)->pluck('trx_anjab_uraian_jabatan_id');
            $data['id'] = $request->id;
    		$data['currentAbk'] = $currentAbk;
            return view('skj.form-create', $data);
    	}elseif ($request->aksi == 'edit-skj') {
            $abk = TrxAnjabAbk::findOrFail($request->id);
            $tugasJabatan = TrxAnjabUraianJabatan::where('trx_anjab_id', $abk->trx_anjab_id)->get();

            $data['abk'] = $abk;
            $data['tugasJabatan'] = $tugasJabatan;
    		return view('skj.form-edit', $data);
    	}
    }
    function save2($id = null, Request $request){
                // return $request;
                // dd($request);
                // $anjab = TrxAnjab::findOrFail($request->id);
                // return $anjab;
                if($id){
                    $skj = TrxSkjUpload::find($request->id);
                    if($skj)
                    {
                    // $skj->kebutuhan = $request->kebutuhan;
                    $skj->trx_anjab_id = $request->txtid;
            
                    if($request->hasfile('dokumen')){
                        $file = $request->file('dokumen');
                        if($file->getClientOriginalExtension() != 'pdf' && $file->getClientOriginalExtension() != 'xls' &&  $file->getClientOriginalExtension() != 'xlsx'){
                            return redirect()->back()->with('notify', 'Dokumen harus PDF / Excel');
                        }
                        $name = 'evjab-'.'skj'.'-'.rand(1,9000).'.'.$file->getClientOriginalExtension();
                        $file->move('public/skj/', $name);
                        $skj->dokumen_pendukung = $name;
            
                        // dd($skj);
                        // return $skj;
                        $skj->update();
                        return redirect()->back()->with('notify', 'SKJ berhasil tersimpan');
                    }
                } else {
                    $skj = new TrxSkjUpload;
            
                    // $skj->kebutuhan = $request->kebutuhan;
                    $skj->trx_anjab_id = $request->txtid;
            
                    if($request->hasfile('dokumen')){
                        $file = $request->file('dokumen');
                        if($file->getClientOriginalExtension() != 'pdf' && $file->getClientOriginalExtension() != 'xls' &&  $file->getClientOriginalExtension() != 'xlsx'){
                            return redirect()->back()->with('notify', 'Dokumen harus PDF / Excel');
                        }
                        $name = 'evjab-'.'skj'.'-'.rand(1,9000).'.'.$file->getClientOriginalExtension();
                        $file->move('public/skj/', $name);
                        $skj->dokumen_pendukung = $name;
            
                        // dd($skj);
                        // return $skj;
                        $skj->save();
                        return redirect()->back()->with('notify', 'SKJ berhasil tersimpan');
                }
            }
        }

        // dd('1');
    }

    function save(Request $request){
        // return $request;
        // dd($request);
        // $anjab = TrxAnjab::findOrFail($request->id);
        // return $anjab;
            // return $request;
        if($request->aksi == 'create-khusus'){
            $skj = new TrxSkjUpload;

    
            // $skj->kebutuhan = $request->kebutuhan;
            $skj->trx_anjab_id = $request->txtid;
    
            if($request->hasfile('dokumen')){
                $file = $request->file('dokumen');
                if($file->getClientOriginalExtension() != 'pdf' && $file->getClientOriginalExtension() != 'xls' &&  $file->getClientOriginalExtension() != 'xlsx'){
                    return redirect()->back()->with('notify', 'Dokumen harus PDF / Excel');
                }
                $name = 'skj-'.'skj'.'-'.rand(1,9000).'.'.$file->getClientOriginalExtension();
                $file->move('public/skj/', $name);
                $skj->dokumen_pendukung = $name;
    
                // dd($skj);
                // return $skj;
                $skj->save();

                return redirect()->back()->with('notify', 'SKJ berhasil tersimpan');
            }
        } elseif ($request->aksi == 'edit-skj'){
            // return $request;
            $skj = TrxSkjUpload::findorFail($request->id);

            // $skj->trx_anjab_id = $request->txtid;
    
            if($request->hasfile('dokumen')){
                $file = $request->file('dokumen');
                if($file->getClientOriginalExtension() != 'pdf' && $file->getClientOriginalExtension() != 'xls' &&  $file->getClientOriginalExtension() != 'xlsx'){
                    return redirect()->back()->with('notify', 'Dokumen harus PDF / Excel');
                }
                $name = 'evjab-'.'skj'.'-'.rand(1,9000).'.'.$file->getClientOriginalExtension();
                $file->move('public/skj/', $name);
                $skj->dokumen_pendukung = $name;
    
                // dd($skj);
                // return $skj;
                $skj->update();
                return redirect()->back()->with('notify', 'SKJ berhasil dirubah');
            }

        }

// dd('1');
    }

    function edit(Request $request) {

        // return $request->all();
            $skj = TrxSkjUpload::findOrFail($request->id);

            // $skj->trx_anjab_id = $request->txtid;
    
            if($request->hasfile('dokumen')){
                $file = $request->file('dokumen');
                if($file->getClientOriginalExtension() != 'pdf' && $file->getClientOriginalExtension() != 'xls' &&  $file->getClientOriginalExtension() != 'xlsx'){
                    return redirect()->back()->with('notify', 'Dokumen harus PDF / Excel');
                }
                $name = 'evjab-'.'skj'.'-'.rand(1,9000).'.'.$file->getClientOriginalExtension();
                $file->move('public/skj/', $name);
                $skj->dokumen_pendukung = $name;
    
                // dd($skj);
                // return $skj;
                $skj->update();
                return redirect()->back()->with('notify', 'SKJ berhasil dirubah');
            }
    }

    function delete (Request $request){
        // return $request;
        $skj = TrxSkjUpload::findOrFail($request->id);
        $dokumenPendukung = $skj->dokumen_pendukung;
        $skj->delete();

        unlink('public/skj/'.$dokumenPendukung);

        return redirect()->back()->with('notify', 'Skj berhasil dihapus');
    }
}

