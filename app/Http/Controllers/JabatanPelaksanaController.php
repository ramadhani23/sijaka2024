<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\MJabatanPelaksana;
use App\Models\MJabatanFungsional;
use App\Models\MJabatanFungsionalJenjang;
use App\Models\MJabatanFungsionalSyarat;
use App\Repositories\JabatanRepository;

class JabatanPelaksanaController extends Controller
{
    function browse(Request $request, JabatanRepository $jabatanRepo){
        $data['section'] = "jabatan-pelaksana";
        $data['page_section'] = "jabatan-pelaksana";
        $data['page'] = "Data Jabatan Pelaksana";

        
        $jabatan = $jabatanRepo->getJabatanPelaksana();

        $data['jabatan'] = $jabatan;
        // return $jabatan->count();
        return view('master.jabatan-pelaksana.browse', $data);
    }

    function getForm(Request $request, JabatanRepository $jabatanRepo){
        if($request->aksi == 'create-jabatan'){

            $urusanPemerintahan = $jabatanRepo->getUrusanPemerintahan();

            $data = [
                'urusanPemerintahan' => $urusanPemerintahan,
            ];
            return view('master.jabatan-pelaksana.form-create', $data);
        }elseif($request->aksi == 'edit-jabatan'){
            $jabatan = MJabatanPelaksana::findOrFail($request->id);
            $urusanPemerintahan = $jabatanRepo->getUrusanPemerintahan();

            $data = [
                'jabatan' => $jabatan,
                'urusanPemerintahan' => $urusanPemerintahan,
            ];
            return view('master.jabatan-pelaksana.form-edit', $data);
        }
    }

    function save(Request $request, JabatanRepository $jabatanRepo){
        // return $request;
        if($request->aksi == 'add-jabatan'){
            // $kode_urusan_pemerintahan   = $request->kode_urusan_pemerintahan;
            // $kode_urusan_pemerintahan   = $request->kode;
            $instansi_teknis            = $request->instansi_teknis;
            $jenjang_pendidikan         = $request->jenjang_pendidikan;
            $klasifikasi                = $request->klasifikasi;
            $kode                       = $request->kode;
            $jabatan                    = $request->jabatan;
            $kualifikasi_pendidikan     = $request->kualifikasi_pendidikan;
            $tugas                      = $request->tugas;
            $kode_bkpp                  = $request->kode_bkpp;
            $dasar_hukum                      = $request->dasar_hukum;
            $jenis                      = $request->jenis;
            $_kode                      = $request->_kode .".". $request->kode_urusan_pemerintahan .".". $request->kode ."."."24";
            // dd($instansi_teknis);
            // $lastUrutan = MJabatanPelaksana::orderBy('urutan')->first();
            // if($lastUrutan->urutan){
            //     $urutan = intVal($lastUrutan->urutan) + 1;
            // }else{
            //     $urutan = 1;
            // }
            // return $request;
            $jabatanRepo->saveJabatanPelaksana($kualifikasi_pendidikan, $jabatan, $klasifikasi, $instansi_teknis, $jenjang_pendidikan, $kode, $tugas, $kode_bkpp, $dasar_hukum, $jenis, $_kode);

            return redirect()->back()->with('notify', 'Jabatan berhasil ditambahkan');
        }elseif ($request->aksi == 'update-jabatan') {
            $jabatan = MJabatanPelaksana::findOrFail($request->id);

            $jabatan->kode_urusan_pemerintahan   = $request->kode_urusan_pemerintahan;
            $jabatan->kode                       = $request->kode;
            $jabatan->jabatan                    = $request->jabatan;
            $jabatan->instansi_teknis                       = $request->instansi_teknis;
            $jabatan->jenjang_pendidikan                       = $request->jenjang_pendidikan;
            $jabatan->kualifikasi_pendidikan     = $request->kualifikasi_pendidikan;
            $jabatan->klasifikasi                = $request->klasifikasi;
            $jabatan->tugas_jabatan              = $request->tugas;
            $jabatan->kode_bkpp                  = $request->kode_bkpp;
            $jabatan->jenis                      = $request->jenis;
            $jabatan->_kode                      = $request->_kode .".". $request->kode_urusan_pemerintahan .".". $request->kode."."."24";
            // return $jabatan;
            $jabatan->update();

            return redirect()->back()->with('notify', 'Jabatan Pelaksana berhasil diperbarui');
        }
    }

    function delete(Request $request){
        $jabatan = MJabatanPelaksana::findOrFail($request->id);

        $jabatan->delete();

        return redirect()->back()->with('notify', 'Jabatan Pelaksana berhasil dihapus');
    }
}
