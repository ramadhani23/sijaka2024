<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabSyaratJabatan;
use App\Models\MBakatKerja;
use App\Models\MUpayaFisik;
use App\Models\MTemperamenKerja;
use App\Models\MMinatKerja;
use App\Models\MFungsiPekerjaan;

use DB;

class AnjabSyaratJabatanController extends Controller
{
    function browse($id){

    	$data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "Detail Anjab";
    	$data['pages'] = "detail_syarat_jabatan";

        $bakatKerja = MBakatKerja::all();
        $upayaFisik = MUpayaFisik::all();
        $temperamenKerja = MTemperamenKerja::all();
        $minatKerja = MMinatKerja::all();
        $fungsiPekerjaan = MFungsiPekerjaan::all();

        $data['bakatKerja'] = $bakatKerja;
        $data['upayaFisik'] = $upayaFisik;
        $data['temperamenKerja'] = $temperamenKerja;
        $data['minatKerja'] = $minatKerja;
        $data['fungsiPekerjaan'] = $fungsiPekerjaan;

    	$data['anjab'] = TrxAnjab::findOrFail($id);
        $data['syaratJabatan'] = TrxAnjabSyaratJabatan::where('trx_anjab_id', $id)->first();
    	return view('anjab.anjab-detail-syarat-jabatan', $data);

    }

    function getForm(Request $request){

        $bakatKerja = MBakatKerja::all();
        $upayaFisik = MUpayaFisik::all();
        $temperamenKerja = MTemperamenKerja::all();
        $minatKerja = MMinatKerja::all();
        $fungsiPekerjaan = MFungsiPekerjaan::all();

        $data['bakatKerja'] = $bakatKerja;
        $data['upayaFisik'] = $upayaFisik;
        $data['temperamenKerja'] = $temperamenKerja;
        $data['minatKerja'] = $minatKerja;
        $data['fungsiPekerjaan'] = $fungsiPekerjaan;

    	if($request->aksi == 'add-data'){
            $data['id'] = $request->id;
    		return view('anjab.syarat_jabatan.form-create-syarat-jabatan', $data);
    	}elseif ($request->aksi == 'edit-data') {
            $data['syarat_jabatan'] = TrxAnjabSyaratJabatan::where('trx_anjab_id', $request->id)->firstOrFail();
    		return view('anjab.syarat_jabatan.form-edit-syarat-jabatan', $data);
    	}
    }

     function save(Request $request){

        if($request->aksi == 'create'){
            $syarat_jabatan = new TrxAnjabSyaratJabatan;
            
            // $intelegensia = $request->intelegensia;
            // if ($intelegensia != 1) {
            //     $intelegensia = 0;
            // }

            // $bakat_verbal = $request->bakat_verbal;
            // if ($bakat_verbal != 1) {
            //     $bakat_verbal = 0;
            // }

            // $bakat_numerik = $request->bakat_numerik;
            // if ($bakat_numerik != 1) {
            //     $bakat_numerik = 0;
            // }

            // $bakat_ketelitian = $request->bakat_ketelitian;
            // if ($bakat_ketelitian != 1) {
            //     $bakat_ketelitian = 0;
            // }

            // $directing_control_planning = $request->directing_control_planning;
            // if ($directing_control_planning != 1) {
            //     $directing_control_planning = 0;
            // }

            // $feeling_idea_fact = $request->feeling_idea_fact;
            // if ($feeling_idea_fact != 1) {
            //     $feeling_idea_fact = 0;
            // }

            // $influencing = $request->influencing;
            // if ($influencing != 1) {
            //     $influencing = 0;
            // }

            // $measurable_verifiable_creteria = $request->measurable_verifiable_creteria;
            // if ($measurable_verifiable_creteria != 1) {
            //     $measurable_verifiable_creteria = 0;
            // }

            // $dealing_with_people = $request->dealing_with_people;
            // if ($dealing_with_people != 1) {
            //     $dealing_with_people = 0;
            // }

            // $repetitive_continuous = $request->repetitive_continuous;
            // if ($repetitive_continuous != 1) {
            //     $repetitive_continuous = 0;
            // }

            // $investigatif = $request->investigatif;
            // if ($investigatif != 1) {
            //     $investigatif = 0;
            // }

            // $konvensional = $request->konvensional;
            // if ($konvensional != 1) {
            //     $konvensional = 0;
            // }

            // $duduk = $request->duduk;
            // if ($duduk != 1) {
            //     $duduk = 0;
            // }

            // $berbicara = $request->berbicara;
            // if ($berbicara != 1) {
            //     $berbicara = 0;
            // }

            // $melihat = $request->melihat;
            // if ($melihat != 1) {
            //     $melihat = 0;
            // }

            // $mengkoordinasi_data = $request->mengkoordinasi_data;
            // if ($mengkoordinasi_data != 1) {
            //     $mengkoordinasi_data = 0;
            // }

            // $menganalisis_data = $request->menganalisis_data;
            // if ($menganalisis_data != 1) {
            //     $menganalisis_data = 0;
            // }

            // $menyusun_data = $request->menyusun_data;
            // if ($menyusun_data != 1) {
            //     $menyusun_data = 0;
            // }

            // $menasehati = $request->menasehati;
            // if ($menasehati != 1) {
            //     $menasehati = 0;
            // }

            // $berunding = $request->berunding;
            // if ($berunding != 1) {
            //     $berunding = 0;
            // }

            // $mengajar = $request->mengajar;
            // if ($mengajar != 1) {
            //     $mengajar = 0;
            // }

            // $menyelia = $request->menyelia;
            // if ($menyelia != 1) {
            //     $menyelia = 0;
            // }


            $syarat_jabatan->keterampilan_kerja = $request->keterampilan_kerja;
            // $syarat_jabatan->intelegensia = $intelegensia;
            // $syarat_jabatan->directing_control_planning = $directing_control_planning;
            // $syarat_jabatan->bakat_verbal = $bakat_verbal;
            // $syarat_jabatan->bakat_numerik = $bakat_numerik;
            // $syarat_jabatan->bakat_ketelitian = $bakat_ketelitian;
            // $syarat_jabatan->feeling_idea_fact = $feeling_idea_fact;
            // $syarat_jabatan->influencing = $influencing;
            // $syarat_jabatan->dealing_with_people = $dealing_with_people;
            // $syarat_jabatan->measurable_verifiable_creteria = $measurable_verifiable_creteria;
            // $syarat_jabatan->repetitive_continuous = $repetitive_continuous;
            // $syarat_jabatan->investigatif = $investigatif;
            // $syarat_jabatan->konvensional = $konvensional;
            // $syarat_jabatan->mengkoordinasi_data = $mengkoordinasi_data;
            // $syarat_jabatan->menganalisis_data = $menganalisis_data;
            // $syarat_jabatan->melihat = $melihat;
            // $syarat_jabatan->duduk = $duduk;
            // $syarat_jabatan->berbicara = $berbicara;
            // $syarat_jabatan->menyusun_data= $menyusun_data;
            // $syarat_jabatan->menasehati= $menasehati;
            // $syarat_jabatan->berunding= $berunding;
            // $syarat_jabatan->mengajar= $mengajar;
            // $syarat_jabatan->menyelia= $menyelia;
            $syarat_jabatan->minat_kerja = $request->minat_kerja != NULL ? $request->minat_kerja:array();
            $syarat_jabatan->upaya_fisik = $request->upaya_fisik != NULL ? $request->upaya_fisik:array();
            $syarat_jabatan->temperamen_kerja = $request->temperamen_kerja != NULL ? $request->temperamen_kerja:array();
            $syarat_jabatan->bakat_kerja = $request->bakat_kerja != NULL ? $request->bakat_kerja:array();
            $syarat_jabatan->fungsi_pekerjaan = $request->fungsi_pekerjaan != NULL ? $request->fungsi_pekerjaan:array();

            $syarat_jabatan->jenis_kelamin = $request->jenis_kelamin;
            $syarat_jabatan->umur = $request->umur;
            $syarat_jabatan->tinggi_badan = $request->tinggi_badan;
            $syarat_jabatan->berat_badan = $request->berat_badan;
            $syarat_jabatan->postur_badan = $request->postur_badan;
            $syarat_jabatan->penampilan = $request->penampilan;
            $syarat_jabatan->trx_anjab_id = $request->id;
            $syarat_jabatan->created_at = date('Y-m-d H:i:s');
            $syarat_jabatan->save();
            return redirect()->route('anjab.browse-syarat-jabatan',$request->id)->with('notify', 'Syarat Jabatan berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {
            $syarat_jabatan = TrxAnjabSyaratJabatan::findOrFail($request->id);

            // $intelegensia = $request->intelegensia;
            // if ($intelegensia != 1) {
            //     $intelegensia = 0;
            // }

            // $bakat_verbal = $request->bakat_verbal;
            // if ($bakat_verbal != 1) {
            //     $bakat_verbal = 0;
            // }

            // $bakat_numerik = $request->bakat_numerik;
            // if ($bakat_numerik != 1) {
            //     $bakat_numerik = 0;
            // }

            // $bakat_ketelitian = $request->bakat_ketelitian;
            // if ($bakat_ketelitian != 1) {
            //     $bakat_ketelitian = 0;
            // }

            // $directing_control_planning = $request->directing_control_planning;
            // if ($directing_control_planning != 1) {
            //     $directing_control_planning = 0;
            // }

            // $feeling_idea_fact = $request->feeling_idea_fact;
            // if ($feeling_idea_fact != 1) {
            //     $feeling_idea_fact = 0;
            // }

            // $influencing = $request->influencing;
            // if ($influencing != 1) {
            //     $influencing = 0;
            // }

            // $measurable_verifiable_creteria = $request->measurable_verifiable_creteria;
            // if ($measurable_verifiable_creteria != 1) {
            //     $measurable_verifiable_creteria = 0;
            // }

            // $dealing_with_people = $request->dealing_with_people;
            // if ($dealing_with_people != 1) {
            //     $dealing_with_people = 0;
            // }

            // $repetitive_continuous = $request->repetitive_continuous;
            // if ($repetitive_continuous != 1) {
            //     $repetitive_continuous = 0;
            // }

            // $investigatif = $request->investigatif;
            // if ($investigatif != 1) {
            //     $investigatif = 0;
            // }

            // $konvensional = $request->konvensional;
            // if ($konvensional != 1) {
            //     $konvensional = 0;
            // }

            // $duduk = $request->duduk;
            // if ($duduk != 1) {
            //     $duduk = 0;
            // }

            // $berbicara = $request->berbicara;
            // if ($berbicara != 1) {
            //     $berbicara = 0;
            // }

            // $melihat = $request->melihat;
            // if ($melihat != 1) {
            //     $melihat = 0;
            // }

            // $mengkoordinasi_data = $request->mengkoordinasi_data;
            // if ($mengkoordinasi_data != 1) {
            //     $mengkoordinasi_data = 0;
            // }

            // $menganalisis_data = $request->menganalisis_data;
            // if ($menganalisis_data != 1) {
            //     $menganalisis_data = 0;
            // }

            // $menyusun_data = $request->menyusun_data;
            // if ($menyusun_data != 1) {
            //     $menyusun_data = 0;
            // }

            // $menasehati = $request->menasehati;
            // if ($menasehati != 1) {
            //     $menasehati = 0;
            // }

            // $berunding = $request->berunding;
            // if ($berunding != 1) {
            //     $berunding = 0;
            // }

            // $mengajar = $request->mengajar;
            // if ($mengajar != 1) {
            //     $mengajar = 0;
            // }

            // $menyelia = $request->menyelia;
            // if ($menyelia != 1) {
            //     $menyelia = 0;
            // }



            $syarat_jabatan->keterampilan_kerja = $request->keterampilan_kerja;
            // $syarat_jabatan->intelegensia = $intelegensia;
            // $syarat_jabatan->directing_control_planning = $directing_control_planning;
            // $syarat_jabatan->bakat_verbal = $bakat_verbal;
            // $syarat_jabatan->bakat_numerik = $bakat_numerik;
            // $syarat_jabatan->bakat_ketelitian = $bakat_ketelitian;
            // $syarat_jabatan->feeling_idea_fact = $feeling_idea_fact;
            // $syarat_jabatan->influencing = $influencing;
            // $syarat_jabatan->dealing_with_people = $dealing_with_people;
            // $syarat_jabatan->measurable_verifiable_creteria = $measurable_verifiable_creteria;
            // $syarat_jabatan->repetitive_continuous = $repetitive_continuous;
            // $syarat_jabatan->investigatif = $investigatif;
            // $syarat_jabatan->konvensional = $konvensional;
            // $syarat_jabatan->mengkoordinasi_data = $mengkoordinasi_data;
            // $syarat_jabatan->menganalisis_data = $menganalisis_data;
            // $syarat_jabatan->melihat = $melihat;
            // $syarat_jabatan->duduk = $duduk;
            // $syarat_jabatan->berbicara = $berbicara;
            // $syarat_jabatan->menyusun_data= $menyusun_data;
            // $syarat_jabatan->menasehati= $menasehati;
            // $syarat_jabatan->berunding= $berunding;
            // $syarat_jabatan->mengajar= $mengajar;
            // $syarat_jabatan->menyelia= $menyelia;
            $syarat_jabatan->minat_kerja = $request->minat_kerja != NULL ? $request->minat_kerja:array();
            $syarat_jabatan->upaya_fisik = $request->upaya_fisik != NULL ? $request->upaya_fisik:array();
            $syarat_jabatan->temperamen_kerja = $request->temperamen_kerja != NULL ? $request->temperamen_kerja:array();
            $syarat_jabatan->bakat_kerja = $request->bakat_kerja != NULL ? $request->bakat_kerja:array();
            $syarat_jabatan->fungsi_pekerjaan = $request->fungsi_pekerjaan != NULL ? $request->fungsi_pekerjaan:array();
            
            $syarat_jabatan->jenis_kelamin = $request->jenis_kelamin;
            $syarat_jabatan->umur = $request->umur;
            $syarat_jabatan->tinggi_badan = $request->tinggi_badan;
            $syarat_jabatan->berat_badan = $request->berat_badan;
            $syarat_jabatan->postur_badan = $request->postur_badan;
            $syarat_jabatan->penampilan = $request->penampilan;

            $syarat_jabatan->update();
            return redirect()->route('anjab.browse-syarat-jabatan',$request->id_anjab)->with('notify', 'Syarat Jabatan berhasil diperbarui');
        }
    }

    function deleteHasilKerja(Request $request){
        $syarat_jabatan = TrxAnjabSyaratJabatan::findOrFail($request->id);
        $syarat_jabatan->delete();
        return redirect()->back()->with('notify', 'Syarat Jabatan berhasil dihapus');
    }
   
}
