<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MUpayaFisik;

class UpayaFisikController extends Controller
{
    function browse(Request $request){

    	$data['section'] = "upaya-fisik";

    	$data['page_section'] = "upaya-fisik";
    	$data['page'] = "Data bakat Kerja";

    	$upayafisik = MUpayaFisik::query();

    	if($request->keyword){
    		$upayafisik = $upayafisik->where('nama', 'LIKE', '%'.$request->keyword.'%');
    	}

    	$upayafisik = $upayafisik->orderBy('nama', 'ASC')->paginate(20);
        // return $upayafisik;
    	$data['upayafisik'] = $upayafisik;

    	return view('master_upaya_fisik.browse', $data);
    }

    function getForm(Request $request){
    	if($request->aksi == 'create-upayafisik'){
    		return view('master_upaya_fisik.form-create');
    	}elseif($request->aksi == 'edit-upayafisik'){
    		$upayafisik = MUpayafisik::findOrFail($request->id);

    		$data = [
    			'upayafisik' => $upayafisik
    		];
    		return view('master_upaya_fisik.form-edit', $data);
    	}
    }

    function save(Request $request){

    	if($request->aksi == 'add-upayafisik'){
    		$upayafisik = new MUpayafisik;

    		$upayafisik->nama = $request->nama;
    		$upayafisik->keterangan = $request->keterangan;

    		$upayafisik->save();

    		return redirect()->back()->with('notify', 'bakat kerja berhasil ditambahkan');
    	}elseif ($request->aksi == 'update-upayafisik') {
    		$upayafisik = MUpayafisik::findOrFail($request->id);

    		$upayafisik->nama = $request->nama;
    		$upayafisik->keterangan = $request->keterangan;
    		$upayafisik->update();

    		return redirect()->back()->with('notify', 'bakat kerja berhasil diperbarui');
    	}
    }

    function delete(Request $request){
    	$upayafisik = MUpayafisik::findOrFail($request->id);

    	// if($upayafisik->dataAnjab){
    	// 	return redirect()->back()->with('error', 'upayafisik tidak bisa dihapus, digunakan untuk Analisa upayafisik');
    	// }

    	$upayafisik->delete();

    	return redirect()->back()->with('notify', 'bakat kerja berhasil dihapus');
    }
}
