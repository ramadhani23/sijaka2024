<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabEvajabFungsional;
use App\Models\TrxAnjabEvajabStruktural;
use App\Models\TrxAnjabAbk;
use App\Models\TrxAnjabBahanKerja;
use App\Models\TrxAnjabHasilKerja;
use App\Models\TrxAnjabKorelasiJabatan;
use App\Models\TrxAnjabLingkunganKerja;
use App\Models\TrxAnjabPerangkat;
use App\Models\TrxAnjabPrestasiKerja;
use App\Models\TrxAnjabResikoBahaya;
use App\Models\TrxAnjabSyaratJabatan;
use App\Models\TrxAnjabTanggungJawab;
use App\Models\TrxAnjabUraianJabatan;
use App\Models\TrxAnjabUraianJabatanTahapan;
use App\Models\TrxAnjabWewenang;
use App\Models\TrxAnjabEvajab;
use App\Models\MFaktor;
use App\Models\MLevelFaktor;
use App\Models\MUnitKerja;
use App\Models\M_jabatan;
use App\Models\MFaktorDeskripsi;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Repositories\UnitKerjaRepository;


use DB;

class EvajabController extends Controller
{
    function browseFaktorStruktural(){

    	$data['section'] = "faktor";
    	$data['page_section'] = "faktor";
    	$data['page'] = "Detail Faktor";
    	$data['pages'] = "detail_faktor";
        // return $data['faktor'] = MLevelFaktor::with('dataFaktor')->get();
    	// $data['faktor'] = MLevelFaktor::where($dataFaktor, 'STRRUKTURAL')->get();
       $data['faktor'] = MLevelFaktor::with('dataFaktor')->whereHas('dataFaktor', function($query) {
            $query->where('jenis', 'STRUKTURAL')->orderBy('nama');
        })->get();
        // $data['hasil_kerja'] = TrxAnjabHasilKerja::where('trx_anjab_id', $id)->orderBy('id', 'asc')->get();
    	return view('master_faktor_evajab.browse-struktural', $data);

    }
    function browseFaktorFungsional(){

    	$data['section'] = "faktor";
    	$data['page_section'] = "faktor";
    	$data['page'] = "Detail Faktor";
    	$data['pages'] = "detail_faktor";
    	$data['faktor'] = MLevelFaktor::with('dataFaktor')->whereHas('dataFaktor', function($query) {
            $query->where('jenis', 'FUNGSIONAL')->orderBy('nama');
        })->get();
        // $data['hasil_kerja'] = TrxAnjabHasilKerja::where('trx_anjab_id', $id)->orderBy('id', 'asc')->get();
    	return view('master_faktor_evajab.browse-fungsional', $data);

    }

    function getFormFaktor(Request $request){
        // Fungsional dan Pelaksana disamakan, jadi memakai 1 variabel yaitu fungsional
    	if($request->aksi == 'create-faktorstruktural'){
            // return $request;
            // $data['id'] = $request->id;
    		return view('master_faktor_evajab.form-create-struktural');
    	}
    	if($request->aksi == 'create-faktorfungsional'){
    		return view('master_faktor_evajab.form-create-fungsional');
    	}elseif ($request->aksi == 'edit-faktorstruktural') {
            $data['faktor'] = MLevelFaktor::findOrFail($request->id);
    		return view('master_faktor_evajab.form-edit-struktural', $data);
    	}elseif ($request->aksi == 'edit-faktorfungsional') {
            $data['faktor'] = MLevelFaktor::findOrFail($request->id);
    		return view('master_faktor_evajab.form-edit-fungsional', $data);
    	}
    }

    function saveFaktor(Request $request){
        // return $request;
        if($request->aksi == 'add-faktorstuktural'){
            $evjab = new MFaktor;
            $evjab->nama = $request->nama;
            $evjab->uraian = $request->uraian;
            $evjab->jenis = 'STRUKTURAL';
            $evjab->created_at = date('Y-m-d H:i:s');
            $evjab->save();
            return redirect()->route('faktorstruktural.browse',$request->id)->with('notify', 'Faktor berhasil ditambahkan');

        // }elseif ($request->aksi == 'update') {
        //     $evjab = TrxAnjabBahanKerja::findOrFail($request->id);

        //     $evjab->evjab = $request->evjab;
        //     $evjab->penggunaan_dalam_tugas = $request->penggunaan_dalam_tugas;

        //     $evjab->update();
        //     return redirect()->route('anjab.browse-bahan-kerja',$request->id_anjab)->with('notify', 'Anjab berhasil diperbarui');
        // }
        }
        elseif ($request->aksi == 'add-faktorfungsional') {
            $evjab = new MFaktor;
            $evjab->nama = $request->nama;
            $evjab->uraian = $request->uraian;
            $evjab->jenis = 'FUNGSIONAL';
            $evjab->created_at = date('Y-m-d H:i:s');
            $evjab->save();
            return redirect()->route('faktorfungsional.browse',$request->id)->with('notify', 'Faktor berhasil ditambahkan');
        }
        elseif($request->aksi == 'edit-faktorstruktural'){
            $evjab = MLevelFaktor::find($request->id);
            $evjab->nilai = $request->nilai;
            $evjab->uraian = $request->uraian;
            // $evjab->jenis = 'STRUKTURAL';
            $evjab->created_at = date('Y-m-d H:i:s');
            $evjab->update();
            return redirect()->route('faktorstruktural.browse',$request->id)->with('notify', 'Faktor berhasil diubah');

        }
        elseif($request->aksi == 'edit-faktorfungsional'){
            $evjab = MLevelFaktor::find($request->id);
            $evjab->nilai = $request->nilai;
            $evjab->uraian = $request->uraian;
            // $evjab->jenis = 'FUNGSIONAL';
            $evjab->created_at = date('Y-m-d H:i:s');
            $evjab->update();
            return redirect()->route('faktorfungsional.browse',$request->id)->with('notify', 'Faktor berhasil diubah');

        }
    }

    // function editFaktor(Request $request){
    //     return $request;
    //     if($request->aksi == 'edit-faktorstuktural'){
    //         $bahan_kerja = MFaktor::find($request->id);
    //         $bahan_kerja->nama = $request->nama;
    //         $bahan_kerja->uraian = $request->uraian;
    //         $bahan_kerja->jenis = 'STRUKTURAL';
    //         $bahan_kerja->created_at = date('Y-m-d H:i:s');
    //         return $bahan_kerja;
    //         $bahan_kerja->update();
    //         return redirect()->route('faktorstruktural.browse',$request->id)->with('notify', 'Faktor berhasil diubah');
    //     }
    //     else {
    //         $bahan_kerja = MFaktor::find($id);
    //         $bahan_kerja->nama = $request->nama;
    //         $bahan_kerja->uraian = $request->uraian;
    //         $bahan_kerja->jenis = 'FUNGSIONAL';
    //         $bahan_kerja->created_at = date('Y-m-d H:i:s');
    //         $bahan_kerja->update();
    //         return redirect()->route('faktorfungsional.browse',$request->id)->with('notify', 'Faktor berhasil diubah');
    //     }
    // }
    function deleteFaktor(Request $request){
        $faktor = MFaktor::findOrFail($request->id);
        $faktor->delete();
        return redirect()->back()->with('notify', 'Faktor berhasil dihapus');
    }

    function browse(Request $request, $jenis, UnitKerjaRepository $unitKerjaRepo){
    	$data['section'] = "evajab";    
    	$data['page'] = "Evaluasi Jabatan";
    	$data['pages'] = "evajab";
        $data['jenis'] = $jenis;

        // $data['unit_kerja'] = MUnitKerja::where('kodeunit', 'like', '%00')->get();

        $anjab = TrxAnjab::query();

        if($jenis == 'struktural'){
            $data['section'] = "evajab-struktural";  
            $data['page_section'] = "evajab-struktural";  
            $data['page'] = "Evaluasi Jabatan";
            $data['pages'] = "evajab";

            // $anjab = TrxAnjab::query();
            // $anjab=DB::table('trx_anjab')->get();
            // dd($anjab);
            $evjab = TrxAnjabEvajabStruktural::query();

            $kodeOpd = 0;

            if($request->kode_opd){
                $kodeOpd = $request->kode_opd;
            }

            if(\MojokertokabUser::getUser()->role == 'PD'){
                $kodeOpd = \MojokertokabUser::getUser()->kode_opd;
            }


            if(\MojokertokabUser::getUser()->role == 'PD'){
                
                if(strlen(\MojokertokabUser::getUser()->kode_opd) == 15){
                    $anjab = $anjab->where('unit_kerja_eselon3', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');

                    $evjab = $evjab->whereHas('dataAnjab', function($query){
                        return $query->where('unit_kerja_eselon3', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');
                    });
                }else{
                    $anjab = $anjab->where('unit_kerja', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%')->Where('jenis_jabatan', '1')->get();
                    // return $anjab;
                    // dd($anjab);
                    $anjabkosong = DB::table("trx_anjab as a")
                    ->whereNotIn("a.id", function($query) {
                        $query->select("trx_anjab_id")->from("trx_anjab_evajab_struktural");
                    })
                    ->where("a.unit_kerja", "LIKE", \MojokertokabUser::getUser()->kode_opd.'%')
                    ->where("a.jenis_jabatan", 1)
                    ->where("deleted_at", null)
                    ->selectRaw("
                        null as id,
                        a.id as trx_anjab_id,
                        null as faktor1,
                        null as text_faktor1,
                        null as faktor2,
                        null as text_faktor2,
                        null as faktor3,
                        null as text_faktor3,
                        null as faktor4A,
                        null as text_faktor4A,
                        null as faktor4B,
                        null as text_faktor4B,
                        null as faktor5,
                        null as text_faktor5,
                        null as faktor6,
                        null as text_faktor6,
                        null as created_at,
                        null as updated_at,
                        null as deleted_at,
                        null as status,
                        null as m_level_faktor_id
                    ");

                    $evjab = $evjab->whereHas('dataAnjab', function($query) use ($kodeOpd){
                        return $query->where('_kode_jabatan', '!=', 'NULL')->where('unit_kerja', 'LIKE', $kodeOpd.'%')->where('jenis_jabatan', '=', '1');
                    })
                    ->unionAll($anjabkosong);
                }
                // return $evjab;
                $data['unit_kerja'] = $kodeOpd;
            } else {
                $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
                $data['unitKerja'] = $unitKerja;
                
                // return $unitKerja;
                // $unitk= MUnitkerja::get();
                // $dataOlahunitK=[];
                // foreach($unitk as $untk){
                    
                    
                //     $kode=substr($untk->kodeunit, 0,2);
                //     // echo $kode
                //     if(substr($untk->kodeunit, 0, 2)=="01"){
                //         if (strlen($untk->kodeunit)==6) {
                //             $dataOlahunitK[]=[
                //                 "kodeunit"=>$untk->kodeunit,
                //                 "unitkerja"=>$untk->unitkerja
                //             ];
                //         }
                //     }else{
                //         if (strlen($untk->kodeunit)==2) {
                //             $kode=substr($untk->kodeunit, 0,2);
                //             $dataOlahunitK[]=[
                //                 "kodeunit"=>$untk->kodeunit,
                //                 "unitkerja"=>$untk->unitkerja
                //             ];
                //         }
                //     }
                // }
                // $data['unitKerja']=$dataOlahunitK;

                $unitKerja        = $request->unit_kerja;
                $jenis_jabatan    = 1;
                // return $jenis_jabatan;
                if($unitKerja){
                    $anjab = $anjab->where('unit_kerja', 'LIKE',  $unitKerja.'%')->Where('jenis_jabatan', '1')->get();
                    // dd($anjab);
                    $anjabkosong = DB::table("trx_anjab as a")
                    ->whereNotIn("a.id", function($query) {
                        $query->select("trx_anjab_id")->from("trx_anjab_evajab_struktural");
                    })
                    ->where("a.unit_kerja", "LIKE",  $unitKerja.'%')
                    ->where("a.jenis_jabatan", 1)
                    ->where("deleted_at", null)
                    ->selectRaw("
                        null as id,
                        a.id as trx_anjab_id,
                        null as faktor1,
                        null as text_faktor1,
                        null as faktor2,
                        null as text_faktor2,
                        null as faktor3,
                        null as text_faktor3,
                        null as faktor4A,
                        null as text_faktor4A,
                        null as faktor4B,
                        null as text_faktor4B,
                        null as faktor5,
                        null as text_faktor5,
                        null as faktor6,
                        null as text_faktor6,
                        null as created_at,
                        null as updated_at,
                        null as deleted_at,
                        null as status,
                        null as m_level_faktor_id
                    ");

                    $evjab = $evjab->whereHas('dataAnjab', function($query) use ( $unitKerja){
                        return $query->where('_kode_jabatan', '!=', 'NULL')->where('unit_kerja', 'LIKE',  $unitKerja.'%')->where('jenis_jabatan', '=', '1');
                    })
                    ->unionAll($anjabkosong);
                
                }
                // return $evjab;
                // if($jenis_jabatan){
                //     $evjab = $evjab->whereHas('dataAnjab', function($query) use ($jenis_jabatan) {
                //         return $query->where('jenis_jabatan', $jenis_jabatan);
                //     });
                //     // $evjab = $evjab->with(['jabatan' => function($query) use ($jenis_jabatan) {
                //     //     $query->where('jenis_jabatan', $jenis_jabatan);
                //     // }]);

                //     //   $evjab = $evjab->with(['dataAnjab' => function($query) use ($jenis_jabatan) {
                //     //     $query->with('jabatan')->where('jenis_jabatan', $jenis_jabatan);
                //     // }]);

                // //    return $evjab = $evjab->where('jenis_jabatan', 'LIKE', $jenis_jabatan.'%')->get();
                // }
                // return $evjab; 
            $data['unit_kerja'] = $unitKerja;
            }
            $data['evjab'] = $evjab->get()->sortByDesc('kelas_jabatan');

            return view('evjab.browse-struktural', $data);
        }elseif ($jenis == 'fungsional') {
            // $anjab = $anjab->where('jenis_jabatan', '!=', '1')->whereDoesntHave('dataEvajabFungsional');
            $data['section'] = "evajab-fungsional";  
            $data['page_section'] = "evajab-fungsional";  
            $data['page'] = "Evaluasi Jabatan";
            $data['pages'] = "evajab";

            $anjab = TrxAnjab::query();
            $evjab = TrxAnjabEvajabFungsional::query();
            // $anjab=DB::table('trx_anjab')->get();
            // dd($anjab);
        //     return $evjab =TrxAnjabEvajabFungsional::whereHas('dataAnjab', function($query) {
        //         $query->where('unit_kerja', 'like', 21 . '%')->where('jenis_jabatan' , 3);
        //    })->get();
            

            $kodeOpd = 0;

            if($request->kode_opd){
                $kodeOpd = $request->kode_opd;
            }

            if(\MojokertokabUser::getUser()->role == 'PD'){
                $kodeOpd = \MojokertokabUser::getUser()->kode_opd;
            }

            $data['page_section'] = "evajab-fungsional";

            if(\MojokertokabUser::getUser()->role == 'PD'){ 
                // return \MojokertokabUser::getUser()->kode_opd;
                if(strlen(\MojokertokabUser::getUser()->kode_opd) == 15){
                    // die;
                    $anjab = $anjab->where('unit_kerja_eselon3', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');

                    $evjab = $evjab->whereHas('dataAnjab', function($query){
                        return $query->where('unit_kerja_eselon3', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');
                    });
                }else{
                    $anjab = $anjab->where('unit_kerja', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%')->Where('jenis_jabatan', '2')->get();
                    // dd($anjab);
                    $anjabkosong = DB::table("trx_anjab as a")
                    ->whereNotIn("a.id", function($query) {
                        $query->select("trx_anjab_id")->from("trx_anjab_evajab_fungsional");
                    })
                    ->where("a.unit_kerja", "LIKE", \MojokertokabUser::getUser()->kode_opd.'%')
                    ->where("a.jenis_jabatan", 2)
                    ->where("deleted_at", null)
                    ->selectRaw("
                        null as id,
                        a.id as trx_anjab_id,
                        null as faktor1,
                        null as text_faktor1,
                        null as faktor2,
                        null as text_faktor2,
                        null as faktor3,
                        null as text_faktor3,
                        null as faktor4,
                        null as text_faktor4,
                        null as faktor5,
                        null as text_faktor5,
                        null as faktor6,
                        null as text_faktor6,
                        null as faktor7,
                        null as text_faktor7,
                        null as faktor8,
                        null as text_faktor8,
                        null as faktor9,
                        null as text_faktor9,
                        null as created_at,
                        null as updated_at,
                        null as deleted_at,
                        null as status
                    ");

                    $evjab = $evjab->whereHas('dataAnjab', function($query) use ($kodeOpd){
                        return $query->where('_kode_jabatan', '!=', 'NULL')->where('unit_kerja', 'LIKE', $kodeOpd.'%')->where('jenis_jabatan', '=', '2');
                    })
                    ->unionAll($anjabkosong);
                }
                $data['unit_kerja'] = $kodeOpd;
                // $data['page_section'] = "evajab-fungsional";
            } else {
                $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
                $data['unitKerja'] = $unitKerja;
                    // $unitk= MUnitkerja::get();
                    // $dataOlahunitK=[];
                    // foreach($unitk as $untk){
                        
                        
                    //     $kode=substr($untk->kodeunit, 0,2);
                    //     // echo $kode
                    //     if(substr($untk->kodeunit, 0, 2)=="01"){
                    //         if (strlen($untk->kodeunit)==6) {
                    //             $dataOlahunitK[]=[
                    //                 "kodeunit"=>$untk->kodeunit,
                    //                 "unitkerja"=>$untk->unitkerja
                    //             ];
                    //         }
                    //     }else{
                    //         if (strlen($untk->kodeunit)==2) {
                    //             $kode=substr($untk->kodeunit, 0,2);
                    //             $dataOlahunitK[]=[
                    //                 "kodeunit"=>$untk->kodeunit,
                    //                 "unitkerja"=>$untk->unitkerja
                    //             ];
                    //         }
                    //     }
                    // }
                    // $data['unitKerja']=$dataOlahunitK;
                    $unitKerja        = $request->unit_kerja;
                    $jenis_jabatan    = 2;
                    // return $jenis_jabatan;
                    if($unitKerja){
                        $anjab = $anjab->where('unit_kerja', 'LIKE',  $unitKerja.'%')->Where('jenis_jabatan', '2')->get();
                        // dd($anjab);
                        $anjabkosong = DB::table("trx_anjab as a")
                        ->whereNotIn("a.id", function($query) {
                            $query->select("trx_anjab_id")->from("trx_anjab_evajab_fungsional");
                        })
                        ->where("a.unit_kerja", "LIKE",  $unitKerja.'%')
                        ->where("a.jenis_jabatan", 2)
                        ->where("deleted_at", null)
                        ->selectRaw("
                            null as id,
                            a.id as trx_anjab_id,
                            null as faktor1,
                            null as text_faktor1,
                            null as faktor2,
                            null as text_faktor2,
                            null as faktor3,
                            null as text_faktor3,
                            null as faktor4,
                            null as text_faktor4,
                            null as faktor5,
                            null as text_faktor5,
                            null as faktor6,
                            null as text_faktor6,
                            null as faktor7,
                            null as text_faktor7,
                            null as faktor8,
                            null as text_faktor8,
                            null as faktor9,
                            null as text_faktor9,
                            null as created_at,
                            null as updated_at,
                            null as deleted_at,
                            null as status
                        ");
                        $evjab = $evjab->whereHas('dataAnjab', function($query) use ( $unitKerja){
                            return $query->where('_kode_jabatan', '!=', 'NULL')->where('unit_kerja', 'LIKE',  $unitKerja.'%')->where('jenis_jabatan', '=', '2');
                        })
                        ->unionAll($anjabkosong);
                    }
                    // return $evjab;
                    // if($jenis_jabatan){
                    //     $evjab = $evjab->whereHas('dataAnjab', function($query) use ($jenis_jabatan) {
                    //         return $query->where('jenis_jabatan', $jenis_jabatan);
                    //     });
                    //     // $evjab = $evjab->with(['jabatan' => function($query) use ($jenis_jabatan) {
                    //     //     $query->where('jenis_jabatan', $jenis_jabatan);
                    //     // }]);

                    //     //   $evjab = $evjab->with(['dataAnjab' => function($query) use ($jenis_jabatan) {
                    //     //     $query->with('jabatan')->where('jenis_jabatan', $jenis_jabatan);
                    //     // }]);

                    // //    return $evjab = $evjab->where('jenis_jabatan', 'LIKE', $jenis_jabatan.'%')->get();
                    // }
                    // return $evjab; 
                    $data['unit_kerja'] = $unitKerja;
                }
           $data['evjab'] = $evjab->get()->sortByDesc('kelas_jabatan');
        //    return $data;
            // return $evjab;
            // $data['anjab'] = $anjab;
            // dd($data['evjab']);
            return view('evjab.browse-fungsional', $data);
            


        }elseif ($jenis == 'pelaksana') {
            $data['section'] = "evajab-pelaksana";  
            $data['page_section'] = "evajab-pelaksana";
            $data['page'] = "Evaluasi Jabatan";
            $data['pages'] = "evajab";
            // $anjab = $anjab->where('jenis_jabatan', 3)->whereDoesntHave('dataEvajabFungsional');
            $evjab = TrxAnjabEvajabFungsional::query();
            $kodeOpd = 0;
            if($request->kode_opd){
                $kodeOpd = $request->kode_opd;
            }

            if(\MojokertokabUser::getUser()->role == 'PD'){
                $kodeOpd = \MojokertokabUser::getUser()->kode_opd;
            }
            if(\MojokertokabUser::getUser()->role == 'PD'){
                if(strlen(\MojokertokabUser::getUser()->kode_opd) == 15){
                    $anjab = $anjab->where('unit_kerja_eselon3', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');

                    $evjab = $evjab->whereHas('dataAnjab', function($query){
                        return $query->where('unit_kerja_eselon3', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');
                    });
                }else{
                    $anjab = $anjab->where('unit_kerja', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%')->Where('jenis_jabatan', '3')->get();
                    // dd($anjab);
                    $anjabkosong = DB::table("trx_anjab as a")
                    ->whereNotIn("a.id", function($query) {
                        $query->select("trx_anjab_id")->from("trx_anjab_evajab_fungsional");
                    })
                    ->where("a.unit_kerja", "LIKE", \MojokertokabUser::getUser()->kode_opd.'%')
                    ->where("a.jenis_jabatan", 3)
                    ->where("deleted_at", null)
                    ->selectRaw("
                        null as id,
                        a.id as trx_anjab_id,
                        null as faktor1,
                        null as text_faktor1,
                        null as faktor2,
                        null as text_faktor2,
                        null as faktor3,
                        null as text_faktor3,
                        null as faktor4,
                        null as text_faktor4,
                        null as faktor5,
                        null as text_faktor5,
                        null as faktor6,
                        null as text_faktor6,
                        null as faktor7,
                        null as text_faktor7,
                        null as faktor8,
                        null as text_faktor8,
                        null as faktor9,
                        null as text_faktor9,
                        null as created_at,
                        null as updated_at,
                        null as deleted_at,
                        null as status
                    ");
                    $evjab = $evjab->whereHas('dataAnjab', function($query) use ($kodeOpd){
                        return $query->where('_kode_jabatan', '!=', 'NULL')->where('unit_kerja', 'LIKE', $kodeOpd.'%')->where('jenis_jabatan', '=', '3');
                    })
                    ->unionAll($anjabkosong);
                }
                $data['unit_kerja'] = $kodeOpd;

            } else {
                $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
                $data['unitKerja'] = $unitKerja;
                // $unitk= MUnitkerja::get();
                // $dataOlahunitK=[];
                // foreach($unitk as $untk){
                //     $kode=substr($untk->kodeunit, 0,2);
                //     // echo $kode
                //     if(substr($untk->kodeunit, 0, 2)=="01"){
                //         if (strlen($untk->kodeunit)==6) {
                //             $dataOlahunitK[]=[
                //                 "kodeunit"=>$untk->kodeunit,
                //                 "unitkerja"=>$untk->unitkerja
                //             ];
                //         }
                //     }else{
                //         if (strlen($untk->kodeunit)==2) {
                //             $kode=substr($untk->kodeunit, 0,2);
                //             $dataOlahunitK[]=[
                //                 "kodeunit"=>$untk->kodeunit,
                //                 "unitkerja"=>$untk->unitkerja
                //             ];
                //         }
                //     }
                // }
                // $data['unitKerja']=$dataOlahunitK;
                $unitKerja        = $request->unit_kerja;
                $jenis_jabatan    = 3;
                // return $jenis_jabatan;
                if($unitKerja){
                    $anjab = $anjab->where('unit_kerja', 'LIKE',  $unitKerja.'%')->Where('jenis_jabatan', '3')->get();
                    // dd($anjab);
                    $anjabkosong = DB::table("trx_anjab as a")
                    ->whereNotIn("a.id", function($query) {
                        $query->select("trx_anjab_id")->from("trx_anjab_evajab_fungsional");
                    })
                    ->where("a.unit_kerja", "LIKE",  $unitKerja.'%')
                    ->where("a.jenis_jabatan", 3)
                    ->where("deleted_at", null)
                    ->selectRaw("
                        null as id,
                        a.id as trx_anjab_id,
                        null as faktor1,
                        null as text_faktor1,
                        null as faktor2,
                        null as text_faktor2,
                        null as faktor3,
                        null as text_faktor3,
                        null as faktor4,
                        null as text_faktor4,
                        null as faktor5,
                        null as text_faktor5,
                        null as faktor6,
                        null as text_faktor6,
                        null as faktor7,
                        null as text_faktor7,
                        null as faktor8,
                        null as text_faktor8,
                        null as faktor9,
                        null as text_faktor9,
                        null as created_at,
                        null as updated_at,
                        null as deleted_at,
                        null as status
                    ");

                    $evjab = $evjab->whereHas('dataAnjab', function($query) use ( $unitKerja){
                        return $query->where('_kode_jabatan', '!=', 'NULL')->where('unit_kerja', 'LIKE',  $unitKerja.'%')->where('jenis_jabatan', '=', '3');
                    })
                    ->unionAll($anjabkosong);
                }
                // return $evjab;
                // if($jenis_jabatan){
                //     $evjab = $evjab->whereHas('dataAnjab', function($query) use ($jenis_jabatan) {
                //         return $query->where('jenis_jabatan', $jenis_jabatan);
                //     });
                //     // $evjab = $evjab->with(['jabatan' => function($query) use ($jenis_jabatan) {
                //     //     $query->where('jenis_jabatan', $jenis_jabatan);
                //     // }]);

                //     //   $evjab = $evjab->with(['dataAnjab' => function($query) use ($jenis_jabatan) {
                //     //     $query->with('jabatan')->where('jenis_jabatan', $jenis_jabatan);
                //     // }]);

                // //    return $evjab = $evjab->where('jenis_jabatan', 'LIKE', $jenis_jabatan.'%')->get();
                // }
                // return $evjab;
                $data['unit_kerja'] = $unitKerja; 
            }

            $data['evjab'] = $evjab->get()->sortByDesc('kelas_jabatan');
            // dd( $data['evjab']);
            // dd($data['evjab'][0]->dataAnjab);
            // $data['anjab'] = $anjab;
            // return $data;

            return view('evjab.browse-fungsional', $data);
        }

    }

    function pilihAnjab(Request $request){
        $data['section'] = "evajab";

        $data['page'] = "Evaluasi Jabatan";
        $data['pages'] = "evajab";

        return redirect()->route('evajab.detail', $request->jabatan);
    }

    function detail(Request $request, $id){

        
        // return $request;
        $data['section'] = "evajab";
        $data['page_section'] = "evajab-struktural";
        $data['page'] = "Evaluasi Jabatan";
        $data['pages'] = "evajab";
        $anjab = TrxAnjab::where('id', $id)->first();
        $data['unit_kerja'] = $request->unit_kerja;
        // return $data;

        if($anjab->jenis_jabatan == 1){
            $data['page_section'] = "evajab-struktural";
            $faktorEvajab = TrxAnjabEvajabStruktural::where('trx_anjab_id', $id)->first();
            $data['faktorEvajab'] = $faktorEvajab;

            $data['faktor1'] = MFaktor::where('id', 1)->first();
            $data['faktor2'] = MFaktor::where('id', 2)->first();
            $data['faktor3'] = MFaktor::where('id', 3)->first();
            $data['faktor4A'] = MFaktor::where('id', 4)->first();
            $data['faktor4B'] = MFaktor::where('id', 5)->first();
            $data['faktor5'] = MFaktor::where('id', 6)->first();
            $data['faktor6'] = MFaktor::where('id', 7)->first();

            $data['level_faktor1'] = MLevelFaktor::where('m_faktor_id', 1)->get();
            $data['level_faktor2'] = MLevelFaktor::where('m_faktor_id', 2)->get();
            $data['level_faktor3'] = MLevelFaktor::where('m_faktor_id', 3)->get();
            $data['level_faktor4A'] = MLevelFaktor::where('m_faktor_id', 4)->get();
            $data['level_faktor4B'] = MLevelFaktor::where('m_faktor_id', 5)->get();
            $data['level_faktor5'] = MLevelFaktor::where('m_faktor_id', 6)->get();
            $data['level_faktor6'] = MLevelFaktor::where('m_faktor_id', 7)->get();

            $data['anjab'] = $anjab;
            // return $data;
            

            return view('evjab.struktural.detail', $data);
        }elseif($anjab->jenis_jabatan == 2 || $anjab->jenis_jabatan == 3){
            if ($anjab->jenis_jabatan == 2) {
                $data['page_section'] = "evajab-fungsional";
                # code...
            } else {
                $data['page_section'] = "evajab-pelaksana";
            }
            $mFaktor = MFaktor::where('jenis', 'FUNGSIONAL')->orderBy('nama', 'ASC')->get();

            $faktorEvajab = TrxAnjabEvajabFungsional::where('trx_anjab_id', $id)->first();
            // return $faktorEvajab;

            $faktor1 = MFaktor::where('id', 8)->first();
            $faktor2 = MFaktor::where('id', 9)->first();
            $faktor3 = MFaktor::where('id', 10)->first();
            $faktor4 = MFaktor::where('id', 11)->first();
            $faktor5 = MFaktor::where('id', 12)->first();
            $faktor6 = MFaktor::where('id', 13)->first();
            $faktor7 = MFaktor::where('id', 14)->first();
            $faktor8 = MFaktor::where('id', 15)->first();
            $faktor9 = MFaktor::where('id', 16)->first();

            $levelFaktor1 = MLevelFaktor::where('m_faktor_id', 8)->get();
            $levelFaktor2 = MLevelFaktor::where('m_faktor_id', 9)->get();
            $levelFaktor3 = MLevelFaktor::where('m_faktor_id', 10)->get();
            $levelFaktor4 = MLevelFaktor::where('m_faktor_id', 11)->get();
            $levelFaktor5 = MLevelFaktor::where('m_faktor_id', 12)->get();
            $levelFaktor6 = MLevelFaktor::where('m_faktor_id', 13)->get();
            $levelFaktor7 = MLevelFaktor::where('m_faktor_id', 14)->get();
            $levelFaktor8 = MLevelFaktor::where('m_faktor_id', 15)->get();
            $levelFaktor9 = MLevelFaktor::where('m_faktor_id', 16)->get();

            $data = [
                'faktor1' => $faktor1,
                'faktor2' => $faktor2,
                'faktor3' => $faktor3,
                'faktor4' => $faktor4,
                'faktor5' => $faktor5,
                'faktor6' => $faktor6,
                'faktor7' => $faktor7,
                'faktor8' => $faktor8,
                'faktor9' => $faktor9,
                'levelFaktor1' => $levelFaktor1,
                'levelFaktor2' => $levelFaktor2,
                'levelFaktor3' => $levelFaktor3,
                'levelFaktor4' => $levelFaktor4,
                'levelFaktor5' => $levelFaktor5,
                'levelFaktor6' => $levelFaktor6,
                'levelFaktor7' => $levelFaktor7,
                'levelFaktor8' => $levelFaktor8,
                'levelFaktor9' => $levelFaktor9,
                'anjab' => $anjab,
                'faktorEvajab'  => $faktorEvajab,
                'mFaktor'       => $mFaktor
                
            ];
            $data['unit_kerja'] = $request->unit_kerja;
            // return $data;
            // return $data['unit_kerja'] = $request->unit_kerja;

            return view('evjab.fungsional.detail', $data);
        }



    }

    function getForm(Request $request){
        if($request->aksi == 'add-data'){
            $tugasJabatan = TrxAnjabUraianJabatan::where('trx_anjab_id', $request->id)->get();

            $data['id'] = $request->id;
            $data['tugasJabatan'] = $tugasJabatan;
            return view('evjab.form-create', $data);
        }
    }

    function save(Request $request){
        $anjab = TrxAnjab::where('id', $request->id)->first();
        // return $request->level_faktor1;
        if($anjab->jenis_jabatan == 1){
            // return $request;
            
            $evajab = new TrxAnjabEvajabStruktural;
            $evajab->faktor1 = $request->level_faktor1;
            $evajab->text_faktor1 = $request->text_faktor1;
            $evajab->faktor2 = $request->level_faktor2;
            $evajab->text_faktor2 = $request->text_faktor2;
            $evajab->faktor3 = $request->level_faktor3;
            $evajab->text_faktor3 = $request->text_faktor3;
            $evajab->faktor4A = $request->level_faktor4A;
            $evajab->text_faktor4A = $request->text_faktor4;
            $evajab->faktor4B = $request->level_faktor4B;
            $evajab->text_faktor4B = $request->text_faktor5;
            $evajab->faktor5 = $request->level_faktor5;
            $evajab->text_faktor5 = $request->text_faktor6;
            $evajab->faktor6 = $request->level_faktor6;
            $evajab->text_faktor6 = $request->text_faktor7;
            $evajab->trx_anjab_id = $request->id;
            $evajab->created_at = date('Y-m-d H:i:s');
            $saved = $evajab->save();
            if($saved){
                return redirect()->route('evajab.detail',['id' => $request->id,'unit_kerja' => $anjab->unit_kerja])->with('notify', 'Faktor Evajab berhasil ditambahkan !');
            }
            // else{
            //     return redirect()->back()->with('alert', 'Faktor Evajab gagal ditambahkan');
            // }
        }elseif($anjab->jenis_jabatan == 2 || $anjab->jenis_jabatan == 3){
            // dd('aaaa');
            $evajab = new TrxAnjabEvajabFungsional;
            $evajab->faktor1 = $request->level_faktor1;
            $evajab->text_faktor1 = $request->text_faktor1;
            $evajab->faktor2 = $request->level_faktor2;
            $evajab->text_faktor2 = $request->text_faktor2;
            $evajab->faktor3 = $request->level_faktor3;
            $evajab->text_faktor3 = $request->text_faktor3;
            $evajab->faktor4 = $request->level_faktor4;
            $evajab->text_faktor4 = $request->text_faktor4;
            $evajab->faktor5 = $request->level_faktor5;
            $evajab->text_faktor5 = $request->text_faktor5;
            $evajab->faktor6 = $request->level_faktor6;
            $evajab->text_faktor6 = $request->text_faktor6;
            $evajab->faktor7 = $request->level_faktor7;
            $evajab->text_faktor7 = $request->text_faktor7;
            $evajab->faktor8 = $request->level_faktor8;
            $evajab->text_faktor8 = $request->text_faktor8;
            $evajab->faktor9 = $request->level_faktor9;
            $evajab->text_faktor9 = $request->text_faktor9;

            $evajab->trx_anjab_id = $request->id;
            $evajab->created_at = date('Y-m-d H:i:s');
            $saved = $evajab->save();
            if($saved){
                return redirect()->route('evajab.detail',['id' => $request->id,'unit_kerja' => $anjab->unit_kerja])->with('notify', 'Faktor Evajab berhasil ditambahkan !');
            }
            // else{
            //     return redirect()->back()->with('alert', 'Faktor Evajab gagal ditambahkan');
            // }
        }


    }

    function update(Request $request){
        // return $request;
        $anjab = TrxAnjab::where('id', $request->id)->first();
        if($anjab->jenis_jabatan == 1){
            $evajab = TrxAnjabEvajabStruktural::find($request->id_evajab);
            $evajab->faktor1 = $request->level_faktor1;
            $evajab->text_faktor1 = $request->text_faktor1;
            $evajab->faktor2 = $request->level_faktor2;
            $evajab->text_faktor2 = $request->text_faktor2;
            $evajab->faktor3 = $request->level_faktor3;
            $evajab->text_faktor3 = $request->text_faktor3;
            $evajab->faktor4A = $request->level_faktor4A;
            $evajab->text_faktor4A = $request->text_faktor4;
            $evajab->faktor4B = $request->level_faktor4B;
            $evajab->text_faktor4B = $request->text_faktor5;
            $evajab->faktor5 = $request->level_faktor5;
            $evajab->text_faktor5 = $request->text_faktor6;
            $evajab->faktor6 = $request->level_faktor6;
            $evajab->text_faktor6 = $request->text_faktor7;
            $saved = $evajab->update();
            if($saved){
                return redirect()->route('evajab.detail',['id' => $request->id,'unit_kerja' => $anjab->unit_kerja])->with('notify', 'Faktor Evajab berhasil di update !');
            }
            // else{
            //     return redirect()->back()->with('alert', 'Faktor Evajab gagal ditambahkan');
            // }
        }elseif($anjab->jenis_jabatan == 2 || $anjab->jenis_jabatan == 3){
            // return $request;
            $evajab = TrxAnjabEvajabFungsional::find($request->id_evajab);

            $evajab->faktor1 = $request->level_faktor1;
            $evajab->text_faktor1 = $request->text_faktor1;
            $evajab->faktor2 = $request->level_faktor2;
            $evajab->text_faktor2 = $request->text_faktor2;
            $evajab->faktor3 = $request->level_faktor3;
            $evajab->text_faktor3 = $request->text_faktor3;
            $evajab->faktor4 = $request->level_faktor4;
            $evajab->text_faktor4 = $request->text_faktor4;
            $evajab->faktor5 = $request->level_faktor5;
            $evajab->text_faktor5 = $request->text_faktor5;
            $evajab->faktor6 = $request->level_faktor6;
            $evajab->text_faktor6 = $request->text_faktor6;
            $evajab->faktor7 = $request->level_faktor7;
            $evajab->text_faktor7 = $request->text_faktor7;
            $evajab->faktor8 = $request->level_faktor8;
            $evajab->text_faktor8 = $request->text_faktor8;
            $evajab->faktor9 = $request->level_faktor9;
            $evajab->text_faktor9 = $request->text_faktor9;
            // return $evajab;
            $saved = $evajab->update();
            if($saved){
                return redirect()->route('evajab.detail',['id' => $request->id,'unit_kerja' => $anjab->unit_kerja])->with('notify', 'Faktor Evajab berhasil di update !');
            }
            // else{
            //     return redirect()->back()->with('alert', 'Faktor Evajab gagal ditambahkan');
            // }
        }

    }

    public function getJabatan(Request $request){

        if (isset($request->q)) {

            $jabatan = M_jabatan::get();
            if ($jabatan) {

                foreach ($jabatan as $key => $value) {
                    $row['id'] = $value['id'];
                    $row['full_name'] = $value['nama'];
                    $row['jabatan'] = $value['nama'];

                    $output[] = $row;

                }
                $data['items'] = $output;
                echo json_encode($data);
            }else{
                exit();
            }


        }
    }

    public function getLevelFaktor(Request $request){
        if (isset($request->id)) {
            $levelFaktor = MLevelFaktor::where('id',$request->id)->first();
            $faktor = MFaktor::where('id',$request->faktor_id)->first();

            $html ='<div class="form-group row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                        <div class="rounded bg-gray-100 p-5">
                            <b>TINGKAT FAKTOR '.explode('Faktor ', $faktor->nama)[1].' - '.explode('Level ', $levelFaktor->nama)[1].'</b> &#8212; NILAI '.$levelFaktor->nilai.'<br><br>
                            <b>PENJELASAN</b> : <br><br>'.$levelFaktor->uraian.'
                            <input type="hidden" name="text_faktor'.$request->faktor_id.'" value="'.$levelFaktor->uraian.'" >
                        </div>
                    </div>
                </div>';
            echo $html;

        }
    }

    function viewPrint(Request $request){
        $data['jabatan'] = $request->jabatan;
        $data['unit_kerja'] = $request->unit_kerja;

        return view('evajab.form-iframe',$data);
    }

    function printEvajab($jabatan, $unitkerja, $anjab_id){
        // return $anjab_id;
        $data['anjab'] = TrxAnjab::where('m_jabatan_id',$jabatan)->where('unit_kerja', 'LIKE', $unitkerja. '%')->where('id', $anjab_id)->first();
        // return $data['anjab'];
        $data['anjab_uraian'] = TrxAnjabUraianJabatan::with('dataUraianJabatan')->where('trx_anjab_id', $data['anjab']->id)->get(); 
        // $data['anjab_abk'] = TrxAnjabAbk::where('trx_anjab_id',$data['anjab']->id)->get();
        // $data['anjab_bahan_kerja'] = TrxAnjabBahanKerja::where('trx_anjab_id',$data['anjab']->id)->get();
        $data['anjab_hasil_kerja'] = TrxAnjabHasilKerja::where('trx_anjab_id',$data['anjab']->id)->get();
        // $data['anjab_korelasi_jabatan'] = TrxAnjabKorelasiJabatan::where('trx_anjab_id',$data['anjab']->id)->get();
        // $data['anjab_lingkungan_kerja'] = TrxAnjabLingkunganKerja::where('trx_anjab_id',$data['anjab']->id)->get();
        // $data['anjab_perangkat'] = TrxAnjabPerangkat::where('trx_anjab_id',$data['anjab']->id)->get();
        // $data['anjab_prestasi_kerja'] = TrxAnjabPrestasiKerja::where('trx_anjab_id',$data['anjab']->id)->get();
        // $data['anjab_resiko_bahaya'] = TrxAnjabResikoBahaya::where('trx_anjab_id',$data['anjab']->id)->get();
        $data['anjab_tanggung_jawab'] = TrxAnjabTanggungJawab::where('trx_anjab_id',$data['anjab']->id)->get();
        // $data['anjab_wewenang'] = TrxAnjabWewenang::where('trx_anjab_id',$data['anjab']->id)->get();
        if($data['anjab']->jenis_jabatan == 1){
            $data['faktor_evajab'] = TrxAnjabEvajabStruktural::where('trx_anjab_id',$data['anjab']->id)->first();
        } else {
            // echo 'oke';
            $data['faktor_evajab'] = TrxAnjabEvajabFungsional::where('trx_anjab_id',$data['anjab']->id)->first();
            // return $data['anjab']->id;
        }
        // return $data['anjab']->jenis_jabatan;
        $pdf = PDF::loadView('evjab.form-view-print', $data);
        // die;
        $customPaper = array(0,0,595,935);
        $pdf->setPaper($customPaper);

        return $pdf->stream('Lembar Evajab');
    }

    public function getUnitKerja(Request $request){
        // var_dump($request->id);exit();
        if (isset($request->id)) {
            $jabatan = TrxAnjab::where('m_jabatan_id',$request->id)->get();


            echo '<option value="" >-- PILIH UNIT KERJA --</option>';
            foreach($jabatan as $key => $value) {
                $unit_kerja = MUnitKerja::where('kodeunit', 'like',$value['unit_kerja_eselon2'])->first();
                echo '<option value="'.$unit_kerja->kodeunit.'">'.$unit_kerja->unitkerja.'</option>';
            }
        }
    }

    public function verifikasi($id){
        // dd($id);
        $evjab = TrxAnjabEvajabStruktural::findOrFail($id);
        $evjab->status = "ya";
        $evjab->created_at = date('Y-m-d H:i:s');
        $evjab->save();
        return redirect()->back()->with('notify', 'Evajab Berhasil Diverifikasi');
        }

        function batalverifikasi($id){
            // return $request->id;
            $evajab = TrxAnjabEvajabStruktural::findOrFail($id);
            $evajab->status = "tidak";
            $evajab->created_at = date('Y-m-d H:i:s');
            // return $evajab;
            $evajab->update();
            return redirect()->back()->with('notify', 'Evajab Batal Diverifikasi');
        }

}
