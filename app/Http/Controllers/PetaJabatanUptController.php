<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PetaJabatanUptController extends Controller
{
    public function browse(Request $request)
    {
        // return $request;
        // Ambil kodeopd dari request
        // Ambil kode_opd dari user yang login
        $userKodeOpd = \MojokertokabUser::getUser()->kode_opd;

        // Cek jika kode_opd user 4 digit dan diawali "28", gunakan userKodeOpd. Jika tidak, ambil dari request.
        $kodeopd = (strlen($userKodeOpd) === 4 && str_starts_with($userKodeOpd, '28')) 
            ? $userKodeOpd 
            : $request->input('kodeopd');

         // 🚫 Jika kodeopd adalah '28', hentikan di sini (tampilkan pesan atau kosongkan view)
        if ($kodeopd === '28') {
            return redirect()->back(); // Kembali ke halaman sebelumnya tanpa aksi
        }
        // Ambil kodeopd dari user yang sedang login
        // Ambil data unit kerja dari tabel m_unit_kerja_2
        $unitKerja = DB::table('m_unit_kerja_2')
        ->where('unitkerja', 'like', '%Puskesmas%') // Filter unit kerja yang mengandung "Puskesmas"
        ->where('kodeunit', 'like', '28__')         // '28' diikuti 2 karakter apa saja (total 4 karakter)
        ->select('kodeunit', 'unitkerja')
        ->orderBy('unitkerja')
        ->get();

    
        // Ambil unit kerja dari tabel m_unit_kerja_2 berdasarkan kode_opd
        $kepala_puskesmas = DB::table('m_unit_kerja_2')
            ->where('kodeunit', $kodeopd)
            ->select('unitkerja') // Ambil kolom unitkerja
            ->first(); // Ambil satu baris data
        // return $kepala_puskesmas;
        // return $kepala_puskesmas;
        // Jika unitkerja ditemukan, gunakan sebagai nama kepala puskesmas, jika tidak, gunakan default
        $kepala_puskesmas = [
            'id' => 1,
            'jabatan' => 'Kepala ' . optional($kepala_puskesmas)->unitkerja ?? 'Puskesmas'
        ];
    
        // Ambil data jabatan dari trx_anjab dengan join ke tabel jabatan yang sesuai
        // $jabatan = DB::table('trx_anjab')
        //     ->leftJoin('m_jabatan_fungsional', function ($join) {
        //         $join->on('trx_anjab.m_jabatan_id', '=', 'm_jabatan_fungsional.id')
        //              ->where('trx_anjab.jenis_jabatan', 2);
        //     })
        //     ->leftJoin('m_jabatan_pelaksana', function ($join) {
        //         $join->on('trx_anjab.m_jabatan_id', '=', 'm_jabatan_pelaksana.id')
        //              ->where('trx_anjab.jenis_jabatan', 3);
        //     })
        //     ->whereIn('trx_anjab.jenis_jabatan', [2, 3])
        //     ->where('trx_anjab.unit_kerja', $kodeopd)
        //     ->whereNull('trx_anjab.deleted_at')
        //     ->select(
        //         'trx_anjab.id',
        //         'trx_anjab.jenis_jabatan', 
        //         DB::raw("COALESCE(m_jabatan_fungsional.jabatan, m_jabatan_pelaksana.jabatan) as nama_jabatan")
        //     )
        //     ->get()
        //     ->groupBy('jenis_jabatan'); // 👈 Tambahkan ini

        $jabatan = DB::table('trx_anjab')
            ->leftJoin('m_jabatan_fungsional', function ($join) {
                $join->on('trx_anjab.m_jabatan_id', '=', 'm_jabatan_fungsional.id')
                    ->where('trx_anjab.jenis_jabatan', 2);
            })
            ->leftJoin('m_jabatan_pelaksana', function ($join) {
                $join->on('trx_anjab.m_jabatan_id', '=', 'm_jabatan_pelaksana.id')
                    ->where('trx_anjab.jenis_jabatan', 3);
            })
            ->select(
                'trx_anjab.id',
                'trx_anjab.jenis_jabatan',
                DB::raw("COALESCE(m_jabatan_fungsional.jabatan, m_jabatan_pelaksana.jabatan) as nama_jabatan"),
                'trx_anjab.m_jabatan_fungsional_jenjang as jenjang', 'trx_anjab.unit_kerja as unitKerja' // 🔑 Ambil langsung dari trx_anjab
            )
            ->whereIn('trx_anjab.jenis_jabatan', [2, 3])
            ->where('trx_anjab.unit_kerja', $kodeopd)
            ->whereNull('trx_anjab.deleted_at')
            ->get()
            ->groupBy('jenis_jabatan');
        // ✅ Ambil unitKerja tanpa error
        $unitKerja = $jabatan->flatten()->first()->unitKerja ?? null;
        
        
    
        return view('petajabatanupt.browse', compact('kepala_puskesmas', 'jabatan', 'unitKerja'));
    }
    
}
