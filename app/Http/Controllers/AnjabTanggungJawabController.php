<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabTanggungJawab;
use DB;

class AnjabTanggungJawabController extends Controller
{
    function browse($id){

    	$data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "Detail Anjab";
    	$data['pages'] = "detail_tanggung_jawab";
    	$data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['tanggung_jawab'] = TrxAnjabTanggungJawab::where('trx_anjab_id', $id)->orderBy('id', 'asc')->paginate(10);
    	return view('anjab.anjab-detail-tanggung-jawab', $data);

    }

    function getForm(Request $request){
    	if($request->aksi == 'add-data'){
            $data['id'] = $request->id;
    		return view('anjab.tanggung-jawab.form-create-anjab-tanggung-jawab', $data);
    	}elseif ($request->aksi == 'edit-data') {
            $data['tanggung_jawab'] = TrxAnjabTanggungJawab::findOrFail($request->id);
    		return view('anjab.tanggung-jawab.form-edit-anjab-tanggung-jawab', $data);
    	}
    }

     function save(Request $request){
        if($request->aksi == 'create'){
            $tanggung_jawab = new TrxAnjabTanggungJawab;

            $tanggung_jawab->tanggung_jawab = $request->tanggung_jawab;
            $tanggung_jawab->trx_anjab_id = $request->id;
            $tanggung_jawab->created_at = date('Y-m-d H:i:s');
            
            $tanggung_jawab->save();
            return redirect()->route('anjab.browse-tanggung-jawab',$request->id)->with('notify', 'Tanggung Jawab berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {
            $tanggung_jawab = TrxAnjabTanggungJawab::findOrFail($request->id);

            $tanggung_jawab->tanggung_jawab = $request->tanggung_jawab;

            $tanggung_jawab->update();
            return redirect()->route('anjab.browse-tanggung-jawab',$request->id_anjab)->with('notify', 'Tanggung Jawab berhasil diperbarui');
        }
    }

    function deleteTanggungJawab(Request $request){
        $tanggung_jawab = TrxAnjabTanggungJawab::findOrFail($request->id);
        $tanggung_jawab->delete();
        return redirect()->back()->with('notify', 'Tanggung Jawab berhasil dihapus');
    }

   
}
