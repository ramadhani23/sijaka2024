<?php

namespace App\Http\Controllers;

use App\Models\MJabatan;
use Illuminate\Http\Request;

use App\Models\MJabatanFungsional;
use App\Models\MJabatanFungsionalJenjang;
use App\Models\MJabatanFungsionalSyarat;
use App\Repositories\JabatanRepository;

class JabatanFungsionalController extends Controller
{
    function browse(Request $request, JabatanRepository $jabatanRepo){
        $data['section'] = "jabatan-fungsional";
        $data['page_section'] = "jabatan-fungsional";
        $data['page'] = "Data Jabatan Fungsional";

        $jabatan = $jabatanRepo->getJabatanFungsional();

        $data['jabatan'] = $jabatan;
        return view('master.jabatan-fungsional.browse', $data);
    }

    function getForm(Request $request, JabatanRepository $jabatanRepo){
        if($request->aksi == 'create-jabatan'){


            $rumpun = $jabatanRepo->getRumpunJF();
            $instansiPembina = $jabatanRepo->getInstansiPembinaJF();

            $data = [
                'rumpun' => $rumpun,
                'instansiPembina' => $instansiPembina,
            ];
            return view('master.jabatan-fungsional.form-create', $data);
        }elseif($request->aksi == 'edit-jabatan'){
            $jabatan = MJabatanFungsional::findOrFail($request->id);

            $rumpun = $jabatanRepo->getRumpunJF();
            $instansiPembina = $jabatanRepo->getInstansiPembinaJF();

            $data = [
                'rumpun' => $rumpun,
                'instansiPembina' => $instansiPembina,
                'jabatan' => $jabatan
            ];
            return view('master.jabatan-fungsional.form-edit', $data);
        }elseif($request->aksi == 'create-jenjang'){

            $jabatan = MJabatanFungsional::findOrFail($request->id);
            $data = [
                'jabatan' => $jabatan
            ];
            return view('master.jabatan-fungsional.form-create-jenjang', $data);
        }elseif($request->aksi == 'edit-jenjang'){
            $jenjang = MJabatanFungsionalJenjang::findOrFail($request->id);

            $data = [
                'jenjang' => $jenjang
            ];
            return view('master.jabatan-fungsional.form-edit-jenjang', $data);
        }elseif($request->aksi == 'create-syarat'){
            $jabatan = MJabatanFungsional::findOrFail($request->id);
            $data = [
                'jabatan' => $jabatan
            ];
            return view('master.jabatan-fungsional.form-create-syarat', $data);
        }elseif($request->aksi == 'edit-syarat'){
            $syarat = MJabatanFungsionalSyarat::findOrFail($request->id);

            $data = [
                'syarat' => $syarat
            ];
            return view('master.jabatan-fungsional.form-edit-syarat', $data);
        }
    }

        function save(Request $request, JabatanRepository $jabatanRepo){
        // return $request;
        if($request->aksi == 'add-jabatan'){
            $nama               = $request->nama;
            $rumpun             = $request->rumpun;
            $instansiPembina    = $request->instansi_pembina;
            $uraianTugas        = $request->tugas;
            $kategori           = $request->kategori;

            $lastUrutan = MJabatanFungsional::withTrashed()->where('_kode', 'LIKE', '13.'.$instansiPembina.'.'.$rumpun.'.%')->orderBy('urutan', 'DESC')->first();
            // return $lastUrutan;
            if(isset($lastUrutan->urutan)){
                $urutan = intVal($lastUrutan->urutan) + 1;
            }else{
                $urutan = 1;
            }
            // return $urutan;

            $jabatanRepo->saveJabatanFungsional($nama, $uraianTugas, $instansiPembina, $rumpun, $urutan, $kategori);
            // return $cetak;
            return redirect()->back()->with('notify', 'Jabatan berhasil ditambahkan');
        }elseif ($request->aksi == 'update-jabatan') {
            $jabatan = MJabatanFungsional::findOrFail($request->id);

            $jabatan->jabatan                                = $request->nama;
            $jabatan->m_jabatan_fungsional_rumpun_id              = $request->rumpun;
            $jabatan->m_jabatan_fungsional_instansi_pembina_id    = $request->instansiPembina;
            $jabatan->tugas                                       = $request->tugas;
            $jabatan->kategori                                    = $request->kategori;

            $jabatan->update();

            return redirect()->back()->with('notify', 'Jabatan Fungsional berhasil diperbarui');
        }elseif($request->aksi == 'add-jenjang'){
            $jabatan = new MJabatanFungsionalJenjang();

            $jabatan->id_m_jabatan_fungsional   = $request->id_m_jabatan_fungsional;
            $jabatan->kode_jabatan              = $request->kode_jabatan;
            $jabatan->jenjang                   = $request->jenjang;
            $jabatan->golru                     = $request->golru;
            $jabatan->angka_kredit              = $request->angka_kredit;

            $jabatan->save();

            return redirect()->back()->with('notify', 'Jabatan berhasil ditambahkan');
        }elseif ($request->aksi == 'update-jenjang') {
            $jabatan = MJabatanFungsionalJenjang::findOrFail($request->id);

            $jabatan->id_m_jabatan_fungsional   = $request->id_m_jabatan_fungsional;
            $jabatan->kode_jabatan              = $request->kode_jabatan;
            $jabatan->jenjang                   = $request->jenjang;
            $jabatan->golru                     = $request->golru;
            $jabatan->angka_kredit              = $request->angka_kredit;

            $jabatan->update();

            return redirect()->back()->with('notify', 'Jabatan Fungsional berhasil diperbarui');
        }elseif($request->aksi == 'add-syarat'){
            $jabatan = new MJabatanFungsionalSyarat();

            $jabatan->id_m_jabatan_fungsional    = $request->id_m_jabatan_fungsional;
            $jabatan->kode_jabatan               = $request->kode_jabatan;
            $jabatan->pendidikan                 = $request->pendidikan;

            $jabatan->save();

            return redirect()->back()->with('notify', 'Jabatan berhasil ditambahkan');
        }elseif ($request->aksi == 'update-syarat') {
            $jabatan = MJabatanFungsionalSyarat::findOrFail($request->id);

            $jabatan->id_m_jabatan_fungsional    = $request->id_m_jabatan_fungsional;
            $jabatan->kode_jabatan               = $request->kode_jabatan;
            $jabatan->pendidikan                 = $request->pendidikan;

            $jabatan->update();

            return redirect()->back()->with('notify', 'Jabatan Fungsional berhasil diperbarui');
        }
    }

    public function detail(Request $request, $id)
    {
        $data['section']        = "jabatan-fungsional";
        $data['page_section']   = "jabatan-fungsional";
        $data['page1']          = "Data Jabatan Fungsional Jenjang";
        $data['page2']          = "Data Jabatan Fungsional Syarat";

        $data['fungsional']     = MJabatanFungsional::findOrFail($id);
        $data['jenjang']        = MJabatanFungsionalJenjang::orderBy('id', 'ASC')->where('id_m_jabatan_fungsional' , $data['fungsional']->id)->get();
        $data['syarat']         = MJabatanFungsionalSyarat::orderBy('id', 'ASC')->where('id_m_jabatan_fungsional' , $data['fungsional']->id)->get();

        return view('master.jabatan-fungsional.detail', $data);
    }

    function delete(Request $request){
        $jabatan = MJabatanFungsional::findOrFail($request->id);

        $jabatan->delete();

        return redirect()->back()->with('notify', 'Jabatan berhasil dihapus');
    }

    function deleteJenjang(Request $request){
        $jabatan = MJabatanFungsionalJenjang::findOrFail($request->id);

        $jabatan->delete();

        return redirect()->back()->with('notify', 'Jabatan berhasil dihapus');
    }

    function deleteSyarat(Request $request){
        $jabatan = MJabatanFungsionalSyarat::findOrFail($request->id);

        $jabatan->delete();

        return redirect()->back()->with('notify', 'Jabatan berhasil dihapus');
    }
}
