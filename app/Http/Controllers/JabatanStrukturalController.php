<?php

namespace App\Http\Controllers;

use App\Models\MJabatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JabatanStrukturalController extends Controller
{
    // function browse(Request $request){

    //     $data['section'] = "jabatan";
    //     $data['page_section'] = "jabatan";
    //     $data['page'] = "Data Jabatan Struktural";

    //     $jabatan = MJabatan::query();

    //     if($request->keyword){
    //         $jabatan = $jabatan->where('jabatan', 'LIKE', '%'.$request->keyword.'%');
    //     }

    //     $jabatan = $jabatan->orderBy('jabatan', 'ASC')->get();

    //     $data['jabatan'] = $jabatan;
    //     return view('master-jabatan.browse', $data);
    // }
    //     public function browse(Request $request)
    // {
    //     $data['section'] = "jabatan";
    //     $data['page_section'] = "jabatan";
    //     $data['page'] = "Data Jabatan Struktural";

    //     // Memulai query
    //     $jabatan = MJabatan::query();

    //     // Menambahkan filter keyword jika ada
    //     if ($request->keyword) {
    //         $jabatan = $jabatan->where('jabatan', 'LIKE', '%'.$request->keyword.'%');
    //     }

    //     // Menambahkan kondisi untuk menampilkan data dengan catatan 'double' (hanya satu kali) atau null
    //     $jabatan = $jabatan->where(function($query) {
    //         $query->whereRaw('catatan = "double"')
    //             ->orWhereNull('catatan');
    //     });

    //     // Mengurutkan data
    //     $jabatan = $jabatan->orderBy('jabatan', 'ASC')->get();

    //     // Mengisi data untuk dikirim ke view
    //     $data['jabatan'] = $jabatan;

    //     // Mengembalikan view dengan data yang sudah difilter
    //     return view('master-jabatan.browse', $data);
    // }

    public function browse(Request $request)
        {
            $data['section'] = "jabatan";
            $data['page_section'] = "jabatan";
            $data['page'] = "Data Jabatan Struktural";

            $jabatan = MJabatan::query();

        // Menambahkan filter keyword jika ada
        if ($request->keyword) {
            $jabatan->where('jabatan', 'LIKE', '%' . $request->keyword . '%');
        }

        // Subquery untuk menentukan apakah ada relasi
        $subQuery = DB::table('trx_anjab')
            ->select(DB::raw('1'))
            ->whereColumn('trx_anjab.m_jabatan_id', 'm_jabatan_struktural.id')
            ->limit(1);

        // Query utama dengan logika untuk memilih data yang lebih relevan
        $jabatan = $jabatan
            ->select('m_jabatan_struktural.id',
                    'm_jabatan_struktural.jabatan',
                    'm_jabatan_struktural._kode',
                    'm_jabatan_struktural.kualifikasi_pendidikan',
                    'm_jabatan_struktural.tugas_jabatan',
                    DB::raw('MAX(EXISTS (' . $subQuery->toSql() . ')) as has_relation'),
                    DB::raw('MAX(catatan IS NULL) as is_null')) 
            ->leftJoin('trx_anjab', 'trx_anjab.m_jabatan_id', '=', 'm_jabatan_struktural.id')
            ->groupBy('m_jabatan_struktural.id', 'm_jabatan_struktural.jabatan', 'm_jabatan_struktural._kode', 'm_jabatan_struktural.kualifikasi_pendidikan', 'm_jabatan_struktural.tugas_jabatan')
            ->orderBy('has_relation', 'DESC')
            ->orderBy('is_null', 'DESC')
            ->get()
            ->unique('jabatan'); // memastikan data yang sama hanya muncul satu kali

        // Mengisi data untuk dikirim ke view
        $data['jabatan'] = $jabatan;
        // return $data;

        // Mengembalikan view dengan data yang sudah difilter
        return view('master-jabatan.browse', $data);




        }

    function getForm(Request $request){
        // return $request;
        if($request->aksi == 'create-jabatan'){
            return view('master-jabatan.form-create');
        }elseif($request->aksi == 'edit-jabatan'){
            $jabatan = MJabatan::findOrFail($request->id);

            $data = [
                'jabatan' => $jabatan
            ];
            // return $data;
            return view('master-jabatan.form-edit', $data);
        }
    }

    function save(Request $request){
        // return $request;
        if($request->aksi == 'add-jabatan'){
            $jabatan = new MJabatan;

            $jabatan->jabatan = $request->nama;
            $jabatan->kualifikasi_pendidikan = $request->kualifikasi_pendidikan;
            $jabatan->tugas_jabatan = $request->tugas_jabatan;
            $jabatan->diklat_penjenjangan = $request->diklat_penjenjangan;
            $jabatan->dasar_hukum = $request->dasar_hukum;

            $jabatan->save();

            return redirect()->back()->with('notify', 'Jabatan berhasil ditambahkan');
        }elseif ($request->aksi == 'update-jabatan') {
            $jabatan = MJabatan::findOrFail($request->id);

            $jabatan->jabatan = $request->nama;
            $jabatan->kualifikasi_pendidikan = $request->kualifikasi_pendidikan;
            $jabatan->tugas_jabatan = $request->tugas_jabatan;
            $jabatan->diklat_penjenjangan = $request->diklat_penjenjangan;
            $jabatan->dasar_hukum = $request->dasar_hukum;

            $jabatan->update();

            return redirect()->back()->with('notify', 'Jabatan berhasil diperbarui');
        }
    }

    function delete(Request $request){
        $jabatan = MJabatan::findOrFail($request->id);
        // return $jabatan;
        if($jabatan->dataAnjab){
            return redirect()->back()->with('error', 'Jabatan tidak bisa dihapus, digunakan untuk Analisa Jabatan');
        }

        $jabatan->delete();

        return redirect()->back()->with('notify', 'Jabatan berhasil dihapus');
    }
}
