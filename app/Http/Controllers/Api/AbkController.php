<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\AbkRepository;

class AbkController extends Controller
{
    function getAbk(Request $request, AbkRepository $abkRepo){
        $kodeSipd = $request->opd_sipd;
        $kodeBkpp = $request->opd_bkpp;

        if($kodeBkpp){
            $abk = $abkRepo->getAbkByBkpp($kodeBkpp);    
        }

        return response()->json($abk);
    }
}
