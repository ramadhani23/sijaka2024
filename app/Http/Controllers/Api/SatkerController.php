<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MUnitKerja;
use App\Models\MKelembagaan;
use DB;

class SatkerController extends Controller
{
    
    public function getSatker(Request $request){
        
        $satker = MKelembagaan::where('kode_bkpp', '!=', NULL)->where('id', '!=', 1)->get();
        
        $data = [
            'status' => true,
            'code'  => 200,
            'result' => $satker
        ];

        return response()->json($data);
    }

    public function getDetailSatker(Request $request, $kode){
        
        $satker = MKelembagaan::where('kode_bkpp', $kode)->first();
       
        $data = [
            'status' => true,
            'code'  => 200,
            'result' => $satker
        ];

        return response()->json($data);
    }

    public function getDetailSatker2(Request $request){
        
        $satker = MKelembagaan::where('kode_bkpp', $request->satker_id)->first();
       
        $data = [
            'status' => true,
            'code'  => 200,
            'result' => $satker
        ];

        return response()->json($data);
    }
}
