<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserSecondary;
use App\Models\MUnitKerja;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Http;
use App\Repositories\UnitKerjaRepository;


class UserController extends Controller
{
    function browse(){
        $data['section'] = "master-user";
        $data['page_section'] = "master-user";
        $data['page'] = "Pengguna";

    	$user = User::all();
    	$data['users'] = $user;
    	return view('master-user.browse', $data);
    }

    function getForm(Request $request, UnitKerjaRepository $unitKerjaRepo){
    	// $unitKerja = MUnitKerja::all();
        // $response  = Http::get('http://api.simpeg.mojokertokab.go.id/api/satker');
        // $unitKerja = json_decode($response->body())->satker;
		$unitKerja = $unitKerjaRepo->getActiveUnitKerjaPd();

        // return $data['unitKerja'] = $unitKerja;
        $data['unitKerja'] = MUnitkerja::all();
		// return $data;
		// foreach ($unitKerja as $value) {
		// 	if ($value->kodeunit == 28) {
		// 		echo 'oke';
		// 	}
		// }
		// 
		// return $unitKerja;
		// $data['unitKerjadb'] = $unitKerjadb;
		// dd($data['unitKerja']);
		// return $unitKerja;
    	if($request->aksi == 'add-user'){
    		return view('master-user.form-create', $data);
    	}elseif ($request->aksi == 'edit-user') {
    		$user = User::with('dataOpd')->where('username', $request->id)->firstOrFail();
    		$data['user'] = $user;
    		return view('master-user.form-edit', $data);
    	}
    }

	public function getdb(UnitKerjaRepository $unitKerjaRepo)
	{
		//  $unitKerja = $unitKerjaRepo->getActiveUnitKerjaPd();
        return $response  = Http::withoutVerifying()->get('https://api-simpeg.mojokertokab.go.id/api/detail-pegawai-satker?satker_id=2822&access_token=K@bM0joKert0Y3s');

	}

    function getPegawaiFromSimpeg(Request $request){
        $response  = Http::withoutVerifying()->get('https://api-simpeg.mojokertokab.go.id/api/detail-pegawai?nip='.$request->username.'&access_token=K@bM0joKert0Y3s');
		// return $response;
        if($response->successful()){
            $pegawai = json_decode($response->body())->pegawai;
            return json_encode($pegawai);
        }
    }

    function getPegawaiSatkerFromSimpeg(Request $request){
        $response  = Http::withoutVerifying()->get('https://api-simpeg.mojokertokab.go.id/api/detail-pegawai-satker?satker_id='.$request->kode_opd.'&access_token=K@bM0joKert0Y3s');
		// return $response;



        if($response->successful()){

            $pegawai = json_decode($response->body())->pegawai;
			// return $pegawai;
			// dd($pegawai);
            foreach($pegawai as $key => $value) {
				if (stripos($request->kode_opd,'28') !== FALSE) {
						// echo '<option value="'.$value->nip_baru.'">'.$value->namalengkap.' ('.$value->nip_baru .')</option>';

					if (str_contains($value->satker_id, $request->kode_opd)) {
						echo '<option value="'.$value->nip_baru.'">'.$value->namalengkap.' ('.$value->nip_baru .')</option>';
					}
				} else {
					echo '<option value="'.$value->nip_baru.'">'.$value->namalengkap.' ('.$value->nip_baru .')</option>';
				}
            }
            // return json_encode($pegawai);
        }

    }

    // function save(Request $request){
	// 	// return $request;
    // 	if($request->aksi == 'add-user'){
    // 		$findUser = User::where('username', $request->username)->first();
    // 		if($findUser){
    // 			return redirect()->back()->with('error', 'Pegawai telah terdaftar');
    // 		}

    // 		$user = new User;

    // 		$user->username = $request->pegawai;
    // 		$user->option = $request->json;
    // 		$user->role = $request->role;
    // 		$user->kode_opd = $request->kode_opd;

    // 		$user->save();

    // 		return redirect()->back()->with('notify', 'Pengguna berhasil ditambahkan');

    // 	}elseif ($request->aksi == 'edit-user') {
    // 		$user = User::where('username', $request->username)->firstOrFail();
	// 		// return $user;
    // 		$user->kode_opd = $request->kode_opd;
    // 		$user->role = $request->role;
	// 		// return $user;
    // 		$user->update();

    // 		return redirect()->back()->with('notify', 'Pengguna berhasil diperbarui');
    // 	}
    // }
	function save(Request $request) {
		// Menggunakan transaksi untuk memastikan konsistensi data di kedua database
		DB::beginTransaction();
		try {
			if ($request->aksi == 'add-user') {
				$findUser = User::where('username', $request->username)->first();
				if ($findUser) {
					return redirect()->back()->with('error', 'Pegawai telah terdaftar');
				}
	
				// Simpan ke database pertama
				$user = new User;
				$user->username = $request->pegawai;
				$user->option = $request->json;
				$user->role = $request->role;
				$user->kode_opd = $request->kode_opd;
				$user->save();
	
				// Simpan ke database kedua
				$secondaryUser = new UserSecondary;
				$secondaryUser->username = $request->pegawai;
				$secondaryUser->option = $request->json;
				$secondaryUser->role = $request->role;
				$secondaryUser->kode_opd = $request->kode_opd;
				$secondaryUser->save();
	
				DB::commit(); // Commit transaksi jika semua berhasil
				return redirect()->back()->with('notify', 'Pengguna berhasil ditambahkan');
	
			} elseif ($request->aksi == 'edit-user') {
				// Update di database pertama
				$user = User::where('username', $request->username)->firstOrFail();
				$user->kode_opd = $request->kode_opd;
				$user->role = $request->role;
				$user->update();
	
				// Update di database kedua
				$secondaryUser = UserSecondary::where('username', $request->username)->firstOrFail();
				$secondaryUser->kode_opd = $request->kode_opd;
				$secondaryUser->role = $request->role;
				$secondaryUser->update();
	
				DB::commit(); // Commit transaksi jika semua berhasil
				return redirect()->back()->with('notify', 'Pengguna berhasil diperbarui');
			}
		} catch (Exception $e) {
			DB::rollBack(); // Rollback transaksi jika ada error
			return redirect()->back()->with('error', $e->getMessage());
		}
	}

    function delete(Request $request){
			DB::beginTransaction();
		try {
			// Hapus dari database pertama
			$user = User::where('username', $request->id)->firstOrFail();
			$user->delete();

			// Hapus dari database kedua
			$secondaryUser = UserSecondary::where('username', $request->id)->firstOrFail();
			$secondaryUser->delete();

			DB::commit(); // Commit transaksi jika semua berhasil
			return redirect()->back()->with('notify', 'Pengguna berhasil dihapus');
		} catch (Exception $e) {
			DB::rollBack(); // Rollback transaksi jika ada error
			return redirect()->back()->with('error', $e->getMessage());
		}
    }
}
