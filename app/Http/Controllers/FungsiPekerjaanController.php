<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MFungsiPekerjaan;

class FungsiPekerjaanController extends Controller
{
    function browse(Request $request){

    	$data['section'] = "fungsi-pekerjaan";
    	$data['page_section'] = "fungsi-pekerjaan";
    	$data['page'] = "Data Fungsi Pekerjaan";

    	$jabatan = MFungsiPekerjaan::query();

    	if($request->keyword){
    		$jabatan = $jabatan->where('nama', 'LIKE', '%'.$request->keyword.'%');
    	}

    	$jabatan = $jabatan->orderBy('nama', 'ASC')->paginate(20);

    	$data['jabatan'] = $jabatan;
    	return view('master_fungsi_pekerjaan.browse', $data);
    }

    function getForm(Request $request){
    	if($request->aksi == 'create-jabatan'){
    		return view('master_fungsi_pekerjaan.form-create');
    	}elseif($request->aksi == 'edit-jabatan'){
    		$jabatan = MFungsiPekerjaan::findOrFail($request->id);

    		$data = [
    			'jabatan' => $jabatan
    		];
    		return view('master_fungsi_pekerjaan.form-edit', $data);
    	}
    }

    function save(Request $request){

    	if($request->aksi == 'add-jabatan'){
    		$jabatan = new MFungsiPekerjaan;

    		$jabatan->nama = $request->nama;
    		$jabatan->keterangan = $request->keterangan;

    		$jabatan->save();

    		return redirect()->back()->with('notify', 'Fungsi pekerjaan berhasil ditambahkan');
    	}elseif ($request->aksi == 'update-jabatan') {
    		$jabatan = MFungsiPekerjaan::findOrFail($request->id);

    		$jabatan->nama = $request->nama;
    		$jabatan->keterangan = $request->keterangan;
    		$jabatan->update();

    		return redirect()->back()->with('notify', 'Fungsi pekerjaan berhasil diperbarui');
    	}
    }

    function delete(Request $request){
    	$jabatan = MFungsiPekerjaan::findOrFail($request->id);

    	// if($jabatan->dataAnjab){
    	// 	return redirect()->back()->with('error', 'Jabatan tidak bisa dihapus, digunakan untuk Analisa Jabatan');
    	// }

    	$jabatan->delete();

    	return redirect()->back()->with('notify', 'Fungsi Pekerjaan berhasil dihapus');
    }
}
