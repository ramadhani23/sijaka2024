<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MPeriode;

class PeriodeController extends Controller
{
    function browse(Request $request){

    	$data['section'] = "master-periode";
    	$data['page_section'] = "master-periode";
    	$data['page'] = "Data Periode";

    	$periode = MPeriode::query();

    	
    	$periode = $periode->orderBy('periode', 'ASC')->paginate(20);

    	$data['periode'] = $periode;
    	return view('master-periode.browse', $data);
    }

    function getForm(Request $request){
    	if($request->aksi == 'create-periode'){
    		return view('master-periode.form-create');
    	}elseif($request->aksi == 'edit-periode'){
    		$periode = Mperiode::findOrFail($request->id);
    		$data['periode'] = $periode;
    		return view('master-periode.form-edit', $data);
    	}
    }

    function save(Request $request){

    	if($request->aksi == 'add-periode'){
    		$periode = new MPeriode;

    		$periode->periode = $request->periode;
    		$periode->is_aktif = $request->is_aktif;
    		$periode->created_at = date('Y-m-d H:i:s');
            $periode->updated_at =  date('Y-m-d H:i:s');

    		$periode->save();

    		return redirect()->back()->with('notify', 'periode berhasil ditambahkan');
    	}elseif ($request->aksi == 'update-periode') {
    		$periode = MPeriode::findOrFail($request->id);

    		$periode->periode = $request->periode;
    		$periode->is_aktif = $request->is_aktif;
            $periode->updated_at =  date('Y-m-d H:i:s');

    		$periode->update();

    		return redirect()->back()->with('notify', 'periode berhasil diperbarui');
    	}
    }

    function delete(Request $request){
    	$periode = MPeriode::findOrFail($request->id);

    	$periode->delete();

    	return redirect()->back()->with('notify', 'periode berhasil dihapus');
    }
}
