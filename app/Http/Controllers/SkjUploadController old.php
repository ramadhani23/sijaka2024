<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;

use App\Models\TrxSkjUpload;

use App\Models\MUnitKerja;
use App\Models\MJabatan;
use App\Models\MBakatKerja;
use App\Models\MTemperamenKerja;
use App\Models\MMinatKerja;
use App\Models\MFungsiPekerjaan;
use App\Models\MJabatanFungsional;
use App\Models\MUpayaFisik;
use App\Models\MJabatanPelaksana;
use App\Models\MJabatanStruktural;
use App\Models\MJabatanFungsionalJenjang;

use App\Repositories\AnjabRepository;
use App\Repositories\UnitKerjaRepository;
use App\Repositories\JabatanRepository;

class SkjUploadController extends Controller
{
    function browse(Request $request, AnjabRepository $anjabRepo, UnitKerjaRepository $unitKerjaRepo){
		
		// if (condition) {
		// 	# code...
		// }
        // return $request;
    	$data['section']                                           = "anjab";
    	$data['page_section']                                      = "anjab";
    	$data['page']                                              = "Analisis Jabatan";

    	$unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();

        // 1 Struktural 2 FUngsional 3 Pelaksana
        $jenis                                                  = 1;
        $kodeOpd                                                = 9999;

        if($request->jenis){
            $jenis                                              = $request->jenis;
        }

        if($request->unit_kerja){
            $kodeOpd                                            = $request->unit_kerja;
        }

        if(\MojokertokabUser::getUser()->role == 'PD'){
            $kodeOpd                                            = \MojokertokabUser::getUser()->kode_opd;

            $tempKodeOPD = substr($kodeOpd, 0,2);

            if ($tempKodeOPD == '01'){
                $kodeOpd = substr($kodeOpd, 0,6);
            } else {
                $kodeOpd = $tempKodeOPD;
            }

            $kodeOpd;
        }

        $pd                                                     = MUnitkerja::where('kodeunit', $kodeOpd)->first();
        $skj                                                    = $anjabRepo->getDataSkj($kodeOpd, $jenis);

        // return $skj;

        $anjab                                                  = $anjabRepo->getDataAnjab($kodeOpd, $jenis);
        // return $anjab;

    	// if(\MojokertokabUser::getUser()->role == 'PD'){
    	// 	$anjab = $anjabRepo->getDataAnjab($kodeOpd, $jenis);
    	// }else{
    	// 	if($request->unit_kerja){
    	// 		$anjab = $anjabRepo->getDataAnjab($kodeOpd, $jenis);
    	// 	}else{
    	// 		$anjab = [];
    	// 	}
    	// }

        // dd($anjab);

            

        // if(\MojokertokabUser::getUser()->role == 'PD'){
        //     // $data = [
        //     //     'anjab' => $anjab->filter(function ($item) {
        //     //         return $item->user_opd == \MojokertokabUser::getUser()->kode_opd;
        //     //     }),
        //     //     'unitKerja' => $unitKerja,
        //     //     'selectedUnitKerja' => $request->unit_kerja,
        //     //     'jenis' => $jenis,
        //     //     'pd' => $pd
        //     // ];

        //     $data = [
        //         'anjab' => $anjab,
        //         'unitKerja' => $unitKerja,
        //         'selectedUnitKerja' => $request->unit_kerja,
        //         'jenis' => $jenis,
        //         'pd' => $pd
        //     ];
        // }

        // if(\MojokertokabUser::getUser()->role == 'ADMIN'){
            $data = [
                'skj' => $skj,
                'unitKerja' => $unitKerja,
                'selectedUnitKerja' => $request->unit_kerja,
                'jenis' => $jenis,
                'pd' => $pd
            ];
            // return $data;

			// dd($data['selectedUnitKerja']);
        // }

    	return view('skj.browse', $data);
    }

    function getForm(Request $request, AnjabRepository $anjabRepo, UnitKerjaRepository $unitKerjaRepo){

        if($request->aksi == 'add-skj'){
			// return $request;
            $pd                                                 = MUnitkerja::where('kodeunit', $request->kode_opd)->first();
            // $jenisPd                                            = $pd->jenis; // 1 Dinas, 2 Kecamatan, 3 UPT / Korwil
            $unitKerjaOnKode                                    = $unitKerjaRepo->getUnitKerjaOnKode($request->kode_opd);
            $jenisJabatan                                       = $request->jenis_jabatan;
			$jenjang                                       		= $request->jenjang;

            $pejabatStruktural                                  = $anjabRepo->getStukturalAnjab($request->kode_opd);

                if($jenisJabatan == '3'){
                    $data = [
                        'pejabatStruktural' => $pejabatStruktural,
                        'unitKerja' => $unitKerjaOnKode,
                    ];
                    return view('skj.form-create', $data);
                }elseif ($jenisJabatan == '2') {
                    $data = [
                        'pejabatStruktural' => $pejabatStruktural,
                        'unitKerja' => $unitKerjaOnKode,
                    ];
                    return view('skj.form-create', $data);
                }elseif ($jenisJabatan == '1') {
                    $data = [
                        'pejabatStruktural' => $pejabatStruktural,
                        'unitKerja' => $unitKerjaOnKode,
                    ];
                    return view('skj.form-create', $data);
                }

        }elseif ($request->aksi == 'edit-anjab') {
            $pd                                                 = MUnitkerja::where('kodeunit', $request->kode_opd)->first();
            // $jenisPd                                            = $pd->jenis; // 1 Dinas, 2 Kecamatan, 3 UPT / Korwil
            $unitKerjaOnKode                                    = $unitKerjaRepo->getUnitKerjaOnKode($request->kode_opd);
            $jenisJabatan                                       = $request->jenis_jabatan;
			// $jenjang                                       		= $request->m_jabatan_fungsional_jenjang;
			$jenjang                                       		= $request->jenjang;

            $pejabatStruktural                                  = $anjabRepo->getStukturalAnjab($request->kode_opd);

            // if($jenisPd == 'DINAS'){
                if($jenisJabatan == '3'){
                    $anjab                                      = $anjabRepo->getDetailAnjab($request->id);

                    $data = [
                        'pejabatStruktural' => $pejabatStruktural,
                        'unitKerja' => $pd,
                        'anjab' => $anjab,
                    ];
                    return view('anjab.anjab-dinas.form-edit-pelaksana', $data);
                }if($jenisJabatan == '2'){
                    $anjab                                      = $anjabRepo->getDetailAnjab($request->id);
					// $jenjang = $jenjang;
					// dd($anjab);
                    $data = [
                        'pejabatStruktural' => $pejabatStruktural,
                        'unitKerja' => $pd,
                        'anjab' => $anjab,
						'jenjang' => $jenjang
                    ];
					// dd($data['jenjang']);
                    return view('anjab.anjab-dinas.form-edit-fungsional', $data);
                }elseif($jenisJabatan == '1'){
                    $anjab                                      = $anjabRepo->getDetailAnjab($request->id);
                    $data = [
                        'pejabatStruktural' => $pejabatStruktural,
                        'unitKerja' => $unitKerjaOnKode,
                        'anjab' => $anjab,
                    ];
                    return view('anjab.anjab-dinas.form-edit-struktural', $data);
                }
            // }
        }

    	
    }

    function save2(Request $request, AnjabRepository $anjabRepo, JabatanRepository $jabatanRepo){
        // return $request;
    	// $jabatan = MJabatan::findOrFail($request->jabatan);
        $jenis                                                  = $request->jenis;

        if($request->aksi == 'create'){
            $jabatan                                            = $jabatanRepo->getRowJabatan($request->kode_jabatan, $request->jenis);
            // $kodeJabatan                                        = $jabatan->{'_kode'};
			$jenjangfungsional                                  = $request->jenjang;
            $atasanLangsung                                     = $request->atasan_langsung;
            if($request->jenis == '3'){
                $anjabAtasan                                    = $anjabRepo->getDetailAnjab($atasanLangsung);
                $unitKerja                                      = $anjabAtasan->unit_kerja;
            }elseif($request->jenis == '2'){
                $anjabAtasan                                    = $anjabRepo->getDetailAnjab($atasanLangsung);
                $unitKerja                                      = $anjabAtasan->unit_kerja;
            }elseif($request->jenis == '1'){
                $unitKerja                                      = $request->pd;
            }
			
            // Cek Jika Jabatan sudah dianalisa di Unit Kerja yang sama
            $periode                                            = 2;
            // $anjabExist                                         = $anjabRepo->checkAnjabIfExist($periode, $kodeJabatan, $unitKerja, $jenjangfungsional);
			
			// if($anjabExist == 1){
            //     return response()->json(['message'=>'Data sudah ada'], 409);
            // }

            $kodeOpd                                            = $request->kode_opd;


            if($request->jenis == '3'){
                $diklatTeknis                                   = $request->diklat_teknis;
                $pengalamanKerja                                = $request->pengalaman_kerja;
                $pengetahuanKerja                               = $request->pengetahuan_kerja;
                

                $anjab                                          = $anjabRepo->createAnjabPelaksana($jabatan->id, $kodeJabatan, $kodeOpd, $jenis, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $atasanLangsung, \MojokertokabUser::getUser()->username, $unitKerja, 2);

            }elseif($request->jenis == '2'){
                $diklatTeknis                                   = $request->diklat_teknis;
                $pengalamanKerja                                = $request->pengalaman_kerja;
                $pengetahuanKerja                               = $request->pengetahuan_kerja;
				$jenjangfungsional                              = $request->jenjang;
				$diklatpenjenjangan                             = $request->diklat_penjenjangan;
				$kualifikasipendidikan                          = $request->kualifikasi_pendidikan;


                $anjab                                          = $anjabRepo->createAnjabFungsional($jabatan->id, $kodeJabatan, $kodeOpd, $jenis, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $jenjangfungsional, $diklatpenjenjangan, $kualifikasipendidikan, $atasanLangsung, \MojokertokabUser::getUser()->username, $unitKerja, 2);

            }elseif($request->jenis == '1'){
                $pangkat                                        = $request->pangkat;
                $ikhtisarJabatan                                = $request->ikhtisar_jabatan;
                $kualifikasiPendidikan                          = $request->kualifikasi_pendidikan;
                $diklatTeknis                                   = $request->diklat_teknis;
                $pengalamanKerja                                = $request->pengalaman_kerja;
                $pengetahuanKerja                               = $request->pengetahuan_kerja;
                $eselon                                         = $request->eselon_jabatan;
                $nama_skj                                       = $request->nama_skj;
                $file_skj                                       = $request->file_skj;

                $anjab                                          = $anjabRepo->createSkjStruktural($jabatan->id, $kodeJabatan, $eselon, $kodeOpd, $jenis, $pangkat, $ikhtisarJabatan, $kualifikasiPendidikan, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $atasanLangsung, $nama_skj, $file_skj, \MojokertokabUser::getUser()->username, $unitKerja, 2);
            }

            return response()->json(201);

        }elseif ($request->aksi == 'update') {
            $anjab                                              = TrxAnjab::findOrFail($request->id);

            $jabatan                                            = $jabatanRepo->getRowJabatan($request->kode_jabatan, $request->jenis);

            $kodeJabatan                                        = $jabatan->{'_kode'};
			$jenjangfungsional                                  = $request->jenjang;

            $atasanLangsung                                     = $request->atasan_langsung;
            if($request->jenis == '3'){
                $anjabAtasan                                    = $anjabRepo->getDetailAnjab($atasanLangsung);
                $unitKerja                                      = $anjabAtasan->unit_kerja;
            }elseif($request->jenis == '2'){
                $anjabAtasan                                    = $anjabRepo->getDetailAnjab($atasanLangsung);
                $unitKerja                                      = $anjabAtasan->unit_kerja;
            }elseif($request->jenis == '1'){
                $unitKerja                                      = $request->pd;
            }

            $id                                                 = $request->id;

            // Cek Jika Jabatan sudah dianalisa di Unit Kerja yang sama
            $periode                                            = 2;
            $anjabExist                                         = $anjabRepo->checkAnjabIfExist($periode, $kodeJabatan, $unitKerja, $jenjangfungsional);
            if($anjabExist){
                // return response()->json(['message'=>'Data sudah ada'], 409);
            }

            $kodeOpd                                            = $request->kode_opd;

            if($request->jenis == '3'){
                $diklatTeknis                                   = $request->diklat_teknis;
                $pengalamanKerja                                = $request->pengalaman_kerja;
                $pengetahuanKerja                               = $request->pengetahuan_kerja;

                $anjab                                          = $anjabRepo->updateAnjabPelaksana($id, $jabatan->id, $kodeJabatan, $kodeOpd, $jenis, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $atasanLangsung, \MojokertokabUser::getUser()->username, $unitKerja,2);

            }elseif($request->jenis == '2'){
                $diklatTeknis                                   = $request->diklat_teknis;
                $pengalamanKerja                                = $request->pengalaman_kerja;
                $pengetahuanKerja                               = $request->pengetahuan_kerja;
				$jenjangfungsional                              = $request->jenjang;
				$diklatpenjenjangan                             = $request->diklat_penjenjangan;
				$kualifikasipendidikan                          = $request->kualifikasi_pendidikan;

                $anjab                                          = $anjabRepo->updateAnjabFungsional($id, $jabatan->id, $kodeJabatan, $kodeOpd, $jenis, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $jenjangfungsional, $diklatpenjenjangan, $kualifikasipendidikan, $atasanLangsung, \MojokertokabUser::getUser()->username, $unitKerja,2);

            }elseif($request->jenis == '1'){
                $pangkat                                        = $request->pangkat;
                $ikhtisarJabatan                                = $request->ikhtisar_jabatan;
                $kualifikasiPendidikan                          = $request->kualifikasi_pendidikan;
                $diklatTeknis                                   = $request->diklat_teknis;
                $pengalamanKerja                                = $request->pengalaman_kerja;
                $pengetahuanKerja                               = $request->pengetahuan_kerja;
                $eselon                                         = $request->eselon_jabatan;

                $anjab                                          = $anjabRepo->updateAnjabStruktural($id, $jabatan->id, $kodeJabatan, $eselon, $kodeOpd, $jenis, $pangkat, $ikhtisarJabatan, $kualifikasiPendidikan, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $atasanLangsung, \MojokertokabUser::getUser()->username, $unitKerja, 2);
            }



            // $anjab->nama = $jabatan->nama;
            // $anjab->kode_opd = $request->pd;
            // $anjab->jenis_jabatan = $request->jenis_jabatan;
            // $anjab->eselon_jabatan = $request->eselon_jabatan;
            // $anjab->kode_jabatan_2 = $request->kode_jabatan;
            // // $anjab->unit_kerja_eselon2 = $request->unit_kerja_eselon2;
            // // $anjab->unit_kerja_eselon3 = $request->unit_kerja_eselon3;
            // // $anjab->unit_kerja_eselon4 = $request->unit_kerja_eselon4;
            // // $anjab->ikhtisar_jabatan = $request->ikhtisar_jabatan;
            // // $anjab->kualifikasi_pendidikan = $request->kualifikasi_pendidikan;
            // // $anjab->diklat_penjenjangan = $request->diklat_penjenjangan;
            // $anjab->diklat_teknis = $request->diklat_teknis;
            // $anjab->pengalaman_kerja = $request->pengalaman_kerja;
            // $anjab->pengetahuan_kerja = $request->pengetahuan_kerja;
            // // $anjab->pangkat = $request->pangkat;
            // $anjab->parent = $request->atasan_langsung;

            // $anjab->update();

            // return redirect()->back()->with('notify', 'Anjab berhasil diperbarui');
            if($anjab){
                // return response()->json(200);
                return response()->json(200);
            }

        }
    }

    function save(Request $request){
        return $request->all();
        
        $jabatan                                      = $jabatanRepo->getRowJabatan($request->kode_jabatan, $request->jenis);
        $skj                                          = new TrxSkjUpload;
        $jenis                                        = $request->jenis;
        $unitKerja                                    = $request->pd;
        $skj->nama_skj                                = $request->nama_skj;

        if($request->hasfile('file_skj')){
            $file = $request->file('file_skj');
            if($file->getClientOriginalExtension() != 'pdf' && $file->getClientOriginalExtension() != 'xls' &&  $file->getClientOriginalExtension() != 'xlsx'){
                return redirect()->back()->with('notify', 'Dokumen harus PDF / Excel');
            }
            $name = 'skj-'.$skj->nama_skj.'-'.$skj->id.'.'.$file->getClientOriginalExtension();
            $file->move('skj/', $name);
            $skj->file_skj = $name;

            // dd($skj);
           
        }
        return $skj;
        $skj->save();
        return redirect()->back()->with('notify', 'SKJ berhasil tersimpan');

    }
}
