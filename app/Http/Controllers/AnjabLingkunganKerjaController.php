<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabLingkunganKerja;
use DB;

class AnjabLingkunganKerjaController extends Controller
{
    function browse($id){

    	$data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "Detail Anjab";
    	$data['pages'] = "detail_lingkungan_kerja";
    	$data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['lingkungan_kerja'] = TrxAnjabLingkunganKerja::where('trx_anjab_id', $id)->orderBy('id', 'asc')->first();
    	return view('anjab.anjab-detail-lingkungan-kerja', $data);

    }

    function getForm(Request $request){
    	if($request->aksi == 'add-data'){
            $data['id'] = $request->id;
    		return view('anjab.lingkungan_kerja.form-create-lingkungan-kerja', $data);
    	}elseif ($request->aksi == 'edit-data') {
            $data['lingkungan_kerja'] = TrxAnjabLingkunganKerja::where('trx_anjab_id', $request->id)->firstOrFail();
    		return view('anjab.lingkungan_kerja.form-edit-lingkungan-kerja', $data);
    	}
    }

     function save(Request $request){
        if($request->aksi == 'create'){
            $lingkungan_kerja = new TrxAnjabLingkunganKerja;

            $lingkungan_kerja->tempat_kerja = $request->tempat_kerja;
            $lingkungan_kerja->suhu = $request->suhu;
            $lingkungan_kerja->udara = $request->udara;
            $lingkungan_kerja->keadaan_ruangan = $request->keadaan_ruangan;
            $lingkungan_kerja->letak = $request->letak;
            $lingkungan_kerja->penerangan = $request->penerangan;
            $lingkungan_kerja->suara = $request->suara;
            $lingkungan_kerja->keadaan_tempat_kerja = $request->keadaan_tempat_kerja;
            $lingkungan_kerja->getaran = $request->getaran;
            $lingkungan_kerja->trx_anjab_id = $request->id;
            $lingkungan_kerja->created_at = date('Y-m-d H:i:s');
            $lingkungan_kerja->save();
            return redirect()->route('anjab.browse-lingkungan-kerja',$request->id)->with('notify', 'Hasil Kerja berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {
            $lingkungan_kerja = TrxAnjabLingkunganKerja::findOrFail($request->id);

            $lingkungan_kerja->tempat_kerja = $request->tempat_kerja;
            $lingkungan_kerja->suhu = $request->suhu;
            $lingkungan_kerja->udara = $request->udara;
            $lingkungan_kerja->keadaan_ruangan = $request->keadaan_ruangan;
            $lingkungan_kerja->letak = $request->letak;
            $lingkungan_kerja->penerangan = $request->penerangan;
            $lingkungan_kerja->suara = $request->suara;
            $lingkungan_kerja->keadaan_tempat_kerja = $request->keadaan_tempat_kerja;
            $lingkungan_kerja->getaran = $request->getaran;

            $lingkungan_kerja->update();
            return redirect()->route('anjab.browse-lingkungan-kerja',$request->id_anjab)->with('notify', 'Hasil Kerja berhasil diperbarui');
        }
    }

    function deleteLingkunganKerja (Request $request){
        $lingkungan_kerja = TrxAnjabLingkunganKerja::findOrFail($request->id);
        $lingkungan_kerja->delete();
        return redirect()->back()->with('notify', 'Hasil Kerja berhasil dihapus');
    }
   
}
