<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabKorelasiJabatan;
use DB;

class AnjabKorelasiJabatanController extends Controller
{
    function browse($id){

    	$data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "Detail Anjab";
    	$data['pages'] = "detail_korelasi_jabatan";
    	$data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['korelasi_jabatan'] = TrxAnjabKorelasiJabatan::where('trx_anjab_id', $id)->orderBy('id', 'asc')->paginate(10);
    	return view('anjab.anjab-detail-korelasi-jabatan', $data);

    }

    function getForm(Request $request){
    	if($request->aksi == 'add-data'){
            $data['id'] = $request->id;
    		return view('anjab.korelasi_jabatan.form-create-korelasi-jabatan', $data);
    	}elseif ($request->aksi == 'edit-data') {
            $data['korelasi_jabatan'] = TrxAnjabKorelasiJabatan::findOrFail($request->id);
    		return view('anjab.korelasi_jabatan.form-edit-korelasi-jabatan', $data);
    	}
    }

     function save(Request $request){
        if($request->aksi == 'create'){
            $korelasi_jabatan = new TrxAnjabKorelasiJabatan;


            $korelasi_jabatan->jabatan = $request->jabatan;
            $korelasi_jabatan->unit_kerja = $request->unit_kerja;
            $korelasi_jabatan->hal = $request->hal;
            $korelasi_jabatan->trx_anjab_id = $request->id;
            $korelasi_jabatan->created_at = date('Y-m-d H:i:s');
            $korelasi_jabatan->save();
            return redirect()->route('anjab.browse-korelasi-jabatan',$request->id)->with('notify', 'Hasil Kerja berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {
            $korelasi_jabatan = TrxAnjabKorelasiJabatan::findOrFail($request->id);

            $korelasi_jabatan->jabatan = $request->jabatan;
            $korelasi_jabatan->unit_kerja = $request->unit_kerja;
            $korelasi_jabatan->hal = $request->hal;

            $korelasi_jabatan->update();
            return redirect()->route('anjab.browse-korelasi-jabatan',$request->id_anjab)->with('notify', 'Hasil Kerja berhasil diperbarui');
        }
    }

    function deleteKorelasi(Request $request){
        $korelasi_jabatan = TrxAnjabKorelasiJabatan::findOrFail($request->id);
        $korelasi_jabatan->delete();
        return redirect()->back()->with('notify', 'Hasil Kerja berhasil dihapus');
    }
   
}
