<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\MJabatan;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabEvajabFungsional;
use App\Models\TrxAnjabEvajabStruktural;
use App\Models\MUnitKerja;
use App\Models\RkpKebutuhanPegawai;
use App\Repositories\AbkRepository;
use App\Repositories\AnjabRepository;
use App\Repositories\UnitKerjaRepository;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    function index(UnitKerjaRepository $unitKerjaRepo, AnjabRepository $anjabRepo, AbkRepository $abkRepo){
        // Jika role adalah KEMENDAGRI, langsung kembalikan tampilan atau data tertentu
    if (\MojokertokabUser::getUser()->role == 'KEMENDAGRI') {
        return view('dashboard', [
            'section' => "dashboard",
            'page_section' => "dashboard",
            'page' => "Dashboard",
            'message' => "Akses terbatas untuk role KEMENDAGRI",
        ]);
    }
    
    	$data['section'] = "dashboard";
    	$data['page_section'] = "dashboard";
    	$data['page'] = "Dashboard";

    	$jabatan = MJabatan::count();
    	$anjab = TrxAnjab::count();

        $unitKerja = $unitKerjaRepo->getActiveUnitKerjaPd();
        // return $unitKerja;

        $activePeriode = \MojokertokabApp::getActivePeriod();

        $kebutuhanSeluruhPegawai = 0;

        foreach($unitKerja as $itemUnitKerja){
            $rkpKebutuhanPegawai = RkpKebutuhanPegawai::where('kodeunit', $itemUnitKerja->kodeunit)->where('m_periode_id', $activePeriode->id)->first();
            $itemUnitKerja->jumlah_kebutuhan_pegawai_struktural = @$rkpKebutuhanPegawai->kebutuhan_pegawai_struktural;
            $itemUnitKerja->jumlah_kebutuhan_pegawai_fungsional = @$rkpKebutuhanPegawai->kebutuhan_pegawai_fungsional;
            $itemUnitKerja->jumlah_kebutuhan_pegawai_pelaksana = @$rkpKebutuhanPegawai->kebutuhan_pegawai_pelaksana;

            $kebutuhanSeluruhPegawai += $itemUnitKerja->jumlah_kebutuhan_pegawai_struktural + $itemUnitKerja->jumlah_kebutuhan_pegawai_fungsional + $itemUnitKerja->jumlah_kebutuhan_pegawai_pelaksana;
        }

        $evjabStruktural = TrxAnjabEvajabStruktural::count();
        $evjabFungsional = TrxAnjabEvajabFungsional::count();

        $satker = substr(\MojokertokabUser::getUser()->option->satker_id,0,2);
        if ($satker == '01') {
            $satker = substr(\MojokertokabUser::getUser()->option->satker_id,0,6);
        } elseif ($satker == '28') {
            $unitK = \MojokertokabUser::getUser()->option->satker_id;
            if ($unitK == '2801') {
                $unitKerja = '28';
            } else {
                $satker = substr(\MojokertokabUser::getUser()->option->satker_id,0,4);
                
            }
            
        }

        $whereSatker = MUnitKerja::where('kodeunit',$satker)->first();

        if(\MojokertokabUser::getUser()->role == 'PD'){
            $kodeOpd    = \MojokertokabUser::getUser()->kode_opd;
            $tempKodeOPD = substr($kodeOpd, 0,2);
            if ($tempKodeOPD == '01'){
                $kodeOpd = substr($kodeOpd, 0,6);
            }  elseif($tempKodeOPD == '28'){
				$unitKerja = \MojokertokabUser::getUser()->option->satker_id;
			}  else {
                $kodeOpd = $tempKodeOPD;
            }
            $kodeOpd;
        }
        if (\MojokertokabUser::getUser()->role == 'PD') {
        // count anjab
            $data['anjabStruktural']    = $anjabRepo->getDataAnjab($kodeOpd, '1')->count();
            $data['anjabFungsional']    = $anjabRepo->getDataAnjab($kodeOpd, '2')->count();
            $data['anjabPelaksana']     = $anjabRepo->getDataAnjab($kodeOpd, '3')->count();
            
            // count anjab verifikasi
            $data['anjabVerifyStruktural']      = $anjabRepo->getDataVerify($kodeOpd, '1')->count();
            $data['anjabVerifyFungsional']      = $anjabRepo->getDataVerify($kodeOpd, '2')->count();
            $data['anjabVerifyPelaksana']       = $anjabRepo->getDataVerify($kodeOpd, '3')->count();
            
            //abkcountFungsional
            $data['abkVerifyStruktural']        = $abkRepo->getAbkCountVerify('1')->count();
            $data['abkVerifyFungsional']        = $abkRepo->getAbkCountVerify('2')->count();
            $data['abkVerifyPelaksana']         = $abkRepo->getAbkCountVerify('3')->count();

            // return $data['anjabStruktural']    = $anjabRepo->getDataAnjab($kodeOpd, '1')->get();
        }

    	$data['jumlahJabatan'] = $jabatan;
    	$data['jumlahAnjab'] = $anjab;
        $data['jumlahEvjab'] = $evjabFungsional + $evjabStruktural;
        $data['unitKerja'] = $unitKerja;
        $data['kebutuhanSeluruhPegawai'] = $kebutuhanSeluruhPegawai;
        $data['satker'] = $whereSatker->unitkerja;

    	return view('dashboard', $data);
    }

    function getStrukturOrganisasi(){

        $anjab = TrxAnjab::where('parent', NULL)->first();

        $palingAtas = [
            'name' => $anjab->nama,
            'title' => $anjab->unit_kerja_eselon2,
        ];

        $bawahan1 = TrxAnjab::where('parent', $anjab->id)->get();

        $i = 0;
        foreach($bawahan1 as $bawahan){
            $palingAtas['children'][$i] = [
                'name' => $bawahan->nama,
                'title' => $bawahan->unit_kerja_eselon2
            ];
            $i++;

            $bawahan2 = TrxAnjab::where('parent', $bawahan->id)->get();

            $j = 0;
            foreach($bawahan2 as $itemBawahan){
                $palingAtas['children'][$i]['a'] = 1;
                $j++;
            }
        }
     

        $data = $palingAtas;
        

        if(\MojokertokabUser::getUser()->role == 'PD'){

        }else{

        }

        return response()->json($data);
    }

    private function getChildren(){

    }
}
