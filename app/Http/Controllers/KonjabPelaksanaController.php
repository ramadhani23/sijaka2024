<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MKonjabPelaksana;

class KonjabPelaksanaController extends Controller
{
    function browse (Request $request) {
        $data['section'] = "jabatan-pelaksana";

    	$data['page_section'] = "jabatan-pelaksana";
    	$data['page'] = "Data Konversi Jabatan Pelaksana";

    	$konjabpelaksana = MKonjabPelaksana::get();

    	if($request->keyword){
    		$konjabpelaksana = $konjabpelaksana->where('jabatan_lama', 'LIKE', '%'.$request->keyword.'%');
    	}

    	// $konjabpelaksana = $konjabpelaksana->orderBy('nama', 'ASC')->paginate(20);
        // return $konjabpelaksana;
    	$data['konjabpelaksana'] = $konjabpelaksana;
		// return $data;
    	return view('master_konjab_pelaksana.browse', $data);
    }

	function getForm(Request $request){
    	if($request->aksi == 'create-konjabpelaksana'){
    		return view('master_konjab_pelaksana.form-create');
    	}elseif($request->aksi == 'edit-konjabpelaksana'){
    		$konjabpelaksana = MKonjabPelaksana::findOrFail($request->id);

    		$data = [
    			'konjabpelaksana' => $konjabpelaksana
    		];
    		return view('master_konjab_pelaksana.form-edit', $data);
    	}
    }

	function save(Request $request){
		// return $request;
    	if($request->aksi == 'add-konjabpelaksana'){
    		$konjabpelaksana = new MKonjabPelaksana;

    		$konjabpelaksana->jabatan_lama = $request->jabatan_lama;
    		$konjabpelaksana->jabatan_baru = $request->jabatan_baru;
			// return $konjabpelaksana;
    		$konjabpelaksana->save();

    		return redirect()->back()->with('notify', 'Konversi Jabatan Pelaksana berhasil ditambahkan');
    	}elseif ($request->aksi == 'update-konjabpelaksana') {
    		$konjabpelaksana = MKonjabPelaksana::findOrFail($request->id);

    		$konjabpelaksana->jabatan_lama = $request->jabatan_lama;
    		$konjabpelaksana->jabatan_baru = $request->jabatan_baru;
    		$konjabpelaksana->update();

    		return redirect()->back()->with('notify', 'Konversi Jabatan Pelaksana berhasil diperbarui');
    	}
    }

    function delete(Request $request){
    	$konjabpelaksana = MKonjabPelaksana::findOrFail($request->id);

    	// if($konjabpelaksana->dataAnjab){
    	// 	return redirect()->back()->with('error', 'konjabpelaksana tidak bisa dihapus, digunakan untuk Analisa konjabpelaksana');
    	// }

    	$konjabpelaksana->delete();

    	return redirect()->back()->with('notify', 'bakat kerja berhasil dihapus');
    }
}
