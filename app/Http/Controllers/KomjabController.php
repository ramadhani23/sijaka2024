<?php

namespace App\Http\Controllers;
use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\MUnitKerja;
use App\Models\TrxAnjab;
use App\Models\TrxKomjab;
use App\Models\TrxKomjabTeknis;
use App\Models\MKamusKompetensiLevel;
use App\Models\TrxKomjabTeknisIndikator;
use App\Models\MKamusKompetensiLevelIndikator;
use App\Models\TrxKomjabPersyaratanJabatanTeknis;
use App\Models\TrxKomjabPersyaratanJabatanPengalamanKerja;
use App\Models\TrxKomjabPersyaratanJabatanIndikatorKinerja;
use DB;

class KomjabController extends Controller
{
    function browse($jenis){
        $data['section'] = "komjab";
        $data['pages'] = "list_kompetensi_jabatan";

        $anjab = TrxAnjab::query();
        $komjab = TrxAnjab::query();

        $anjab = $anjab->whereDoesntHave('dataKomjab');
        $komjab = $komjab->whereHas('dataKomjab');
        if ($jenis == 'struktural') {

            $anjab = $anjab->where('jenis_jabatan', '1');
            $komjab = $komjab->where('jenis_jabatan', '1');

            $data['page_section'] = "komjab-struktural";
            $data['page'] = "Standar Kompetensi Jabatan Struktural";
            // $anjab = TrxAnjab::where('jenis_jabatan', '1')();
            // $komjab = TrxAnjab::where('jenis_jabatan', '1')->paginate(25);
            if(\MojokertokabUser::getUser()->role == 'PD'){
                if(strlen(\MojokertokabUser::getUser()->kode_opd) == 6){
                    $anjab = $anjab->where('unit_kerja_eselon3', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');

                    $komjab = $komjab->where('unit_kerja_eselon3', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');
                }else{
                    $anjab = $anjab->where('kode_opd',  \MojokertokabUser::getUser()->kode_opd);
                    $komjab = $komjab->where('kode_opd', \MojokertokabUser::getUser()->kode_opd);
                }
            }

            $data['anjab'] = $anjab->paginate(25);
            $data['komjab'] = $komjab->paginate(25);
            
            return view('komjab.browse-struktural', $data);
        }else{

            $anjab = $anjab->where('jenis_jabatan', '!=', '1');
            $komjab = $komjab->where('jenis_jabatan', '!=', '1');

            $data['page_section'] = "komjab-fungsional";
            $data['page'] = "Standar Kompetensi Jabatan Fungsional";

            // $anjab = TrxAnjab::where('jenis_jabatan','!=',1)->whereDoesntHave('dataKomjab')->get();
            // $komjab = TrxAnjab::where('jenis_jabatan','!=', 1)->whereHas('dataKomjab')->paginate(25);

            if(\MojokertokabUser::getUser()->role == 'PD'){
                if(strlen(\MojokertokabUser::getUser()->kode_opd) == 6){
                    $anjab = $anjab->where('unit_kerja_eselon3', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');

                    $komjab = $komjab->where('unit_kerja_eselon3', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');
                }else{
                    $anjab = $anjab->where('kode_opd',  \MojokertokabUser::getUser()->kode_opd);
                    $komjab = $komjab->where('kode_opd', \MojokertokabUser::getUser()->kode_opd);
                }
            }

            $data['anjab'] = $anjab->paginate();
            $data['komjab'] = $komjab->paginate();

            return view('komjab.browse-fungsional', $data);
        }

    }


    function pilihAnjab(Request $request){
        $data['section'] = "komjab";
    	$data['page_section'] = "komjab";
    	$data['page'] = "Input Kompetensi Jabatan";
    	$data['pages'] = "detail_input_komjab";
        $data['anjab'] = TrxAnjab::where('id', $request->jabatan)->first();
        $data['anjabData'] = TrxAnjab::where('id', $request->jabatan)->get();
    	$data['anjabAll'] = TrxAnjab::all();
    	$data['level_kompetensi_integeritas'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',1)->get();
    	$data['level_kompetensi_kerjasama'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',2)->get();
    	$data['level_kompetensi_komunikasi'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',3)->get();
    	$data['level_kompetensi_orientasi'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',4)->get();
    	$data['level_kompetensi_pelayanan'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',5)->get();
    	$data['level_kompetensi_pengembangan'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',6)->get();
    	$data['level_kompetensi_perubahan'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',7)->get();
    	$data['level_kompetensi_keputusan'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',8)->get();
    	$data['level_kompetensi_perekat'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',9)->get();
    	return view('komjab.input-komjab', $data);
    }

     function detail($id){
        $data['section'] = "komjab";
        $data['page_section'] = "komjab";
        $data['page'] = "Input Kompetensi Jabatan";
        $data['pages'] = "detail_input_komjab";
        $data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['komjab'] = TrxKomjab::where('trx_anjab_id',$id)->first();
        
        $data['anjabAll'] = TrxAnjab::all();
        $data['level_kompetensi_integeritas'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',1)->get();
        $data['level_kompetensi_kerjasama'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',2)->get();
        $data['level_kompetensi_komunikasi'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',3)->get();
        $data['level_kompetensi_orientasi'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',4)->get();
        $data['level_kompetensi_pelayanan'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',5)->get();
        $data['level_kompetensi_pengembangan'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',6)->get();
        $data['level_kompetensi_perubahan'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',7)->get();
        $data['level_kompetensi_keputusan'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',8)->get();
        $data['level_kompetensi_perekat'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',9)->get();
        return view('komjab.detail-komjab', $data);
    }


	function save(Request $request){
       
        if($request->type == 'save'){

            $komjab = new TrxKomjab;
            $komjab->trx_anjab_id = $request->id_jabatan;
            $komjab->kelompok_jabatan = $request->kelompok_jabatan;
            $komjab->urusan_pemerintah = $request->urusan_pemerintah;
            $komjab->id_m_kompetensi_level_integritas = $request->level_integeritas;
            $komjab->id_m_kompetensi_level_kerjasama = $request->level_kerjasama;
            $komjab->id_m_kompetensi_level_komunikasi = $request->level_komunikasi;
            $komjab->id_m_kompetensi_level_orientasi = $request->level_orientasi;
            $komjab->id_m_kompetensi_level_pelayanan = $request->level_pelayanan;
            $komjab->id_m_kompetensi_level_pengembangan = $request->level_pengembangan;
            $komjab->id_m_kompetensi_level_perubahan = $request->level_perubahan;
            $komjab->id_m_kompetensi_level_keputusan = $request->level_keputusan;
            $komjab->id_m_kompetensi_level_perekat_bangsa = $request->level_perekat;
            $saved = $komjab->save();
            return redirect()->route('komjab.detail',$request->id_jabatan)->with('notify','Kompetensi jabatan berhasil ditambah!');
        }elseif($request->type == 'update'){
            $komjab = TrxKomjab::find($request->id_komjab);
            $komjab->trx_anjab_id = $request->id_anjab;
            $komjab->kelompok_jabatan = $request->kelompok_jabatan;
            $komjab->urusan_pemerintah = $request->urusan_pemerintah;
            $komjab->id_m_kompetensi_level_integritas = $request->level_integeritas;
            $komjab->id_m_kompetensi_level_kerjasama = $request->level_kerjasama;
            $komjab->id_m_kompetensi_level_komunikasi = $request->level_komunikasi;
            $komjab->id_m_kompetensi_level_orientasi = $request->level_orientasi;
            $komjab->id_m_kompetensi_level_pelayanan = $request->level_pelayanan;
            $komjab->id_m_kompetensi_level_pengembangan = $request->level_pengembangan;
            $komjab->id_m_kompetensi_level_perubahan = $request->level_perubahan;
            $komjab->id_m_kompetensi_level_keputusan = $request->level_keputusan;
            $komjab->id_m_kompetensi_level_perekat_bangsa = $request->level_perekat;
            $saved = $komjab->update();
           
           
            return redirect()->route('komjab.detail',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil diperbarui!');
        }elseif($request->type_syarat_jabatan == 'update-persyaratan-jabatan'){
            $komjab = TrxKomjab::find($request->id_komjab);
            $komjab->jenjang = $request->jenjang;
            $komjab->pangkat = $request->pangkat;
            $komjab->manajerial = $request->manajerial;
            $komjab->bidang_ilmu = $request->bidang_ilmu;
            $komjab->status_manajerial = $request->status_manajerial;
            $saved = $komjab->update();

            return redirect()->route('komjab.detail',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil diperbarui!');
        }elseif ($request->aksi == 'create-teknis') {
            $teknis = new TrxKomjabTeknis;
                        
            $teknis->level = $request->level_teknis;
            $teknis->nama_kompetensi = $request->nama_kompetensi;
            $teknis->deskripsi = $request->deskripsi;
            $teknis->id_trx_komjab = $request->id_komjab;
            $teknis->save();

            return redirect()->route('komjab.detail',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil ditambah!');
        }elseif ($request->aksi == 'update-teknis') {

            $teknis = TrxKomjabTeknis::find($request->id);
                        
            $teknis->level = $request->level_teknis;
            $teknis->nama_kompetensi = $request->nama_kompetensi;
            $teknis->deskripsi = $request->deskripsi;
            $teknis->id_trx_komjab = $request->id_komjab;

            $teknis->update();

            return redirect()->route('komjab.detail',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil diperbarui!');
        }elseif ($request->aksi == 'create-indikator') {
            $indikator = new TrxKomjabTeknisIndikator;
            $indikator->id_trx_komjab_teknis = $request->id_teknis;
            $indikator->indikator = $request->indikator;
            $indikator->save();

            return redirect()->route('komjab.detail',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil ditambah!');
        }elseif ($request->aksi == 'update-indikator') {

            $indikator = TrxKomjabTeknisIndikator::find($request->id);
            $indikator->indikator = $request->indikator;
            $indikator->update();

            return redirect()->route('komjab.detail',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil diperbarui!');
        }elseif ($request->aksi == 'create-teknis-persyaratan-jabatan') {
            $teknis = new TrxKomjabPersyaratanJabatanTeknis;
            $teknis->id_trx_komjab = $request->id_komjab;
            $teknis->teknis = $request->teknis;
            $teknis->status_teknis = $request->status_teknis;
            $teknis->save();

            return redirect()->route('komjab.detail',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil ditambah!');
        }elseif ($request->aksi == 'update-teknis-persyaratan-jabatan') {

            $teknis = TrxKomjabPersyaratanJabatanTeknis::find($request->id);
            $teknis->teknis = $request->teknis;
            $teknis->status_teknis = $request->status_teknis;
            $teknis->update();

            return redirect()->route('komjab.detail',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil diperbarui!');
        }elseif ($request->aksi == 'create-pengalaman-kerja-persyaratan-jabatan') {
            $pengalaman = new TrxKomjabPersyaratanJabatanPengalamanKerja;
            $pengalaman->id_trx_komjab = $request->id_komjab;
            $pengalaman->pengalaman_kerja = $request->pengalaman_kerja;
            $pengalaman->status_pengalaman_kerja = $request->status_pengalaman_kerja;
            $pengalaman->save();

            return redirect()->route('komjab.detail',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil ditambah!');
        }elseif ($request->aksi == 'update-pengalaman-kerja-persyaratan-jabatan') {

            $pengalaman = TrxKomjabPersyaratanJabatanPengalamanKerja::find($request->id);
            $pengalaman->pengalaman_kerja = $request->pengalaman_kerja;
            $pengalaman->status_pengalaman_kerja = $request->status_pengalaman_kerja;
            $pengalaman->update();

            return redirect()->route('komjab.detail',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil diperbarui!');
        }elseif ($request->aksi == 'create-indikator-kinerja-persyaratan-jabatan') {
            $indikator = new TrxKomjabPersyaratanJabatanIndikatorKinerja;
            $indikator->id_trx_komjab = $request->id_komjab;
            $indikator->indikator_kinerja = $request->indikator_kinerja;
            $indikator->save();

            return redirect()->route('komjab.detail',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil ditambah!');
        }elseif ($request->aksi == 'update-indikator-kinerja-persyaratan-jabatan') {

            $indikator = TrxKomjabPersyaratanJabatanIndikatorKinerja::find($request->id);
            $indikator->indikator_kinerja = $request->indikator_kinerja;
            $indikator->update();

            return redirect()->route('komjab.detail',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil diperbarui!');
        }

		

	}
    
    function getForm(Request $request){

        if($request->aksi == 'add-teknis'){
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            return view('komjab.form-create-teknis', $data);
        }elseif ($request->aksi == 'edit-teknis') {
            $data['komjab_teknis'] = TrxKomjabTeknis::findOrFail($request->id);
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            $data['id'] = $request->id;
            
            return view('komjab.form-edit-teknis', $data);
        }elseif ($request->aksi == 'add-indikator') {
            
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            $data['id_teknis'] = $request->id_teknis;
            
            return view('komjab.form-create-indikator', $data);
        }elseif ($request->aksi == 'edit-indikator') {
            $data['komjab_teknis_indikator'] = TrxKomjabTeknisIndikator::findOrFail($request->id);
         
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            $data['id_teknis'] = $request->id_teknis;
            $data['id'] = $request->id;
            
            return view('komjab.form-edit-indikator', $data);
        }elseif($request->aksi == 'add-teknis-persyaratan-jabatan'){
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            return view('komjab.form-create-teknis-persyaratan-jabatan', $data);
        }elseif ($request->aksi == 'edit-teknis-persyaratan-jabatan') {
            $data['persyaratanJabatanTeknis'] = TrxKomjabPersyaratanJabatanTeknis::findOrFail($request->id);
         
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            $data['id'] = $request->id;
            
            return view('komjab.form-edit-teknis-persyaratan-jabatan', $data);
        }elseif($request->aksi == 'add-pengalaman-kerja-persyaratan-jabatan'){
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            return view('komjab.form-create-pengalaman-kerja-persyaratan-jabatan', $data);
        }elseif ($request->aksi == 'edit-pengalaman-kerja-persyaratan-jabatan') {
            $data['persyaratanJabatanPengalamanKerja'] = TrxKomjabPersyaratanJabatanIndikatorKinerja::findOrFail($request->id);
         
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            $data['id'] = $request->id;
            
            return view('komjab.form-edit-pengalaman-kerja-persyaratan-jabatan', $data);
        }elseif($request->aksi == 'add-indikator-kinerja-persyaratan-jabatan'){
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            return view('komjab.form-create-indikator-kinerja-persyaratan-jabatan', $data);
        }elseif ($request->aksi == 'edit-indikator-kinerja-persyaratan-jabatan') {
            $data['persyaratanJabatanIndnkatorKinerja'] = TrxKomjabPersyaratanJabatanIndikatorKinerja::findOrFail($request->id);
         
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            $data['id'] = $request->id;
            
            return view('komjab.form-edit-indikator-kinerja-persyaratan-jabatan', $data);
        }
    }

    function delete(Request $request){
        if($request->aksi == 'delete-teknis'){
            $teknis = TrxKomjabTeknis::findOrFail($request->id);
            $teknis->delete();
            return redirect()->back()->with('notify', 'Teknis berhasil dihapus');
        }elseif ($request->aksi == 'delete-indikator') {
            $indikator = TrxKomjabTeknisIndikator::findOrFail($request->id);
            $indikator->delete();
            return redirect()->back()->with('notify', 'Indikator berhasil dihapus');
        }elseif ($request->aksi == 'delete-teknis-persyaratan-jabatan') {
            $teknis = TrxKomjabPersyaratanJabatanTeknis::findOrFail($request->id);
            $teknis->delete();
            return redirect()->back()->with('notify', 'Persyaratan Jabtaan Teknis berhasil dihapus');
        }elseif ($request->aksi == 'delete-pengalaman-kerja-persyaratan-jabatan') {
            $pengalaman = TrxKomjabPersyaratanJabatanPengalamanKerja::findOrFail($request->id);
            $pengalaman->delete();
            return redirect()->back()->with('notify', 'Persyaratan Jabtaan Pengalaman Kerja berhasil dihapus');
        }elseif ($request->aksi == 'delete-indikator-kinerja-persyaratan-jabatan') {
            $indikator = TrxKomjabPersyaratanJabatanIndikatorKinerja::findOrFail($request->id);
            $indikator->delete();
            return redirect()->back()->with('notify', 'Persyaratan Jabtaan Indikator Kinerja berhasil dihapus');
        }
        
    }

    function printWord(Request $request, $id){
        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
    	$domPdfPath = base_path('vendor/dompdf/dompdf');
        \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
        \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');

    	$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('template-komjab.docx');

        $komjab = TrxKomjab::findOrFail($id);
        $id_anjab = TrxKomjab::where('id',$id)->get()->first()->pluck('trx_anjab_id');
     
        $anjab = TrxAnjab::where('id',$komjab->trx_anjab_id)->get()->first();

        $komjab_teknis_level = $komjab->komjabTeknis?$komjab->komjabTeknis:[];
        $templateProcessor->cloneRow('row_kompetensi_komjab_teknis', $komjab_teknis_level->count());
      
        $i= 10;
        $j= 1;
        $data_row =[];
        foreach($komjab->komjabTeknis as $itemKomjabTeknis){
            $templateProcessor->setValue('row_kompetensi_komjab_teknis#'.$j, $i.'. '.@$itemKomjabTeknis->nama_kompetensi);
            $templateProcessor->setValue('row_level_teknis_komjab#'.$j, @$itemKomjabTeknis->level);
            $templateProcessor->setValue('row_deskripsi_komjab_teknis#'.$j, @$itemKomjabTeknis->deskripsi);
            $k = 1;
            $data_row = [];
            foreach($itemKomjabTeknis->indikatorTeknis as $itemIndikator){
                $data ='4.'.$k.' '.@$itemIndikator->indikator;
                $data_row[] = $data;
                $k++;
            }
            $result = '';
            foreach($data_row as $row){
                $result .= "\r\n".$row."\r\n"; 
            }
            $templateProcessor->setValue('row_indikator_kompetensi_komjab_teknis#'.$j, $result);
            $i++;
            $j++;
        }

        $rowIntegritas = MKamusKompetensiLevel::find($komjab->id_m_kompetensi_level_integritas);
        $indikatorIntegritas = MKamusKompetensiLevelIndikator::where('id_m_kamus_kompetensi_level', $rowIntegritas->id)->get();
        $textIndikatorIntegritas = '';
        $i = 1;
        foreach ($indikatorIntegritas as $itemIndikator) {
            $textIndikatorIntegritas = $textIndikatorIntegritas."\n ".$i.".".$itemIndikator->indikator;
            $i++;
        }

        $rowKerjasama = MKamusKompetensiLevel::find($komjab->id_m_kompetensi_level_kerjasama);
        $indikatorKerjasama = MKamusKompetensiLevelIndikator::where('id_m_kamus_kompetensi_level', $rowKerjasama->id)->get();
        $textIndikatorKerjasama = '';
        $i = 1;
        foreach ($indikatorKerjasama as $itemIndikator) {
            $textIndikatorKerjasama = $textIndikatorKerjasama."\n ".$i.".".$itemIndikator->indikator;
            $i++;
        }

        $rowKomunikasi = MKamusKompetensiLevel::find($komjab->id_m_kompetensi_level_komunikasi);
        $indikatorKomunikasi = MKamusKompetensiLevelIndikator::where('id_m_kamus_kompetensi_level', $rowKomunikasi->id)->get();
        $textIndikatorKomunikasi = '';
        $i = 1;
        foreach ($indikatorKomunikasi as $itemIndikator) {
            $textIndikatorKomunikasi = $textIndikatorKomunikasi."\n ".$i.".".$itemIndikator->indikator;
            $i++;
        }

        $rowOrientasi = MKamusKompetensiLevel::find($komjab->id_m_kompetensi_level_orientasi);
        $indikatorOrientasi = MKamusKompetensiLevelIndikator::where('id_m_kamus_kompetensi_level', $rowOrientasi->id)->get();
        $textIndikatorOrientasi = '';
        $i = 1;
        foreach ($indikatorOrientasi as $itemIndikator) {
            $textIndikatorOrientasi = $textIndikatorOrientasi."\n ".$i.".".$itemIndikator->indikator;
            $i++;
        }

        $rowPelayanan = MKamusKompetensiLevel::find($komjab->id_m_kompetensi_level_pelayanan);
        $indikatorPelayanan = MKamusKompetensiLevelIndikator::where('id_m_kamus_kompetensi_level', $rowPelayanan->id)->get();
        $textIndikatorPelayanan = '';
        $i = 1;
        foreach ($indikatorPelayanan as $itemIndikator) {
            $textIndikatorPelayanan = $textIndikatorPelayanan."\n ".$i.".".$itemIndikator->indikator;
            $i++;
        }

        $rowPerubahan = MKamusKompetensiLevel::find($komjab->id_m_kompetensi_level_perubahan);
        $indikatorPerubahan = MKamusKompetensiLevelIndikator::where('id_m_kamus_kompetensi_level', $rowPerubahan->id)->get();
        $textIndikatorPerubahan = '';
        $i = 1;
        foreach ($indikatorPerubahan as $itemIndikator) {
            $textIndikatorPerubahan = $textIndikatorPerubahan."\n ".$i.".".$itemIndikator->indikator;
            $i++;
        }

        $rowKeputusan = MKamusKompetensiLevel::find($komjab->id_m_kompetensi_level_keputusan);
        $indikatorKeputusan = MKamusKompetensiLevelIndikator::where('id_m_kamus_kompetensi_level', $rowKeputusan->id)->get();
        $textIndikatorKeputusan = '';
        $i = 1;
        foreach ($indikatorKeputusan as $itemIndikator) {
            $teKeputusanorPerubahan = $textIndikatorKeputusan."\n ".$i.".".$itemIndikator->indikator;
            $i++;
        }

        $rowPerekat = MKamusKompetensiLevel::find($komjab->id_m_kompetensi_level_perekat_bangsa);
        $indikatorPerekat = MKamusKompetensiLevelIndikator::where('id_m_kamus_kompetensi_level', $rowPerekat->id)->get();
        $textIndikatorPerekat = '';
        $i = 1;
        foreach ($indikatorPerekat as $itemIndikator) {
            $textIndikatorPerekat = $textIndikatorPerekat."\n ".$i.".".$itemIndikator->indikator;
            $i++;
        }

        
        // $row_pelayanan = MKompetensiLevelPelayanan::where('id',$komjab->id_m_kompetensi_level_pelayanan)->get();
        // $row_perubahan = MKompetensiLevelPerubahan::where('id',$komjab->id_m_kompetensi_level_perubahan)->get();
        // $row_keputusan = MKompetensiLevelKeputusan::where('id',$komjab->id_m_kompetensi_level_keputusan)->get();
        // $row_perekat = MKompetensiLevelPerekatBangsa::where('id',$komjab->id_m_kompetensi_level_perekat_bangsa)->get();

        
        $data = [
            'nama_jabatan' => $anjab->jabatan->nama,
			'ikhtisar_jabatan' => $anjab->ikhtisar_jabatan,
			'kelompok_jabatan' => $komjab->kelompok_jabatan,
			'urusan_pemerintah' => $komjab->urusan_pemerintah,
			'level_integeritas' => $rowIntegritas->level,
            'row_deskripsi_integeritas' => $rowIntegritas->deskripsi,
            'row_indikator_kompetensi_integeritas' => $textIndikatorIntegritas,
			 
            'level_kerjasama' => $rowKerjasama->level,
            'row_deskripsi_kerjasama' => $rowKerjasama->deskripsi,
            'row_indikator_kompetensi_kerjasama' => $textIndikatorKerjasama,

            'level_komunikasi' => $rowKomunikasi->level,
            'row_deskripsi_komunikasi' => $rowKomunikasi->deskripsi,
            'row_indikator_kompetensi_komunikasi' => $textIndikatorKomunikasi,

            'level_orientasi' => $rowOrientasi->level,
            'row_deskripsi_orientasi' => $rowOrientasi->deskripsi,
            'row_indikator_kompetensi_orientasi' => $textIndikatorOrientasi,

            'level_pelayanan' => $rowPelayanan->level,
            'row_deskripsi_pelayanan' => $rowPelayanan->deskripsi,
            'row_indikator_kompetensi_pelayanan' => $textIndikatorPelayanan,

            'level_perubahan' => $rowPerubahan->level,
            'row_deskripsi_perubahan' => $rowPerubahan->deskripsi,
            'row_indikator_kompetensi_perubahan' => $textIndikatorPerubahan,

            'level_keputusan' => $rowKeputusan->level,
            'row_deskripsi_keputusan' => $rowKeputusan->deskripsi,
            'row_indikator_kompetensi_keputusan' => $textIndikatorKeputusan,

            'level_perekat_bangsa' => $rowPerekat->level,
            'row_deskripsi_perekat_bangsa' => $rowPerekat->deskripsi,
            'row_indikator_kompetensi_perekat_bangsa' => $textIndikatorPerekat,

		];
        $templateProcessor->setValues($data);
        $templateProcessor->saveAs('anjab/Komjab '.$anjab->jabatan->nama.' - '.$anjab->dataOpd->unitkerja.'.docx');

		$headers = [
          	'Content-Disposition' => 'attachment',
          	'filename' => 'Komjab '.$anjab->jabatan->nama.' - '.$anjab->dataOpd->unitkerja.'.docx'
       	];

       	return redirect()->to(asset('anjab/Komjab '.$anjab->jabatan->nama.' - '.$anjab->dataOpd->unitkerja.'.docx'));


    }


    function getData(Request $request){
    
        if (isset($request->id)) {
            $id = $request->id;
            $kompetensi = $request->kompetensi;
            // $komjab = TrxKomjab::where($jenis,$id)->get()->first();
            $data['urutan'] = $request->urutan;
            $data['kompetensi'] = $kompetensi;
            $data['level'] = MKamusKompetensilevel::find($id);

            $data['levelindikator'] = MKamusKompetensiLevelIndikator::where('id_m_kamus_kompetensi_level',$id)->get();

            // $data['komjab'] = TrxKomjab::where($jenis,$id)->get()->first();
            return view('anjab.kompetensi_jabatan.level-page',$data);
        }

    }
    function getDataTeknis(Request $request){
        if (isset($request->level)) {
            $level = $request->level;
            $id_komjab = $request->id_komjab;
            $data['komjabTeknis'] = TrxKomjabTeknis::where('id_trx_komjab',$id_komjab)->get();
            $data['komjabTeknisIndikator'] = TrxKomjabTeknisIndikator::where('id_trx_komjab_teknis',$data['komjabTeknis']->id)->get();
            return view('anjab.kompetensi_jabatan.teknis-page',$data);
        }

    }
}
