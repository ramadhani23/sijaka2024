<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\MJabatan;

class JabatanController extends Controller
{
    function browse(Request $request){

    	$data['section'] = "jabatan";
    	$data['page_section'] = "jabatan";
    	$data['page'] = "Data Jabatan";

    	$jabatan = MJabatan::query();

    	if($request->keyword){
    		$jabatan = $jabatan->where('jabatan', 'LIKE', '%'.$request->keyword.'%');
    	}

    	$jabatan = $jabatan->orderBy('jabatan', 'ASC')->paginate(50);

    	$data['jabatan'] = $jabatan;
    	return view('master-jabatan.browse', $data);
    }

    function getForm(Request $request){
    	if($request->aksi == 'create-jabatan'){
    		return view('master-jabatan.form-create');
    	}elseif($request->aksi == 'edit-jabatan'){
    		$jabatan = MJabatan::findOrFail($request->id);

    		$data = [
    			'jabatan' => $jabatan
    		];
    		return view('master-jabatan.form-edit', $data);
    	}
    }

    function save(Request $request){

    	if($request->aksi == 'add-jabatan'){
    		$jabatan = new MJabatan;

    		$jabatan->nama = $request->nama;
    		$jabatan->kualifikasi_pendidikan = $request->kualifikasi_pendidikan;
    		$jabatan->tugas_jabatan = $request->tugas_jabatan;
            $jabatan->diklat_penjenjangan = $request->diklat_penjenjangan;
    		$jabatan->dasar_hukum = $request->dasar_hukum;

			// return $jabatan;
    		$jabatan->save();

    		return redirect()->back()->with('notify', 'Jabatan berhasil ditambahkan');
    	}elseif ($request->aksi == 'update-jabatan') {
    		$jabatan = MJabatan::findOrFail($request->id);

    		$jabatan->nama = $request->nama;
    		$jabatan->kualifikasi_pendidikan = $request->kualifikasi_pendidikan;
    		$jabatan->tugas_jabatan = $request->tugas_jabatan;
            $jabatan->diklat_penjenjangan = $request->diklat_penjenjangan;
    		$jabatan->dasar_hukum = $request->dasar_hukum;

    		$jabatan->update();

    		return redirect()->back()->with('notify', 'Jabatan berhasil diperbarui');
    	}
    }

    function delete(Request $request){
    	$jabatan = MJabatan::findOrFail($request->id);

    	if($jabatan->dataAnjab){
    		return redirect()->back()->with('error', 'Jabatan tidak bisa dihapus, digunakan untuk Analisa Jabatan');
    	}

    	$jabatan->delete();

    	return redirect()->back()->with('notify', 'Jabatan berhasil dihapus');
    }
}
