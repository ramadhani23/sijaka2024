<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabBahanKerja;
use DB;

class AnjabBahanKerjaController extends Controller
{
    function browse($id){

    	$data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "Detail Anjab";
    	$data['pages'] = "detail_bahan_kerja";
    	$data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['bahan_kerja'] = TrxAnjabBahanKerja::where('trx_anjab_id', $id)->orderBy('id', 'asc')->paginate(10);
    	return view('anjab.anjab-detail-bahan-kerja', $data);

    }

    function getForm(Request $request){
    	if($request->aksi == 'add-data'){
            $data['id'] = $request->id;
            
    		return view('anjab.bahan_kerja.form-create-bahan-kerja', $data);
    	}elseif ($request->aksi == 'edit-data') {
            $data['bahan_kerja'] = TrxAnjabBahanKerja::findOrFail($request->id);
    		return view('anjab.bahan_kerja.form-edit-bahan-kerja', $data);
    	}
    }

     function save(Request $request){
        if($request->aksi == 'create'){
            $bahan_kerja = new TrxAnjabBahanKerja;

            $bahan_kerja->bahan_kerja = $request->bahan_kerja;
            $bahan_kerja->penggunaan_dalam_tugas = $request->penggunaan_dalam_tugas;
            $bahan_kerja->trx_anjab_id = $request->id;
            $bahan_kerja->created_at = date('Y-m-d H:i:s');
            $bahan_kerja->save();
            return redirect()->route('anjab.browse-bahan-kerja',$request->id)->with('notify', 'Anjab berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {
            $bahan_kerja = TrxAnjabBahanKerja::findOrFail($request->id);

            $bahan_kerja->bahan_kerja = $request->bahan_kerja;
            $bahan_kerja->penggunaan_dalam_tugas = $request->penggunaan_dalam_tugas;

            $bahan_kerja->update();
            return redirect()->route('anjab.browse-bahan-kerja',$request->id_anjab)->with('notify', 'Anjab berhasil diperbarui');
        }
    }

    function deleteBahanKerja(Request $request){
        $bahan_kerja = TrxAnjabBahanKerja::findOrFail($request->id);
        $bahan_kerja->delete();
        return redirect()->back()->with('notify', 'bahan_kerja berhasil dihapus');
    }
   
}
