<?php

namespace App\Http\Controllers;

// use Illubakate\Http\Request;
use App\Models\MBakatKerja;
use Illuminate\Http\Request;

class BakatKerjaController extends Controller
{
    function browse(Request $request){

    	$data['section'] = "bakat-kerja";

    	$data['page_section'] = "bakat-kerja";
    	$data['page'] = "Data bakat Kerja";

    	$bakatkerja = MBakatKerja::query();

    	if($request->keyword){
    		$bakatkerja = $bakatkerja->where('nama', 'LIKE', '%'.$request->keyword.'%');
    	}

    	$bakatkerja = $bakatkerja->orderBy('nama', 'ASC')->paginate(20);
        // return $bakatkerja;
    	$data['bakatkerja'] = $bakatkerja;

    	return view('master_bakat_kerja.browse', $data);
    }

    function getForm(Request $request){
    	if($request->aksi == 'create-bakatkerja'){
    		return view('master_bakat_kerja.form-create');
    	}elseif($request->aksi == 'edit-bakatkerja'){
    		$bakatkerja = MBakatKerja::findOrFail($request->id);

    		$data = [
    			'bakatkerja' => $bakatkerja
    		];
    		return view('master_bakat_kerja.form-edit', $data);
    	}
    }

    function save(Request $request){

    	if($request->aksi == 'add-bakatkerja'){
    		$bakatkerja = new MBakatKerja;

    		$bakatkerja->nama = $request->nama;
    		$bakatkerja->keterangan = $request->keterangan;

    		$bakatkerja->save();

    		return redirect()->back()->with('notify', 'bakat kerja berhasil ditambahkan');
    	}elseif ($request->aksi == 'update-bakatkerja') {
    		$bakatkerja = MBakatKerja::findOrFail($request->id);

    		$bakatkerja->nama = $request->nama;
    		$bakatkerja->keterangan = $request->keterangan;
    		$bakatkerja->update();

    		return redirect()->back()->with('notify', 'bakat kerja berhasil diperbarui');
    	}
    }

    function delete(Request $request){
    	$bakatkerja = MBakatKerja::findOrFail($request->id);

    	// if($bakatkerja->dataAnjab){
    	// 	return redirect()->back()->with('error', 'bakatkerja tidak bisa dihapus, digunakan untuk Analisa bakatkerja');
    	// }

    	$bakatkerja->delete();

    	return redirect()->back()->with('notify', 'bakat kerja berhasil dihapus');
    }
}
