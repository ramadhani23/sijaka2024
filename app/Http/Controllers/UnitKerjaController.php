<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MUnitKerja;
use DB;

class UnitKerjaController extends Controller
{
    function browse(Request $request){

    	$data['section'] = "unit-kerja";
    	$data['page_section'] = "unit-kerja";
    	$data['page'] = "Data Unit Kerja";

    	$jabatan = MUnitKerja::query();
    	$jabatan = $jabatan->orderBy('kodeunit', 'ASC')->get();

    	$data['jabatan'] = $jabatan;
    	return view('master_unit_kerja.browse', $data);
    }

    function getForm(Request $request){
    	if($request->aksi == 'create-jabatan'){
    		return view('master_unit_kerja.form-create');
    	}elseif($request->aksi == 'edit-jabatan'){
    		$data['jabatan'] = MUnitKerja::where('kodeunit',$request->id)->first();

    		// $data = [
    		// 	'jabatan' => $jabatan
    		// ];
    		return view('master_unit_kerja.form-edit', $data);
    	}
    }

    function save(Request $request){

    	if($request->aksi == 'add-jabatan'){
    		$jabatan = new MUnitKerja;

    		$jabatan->kodeunit = $request->kodeunit;
    		$jabatan->unitkerja = $request->unitkerja;

    		$jabatan->save();

    		return redirect()->back()->with('notify', 'Unit kerja berhasil ditambahkan');
    	}elseif ($request->aksi == 'update-jabatan') {
			// return $request;
			DB::table('m_unit_kerja_2')
				->where('kodeunit',$request->id)
				->update([
					'kodeunit' => $request->kodeunit,
					'unitkerja' => $request->unitkerja

				]);
    		// $jabatan = MUnitKerja::where('kodeunit',$request->id);

    		// $jabatan->kodeunit = $request->kodeunit;
    		// $jabatan->unitkerja = $request->unitkerja;

    		// $jabatan->update();

    		return redirect()->back()->with('notify', 'Unit kerja berhasil diperbarui');
    	}
    }

    function delete(Request $request){
    	DB::table('m_unitkerja')
				->where('kodeunit',$request->id)
				->delete();

    	return redirect()->back()->with('notify', 'Unit kerja berhasil dihapus');
    }
}
