<?php

namespace App\Http\Controllers;
use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\MUnitKerja;
use App\Models\TrxAnjab;
use App\Models\MTrxKomjab;
use App\Models\MTrxKomjabTeknis;
use App\Models\MKamusKompetensiLevel;
use App\Models\MTrxKomjabTeknisIndikator;
use App\Models\MKamusKompetensiLevelIndikator;

use DB;

class AnjabInputKomjabController extends Controller
{
    function input($id){
        $data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "Standard Kompetensi Jabatan";
    	$data['pages'] = "detail_input_komjab";
        $data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['komjab'] = MTrxKomjab::where('trx_anjab_id',$id)->get()->first();
    	$data['anjabAll'] = TrxAnjab::all();
    	$data['level_kompetensi_integeritas'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',1)->get();
    	$data['level_kompetensi_kerjasama'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',2)->get();
    	$data['level_kompetensi_komunikasi'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',3)->get();
    	$data['level_kompetensi_orientasi'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',4)->get();
    	$data['level_kompetensi_pelayanan'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',5)->get();
    	$data['level_kompetensi_pengembangan'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',6)->get();
    	$data['level_kompetensi_perubahan'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',7)->get();
    	$data['level_kompetensi_keputusan'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',8)->get();
    	$data['level_kompetensi_perekat'] = MKamusKompetensiLevel::where('id_m_kamus_kompetensi',9)->get();
    	return view('anjab.kompetensi_jabatan.input-komjab', $data);
    }

	function save(Request $request){
        if($request->type == 'save'){

            $komjab = new MTrxKomjab;
            $komjab->trx_anjab_id = $request->id_jabatan;
            $komjab->kelompok_jabatan = $request->kelompok_jabatan;
            $komjab->urusan_pemerintah = $request->urusan_pemerintah;
            $komjab->id_m_kompetensi_level_integritas = 1;
            $komjab->id_m_kompetensi_level_kerjasama = 2;
            $komjab->id_m_kompetensi_level_komunikasi = 3;
            $komjab->id_m_kompetensi_level_orientasi = 4;
            $komjab->id_m_kompetensi_level_pelayanan = 1;
            $komjab->id_m_kompetensi_level_pengembangan = 3;
            $komjab->id_m_kompetensi_level_perubahan = 1;
            $komjab->id_m_kompetensi_level_keputusan = 1;
            $komjab->id_m_kompetensi_level_perekat_bangsa = 4;

            // $komjab->id_m_kompetensi_level_integritas = $request->level_integeritas;
            // $komjab->id_m_kompetensi_level_kerjasama = $request->level_kerjasama;
            // $komjab->id_m_kompetensi_level_komunikasi = $request->level_komunikasi;
            // $komjab->id_m_kompetensi_level_orientasi = $request->level_orientasi;
            // $komjab->id_m_kompetensi_level_pelayanan = $request->level_pelayanan;
            // $komjab->id_m_kompetensi_level_pengembangan = $request->level_pengembangan;
            // $komjab->id_m_kompetensi_level_perubahan = $request->level_perubahan;
            // $komjab->id_m_kompetensi_level_keputusan = $request->level_keputusan;
            // $komjab->id_m_kompetensi_level_perekat_bangsa = $request->level_perekat;
            $saved = $komjab->save();
    
            // if($saved){
            //     for ($i=0; $i < $request->count_index+1; $i++) { 
            //         $level_teknis = 'level_teknis_'.$i ;
            //         if ($request->$level_teknis != '') {
            //             $teknis = new MTrxKomjabTeknis;
                        
            //             $kompetensi = 'kompetensi_'.$i ;
            //             $deskripsi = 'deskripsi_teknis_'.$i ;
            //             $teknis->level = $request->$level_teknis;
            //             $teknis->nama_kompetensi = $request->$kompetensi;
            //             $teknis->deskripsi = $request->$deskripsi;
            //             $teknis->id_trx_komjab = $komjab->id;
            //             $teknis->save();
            //             $indikator_kompetensi = 'indikator_kompetensi_'.$i;
            //             foreach($request->$indikator_kompetensi as $idx=>$kompetensi){
            //                 $indikator = new MTrxKomjabTeknisIndikator;
            //                 $indikator->id_trx_komjab_teknis = $teknis->id;
            //                 $indikator->indikator = $kompetensi;
            //                 $indikator->save();
            //             }
            //         }
                   
            //     }
            // }
            return redirect()->route('komjab.input',$request->id_jabatan)->with('notify','Kompetensi jabatan berhasil diperbarui!');
        }elseif($request->type == 'update'){
            $komjab = MTrxKomjab::find($request->id_komjab);
            $komjab->trx_anjab_id = $request->id_jabatan;
            $komjab->kelompok_jabatan = $request->kelompok_jabatan;
            $komjab->urusan_pemerintah = $request->urusan_pemerintah;
            $komjab->id_m_kompetensi_level_integritas = 1;
            $komjab->id_m_kompetensi_level_kerjasama = 2;
            $komjab->id_m_kompetensi_level_komunikasi = 3;
            $komjab->id_m_kompetensi_level_orientasi = 4;
            $komjab->id_m_kompetensi_level_pelayanan = 1;
            $komjab->id_m_kompetensi_level_pengembangan = 3;
            $komjab->id_m_kompetensi_level_perubahan = 1;
            $komjab->id_m_kompetensi_level_keputusan = 1;
            $komjab->id_m_kompetensi_level_perekat_bangsa = 4;
            // $komjab->id_m_kompetensi_level_integritas = $request->level_integeritas;
            // $komjab->id_m_kompetensi_level_kerjasama = $request->level_kerjasama;
            // $komjab->id_m_kompetensi_level_komunikasi = $request->level_komunikasi;
            // $komjab->id_m_kompetensi_level_orientasi = $request->level_orientasi;
            // $komjab->id_m_kompetensi_level_pelayanan = $request->level_pelayanan;
            // $komjab->id_m_kompetensi_level_pengembangan = $request->level_pengembangan;
            // $komjab->id_m_kompetensi_level_perubahan = $request->level_perubahan;
            // $komjab->id_m_kompetensi_level_keputusan = $request->level_keputusan;
            // $komjab->id_m_kompetensi_level_perekat_bangsa = $request->level_perekat;
            $saved = $komjab->update();
           
            // if($saved){
            //     $komjab_teknis = MTrxKomjabTeknis::where('id_trx_komjab',$request->id_komjab)->get();
            //     foreach($komjab_teknis as $komTek){
            //         $indikatorOld = MTrxKomjabTeknisIndikator::where('id_trx_komjab_teknis',$komTek->id)->delete();
            //     }
            //     $komjabTeknisOld = MTrxKomjabTeknis::where('id_trx_komjab',$request->id_komjab)->delete();
                
            //     for ($i=0; $i<= $request->count_index ; $i++) { 

            //         $level_teknis = 'level_teknis_'.$i ;
                   
            //         if ($request->$level_teknis != '') {
            //             $teknis = new MTrxKomjabTeknis;
                        
            //             $kompetensi = 'kompetensi_'.$i ;
            //             $deskripsi = 'deskripsi_teknis_'.$i ;
            //             $teknis->level = $request->$level_teknis;
            //             $teknis->nama_kompetensi = $request->$kompetensi;
            //             $teknis->deskripsi = $request->$deskripsi;
            //             $teknis->id_trx_komjab = $komjab->id;
            //             $teknis->save();
            //             $indikator_kompetensi = 'indikator_kompetensi_'.$i;
                           
            //             foreach($request->$indikator_kompetensi as $idx=>$kompetensi){
            //                 $indikator = new MTrxKomjabTeknisIndikator;
            //                 $indikator->id_trx_komjab_teknis = $teknis->id;
            //                 $indikator->indikator = $kompetensi;
            //                 $indikator->save();
            //             }
            //         }
            //     }
            // }
            return redirect()->route('komjab.input',$request->id_jabatan)->with('notify','Kompetensi jabatan berhasil diperbarui!');
        }elseif ($request->aksi == 'create-teknis') {
            $teknis = new MTrxKomjabTeknis;
                        
            $teknis->level = $request->level_teknis;
            $teknis->nama_kompetensi = $request->nama_kompetensi;
            $teknis->deskripsi = $request->deskripsi;
            $teknis->id_trx_komjab = $request->id_komjab;
            $teknis->save();

            return redirect()->route('komjab.input',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil ditambah!');
        }elseif ($request->aksi == 'update-teknis') {

            $teknis = MTrxKomjabTeknis::find($request->id);
                        
            $teknis->level = $request->level_teknis;
            $teknis->nama_kompetensi = $request->nama_kompetensi;
            $teknis->deskripsi = $request->deskripsi;
            $teknis->id_trx_komjab = $request->id_komjab;

            $teknis->update();

            return redirect()->route('komjab.input',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil diperbarui!');
        }elseif ($request->aksi == 'create-indikator') {
            $indikator = new MTrxKomjabTeknisIndikator;
            $indikator->id_trx_komjab_teknis = $request->id;
            $indikator->indikator = $request->indikator;
            $indikator->save();

            return redirect()->route('komjab.input',$request->id_anjab)->with('notify','Kompetensi jabatan berhasil ditambah!');
        }

		

	}

    function getForm(Request $request){
        if($request->aksi == 'add-teknis'){
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            return view('anjab.kompetensi_jabatan.form-create-teknis', $data);
        }elseif ($request->aksi == 'edit-teknis') {
            $data['komjab_teknis'] = MTrxKomjabTeknis::findOrFail($request->id);
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            $data['id'] = $request->id;
            
            return view('anjab.kompetensi_jabatan.form-edit-teknis', $data);
        }elseif ($request->aksi == 'add-indikator') {
            $data['komjab_teknis'] = MTrxKomjabTeknis::findOrFail($request->id);
            $data['id_komjab'] = $request->id_komjab;
            $data['id_anjab'] = $request->id_anjab;
            $data['id_teknis'] = $request->id_teknis;
            
            return view('anjab.kompetensi_jabatan.form-create-indikator', $data);
        }
    }

    function delete(Request $request){
        if($request->aksi == 'delete-teknis'){
            $teknis = MTrxKomjabTeknis::findOrFail($request->id);
            $teknis->delete();
            return redirect()->back()->with('notify', 'Teknis berhasil dihapus');
        }
        
    }

    function printWord(Request $request, $id){
        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
    	$domPdfPath = base_path('vendor/dompdf/dompdf');
        \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
        \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');

    	$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('template-komjab.docx');

        $komjab = MTrxKomjab::findOrFail($id);
        $id_anjab = MTrxKomjab::where('id',$id)->get()->first()->pluck('trx_anjab_id');
     
        $anjab = TrxAnjab::where('id',$komjab->trx_anjab_id)->get()->first();

        $komjab_teknis_level = $komjab->komjabTeknis?$komjab->komjabTeknis:[];
        $templateProcessor->cloneRow('row_kompetensi_komjab_teknis', $komjab_teknis_level->count());
      
        $i= 10;
        $j= 1;
        $data_row =[];
        foreach($komjab->komjabTeknis as $itemKomjabTeknis){
            $templateProcessor->setValue('row_kompetensi_komjab_teknis#'.$j, $i.'. '.@$itemKomjabTeknis->nama_kompetensi);
            $templateProcessor->setValue('row_level_teknis_komjab#'.$j, @$itemKomjabTeknis->level);
            $templateProcessor->setValue('row_deskripsi_komjab_teknis#'.$j, @$itemKomjabTeknis->deskripsi);
            $k = 1;
            $data_row = [];
            foreach($itemKomjabTeknis->indikatorTeknis as $itemIndikator){
                $data ='4.'.$k.' '.@$itemIndikator->indikator;
                $data_row[] = $data;
                $k++;
            }
            $result = '';
            foreach($data_row as $row){
                $result .= "\r\n".$row."\r\n"; 
            }
            $templateProcessor->setValue('row_indikator_kompetensi_komjab_teknis#'.$j, $result);
            $i++;
            $j++;
        }

        // $row_integeritas = MKompetensiLevelIntegeritas::where('id',$komjab->id_m_kompetensi_level_integritas)->get();
        // $row_kerjasama = MKompetensiLevelKerjasama::where('id',$komjab->id_m_kompetensi_level_kerjasama)->get();
        // $row_komunikasi = MKompetensiLevelKomunikasi::where('id',$komjab->id_m_kompetensi_level_komunikasi)->get();
        // $row_orientasi = MKompetensiLevelOrientasi::where('id',$komjab->id_m_kompetensi_level_komunikasi)->get();
        // $row_pelayanan = MKompetensiLevelPelayanan::where('id',$komjab->id_m_kompetensi_level_pelayanan)->get();
        // $row_perubahan = MKompetensiLevelPerubahan::where('id',$komjab->id_m_kompetensi_level_perubahan)->get();
        // $row_keputusan = MKompetensiLevelKeputusan::where('id',$komjab->id_m_kompetensi_level_keputusan)->get();
        // $row_perekat = MKompetensiLevelPerekatBangsa::where('id',$komjab->id_m_kompetensi_level_perekat_bangsa)->get();

        
        $data = [
            'nama_jabatan' => $anjab->jabatan->nama,
			'ikhtisar_jabatan' => $anjab->ikhtisar_jabatan,
			'kelompok_jabatan' => $komjab->kelompok_jabatan,
			'urusan_pemerintah' => $komjab->urusan_pemerintah,
			'level_integeritas' => $komjab->id_m_kompetensi_level_integritas,
			'level_kerjasama' => $komjab->id_m_kompetensi_level_kerjasama,
			'level_komunikasi' => $komjab->id_m_kompetensi_level_komunikasi,
			'level_orientasi' => $komjab->id_m_kompetensi_level_orientasi,
			'level_pelayanan' => $komjab->id_m_kompetensi_level_pelayanan,
			'level_perubahan' => $komjab->id_m_kompetensi_level_perubahan,
			'level_keputusan' => $komjab->id_m_kompetensi_level_keputusan,
			'level_perekat_bangsa' => $komjab->id_m_kompetensi_level_perekat_bangsa


		];
        $templateProcessor->setValues($data);
        $templateProcessor->saveAs('anjab/Komjab '.$anjab->jabatan->nama.' - '.$anjab->dataOpd->unitkerja.'.docx');

		$headers = [
          	'Content-Disposition' => 'attachment',
          	'filename' => 'Komjab '.$anjab->jabatan->nama.' - '.$anjab->dataOpd->unitkerja.'.docx'
       	];

       	return redirect()->to(asset('anjab/Komjab '.$anjab->jabatan->nama.' - '.$anjab->dataOpd->unitkerja.'.docx'));


    }


    function getData(Request $request){
    
        if (isset($request->id)) {
            $id = $request->id;
            $kompetensi = $request->kompetensi;
            // $komjab = MTrxKomjab::where($jenis,$id)->get()->first();
            $data['urutan'] = $request->urutan;
            $data['kompetensi'] = $kompetensi;
            $data['level'] = MKamusKompetensilevel::find($id);

            $data['levelindikator'] = MKamusKompetensiLevelIndikator::where('id_m_kamus_kompetensi_level',$id)->get();

            // $data['komjab'] = MTrxKomjab::where($jenis,$id)->get()->first();
            return view('anjab.kompetensi_jabatan.level-page',$data);
        }

    }
    function getDataTeknis(Request $request){
        if (isset($request->level)) {
            $level = $request->level;
            $id_komjab = $request->id_komjab;
            $data['komjabTeknis'] = MTrxKomjabTeknis::where('id_trx_komjab',$id_komjab)->get();
            $data['komjabTeknisIndikator'] = MTrxKomjabTeknisIndikator::where('id_trx_komjab_teknis',$data['komjabTeknis']->id)->get();
            return view('anjab.kompetensi_jabatan.teknis-page',$data);
        }

    }
}
