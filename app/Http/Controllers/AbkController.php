<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\TrxAnjab;
use App\Models\TrxAnjabAbk;
use App\Models\TrxAbkKhusus;
use App\Models\TrxAnjabUraianJabatan;
use App\Models\TrxAnjabHasilKerja;
use App\Models\MUnitKerja;
use App\Models\User;

use App\Repositories\UnitKerjaRepository;

class AbkController extends Controller
{

    function cekRelasi() {

        // Cari trx_anjab berdasarkan ID
        return $trxAnjab = TrxAnjab::with('dataAbk')->get();
        // // Kembalikan response dengan data yang berelasi
        // return response()->json($trxAnjab);

        
    }
    function browse(Request $request, $jenis, UnitKerjaRepository $unitKerjaRepo){
        // return $request;
        $data['section'] = "abk";

        $data['page'] = "Analisa Beban Kerja";
        $data['pages'] = "abk";

        $data['jenis'] = $jenis;
        // $data['unitKerja'] = MUnitkerja::whereRaw('LENGTH(kodeunit) = 2')->get();
        $user = \MojokertokabUser::getUser();

        $data['unitKerja'] = MUnitkerja::when($user->kode_opd === '28', function ($query) {
                return $query->whereRaw('LENGTH(kodeunit) = 4')
                            ->where('kodeunit', 'like', '28%')
                            ->where('unitkerja', 'like', '%puskesmas%');
            }, function ($query) {
                return $query->whereRaw('LENGTH(kodeunit) = 2');
            })
            ->get();

        // if ( $data['unitKerja'] == '01'){
        //     $unitkerja = substr($unitKerja, 0,6);
        // return $unitkerja;
        // }  elseif ($data['unitKerja'] == '28'){
        //     $unitK = \MojokertokabUser::getUser()->option->satker_id;
        //     if ($unitK == '2801') {
        //         $unitKerja = '28';
        //     } else {
        //         $unitKerja = \MojokertokabUser::getUser()->option->satker_id;
        //     }
        //     // echo 'unit='.$unitKerja.'<br> tempkode= '.$tempKodeOPD;
        //     // die();
        // }

        $anjab = TrxAnjab::query();
        $abk = TrxAnjab::query();

        $unitKerja = 0;

        if($jenis == 'struktural'){
            $data['page_section'] = "abk-struktural";
            $anjab = $anjab->whereDoesntHave('dataAbk')
                                ->whereDoesntHave('dataAbkKhusus')
                                ->where('jenis_jabatan', '1');

            $abk = $abk->where('jenis_jabatan', '1');

            // return $request->unit_kerja;
            if ($request->unit_kerja != null){
                $data['unit_kerja'] = $request->unit_kerja;
                // return $data;
            }

            if (
                \MojokertokabUser::getUser()->role == 'PD' && 
                \MojokertokabUser::getUser()->kode_opd !== '28'
            ){
                // return 'ok';
                // return User::where('option->satker_id', 'LIKE', '38%')->first();
                // return \MojokertokabUser::getUser();
                $unitKerja = \MojokertokabUser::getUser()->option->satker_id;
                $tempKodeOPD = substr($unitKerja, 0,2);
                
                if ($tempKodeOPD == '01'){
                    $unitKerja = substr($unitKerja, 0,6);
                }  elseif($tempKodeOPD == '28'){
                    // $unitKerja = \MojokertokabUser::getUser()->option->satker_id;
                    // $unitKerja=28;
                    $unitK = \MojokertokabUser::getUser()->option->satker_id;
                    if ($unitK == '2801') {
                        $unitKerja = '28';
                    } else {
                        $unitKerja = \MojokertokabUser::getUser()->option->satker_id;
                    }

                    // echo 'unit='.$unitKerja.'<br> tempkode= '.$tempKodeOPD;
                    // die();
                } else {
                    $unitKerja = $tempKodeOPD;
                }

                // return $unitKerja;

                // $abk = $abk->whereRaw('LENGTH(unit_kerja) > 2')->where('unit_kerja', 'like', $unitKerja .'%');
                $abk = $abk->where('unit_kerja', 'like', $unitKerja .'%');
                // dd($abk);
                
            }elseif(
                \MojokertokabUser::getUser()->role == 'ADMIN' || 
                (\MojokertokabUser::getUser()->role == 'PD' && \MojokertokabUser::getUser()->kode_opd === '28')
            ){
                // return $jenis;
                $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
                $data['unitKerja'] = $unitKerja;
                // $unitk= MUnitkerja::get();
                // $dataOlahunitK=[];
                // foreach($unitk as $untk){
                    
                    
                //     $kode=substr($untk->kodeunit, 0,2);
                //     // echo $kode
                //     if(substr($untk->kodeunit, 0, 2)=="01"){
                //         if (strlen($untk->kodeunit)==6) {
                //             $dataOlahunitK[]=[
                //                 "kodeunit"=>$untk->kodeunit,
                //                 "unitkerja"=>$untk->unitkerja
                //             ];
                //         }
                //     }else{
                //         if (strlen($untk->kodeunit)==2) {
                //             $kode=substr($untk->kodeunit, 0,2);
                //             $dataOlahunitK[]=[
                //                 "kodeunit"=>$untk->kodeunit,
                //                 "unitkerja"=>$untk->unitkerja
                //             ];
                //         }
                //     }
                // }
                // $data['unitKerja']=$dataOlahunitK;

                $unitKerja        = $request->unit_kerja;
                $jenis_jabatan    = $request->jenis_jabatan;
                if($unitKerja){
                    $abk = $abk->where('unit_kerja', 'like', $unitKerja .'%');
                }
                if($jenis_jabatan){
                    $abk = $abk->where('jenis_jabatan', 'LIKE', $jenis_jabatan.'%');
                    // return $abk;
                }
            }elseif(\MojokertokabUser::getUser()->role == 'KEMENDAGRI'){
                // return $jenis;
                $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
                $data['unitKerja'] = $unitKerja;
                $unitKerja        = $request->unit_kerja;
                $jenis_jabatan    = $request->jenis_jabatan;
                if($unitKerja){
                    $abk = $abk->where('unit_kerja', 'like', $unitKerja .'%');
                }
                if($jenis_jabatan){
                    $abk = $abk->where('jenis_jabatan', 'LIKE', $jenis_jabatan.'%');
                    // return $abk;
                }
            }
            // if(\MojokertokabUser::getUser()->role == 'PD'){

            //     if(strlen(\MojokertokabUser::getUser()->kode_opd) == 6){
            //         $anjab = $anjab->where('unit_kerja', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');
            //         $abk = $abk->where('unit_kerja', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');
            //     }else{
            //         $anjab = $anjab->where('kode_opd',  \MojokertokabUser::getUser()->kode_opd);

            //         $abk = $abk->where('kode_opd',  \MojokertokabUser::getUser()->kode_opd);
            //     }
            // }else{
            //     $abk = $abk->where('kode_opd',  $request->unit_kerja);
            // }
            if(\MojokertokabUser::getUser()->role == 'PD'|| $request->unit_kerja != NULL){
                $anjab = $anjab->paginate(50);
                $abk = $abk->paginate(50);
            }else{
                $anjab = $anjab->paginate(50);
                $abk = $abk->paginate(50);
            }

            // $data['anjab'] = $anjab;
            // $skj = TrxSkjUpload::all();

            // $data['skj'] = $skj;
            // return $abk;
            $data['abk'] = $abk;
            $data['anjab'] = $abk;
            

        }elseif($jenis == 'fungsional'){
            $data['page_section'] = "abk-fungsional";
            $anjab = $anjab->whereDoesntHave('dataAbk')
                                ->whereDoesntHave('dataAbkKhusus')
                                ->where('jenis_jabatan', '2');

            $abk = $abk->where('jenis_jabatan', '2');

            // return $request->unit_kerja;
            if ($request->unit_kerja != null){
                $data['unit_kerja'] = $request->unit_kerja;
                // return $data;
            }

            if (
                \MojokertokabUser::getUser()->role == 'PD' && 
                \MojokertokabUser::getUser()->kode_opd !== '28'
            ){
                
                // return User::where('option->satker_id', 'LIKE', '38%')->first();
                // return \MojokertokabUser::getUser();
                
                $unitKerja = \MojokertokabUser::getUser()->option->satker_id;

                $tempKodeOPD = substr($unitKerja, 0,2);

                if ($tempKodeOPD == '01'){
                    $unitKerja = substr($unitKerja, 0,6);
                }  elseif ($tempKodeOPD == '28'){
                    $unitK = \MojokertokabUser::getUser()->option->satker_id;
                    if ($unitK == '2801') {
                        $unitKerja = '28';
                    } else {
                        $unitKerja = \MojokertokabUser::getUser()->option->satker_id;
                    }
                    // echo 'unit='.$unitKerja.'<br> tempkode= '.$tempKodeOPD;
                    // die();
                // Jika unit kerja terdiri dari 6 angka, ambil 4 angka pertama
                if (strlen($unitKerja) == 6) {
                    $unitKerja = substr($unitKerja, 0, 4);
                }

                } else {
                    $unitKerja = $tempKodeOPD;
                }

                // return $unitKerja;

                $abk = $abk->where('unit_kerja', 'like', $unitKerja .'%');
                        //    ->whereNotNull('parent');
                // $abk->get();
                // return $abk->get();
                // $abk = $abk->whereRaw('LENGTH(unit_kerja) > 2')->where('unit_kerja', 'like', $unitKerja .'%');
            }elseif(
                \MojokertokabUser::getUser()->role == 'ADMIN' || 
                (\MojokertokabUser::getUser()->role == 'PD' && \MojokertokabUser::getUser()->kode_opd === '28')
            ){
                // dd('masuk');
                // $data['unitKerja'] = MUnitkerja::whereRaw('LENGTH(kodeunit) = 6')->get();
                $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
                $data['unitKerja'] = $unitKerja;

                // $unitk= MUnitkerja::get();
                // $dataOlahunitK=[];
                // foreach($unitk as $untk){
                    
                    
                //     $kode=substr($untk->kodeunit, 0,2);
                //     // echo $kode
                //     if(substr($untk->kodeunit, 0, 2)=="01"){
                //         if (strlen($untk->kodeunit)==6) {
                //             $dataOlahunitK[]=[
                //                 "kodeunit"=>$untk->kodeunit,
                //                 "unitkerja"=>$untk->unitkerja
                //             ];
                //         }
                //     }else{
                //         if (strlen($untk->kodeunit)==2) {
                //             $kode=substr($untk->kodeunit, 0,2);
                //             $dataOlahunitK[]=[
                //                 "kodeunit"=>$untk->kodeunit,
                //                 "unitkerja"=>$untk->unitkerja
                //             ];
                //         }
                //     }
                // }
                // $data['unitKerja']=$dataOlahunitK;

                // die;
                // return $data['unitKerja'];
                $unitKerja        = $request->unit_kerja;
                $jenis_jabatan    = $request->jenis_jabatan;
                // return $unitKerja;
                if ($unitKerja) {
                    if ($user->kode_opd === '28' && $unitKerja === '28') {
                        // Jika login dengan kode_opd '28' dan unitKerja '28', ambil data dengan kolom 'user' sesuai user login
                        $abk = $abk->where('user', \MojokertokabUser::getUser()->username); // Ganti 'user' dengan nama kolom yang sesuai
                    } else {
                        // Selain itu, gunakan like pada unit_kerja
                        $abk = $abk->where('unit_kerja', 'like', $unitKerja . '%');
                    }
                    // return $abk->get();
                }
                if($jenis_jabatan){
                    $abk = $abk->where('jenis_jabatan', 'LIKE', $jenis_jabatan.'%');
                }
            }elseif(\MojokertokabUser::getUser()->role == 'KEMENDAGRI'){
                // $data['unitKerja'] = MUnitkerja::whereRaw('LENGTH(kodeunit) = 6')->get();
                $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
                $data['unitKerja'] = $unitKerja;
                
                $unitKerja        = $request->unit_kerja;
                $jenis_jabatan    = $request->jenis_jabatan;
                // return $unitKerja;
                if($unitKerja){
                    $abk = $abk->where('unit_kerja', 'like', $unitKerja .'%');
                    // return $abk->get();
                }
                if($jenis_jabatan){
                    $abk = $abk->where('jenis_jabatan', 'LIKE', $jenis_jabatan.'%');
                    // return $abk;
                }
            }
            if(\MojokertokabUser::getUser()->role == 'PD'|| $request->unit_kerja != NULL){
                $anjab = $anjab->paginate(70);
                $abk = $abk->paginate(70);
            }else{
                $anjab = $anjab->paginate(70);
                $abk = $abk->paginate(70);
            }

            $data['abk'] = $abk;
            $data['anjab'] = $abk;
            // return $data;
        }elseif($jenis == 'pelaksana'){
            
            $data['page_section'] = "abk-pelaksana";
            $anjab = $anjab->whereDoesntHave('dataAbk')
                                ->whereDoesntHave('dataAbkKhusus')
                                ->where('jenis_jabatan', '3');

            $abk = $abk->where('jenis_jabatan', '3');
            // return $abk;
            if (
                \MojokertokabUser::getUser()->role == 'PD' && 
                \MojokertokabUser::getUser()->kode_opd !== '28'
            ){

                // return 'masuk';
                // return User::where('option->satker_id', 'LIKE', '38%')->first();
                // return \MojokertokabUser::getUser();
                $unitKerja = \MojokertokabUser::getUser()->option->satker_id;

                $tempKodeOPD = substr($unitKerja, 0,2);

                if ($tempKodeOPD == '01'){
                    $unitKerja = substr($unitKerja, 0,6);
                }  elseif($tempKodeOPD == '28'){
                    if ($unitKerja == '2801') {
                        $unitKerja = '28';
                    } else {
                        $unitKerja = \MojokertokabUser::getUser()->option->satker_id;
                    }
                // Jika unit kerja terdiri dari 6 angka, ambil 4 angka pertama
                if (strlen($unitKerja) == 6) {
                    $unitKerja = substr($unitKerja, 0, 4);
                }

                // dd($unitKerja); // Debugging
                } else {
                $unitKerja = $tempKodeOPD;
                }

                $abk = $abk->where('unit_kerja', 'like', $unitKerja .'%');
                // dd($abk->get());
                // $abk = $abk->whereRaw('LENGTH(unit_kerja) > 2')->where('unit_kerja', 'like', $unitKerja .'%');
            }elseif(
                \MojokertokabUser::getUser()->role == 'ADMIN' || 
                (\MojokertokabUser::getUser()->role == 'PD' && \MojokertokabUser::getUser()->kode_opd === '28')
            ){
                $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
                $data['unitKerja'] = $unitKerja;

                // $unitk= MUnitkerja::get();
                // $dataOlahunitK=[];
                // foreach($unitk as $untk){
                    
                    
                //     $kode=substr($untk->kodeunit, 0,2);
                //     // echo $kode
                //     if(substr($untk->kodeunit, 0, 2)=="01"){
                //         if (strlen($untk->kodeunit)==6) {
                //             $dataOlahunitK[]=[
                //                 "kodeunit"=>$untk->kodeunit,
                //                 "unitkerja"=>$untk->unitkerja
                //             ];
                //         }
                //     }else{
                //         if (strlen($untk->kodeunit)==2) {
                //             $kode=substr($untk->kodeunit, 0,2);
                //             $dataOlahunitK[]=[
                //                 "kodeunit"=>$untk->kodeunit,
                //                 "unitkerja"=>$untk->unitkerja
                //             ];
                //         }
                //     }
                // }
                // $data['unitKerja']=$dataOlahunitK;
                
                $unitKerja        = $request->unit_kerja;
                $jenis_jabatan    = $request->jenis_jabatan;
                if ($unitKerja) {
                    if ($user->kode_opd === '28' && $unitKerja === '28') {
                        // Jika login dengan kode_opd '28' dan unitKerja '28', ambil data dengan kolom 'user' sesuai user login
                        $abk = $abk->where('user', \MojokertokabUser::getUser()->username); // Ganti 'user' dengan nama kolom yang sesuai
                    } else {
                        // Selain itu, gunakan like pada unit_kerja
                        $abk = $abk->where('unit_kerja', 'like', $unitKerja . '%');
                    }
                    // return $abk->get();
                }
                if($jenis_jabatan){
                    $abk = $abk->where('jenis_jabatan', 'LIKE', $jenis_jabatan.'%');
                }
            }elseif(\MojokertokabUser::getUser()->role == 'KEMENDAGRI'){
                $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
                $data['unitKerja'] = $unitKerja;
                $unitKerja        = $request->unit_kerja;
                $jenis_jabatan    = $request->jenis_jabatan;
                if($unitKerja){
                    $abk = $abk->where('unit_kerja', 'like', $unitKerja .'%');
                }
                if($jenis_jabatan){
                    $abk = $abk->where('jenis_jabatan', 'LIKE', $jenis_jabatan.'%');
                }
            }
            if(\MojokertokabUser::getUser()->role == 'PD'|| $request->unit_kerja != NULL){
                $anjab = $anjab->paginate(70);
                $abk = $abk->paginate(70);
            }else{
                $anjab = $anjab->paginate(70);
                $abk = $abk->paginate(70);
            }

            $data['abk'] = $abk;
            $data['anjab'] = $abk;
            // return $data['abk'];
            
            // dd($data);

        }
        // return $data;
        return view('abk.browse', $data);
    }

    function pilihAnjab(Request $request){
        $data['section'] = "abk";

        $data['page'] = "Analisa Beban Kerja";
        $data['pages'] = "abk";

        return redirect()->route('abk.detail', $request->jabatan);
    }

    function detail($id,Request $request){
        // return $request;
        $data['section'] = "abk";

        $data['page'] = "Analisa Beban Kerja";
        $data['pages'] = "abk";
        $abk = TrxAnjabAbk::where('trx_anjab_id', $request->id)
                            ->with('dataTugasJabatan')
                            ->with(['dataAnjab.dataHasilKerja'])
                            ->get();

        $data['abk'] = $abk;
        $data['id'] = $request->id;

         // Tambahkan pengecekan parent
         $hasNoParent = TrxAnjab::where('id', $request->id)
        ->where(function($query) {
            $query->whereNull('parent') // Kondisi pertama: parent null
                ->orWhere(function($subQuery) { // Kondisi kedua: unit_kerja '2835' dan parent tidak null
                    $subQuery->where('unit_kerja', '2835')
                            ->whereNotNull('parent');
                });
        })
        ->exists();

$data['hasNoParent'] = $hasNoParent;
        // return $data;

        return view('abk.detail', $data);
    }

    public function getHasilKerja(Request $request) {
        // Ambil nilai key dari request
        $nilai = $request->key;
        $parts = explode('-', $nilai);
        $hasil = end($parts);
        $index = (int) $hasil; // Pastikan index dalam bentuk integer
    
        // Hitung jumlah data yang memiliki deleted_at di tabel Uraian Jabatan
        $jumlahDihapus = TrxAnjabUraianJabatan::where('trx_anjab_id', $request->hasilkerja)
                                              ->onlyTrashed() // Ambil hanya yang punya deleted_at
                                              ->count();
    
        // Kurangi index dengan jumlah data yang terhapus
        $indexTerkoreksi = max(0, $index - $jumlahDihapus);
    
        // Ambil data hasil kerja berdasarkan index yang sudah dikoreksi
        $data = TrxAnjabHasilKerja::where('trx_anjab_id', $request->hasilkerja)->get();
        
        if ($data->count() > 0 && isset($data[$indexTerkoreksi])) {
            $dataIndex = $data->get($indexTerkoreksi);
        } else {
            $dataIndex = null;
        }
    
        return $dataIndex;
    }
    

    function getForm(Request $request){
        if($request->aksi == 'add-data'){
            $currentAbk = TrxAnjabAbk::where('trx_anjab_id', $request->id)->pluck('trx_anjab_uraian_jabatan_id');
            $tugasJabatan = TrxAnjabUraianJabatan::where('trx_anjab_id', $request->id)->whereNotIn('id', $currentAbk)->get();
            $hasilKerja = TrxAnjabHasilKerja::where('trx_anjab_id', $request->id)->get();
            
            $data['id'] = $request->id;
            $data['tugasJabatan'] = $tugasJabatan;
            $data['hasilKerja'] = $hasilKerja;
            // return $data; 
            return view('abk.form-create', $data);
           
            
            
        }elseif ($request->aksi == 'edit-data') {
            // return $request;
            $abk = TrxAnjabAbk::findOrFail($request->id);
            $tugasJabatan = TrxAnjabUraianJabatan::where('id', $abk->trx_anjab_uraian_jabatan_id)->get();
            // $hasilKerja = TrxAnjabHasilKerja::where('trx_anjab_id', $request->id)->get();

            $data['abk'] = $abk;
            $data['tugasJabatan'] = $tugasJabatan;
            // $data['hasilKerja'] = $hasilKerja;
            // dd($data['abk']);
            // return $data;
            return view('abk.form-edit', $data);
        }elseif ($request->aksi == 'edit-abk-khusus') {
            $abk = TrxAbkKhusus::findOrFail($request->id);
            $anjab = TrxAnjab::query();

            if($request->jenis == 'struktural'){
                $anjab = $anjab->where(function($query){
                    return $query->whereDoesntHave('dataAbk')
                                    ->whereDoesntHave('dataAbkKhusus');
                })->orWhere('id', $abk->trx_anjab_id)->where('jenis_jabatan', '1');

                if(\MojokertokabUser::getUser()->role == 'PD'){

                    if(strlen(\MojokertokabUser::getUser()->kode_opd) == 6){
                        $anjab = $anjab->where('unit_kerja_eselon3', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');
                    }else{
                        $anjab = $anjab->where('kode_opd',  \MojokertokabUser::getUser()->kode_opd);
                    }
                }

                $anjab = $anjab->get();

                $data['abk'] = $abk;
                
                $data['anjab'] = $anjab;

            }elseif ($request->jenis == 'fungsional') {
                $anjab = TrxAnjab::query();

                $anjab = $anjab->where('jenis_jabatan', '!=', '1')->whereDoesntHave('dataAbk')->whereDoesntHave('dataAbkKhusus');

                if(\MojokertokabUser::getUser()->role == 'PD'){
                    // dd($anjab);
                    if(strlen(\MojokertokabUser::getUser()->kode_opd) == 6){
                        $anjab = $anjab->where('unit_kerja_eselon3', 'LIKE',  \MojokertokabUser::getUser()->kode_opd.'%');
                        
                    }else{
                        $anjab = $anjab->where('kode_opd',  \MojokertokabUser::getUser()->kode_opd)->get();
                    if (count($anjab)) {
                        
                $data['anjab'] = $anjab->paginate(25);
                    }
                    }
                }

                // dd($anjab);
                $data['abk'] = $abk;
                // dd($data['abk']);
                // dd($abk);
            }

            return view('abk.form-edit-khusus', $data);
        }
    }

    function save(Request $request){
        // $request;
        $nilai = $request->tugas_jabatan;
        $parts = explode('-', $nilai);
        $tugas_jabatan = $parts[0];
        
        if($request->aksi == 'create'){

            $abk = new TrxAnjabAbk;
            
            $kebutuhan_pegawai = ($request->jumlah_hasil * $request->waktu_penyelesaian) / $request->waktu_efektif;
            $kebutuhan_pegawai_2 = ($request-> jumlah_hasil * $request->waktu_penyelesaian) / 1350;

            $abk->trx_anjab_uraian_jabatan_id = $tugas_jabatan;
            $abk->hasil_kerja = $request->hasil_kerja;
            $abk->uraian_hasil = '';
            $abk->jumlah_hasil = $request->jumlah_hasil;
            $abk->satuan_hasil = $request->satuan_hasil;
            $abk->waktu_penyelesaian = $request->waktu_penyelesaian;
            $abk->waktu_efektif = $request->waktu_efektif;
            $abk->kebutuhan_pegawai = number_format($kebutuhan_pegawai,2);
            $abk->kebutuhan_pegawai_2 = number_format($kebutuhan_pegawai_2,2);
            $abk->trx_anjab_id = $request->id;
            $abk->created_at = date('Y-m-d H:i:s');
            // return $abk;
            $abk->save();
            return redirect()->route('abk.detail',$request->id)->with('notify', 'ABK berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {

            $abk = TrxAnjabAbk::findOrFail($request->id);

            $kebutuhan_pegawai = ($request->jumlah_hasil * $request->waktu_penyelesaian) / $request->waktu_efektif;
            $kebutuhan_pegawai_2 = ($request-> jumlah_hasil * $request->waktu_penyelesaian) / 1350;

            $abk->uraian_tugas = $tugas_jabatan;
            $abk->hasil_kerja = $request->hasil_kerja;
            $abk->uraian_hasil = '';
            $abk->jumlah_hasil = $request->jumlah_hasil;
            $abk->satuan_hasil = $request->satuan_hasil;
            $abk->waktu_penyelesaian = $request->waktu_penyelesaian;
            $abk->waktu_efektif = $request->waktu_efektif;
            $abk->kebutuhan_pegawai = number_format($kebutuhan_pegawai,2);

            $abk->update();
            return redirect()->back()->with('notify', 'ABK berhasil diperbarui');
        }elseif($request->aksi == 'create-khusus'){
            // dd('awdadw');
            // dd($request->all());
            $anjab = TrxAnjab::findOrFail($request->id);

            $abk = new TrxAbkKhusus;
            $abk->kebutuhan = $request->kebutuhan;
            $abk->trx_anjab_id = $request->id;

            if($request->hasfile('dokumen')){
                $file = $request->file('dokumen');
                if($file->getClientOriginalExtension() != 'pdf' && $file->getClientOriginalExtension() != 'xls' &&  $file->getClientOriginalExtension() != 'xlsx'){
                    return redirect()->back()->with('notify', 'Dokumen harus PDF / Excel');
                }
                $name = 'evjab-'.$anjab->nama.'-'.$anjab->id.'.'.$file->getClientOriginalExtension();
                $file->move('public/abk-khusus/', $name);
                $abk->dokumen_pendukung = $name;

                // dd($abk);
                $abk->save();
                return redirect()->back()->with('notify', 'Abk berhasil tersimpan');
            }

            // dd('1');


        }elseif($request->aksi == 'update-khusus'){
            // return $request;
            // dd($request);
            // $anjab = TrxAnjab::findOrFail($request->id);
            // dd($anjab);
            $abk = TrxAbkKhusus::findOrFail($request->id);
            // return $abk;

            $abk->kebutuhan = $request->kebutuhan;
            // $abk->trx_anjab_id = $request->id;
            // return $abk;

            if($request->hasfile('dokumen')){
                $file = $request->file('dokumen');
                if($file->getClientOriginalExtension() != 'pdf' && $file->getClientOriginalExtension() != 'xls' &&  $file->getClientOriginalExtension() != 'xlsx'){
                    return redirect()->back()->with('notify', 'Dokumen harus PDF/Excel');
                }
                $name = time().'-'.rand(1000, 9999).'.'.$file->getClientOriginalExtension();
                $file->move('public/abk-khusus/', $name);
                $abk->dokumen_pendukung = $name;
            }
            // dd($abk);
            $abk->update();
            return redirect()->back()->with('notify', 'Abk berhasil diperbarui');


        }
    }

    // function update(Request $request) {
    //     // dd($request);
    //     // $anjab = TrxAnjab::findOrFail($request->jabatan);

    //     $abk = TrxAbkKhusus::find($request->id);
    //     $files = $abk->dokumen;

    //     $abk->kebutuhan = $request->kebutuhan;
    //     $abk->trx_anjab_id = $request->id;
    //     // dd($abk);
    //     if($request->hasfile('dokumen')){
    //         $file = $request->file('dokumen');
    //         if($file->getClientOriginalExtension() != 'pdf'){
    //             return redirect()->back()->with('notify', 'Dokumen harus PDF');
    //         }
    //         $name = time().'-'.rand(1000, 9999).'.'.$file->getClientOriginalExtension();
    //         $file->move('abk-khusus/', $name);
    //         $abk->dokumen_pendukung = $name;
    //     }
        
    //     // dd($abk);
    //     $saved = $abk->update();
    //     return redirect()->back()->with('notify', 'Abk berhasil diperbarui');
    // }   

    function deleteAbk (Request $request){
        $abk = TrxAnjabAbk::findOrFail($request->id);
        $abk->delete();
        return redirect()->back()->with('notify', 'Hasil Kerja berhasil dihapus');
    }

    function verifikasiStrukturalPelaksana(Request $request){
        if ($request->aksi == 'verifikasi') {
            $abk = TrxAnjabAbk::where('trx_anjab_id', $request->id)->get();
            foreach ($abk as $value) {
                $update = TrxAnjabAbk::where('id', $value->id)->first();
                $update->status = 'verifikasi';
                $update->save();
            }
            return redirect()->back()->with('notify', 'ABK berhasil Diverifikasi');
        } else if ($request->aksi == 'batal') {
            // return $request;
            $abk = TrxAnjabAbk::where('trx_anjab_id', $request->id)->get();
            foreach ($abk as $value) {
                $update = TrxAnjabAbk::where('id', $value->id)->first();
                $update->status = null;
                $update->save();
            }
            return redirect()->back()->with('notify', 'Verifikasi ABK berhasil Dibatalkan');
        }
    }

    function deleteAbkKhusus (Request $request){
        $abk = TrxAbkKhusus::findOrFail($request->id);
        $dokumenPendukung = $abk->dokumen_pendukung;
        $abk->delete();

        unlink('public/abk-khusus/'.$dokumenPendukung);

        return redirect()->back()->with('notify', 'ABK berhasil dihapus');
    }

    function verifikasi(Request $request){
            // return $request->id;
            $abk = TrxAbkKhusus::findOrFail($request->id);
            $abk->status = "verifikasi";
            $abk->created_at = date('Y-m-d H:i:s');
            $abk->save();
            return redirect()->back()->with('notify', 'ABK berhasil Diverifikasi');
    }

    function batalverifikasi(Request $request){
        // return $request->id;
        $abk = TrxAbkKhusus::findOrFail($request->id);
        $abk->status = "belum_verifikasi";
        $abk->created_at = date('Y-m-d H:i:s');
        // return $abk;
        $abk->update();
        return redirect()->back()->with('notify', 'ABK Batal Diverifikasi');
}
}
