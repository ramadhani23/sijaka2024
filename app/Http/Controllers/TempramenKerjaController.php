<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MTemperamenKerja;

class TempramenKerjaController extends Controller
{
    function browse(Request $request){

    	$data['section'] = "tempramen-kerja";
    	$data['page_section'] = "tempramen-kerja";
    	$data['page'] = "Data Tempramen Kerja";

    	$jabatan = MTemperamenKerja::query();

    	if($request->keyword){
    		$jabatan = $jabatan->where('nama', 'LIKE', '%'.$request->keyword.'%');
    	}

    	$jabatan = $jabatan->orderBy('nama', 'ASC')->paginate(20);

    	$data['jabatan'] = $jabatan;
    	return view('master_tempramen_kerja.browse', $data);
    }

    function getForm(Request $request){
    	if($request->aksi == 'create-jabatan'){
    		return view('master_tempramen_kerja.form-create');
    	}elseif($request->aksi == 'edit-jabatan'){
    		$jabatan = MTemperamenKerja::findOrFail($request->id);

    		$data = [
    			'jabatan' => $jabatan
    		];
    		return view('master_tempramen_kerja.form-edit', $data);
    	}
    }

    function save(Request $request){

    	if($request->aksi == 'add-jabatan'){
    		$jabatan = new MTemperamenKerja;

    		$jabatan->nama = $request->nama;
    		$jabatan->keterangan = $request->keterangan;

    		$jabatan->save();

    		return redirect()->back()->with('notify', 'Tempramen Kerja berhasil ditambahkan');
    	}elseif ($request->aksi == 'update-jabatan') {
    		$jabatan = MTemperamenKerja::findOrFail($request->id);

    		$jabatan->nama = $request->nama;
    		$jabatan->keterangan = $request->keterangan;
    		$jabatan->update();

    		return redirect()->back()->with('notify', 'Tempramen Kerja berhasil diperbarui');
    	}
    }

    function delete(Request $request){
    	$jabatan = MTemperamenKerja::findOrFail($request->id);

    	// if($jabatan->dataAnjab){
    	// 	return redirect()->back()->with('error', 'Jabatan tidak bisa dihapus, digunakan untuk Analisa Jabatan');
    	// }

    	$jabatan->delete();

    	return redirect()->back()->with('notify', 'Tempramen Kerja berhasil dihapus');
    }
}
