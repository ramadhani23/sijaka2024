<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabResikoBahaya;
use DB;

class AnjabResikoBahayaController extends Controller
{
    function browse($id){

    	$data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "Detail Anjab";
    	$data['pages'] = "detail_resiko_bahaya";
    	$data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['resiko_bahaya'] = TrxAnjabResikoBahaya::where('trx_anjab_id', $id)->orderBy('id', 'asc')->paginate(10);
    	return view('anjab.anjab-detail-resiko-bahaya', $data);

    }

    function getForm(Request $request){
    	if($request->aksi == 'add-data'){
            $data['id'] = $request->id;
    		return view('anjab.resiko_bahaya.form-create-resiko-bahaya', $data);
    	}elseif ($request->aksi == 'edit-data') {
            $data['resiko_bahaya'] = TrxAnjabResikoBahaya::findOrFail($request->id);
    		return view('anjab.resiko_bahaya.form-edit-resiko-bahaya', $data);
    	}
    }

     function save(Request $request){
        if($request->aksi == 'create'){
            $resiko_bahaya = new TrxAnjabResikoBahaya;


            $resiko_bahaya->resiko_bahaya = $request->resiko_bahaya;
            $resiko_bahaya->penyebab = $request->penyebab;
            $resiko_bahaya->trx_anjab_id = $request->id;
            $resiko_bahaya->created_at = date('Y-m-d H:i:s');
            $resiko_bahaya->save();
            return redirect()->route('anjab.browse-resiko-bahaya',$request->id)->with('notify', 'Hasil Kerja berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {
            $resiko_bahaya = TrxAnjabResikoBahaya::findOrFail($request->id);

            $resiko_bahaya->resiko_bahaya = $request->resiko_bahaya;
            $resiko_bahaya->penyebab = $request->penyebab;

            $resiko_bahaya->update();
            return redirect()->route('anjab.browse-resiko-bahaya',$request->id_anjab)->with('notify', 'Hasil Kerja berhasil diperbarui');
        }
    }

    function deleteResiko(Request $request){
        $resiko_bahaya = TrxAnjabResikoBahaya::findOrFail($request->id);
        $resiko_bahaya->delete();
        return redirect()->back()->with('notify', 'Hasil Kerja berhasil dihapus');
    }
   
}
