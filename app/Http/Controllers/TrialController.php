<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Imports\JabatanImport;

class TrialController extends Controller
{
    public function import() 
{
	    \Excel::import(new JabatanImport, 'jabatan.xlsx');
	}
}
