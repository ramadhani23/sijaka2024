<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabUraianJabatan;
use App\Models\TrxAnjabUraianJabatanTahapan;
use App\Models\TrxAnjabAbk;
use App\Models\TrxAnjabBahanKerja;
use App\Models\TrxAnjabHasilKerja;
use App\Models\TrxAnjabKorelasiJabatan;
use App\Models\TrxAnjabLingkunganKerja;
use App\Models\TrxAnjabPerangkat;
use App\Models\TrxAnjabPrestasiKerja;
use App\Models\TrxAnjabResikoBahaya;
use App\Models\TrxAnjabSyaratJabatan;
use App\Models\TrxAnjabTanggungJawab;
use App\Models\TrxAnjabWewenang;
use App\Models\MUnitKerja;
use App\Models\MJabatan;
use App\Models\MBakatKerja;
use App\Models\MTemperamenKerja;
use App\Models\MMinatKerja;
use App\Models\MFungsiPekerjaan;
use App\Models\MJabatanFungsional;
use App\Models\MUpayaFisik;
use App\Models\MJabatanPelaksana;
use App\Models\MJabatanStruktural;
use App\Models\MJabatanUpt;
use App\Models\MJabatanFungsionalJenjang;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

use App\Repositories\AnjabRepository;
use App\Repositories\UnitKerjaRepository;
use App\Repositories\JabatanRepository;

use DB;

class AnjabController extends Controller
{

    function browse(Request $request, AnjabRepository $anjabRepo, UnitKerjaRepository $unitKerjaRepo){
		// return $request;
		
		// if (condition) {
		// 	# code...
		// }

    	$data['section']                                           = "anjab";
    	$data['page_section']                                      = "anjab";
    	$data['page']                                              = "Analisis Jabatan";
		// return $data;

    	$unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
		// dd($unitKerja); // Menampilkan dan menghentikan eksekusi

        // 1 Struktural 2 FUngsional 3 Pelaksana
        $jenis                                                  = 1;
        $kodeOpd                                                = 9999;

        if($request->jenis){
            $jenis                                              = $request->jenis;
        }

        if($request->unit_kerja){
            $kodeOpd                                            = $request->unit_kerja;
			// $kodeOpd                                            = $request->unit_kerja;
        }
		if ($request->puskesmas) {
			$kodeOpd = $request->puskesmas;
			$puskesmas = $unitKerjaRepo->getActiveUnitKerja();
			
		} else if (\MojokertokabUser::getUser()->role == 'PD') { // Hanya dieksekusi jika puskesmas tidak ada
			$kodeOpd = \MojokertokabUser::getUser()->kode_opd;
		
			$tempKodeOPD = substr($kodeOpd, 0, 2);
		
			if ($tempKodeOPD == '01') {
				$kodeOpd = substr($kodeOpd, 0, 6);
			} elseif ($tempKodeOPD == '28') {
				$puskesmas = $unitKerjaRepo->getActiveUnitKerja();
				$unitKerja = \MojokertokabUser::getUser()->option->satker_id;
			} else {
				$kodeOpd = $tempKodeOPD;
			}
		}

        $pd                                                     = MUnitkerja::where('kodeunit', $kodeOpd)->first();
        $anjab                                                  = $anjabRepo->getDataAnjab($kodeOpd, $jenis);
		// return $anjab;
		// return $anjab;

    	// if(\MojokertokabUser::getUser()->role == 'PD'){
    	// 	$anjab = $anjabRepo->getDataAnjab($kodeOpd, $jenis);
    	// }else{
    	// 	if($request->unit_kerja){
    	// 		$anjab = $anjabRepo->getDataAnjab($kodeOpd, $jenis);
    	// 	}else{
    	// 		$anjab = [];
    	// 	}
    	// }

		// return count($anjab);

        // dd($anjab);

    	foreach ($anjab as $itemAnjab) {
    		// $itemAnjab->nama_jabatan = $itemAnjab->jabatan?$itemAnjab->jabatan->jabatan:$itemAnjab->nama;
    		$itemAnjab->unit_eselon_2                                 = ucwords(strtolower(@$itemAnjab->eselon2->unitkerja));
    		$itemAnjab->unit_eselon_3                                 = ucwords(strtolower(@$itemAnjab->eselon3->unitkerja));
    		$itemAnjab->unit_eselon_4                                 = ucwords(strtolower(@$itemAnjab->eselon4->unitkerja));
    		// $itemAnjab->num_uraian_jabatan = $itemAnjab->dataUraianJabatan->count();
    		// $itemAnjab->num_tanggung_jawab = $itemAnjab->dataTanggungJawab->count();
    		// $itemAnjab->num_wewenang = $itemAnjab->dataWewenang->count();
    		// $itemAnjab->num_korelasi_jabatan = $itemAnjab->dataKorelasiJabatan->count();
    		// $itemAnjab->num_bahan_kerja = $itemAnjab->dataBahanKerja->count();
    		// $itemAnjab->num_alat_kerja = $itemAnjab->dataPerangkat->count();
    		// $itemAnjab->num_hasil_kerja = $itemAnjab->dataHasilKerja->count();
    		// $itemAnjab->kondisi_lingkungan_kerja = $itemAnjab->dataLingkunganKerja;
    		// $itemAnjab->num_resiko_bahaya = $itemAnjab->dataResikoBahaya->count();
    		// $itemAnjab->syarat_jabatan = $itemAnjab->dataSyaratJabatan;
    		// $itemAnjab->num_prestasi_kerja = $itemAnjab->dataPrestasiKerja->count();

    	}

        // if(\MojokertokabUser::getUser()->role == 'PD'){
        //     // $data = [
        //     //     'anjab' => $anjab->filter(function ($item) {
        //     //         return $item->user_opd == \MojokertokabUser::getUser()->kode_opd;
        //     //     }),
        //     //     'unitKerja' => $unitKerja,
        //     //     'selectedUnitKerja' => $request->unit_kerja,
        //     //     'jenis' => $jenis,
        //     //     'pd' => $pd
        //     // ];

        //     $data = [
        //         'anjab' => $anjab,
        //         'unitKerja' => $unitKerja,
        //         'selectedUnitKerja' => $request->unit_kerja,
        //         'jenis' => $jenis,
        //         'pd' => $pd 
        //     ];
        // }

        // if(\MojokertokabUser::getUser()->role == 'ADMIN'){
		// dd($puskesmas);
            $data = [
                'anjab' => $anjab,
                'unitKerja' => $unitKerja,
				'puskesmas' => $puskesmas ?? [],  // Menggunakan array kosong jika $puskesmas tidak ada
                'selectedUnitKerja' => $request->unit_kerja,
				'selectedPuskesmas' => $request->puskesmas,
                'jenis' => $jenis,
                'pd' => $pd,
				'page_section'=> 'anjab'
            ];
			// return $data['selectedPuskesmas'];
			// return $anjab->count();

			// dd($data['selectedUnitKerja']);
        // }
		// return 	$anjab;
		
    	return view('anjab.browse', $data);
    }


    function getForm(Request $request, AnjabRepository $anjabRepo, UnitKerjaRepository $unitKerjaRepo){

        if($request->aksi == 'add-anjab'){
			// return $request;
			// return $request->kode_opd;
            $pd                                                 = MUnitkerja::where('kodeunit', $request->kode_opd)->first();
            // $jenisPd                                            = $pd->jenis; // 1 Dinas, 2 Kecamatan, 3 UPT / Korwil
            $unitKerjaOnKode                                    = $unitKerjaRepo->getUnitKerjaOnKode($request->kode_opd);
            $jenisJabatan                                       = $request->jenis_jabatan;
			$jenjang                                       		= $request->jenjang;

            $pejabatStruktural                                  = $anjabRepo->getStukturalAnjab($request->kode_opd);
			$pejabatFungsional                                  = $anjabRepo->getFungsionalAnjab($request->kode_opd);
			// dd($pd);
			// Cek apakah kode_opd memiliki 4 angka dan diawali dengan '28'
			$isUpt = (strlen($request->kode_opd) == 4) && (substr($request->kode_opd, 0, 2) == '28');

			// Cek apakah ada unit kerja dengan kata "puskesmas" di tabel m_unit_kerja_2
			$hasPuskesmas = DB::table('m_unit_kerja_2')
				->where('kodeunit', $request->kode_opd) // Cocokkan dengan kode_opd user
				->where('unitkerja', 'LIKE', '%puskesmas%') // Unit kerja harus mengandung 'puskesmas'
				->exists(); // Mengecek apakah ada data yang sesuai

			// Perbarui kondisi $isUpt agar hanya true jika kedua kondisi terpenuhi
			$isUpt = $isUpt && $hasPuskesmas;
			// dd($isUpt);

			if ($jenisJabatan == '3') {
				$data = [
					'unitKerja' => $unitKerjaOnKode,
				];
			
				if ($isUpt) {
					// dd($request->kode_opd);
					// Ambil data dari MJabatanUpt berdasarkan kodeunit_instansi
					$data['jabatanUpt'] = MJabatanUpt::where('kodeunit_instansi', $request->kode_opd)->get();
					// $data['pejabatFungsional'] = $pejabatFungsional;
					// dd($data['jabatanUpt']);
				} else {
					// Gunakan data pejabat struktural seperti biasa
					$data['pejabatStruktural'] = $pejabatStruktural;
				}
				// dd($data);
				return view($isUpt ? 'anjab.anjab-upt.form-create-pelaksana' : 'anjab.anjab-dinas.form-create-pelaksana', $data);
			}elseif ($jenisJabatan == '2') {
                    // $data = [
                    //     'pejabatStruktural' => $pejabatStruktural,
                    //     'unitKerja' => $unitKerjaOnKode,
                    // ];
					// return view('anjab.anjab-dinas.form-create-fungsional', $data);
					$data = [
						'unitKerja' => $unitKerjaOnKode,
					];
				
					if ($isUpt) {
						// dd($request->kode_opd);
						// Ambil data dari MJabatanUpt berdasarkan kodeunit_instansi
						$data['jabatanUpt'] = MJabatanUpt::where('kodeunit_instansi', $request->kode_opd)->get();
						// $data['pejabatStruktural'] = $pejabatStruktural;
						// dd($data['jabatanUpt']);
					} else {
						// Gunakan data pejabat struktural seperti biasa
						$data['pejabatStruktural'] = $pejabatStruktural;
					}
					// return $data;
					return view($isUpt ? 'anjab.anjab-upt.form-create-fungsional' : 'anjab.anjab-dinas.form-create-fungsional', $data);
                }elseif ($jenisJabatan == '1') {
                    $data = [
                        'pejabatStruktural' => $pejabatStruktural,
                        'unitKerja' => $unitKerjaOnKode,
						
                    ];
					// return ($data);
                    return view('anjab.anjab-dinas.form-create-struktural', $data);
                }

        }elseif ($request->aksi == 'edit-anjab') {
			
			// return 'bawah';
            $pd                                                 = MUnitkerja::where('kodeunit', $request->kode_opd)->first();
            // $jenisPd                                            = $pd->jenis; // 1 Dinas, 2 Kecamatan, 3 UPT / Korwil
            $unitKerjaOnKode                                    = $unitKerjaRepo->getUnitKerjaOnKode($request->kode_opd);
            $jenisJabatan                                       = $request->jenis_jabatan;
			// $jenjang                                       		= $request->m_jabatan_fungsional_jenjang;
			$jenjang                                       		= $request->jenjang;

            $pejabatStruktural                                  = $anjabRepo->getStukturalAnjab($request->kode_opd);
			$pejabatFungsional                                  = $anjabRepo->getFungsionalAnjab($request->kode_opd);

			// Cek apakah kode_opd memiliki 4 angka dan diawali dengan '28'
			$isUpt = (strlen($request->kode_opd) == 4) && (substr($request->kode_opd, 0, 2) == '28');

			// Cek apakah ada unit kerja dengan kata "puskesmas" di tabel m_unit_kerja_2
			$hasPuskesmas = DB::table('m_unit_kerja_2')
				->where('kodeunit', $request->kode_opd) // Cocokkan dengan kode_opd user
				->where('unitkerja', 'LIKE', '%puskesmas%') // Unit kerja harus mengandung 'puskesmas'
				->exists(); // Mengecek apakah ada data yang sesuai

			// Perbarui kondisi $isUpt agar hanya true jika kedua kondisi terpenuhi
			$isUpt = $isUpt && $hasPuskesmas;
			
            // if($jenisPd == 'DINAS'){
                if($jenisJabatan == '3'){
                    $anjab                                      = $anjabRepo->getDetailAnjab($request->id);

                    $data = [
                        'pejabatStruktural' => $pejabatStruktural,
                        'unitKerja' => $pd,
                        'anjab' => $anjab,
                    ];
                    return view('anjab.anjab-dinas.form-edit-pelaksana', $data);
                }if($jenisJabatan == '2'){
                    $anjab                                      = $anjabRepo->getDetailAnjab($request->id);
					// $jenjang = $jenjang;
					// dd($anjab);
					$data = [
						'pejabatStruktural' => $pejabatStruktural,
						'unitKerja' => $pd,
						'anjab' => $anjab,
						'jenjang' => $jenjang // Tambahkan default jenjang agar tidak error
					];
					// return $data;
					if ($isUpt) {
						// Jika isUpt true, gunakan pejabat fungsional
						
						$data['jabatanUpt'] = MJabatanUpt::where('kodeunit_instansi', $request->kode_opd)->get();
						// dd($data);
					} else {
						// Jika isUpt false, gunakan pejabat struktural dan tambahkan jenjang
						$data['pejabatStruktural'] = $pejabatStruktural;
						// dd($data);
						// $data['jenjang'] = $jenjang;
					}
					// return $data;
                    return view($isUpt ? 'anjab.anjab-upt.form-edit-fungsional' : 'anjab.anjab-dinas.form-edit-fungsional', $data);
					
                }elseif($jenisJabatan == '1'){
                    $anjab                                      = $anjabRepo->getDetailAnjab($request->id);
                    $data = [
                        'pejabatStruktural' => $pejabatStruktural,
                        'unitKerja' => $unitKerjaOnKode,
                        'anjab' => $anjab,
                    ];
                    return view('anjab.anjab-dinas.form-edit-struktural', $data);
                }
            // }
        }

    	if($request->aksi == 'add-anjab'){
			
    		if(\MojokertokabUser::getUser()->role == 'PD'){
	    		if(strlen(\MojokertokabUser::getUser()->kode_opd) == 6){
	    			$pejabatStruktural                                      = $anjabRepo->getStukturalAnjab(\MojokertokabUser::getUser()->kode_opd);
	    		}else{
	    			$pejabatStruktural                                      = $anjabRepo->getStukturalAnjab(\MojokertokabUser::getUser()->kode_opd);
	    		}
	    	}
    		// $pejabatStruktural = $anjabRepo->getStukturalAnjab($anjab->kode_opd);
    		if(\MojokertokabUser::getUser()->role == 'PD'){
    			$unitKerja                                               = MUnitkerja::where('kodeunit', \MojokertokabUser::getUser()->kode_opd)->first();
    			if($unitKerja->kodeunit >= 31 && $unitKerja->kodeunit <= 48){
    				$unitKerjaEselon4                                       = $unitKerjaRepo->getEselon4UnitKerja($unitKerja->kodeunit);
    			}else{
    				$unitKerjaEselon3                                       = $unitKerjaRepo->getEselon3UnitKerja($unitKerja->kodeunit);
    			}
            	// $unitKerjaEselon3 = MUnitkerja::where('kodeunit', 'LIKE', $unitKerja->kodeunit.'%')->where('jenis', 'BIDANG')->get();
            }else{
            	$unitKerja                                         = $unitKerjaRepo->getActiveUnitKerja();
            }

            if(\MojokertokabUser::getUser()->role == 'PD'){
            	$data['pejabatStruktural']                         = $pejabatStruktural;
            }

    		$data['unitKerja']                                     = $unitKerja;

    		\Log::channel('logstash-daily')->info("Username ".\MojokertokabUser::getUser()->username." open form add anjab");

    		if(\MojokertokabUser::getUser()->role == 'PD'){

    			if($unitKerja->kodeunit >= 31 && $unitKerja->kodeunit <= 48){
    				$data['unitKerjaEselon4']                               = $unitKerjaEselon4;
    				return view('anjab.form-create-anjab-kecamatan',$data);
    			}else{
    				$data['unitKerjaEselon3']                               = $unitKerjaEselon3;
    				return view('anjab.form-create-anjab-dinas',$data);
    			}


    		}else{
    			return view('anjab.form-create-anjab',$data);
    		}



    	}elseif ($request->aksi == 'edit-anjab') {

			// return 'bawah';
    		// $pejabatStruktural = TrxAnjab::where('jenis_jabatan', 1)->get();
    		$anjab                                                    = TrxAnjab::findOrFail($request->id);
    		$pejabatStruktural                                        = $anjabRepo->getStukturalAnjab($anjab->kode_opd);

    		// Unit Kerja
    		if(\MojokertokabUser::getUser()->role == 'PD'){
    			$unitKerja                                                  = MUnitkerja::where('kodeunit', \MojokertokabUser::getUser()->kode_opd)->first();
    			if($unitKerja->kodeunit >= 31 && $unitKerja->kodeunit <= 48){
    				$unitKerjaEselon4                                       = $unitKerjaRepo->getEselon4UnitKerja($unitKerja->kodeunit);
    			}else{
    				$unitKerjaEselon3                                       = $unitKerjaRepo->getEselon3UnitKerja($unitKerja->kodeunit);
    			}
            	// $unitKerjaEselon3 = MUnitkerja::where('kodeunit', 'LIKE', $unitKerja->kodeunit.'%')->where('jenis', 'BIDANG')->get();
            }else{
            	$unitKerja                                         = $unitKerjaRepo->getActiveUnitKerja();
            }

            if($anjab->kode_unit == '01'){
            	$kode_unit                                         = substr($anjab->unit_kerja_eselon2, 0, 6);
            }else{
            	$kode_unit                                         = $anjab->unit_kerja_eselon2;
            }

            // if(\MojokertokabUser::getUser()->role == 'PD'){
            // 	$unitKerja = MUnitkerja::where('kodeunit', \MojokertokabUser::getUser()->kode_opd)->get();
            // }else{
            // 	$unitKerja = $unitKerjaRepo->getActiveUnitKerja();
            // }

            $data['unitKerjaEselon3']                           = MUnitKerja::where('kodeunit', 'like',$kode_unit.'%')->where('kodeunit', '!=',$anjab->unit_kerja_eselon2)->get();

            $kode_unit2                                         = substr($anjab->unit_kerja_eselon3, 0, 8);
            $data['unitKerjaEselon4']                           = MUnitKerja::where('kodeunit', 'like',$kode_unit2.'%')->where('kodeunit', '!=',$anjab->unit_kerja_eselon3)->get();

          	$data['anjab']                                       = $anjab;
          	$data['unitKerja']                                   = $unitKerja;
          	$data['pejabatStruktural']                           = $pejabatStruktural;

          	\Log::channel('logstash-daily')->info("Username ".\MojokertokabUser::getUser()->username." open form edit anjab");

          	if(\MojokertokabUser::getUser()->role == 'PD'){
          		return view('anjab.form-edit-anjab-kecamatan', $data);
          	}else{
          		return view('anjab.form-edit-anjab', $data);
          	}


    	}elseif ($request->aksi == 'copy-anjab') {
    		$anjab                                                    = TrxAnjab::findOrFail($request->id);
    		$unitKerja                                                = MUnitkerja::whereRaw('LENGTH(kodeunit) = 2')->orderBy('kodeunit', 'ASC')->get();
    		$data['unitKerja']                                        = $unitKerja;
    		$data['anjab']                                            = $anjab;
    		return view('anjab.form-copy-anjab',$data);
    	}
    }

    function save(Request $request, AnjabRepository $anjabRepo, JabatanRepository $jabatanRepo){
		// dd($request);
    	// $jabatan = MJabatan::findOrFail($request->jabatan);
        $jenis                                                  = $request->jenis;

        if($request->aksi == 'create'){
			// return $request;
            $jabatan                                            = $jabatanRepo->getRowJabatan($request->kode_jabatan, $request->jenis);
            $kodeJabatan                                        = $jabatan->{'_kode'};
			$jenjangfungsional                                  = $request->jenjang;
            $atasanLangsung                                     = $request->atasan_langsung;
            if($request->jenis == '3'){
                $anjabAtasan                                    = $anjabRepo->getDetailAnjab($atasanLangsung);
				// dd($anjabAtasan );
                // Cek jika $anjabAtasan adalah objek yang valid
			if ($anjabAtasan) {
				
				$unitKerja = $anjabAtasan->unit_kerja;
			} else {
				// Tangani jika $anjabAtasan tidak ditemukan
				// Misalnya, beri nilai default atau beri pesan error
				$unitKerja = $request->pd; // atau lakukan sesuatu jika tidak ada data
				// Atau bisa menggunakan:
				// return response()->json(['error' => 'Data tidak ditemukan'], 404);
			}
            }elseif($request->jenis == '2'){
                $anjabAtasan                                    = $anjabRepo->getDetailAnjab($atasanLangsung);
				// dd($anjabAtasan);
                // $unitKerja                                      = $anjabAtasan->unit_kerja;
				$kodeOpd = \MojokertokabUser::getUser()->kode_opd;

				// Cek apakah user memiliki kode_opd yang diawali dengan "28" dan berjumlah 4 angka
				if (preg_match('/^28\d{2}$/', $kodeOpd)) {
					// Cek apakah ada data di tabel m_unit_kerja_2 yang memenuhi syarat
					$kodeUnitPuskesmas = DB::table('m_unit_kerja_2')
						->where('kodeunit', $kodeOpd) // Cocokkan dengan kode_opd user
						->where('unitkerja', 'LIKE', '%puskesmas%') // Unit kerja harus mengandung 'puskesmas'
						->value('kodeunit'); // Ambil nilai dari kolom kodeunit

					if ($kodeUnitPuskesmas) {
						// Jika ditemukan, gunakan kodeunit dari tabel m_unit_kerja_2
						$unitKerja = $kodeUnitPuskesmas;
					} else {
						// Jika tidak ditemukan, lanjutkan ke logika biasa
						$unitKerja = $anjabAtasan ? $anjabAtasan->unit_kerja : $request->pd;
					}
				} else {
					// Jika tidak memenuhi kondisi, lanjutkan ke logika biasa
					$unitKerja = $anjabAtasan ? $anjabAtasan->unit_kerja : $request->pd;
				}


            }elseif($request->jenis == '1'){
                $unitKerja                                      = $request->pd;
            }
			
            // Cek Jika Jabatan sudah dianalisa di Unit Kerja yang sama
            $periode                                            = 2;
            $anjabExist                                         = $anjabRepo->checkAnjabIfExist($periode, $kodeJabatan, $unitKerja, $jenjangfungsional);
			
			if($anjabExist == 1){
                return response()->json(['message'=>'Data sudah ada'], 409);
            }

            $kodeOpd                                            = $request->kode_opd;


            if($request->jenis == '3'){
                $diklatTeknis                                   = $request->diklat_teknis;
                $pengalamanKerja                                = $request->pengalaman_kerja;
                $pengetahuanKerja                               = $request->pengetahuan_kerja;
				$diklatPenjenjangan                               = 'Pelatihan Dasar';

                $anjab                                          = $anjabRepo->createAnjabPelaksana($jabatan->id, $kodeJabatan, $kodeOpd, $jenis, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $diklatPenjenjangan, $atasanLangsung,  \MojokertokabUser::getUser()->username, $unitKerja, 2);

            }elseif($request->jenis == '2'){
                $diklatTeknis                                   = $request->diklat_teknis;
                $pengalamanKerja                                = $request->pengalaman_kerja;
                $pengetahuanKerja                               = $request->pengetahuan_kerja;
				$jenjangfungsional                              = $request->jenjang;
				$diklatpenjenjangan                             = $request->diklat_penjenjangan;
				$kualifikasipendidikan                          = $request->kualifikasi_pendidikan;


                $anjab                                          = $anjabRepo->createAnjabFungsional($jabatan->id, $kodeJabatan, $kodeOpd, $jenis, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $jenjangfungsional, $diklatpenjenjangan, $kualifikasipendidikan, $atasanLangsung, \MojokertokabUser::getUser()->username, $unitKerja, 2);

            }elseif($request->jenis == '1'){
                $pangkat                                        = $request->pangkat;
                $ikhtisarJabatan                                = $request->ikhtisar_jabatan;
                $kualifikasiPendidikan                          = $request->kualifikasi_pendidikan;
                $diklatTeknis                                   = $request->diklat_teknis;
                $pengalamanKerja                                = $request->pengalaman_kerja;
                $pengetahuanKerja                               = $request->pengetahuan_kerja;
                $eselon                                         = $request->eselon_jabatan;

                $anjab                                          = $anjabRepo->createAnjabStruktural($jabatan->id, $kodeJabatan, $eselon, $kodeOpd, $jenis, $pangkat, $ikhtisarJabatan, $kualifikasiPendidikan, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $atasanLangsung, \MojokertokabUser::getUser()->username, $unitKerja, 2);
            }

            return response()->json(201);

        }elseif ($request->aksi == 'update') {
            $anjab                                              = TrxAnjab::findOrFail($request->id);

            $jabatan                                            = $jabatanRepo->getRowJabatan($request->kode_jabatan, $request->jenis);

            $kodeJabatan                                        = $jabatan->{'_kode'};
			$jenjangfungsional                                  = $request->jenjang;

            $atasanLangsung                                     = $request->atasan_langsung;
            if($request->jenis == '3'){
                $anjabAtasan                                    = $anjabRepo->getDetailAnjab($atasanLangsung);
                $unitKerja                                      = $anjabAtasan->unit_kerja;
            }elseif($request->jenis == '2'){
                $anjabAtasan                                    = $anjabRepo->getDetailAnjab($atasanLangsung);
                // $unitKerja                                      = $anjabAtasan->unit_kerja;
				$kodeOpd = \MojokertokabUser::getUser()->kode_opd;

				// Cek apakah user memiliki kode_opd yang diawali dengan "28" dan berjumlah 4 angka
				if (preg_match('/^28\d{2}$/', $kodeOpd)) {
					// Cek apakah ada data di tabel m_unit_kerja_2 yang memenuhi syarat
					$kodeUnitPuskesmas = DB::table('m_unit_kerja_2')
						->where('kodeunit', $kodeOpd) // Cocokkan dengan kode_opd user
						->where('unitkerja', 'LIKE', '%puskesmas%') // Unit kerja harus mengandung 'puskesmas'
						->value('kodeunit'); // Ambil nilai dari kolom kodeunit

					if ($kodeUnitPuskesmas) {
						// Jika ditemukan, gunakan kodeunit dari tabel m_unit_kerja_2
						$unitKerja = $kodeUnitPuskesmas;
					} else {
						// Jika tidak ditemukan, lanjutkan ke logika biasa
						$unitKerja = $anjabAtasan ? $anjabAtasan->unit_kerja : $request->pd;
					}
				} else {
					// Jika tidak memenuhi kondisi, lanjutkan ke logika biasa
					$unitKerja = $anjabAtasan ? $anjabAtasan->unit_kerja : $request->pd;
				}
            } elseif($request->jenis == '1'){
                $unitKerja                                      = $request->pd;
            }

            $id                                                 = $request->id;

            // Cek Jika Jabatan sudah dianalisa di Unit Kerja yang sama
            $periode                                            = 2;
            $anjabExist                                         = $anjabRepo->checkAnjabIfExist($periode, $kodeJabatan, $unitKerja, $jenjangfungsional);
            if($anjabExist){
                // return response()->json(['message'=>'Data sudah ada'], 409);
            }

            $kodeOpd                                            = $request->kode_opd;

            if($request->jenis == '3'){
                $diklatTeknis                                   = $request->diklat_teknis;
                $pengalamanKerja                                = $request->pengalaman_kerja;
                $pengetahuanKerja                               = $request->pengetahuan_kerja;

                $anjab                                          = $anjabRepo->updateAnjabPelaksana($id, $jabatan->id, $kodeJabatan, $kodeOpd, $jenis, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $atasanLangsung, \MojokertokabUser::getUser()->username, $unitKerja,2);

            }elseif($request->jenis == '2'){
                $diklatTeknis                                   = $request->diklat_teknis;
                $pengalamanKerja                                = $request->pengalaman_kerja;
                $pengetahuanKerja                               = $request->pengetahuan_kerja;
				$jenjangfungsional                              = $request->jenjang;
				$diklatpenjenjangan                             = $request->diklat_penjenjangan;
				$kualifikasipendidikan                          = $request->kualifikasi_pendidikan;

                $anjab                                          = $anjabRepo->updateAnjabFungsional($id, $jabatan->id, $kodeJabatan, $kodeOpd, $jenis, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $jenjangfungsional, $diklatpenjenjangan, $kualifikasipendidikan, $atasanLangsung, \MojokertokabUser::getUser()->username, $unitKerja,2);

            }elseif($request->jenis == '1'){
                $pangkat                                        = $request->pangkat;
                $ikhtisarJabatan                                = $request->ikhtisar_jabatan;
                $kualifikasiPendidikan                          = $request->kualifikasi_pendidikan;
                $diklatTeknis                                   = $request->diklat_teknis;
                $pengalamanKerja                                = $request->pengalaman_kerja;
                $pengetahuanKerja                               = $request->pengetahuan_kerja;
                $eselon                                         = $request->eselon_jabatan;

                $anjab                                          = $anjabRepo->updateAnjabStruktural($id, $jabatan->id, $kodeJabatan, $eselon, $kodeOpd, $jenis, $pangkat, $ikhtisarJabatan, $kualifikasiPendidikan, $diklatTeknis, $pengalamanKerja, $pengetahuanKerja, $atasanLangsung, \MojokertokabUser::getUser()->username, $unitKerja, 2);
            }



            // $anjab->nama = $jabatan->nama;
            // $anjab->kode_opd = $request->pd;
            // $anjab->jenis_jabatan = $request->jenis_jabatan;
            // $anjab->eselon_jabatan = $request->eselon_jabatan;
            // $anjab->kode_jabatan_2 = $request->kode_jabatan;
            // // $anjab->unit_kerja_eselon2 = $request->unit_kerja_eselon2;
            // // $anjab->unit_kerja_eselon3 = $request->unit_kerja_eselon3;
            // // $anjab->unit_kerja_eselon4 = $request->unit_kerja_eselon4;
            // // $anjab->ikhtisar_jabatan = $request->ikhtisar_jabatan;
            // // $anjab->kualifikasi_pendidikan = $request->kualifikasi_pendidikan;
            // // $anjab->diklat_penjenjangan = $request->diklat_penjenjangan;
            // $anjab->diklat_teknis = $request->diklat_teknis;
            // $anjab->pengalaman_kerja = $request->pengalaman_kerja;
            // $anjab->pengetahuan_kerja = $request->pengetahuan_kerja;
            // // $anjab->pangkat = $request->pangkat;
            // $anjab->parent = $request->atasan_langsung;

            // $anjab->update();

            // return redirect()->back()->with('notify', 'Anjab berhasil diperbarui');
            if($anjab){
                // return response()->json(200);
                return response()->json(200);
            }

        }
    }

    function copy(Request $request){
    	$anjab                                                     = TrxAnjab::findOrFail($request->id);

    	$newAnjab                                                  = $anjab->replicate();
    	$newAnjab->kode_opd                                        = $request->kode_opd;
    	$newAnjab->save();

    	$bahanKerja                                                = TrxAnjabBahanKerja::where('trx_anjab_id', $request->id)->get();
    	$wewenang                                                  = TrxAnjabWewenang::where('trx_anjab_id', $request->id)->get();
    	$korelasi                                                  = TrxAnjabKorelasiJabatan::where('trx_anjab_id', $request->id)->get();
    	$tanggungJawab                                             = TrxAnjabTanggungJawab::where('trx_anjab_id', $request->id)->get();
    	$abk                                                       = TrxAnjabAbk::where('trx_anjab_id', $request->id)->get();
    	$tugasJabatan                                              = TrxAnjabUraianJabatan::where('trx_anjab_id', $request->id)->get();
    	$hasilKerja                                                = TrxAnjabHasilKerja::where('trx_anjab_id', $request->id)->get();
    	$korelasiJabatan                                           = TrxAnjabKorelasiJabatan::where('trx_anjab_id', $request->id)->get();
    	$lingkunganKerja                                           = TrxAnjabLingkunganKerja::where('trx_anjab_id', $request->id)->get();
    	$perangkat                                                 = TrxAnjabPerangkat::where('trx_anjab_id', $request->id)->get();
    	$prestasiKerja                                             = TrxAnjabPrestasiKerja::where('trx_anjab_id', $request->id)->get();
    	$resikoBahaya                                              = TrxAnjabResikoBahaya::where('trx_anjab_id', $request->id)->get();
    	$syaratJabatan                                             = TrxAnjabSyaratJabatan::where('trx_anjab_id', $request->id)->get();
    	$uraianJabatan                                             = TrxAnjabUraianJabatan::where('trx_anjab_id', $request->id)->get();


    	// Copy Table
    	foreach ($bahanKerja as $rowBahanKerja) {

    		$newBahanKerja                                            = $rowBahanKerja->replicate();
    		$newBahanKerja->trx_anjab_id                              = $newAnjab->id;
    		$newBahanKerja->save();
    	}

    	foreach ($wewenang as $rowWewenang) {

    		$newWewenang                                              = $rowWewenang->replicate();
    		$newWewenang->trx_anjab_id                                = $newAnjab->id;
    		$newWewenang->save();
    	}

    	foreach ($korelasi as $rowKorelasi) {

    		$newKorelasi                                              = $rowKorelasi->replicate();
    		$newKorelasi->trx_anjab_id                                = $newAnjab->id;
    		$newKorelasi->save();
    	}

    	foreach ($tanggungJawab as $rowTanggungJawab) {

    		$newTanggungJawab                                         = $rowTanggungJawab->replicate();
    		$newTanggungJawab->trx_anjab_id                           = $newAnjab->id;
    		$newTanggungJawab->save();
    	}

    	foreach ($tugasJabatan as $rowTugasJabatan) {

    		$newTugasJabatan                                          = $rowTugasJabatan->replicate();
    		$newTugasJabatan->trx_anjab_id                            = $newAnjab->id;
    		$newTugasJabatan->save();
    	}

    	foreach ($abk as $rowAbk) {

    		$newAbk                                                   = $rowAbk->replicate();
    		$newAbk->trx_anjab_id                                     = $newAnjab->id;
    		$newAbk->save();
    	}

    	foreach ($hasilKerja as $rowHasilKErja) {

    		$newHasilKErja                                            = $rowHasilKErja->replicate();
    		$newHasilKErja->trx_anjab_id                              = $newAnjab->id;
    		$newHasilKErja->save();
    	}

    	foreach ($lingkunganKerja as $rowLingkunganKerja) {

    		$newLingkunganKerja                                       = $rowLingkunganKerja->replicate();
    		$newLingkunganKerja->trx_anjab_id                         = $newAnjab->id;
    		$newLingkunganKerja->save();
    	}

    	foreach ($perangkat as $rowPerangkat) {

    		$newPerangkat                                             = $rowPerangkat->replicate();
    		$newPerangkat->trx_anjab_id                               = $newAnjab->id;
    		$newPerangkat->save();
    	}

    	foreach ($prestasiKerja as $rowPrestasiKerja) {

    		$newPrestasiKerja                                         = $rowPrestasiKerja->replicate();
    		$newPrestasiKerja->trx_anjab_id                           = $newAnjab->id;
    		$newPrestasiKerja->save();
    	}

    	foreach ($resikoBahaya as $rowResikoBahaya) {

    		$newResikoBahaya                                          = $rowResikoBahaya->replicate();
    		$newResikoBahaya->trx_anjab_id                            = $newAnjab->id;
    		$newResikoBahaya->save();
    	}

    	foreach ($syaratJabatan as $rowSyaratJabatan) {

    		$newSyaratJabatan                                         = $rowSyaratJabatan->replicate();
    		$newSyaratJabatan->trx_anjab_id                           = $newAnjab->id;
    		$newSyaratJabatan->save();
    	}

    	foreach ($uraianJabatan as $rowUraianJabatan) {

    		$newUraianJabatan                                         = $rowUraianJabatan->replicate();
    		$newUraianJabatan->trx_anjab_id                           = $newAnjab->id;
    		$newUraianJabatan->save();

    		$uraianJabatanTahapan                                     = TrxAnjabUraianJabatanTahapan::where('trx_anjab_id', $request->id)->where('trx_anjab_uraian_jabatan_id', $rowUraianJabatan->id)->get();

    		foreach ($uraianJabatanTahapan as $rowUraianJabatanTahapan) {

	    		$newUraianJabatanTahapan                                 = $rowUraianJabatanTahapan->replicate();
	    		$newUraianJabatanTahapan->trx_anjab_id                   = $newAnjab->id;
	    		$newUraianJabatanTahapan->trx_anjab_uraian_jabatan_id    = $newUraianJabatan->id;
	    		$newUraianJabatanTahapan->save();
	    	}
    	}





    	return redirect()->back()->with('notify', 'Anjab berhasil di copy');
    }

    // function detail($id){

    // 	$data['section'] = "anjab";
    // 	$data['page_section'] = "anjab";
    // 	$data['page'] = "Detail Anjab";
    // 	$data['pages'] = "detail_anjab";
    // 	$data['anjab'] = TrxAnjab::where('id', $id)->first();
    // 	$data['anjab_uraian'] = TrxAnjabUraianJabatan::where('trx_anjab_id', $id)->orderBy('id', 'asc')->paginate(10);
    // 	return view('anjab.anjab-detail', $data);
    // }

    function delete(Request $request){
        $anjab                                                  = TrxAnjab::findOrFail($request->id);
        $anjab->delete();
        return redirect()->back()->with('notify', 'Anjab berhasil dihapus');
    }





    function viewPrint(Request $request){
        $data['id_anjab']                                       = $request->id_anjab;

        return view('anjab.form-iframe',$data);
    }

    function printWord(Request $request, $id){
		// return $request;

		// Path ke folder anjab
		$folderPath = public_path('anjab');

		// Menghapus semua file dalam folder
		$files = File::files($folderPath);
		foreach ($files as $file) {
			File::delete($file);
		}

    	\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
    	$domPdfPath                                                = base_path('vendor/dompdf/dompdf');
        \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
        \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');

    	$templateProcessor                                         = new \PhpOffice\PhpWord\TemplateProcessor('public/template-anjab.docx');
    	$anjab                                                     = TrxAnjab::findOrFail($id);

    	$tugasJabatan                                              = $anjab->dataUraianJabatan;
    	$hasilKerja                                                = $anjab->dataHasilKerja;
    	$tanggungJawab                                             = $anjab->dataTanggungJawab;
    	$korelasiJabatan                                           = $anjab->dataKorelasiJabatan;
    	$perangkatKerja                                            = $anjab->dataPerangkat;
    	$bahanKerja                                                = $anjab->dataBahanKerja;
    	$wewenang                                                  = $anjab->dataWewenang;
    	$resikoBahaya                                              = $anjab->dataResikoBahaya;
    	$prestasiKerja                                             = $anjab->dataPrestasiKerja;

    	$bakatKerja                                                = $anjab->dataSyaratJabatan?$anjab->dataSyaratJabatan->bakat_kerja:[];
    	$temperamenKerja                                           = $anjab->dataSyaratJabatan?$anjab->dataSyaratJabatan->temperamen_kerja:[];
    	$minatKerja                                                = $anjab->dataSyaratJabatan?$anjab->dataSyaratJabatan->minat_kerja:[];
    	$upayaFisik                                                = $anjab->dataSyaratJabatan?$anjab->dataSyaratJabatan->upaya_fisik:[];
    	$fungsiPekerjaan                                           = $anjab->dataSyaratJabatan?$anjab->dataSyaratJabatan->fungsi_pekerjaan:[];

    	$templateProcessor->cloneRow('row_uraian', $tugasJabatan->count());
    	$templateProcessor->cloneRow('row_no_hasil_kerja', $hasilKerja->count());
    	$templateProcessor->cloneRow('row_no_tanggung_jawab', $tanggungJawab->count());
    	$templateProcessor->cloneRow('row_no_perangkat_kerja', $perangkatKerja->count());
    	$templateProcessor->cloneRow('row_no_bahan_kerja', $bahanKerja->count());
    	$templateProcessor->cloneRow('row_no_prestasi_kerja', $prestasiKerja->count());
    	$templateProcessor->cloneRow('row_no_resiko', $resikoBahaya->count());
    	$templateProcessor->cloneRow('row_no_korelasi', $korelasiJabatan->count());
    	$templateProcessor->cloneRow('row_no_wewenang', $wewenang->count());
    	$templateProcessor->cloneRow('row_bakat_kerja', sizeof($bakatKerja));
    	$templateProcessor->cloneRow('row_temp_kerja', sizeof($temperamenKerja));
    	$templateProcessor->cloneRow('row_minat_kerja', sizeof($minatKerja));
    	$templateProcessor->cloneRow('row_upaya_fisik', sizeof($upayaFisik));
    	$templateProcessor->cloneRow('row_fungsi_pekerjaan', sizeof($fungsiPekerjaan));

		$i                                                            = 1;
		$iText                                                        = 'a';
		$totalWaktuPenyelesaian                                       = 0;
		$totalKebutuhanPegawai                                        = 0;
		foreach ($tugasJabatan as $rowTugasJabatan) {
			$templateProcessor->setValue('row_number#'.$i, $iText.'.');
			$templateProcessor->setValue('row_uraian#'.$i, $rowTugasJabatan->uraian_jabatan);
			$templateProcessor->setValue('row_hasil_kerja#'.$i, @$rowTugasJabatan->dataAbk->hasil_kerja);
			$templateProcessor->setValue('row_jumlah_hasil#'.$i, @$rowTugasJabatan->dataAbk->jumlah_hasil);
			$templateProcessor->setValue('row_waktu_penyelesaian#'.$i, @$rowTugasJabatan->dataAbk->waktu_penyelesaian);
			$templateProcessor->setValue('row_waktu_efektif#'.$i, @$rowTugasJabatan->dataAbk->waktu_efektif);
			$templateProcessor->setValue('row_kebutuhan_pegawai#'.$i, @$rowTugasJabatan->dataAbk->kebutuhan_pegawai);
			$i++;
			$iText++;

			$totalWaktuPenyelesaian                                      = $totalWaktuPenyelesaian + @$rowTugasJabatan->dataAbk->waktu_penyelesaian;
			$totalKebutuhanPegawai                                       = $totalKebutuhanPegawai + @$rowTugasJabatan->dataAbk->kebutuhan_pegawai;
		}

		$i                                                            = 1;
		$iText                                                        = 'a';
		foreach ($hasilKerja as $rowHasilKerja) {
			$templateProcessor->setValue('row_no_hasil_kerja#'.$i, $iText.'.');
			$templateProcessor->setValue('row_uraian_hasil_kerja#'.$i, $rowHasilKerja->hasil_kerja);
			$templateProcessor->setValue('row_satuan_hasil_kerja#'.$i, $rowHasilKerja->satuan_hasil);
			$i++;
			$iText++;
		}

		$i                                                            = 1;
		$iText                                                        = 'a';
		foreach ($bahanKerja as $rowBahanKerja) {
			$templateProcessor->setValue('row_no_bahan_kerja#'.$i, $iText.'.');
			$templateProcessor->setValue('row_uraian_bahan_kerja#'.$i, $rowBahanKerja->bahan_kerja);
			$templateProcessor->setValue('row_penggunaan_bahan_kerja#'.$i, $rowBahanKerja->penggunaan_dalam_tugas);
			$i++;
			$iText++;
		}

		$i                                                            = 1;
		$iText                                                        = 'a';
		foreach ($tanggungJawab as $rowTanggungJawab) {
			$templateProcessor->setValue('row_no_tanggung_jawab#'.$i, $iText.'.');
			$templateProcessor->setValue('row_uraian_tanggung_jawab#'.$i, $rowTanggungJawab->tanggung_jawab);
			$i++;
			$iText++;
		}

		$i                                                            = 1;
		$iText                                                        = 'a';
		foreach ($perangkatKerja as $rowPerangkatKerja) {
			$templateProcessor->setValue('row_no_perangkat_kerja#'.$i, $iText.'.');
			$templateProcessor->setValue('row_perangkat_kerja#'.$i, $rowPerangkatKerja->perangkat_kerja);
			$templateProcessor->setValue('row_penggunaan_perangkat_kerja#'.$i, $rowPerangkatKerja->digunakan_untuk_tugas);
			$i++;
			$iText++;
		}

		$i                                                            = 1;
		$iText                                                        = 'a';
		foreach ($prestasiKerja as $rowPrestasiKerja) {
			$templateProcessor->setValue('row_no_prestasi_kerja#'.$i, $iText.'.');
			$templateProcessor->setValue('row_prestasi_kerja#'.$i, $rowPrestasiKerja->prestasi_kerja);
			$i++;
			$iText++;
		}

		$i                                                            = 1;
		$iText                                                        = 'a';
		foreach ($wewenang as $rowWewenang) {
			$templateProcessor->setValue('row_no_wewenang#'.$i, $iText.'.');
			$templateProcessor->setValue('row_wewenang#'.$i, $rowWewenang->wewenang);
			$i++;
			$iText++;
		}

		$i                                                            = 1;
		$iText                                                        = 'a';
		foreach ($korelasiJabatan as $rowKorelasi) {
			$templateProcessor->setValue('row_no_korelasi#'.$i, $iText.'.');
			$templateProcessor->setValue('row_nama_korelasi#'.$i, $rowKorelasi->jabatan);
			$templateProcessor->setValue('row_uker_korelasi#'.$i, $rowKorelasi->unit_kerja);
			$templateProcessor->setValue('row_keterangan_korelasi#'.$i, $rowKorelasi->hal);
			$i++;
			$iText++;
		}

		$i                                                            = 1;
		$iText                                                        = 'a';
		foreach ($resikoBahaya as $rowResiko) {
			$templateProcessor->setValue('row_no_resiko#'.$i, $iText.'.');
			$templateProcessor->setValue('row_resiko#'.$i, $rowResiko->resiko_bahaya);
			$templateProcessor->setValue('row_penyebab_resiko#'.$i, $rowResiko->penyebab);
			$i++;
			$iText++;
		}

		$i                                                            = 1;
		foreach ($bakatKerja as $rowBakatKerja) {
			$detailBakatKerja                                            = MBakatKerja::find($rowBakatKerja);
			$templateProcessor->setValue('row_bakat_kerja#'.$i, @$detailBakatKerja->nama);
			$templateProcessor->setValue('row_uraian_bakat_kerja#'.$i, @$detailBakatKerja->keterangan);
			$i++;
		}
		$i                                                            = 1;
		foreach ($temperamenKerja as $rowTemperamenKerja) {
			$detailTempKerja                                             = MTemperamenKerja::find($rowTemperamenKerja);
			$templateProcessor->setValue('row_temp_kerja#'.$i, @$detailTempKerja->nama);
			$templateProcessor->setValue('row_uraian_temp_kerja#'.$i, @$detailTempKerja->keterangan);
			$i++;
		}
		$i                                                            = 1;
		foreach ($minatKerja as $rowMinatKerja) {
			$detailMinatKerja                                            = MMinatKerja::find($rowMinatKerja);
			$templateProcessor->setValue('row_minat_kerja#'.$i, @$detailMinatKerja->nama);
			$templateProcessor->setValue('row_uraian_minat_kerja#'.$i, @$detailMinatKerja->keterangan);
			$i++;
		}
		$i                                                            = 1;
		foreach ($upayaFisik as $rowUpayaFisik) {
			$detailUpayaFisik                                            = MUpayaFisik::find($rowUpayaFisik);
			$templateProcessor->setValue('row_upaya_fisik#'.$i, @$detailUpayaFisik->nama);
			$templateProcessor->setValue('row_uraian_upaya_fisik#'.$i, @$detailUpayaFisik->keterangan);
			$i++;
		}
		$i                                                            = 1;
		foreach ($fungsiPekerjaan as $rowFungsiPekerjaan) {
			$detailFungsiPekerjaan                                       = MFungsiPekerjaan::find($rowFungsiPekerjaan);
			print_r($detailFungsiPekerjaan->nama);
			echo "<br>";
			print_r($detailFungsiPekerjaan->keterangan);
			echo "<br>";
			$templateProcessor->setValue('row_fungsi_pekerjaan#'.$i, @$detailFungsiPekerjaan->nama);
			$templateProcessor->setValue('row_uraian_fungsi_pekerjaan#'.$i, @$detailFungsiPekerjaan->keterangan);
			$i++;
		}
		// Ambil 2 angka pertama dari unitkerja
		$kodeEselon2 = substr(@$anjab->unit_kerja, 0, 2);
		$unitKerjaEselon2 = DB::table('m_unit_kerja_2')
			->where('kodeunit', $kodeEselon2)
			->value('unitkerja');
		if ($anjab->jenis_jabatan == '1') {
			// return $anjab;
			$data = [
				'nama_jabatan' => $anjab->jabatan->jabatan,
				'ikhtisar_jabatan' => $anjab->ikhtisar_jabatan,
				'unit_kerja_eselon2' => ucwords(strtolower(@$anjab->eselon2->unitkerja)), //kosongi
				'unit_kerja_eselon3' => ucwords(strtolower(@$anjab->eselon3->unitkerja)),
				'unit_kerja_eselon4' => ucwords(strtolower(@$anjab->eselon4->unitkerja)),
				'pangkat' => $anjab->textPangkat,
				'pendidikan_formal' => $anjab->kualifikasi_pendidikan,
				'diklat_jenjang' => $anjab->diklat_penjenjangan,
				'diklat_teknis' => $anjab->diklat_teknis,
				'pengalaman_kerja' => $anjab->pengalaman_kerja,
				'pengetahuan_kerja' => $anjab->pengetahuan_kerja,
				'total_waktu_penyelesaian' => $totalWaktuPenyelesaian,
				'total_kebutuhan_pegawai' => $totalKebutuhanPegawai,
				'total2_kebutuhan_pegawai' => floor($totalKebutuhanPegawai),
				'lingkungan_tempat_kerja' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->tempat_kerja)),
				'lingkungan_suhu' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->suhu)),
				'lingkungan_udara' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->udara)),
				'lingkungan_keadaan_ruangan' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->keadaan_ruangan)),
				'lingkungan_letak' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->letak)),
				'lingkungan_penerangan' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->suara)),
				'lingkungan_suara' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->suara)),
				'lingkungan_keadaan' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->keadaan_tempat_kerja)),
				'lingkungan_getaran' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->getaran)),
				'keterampilan_kerja' => @$anjab->dataSyaratJabatan->keterampilan_kerja,
				'kondisi_fisik_jk' => @$anjab->dataSyaratJabatan->jenis_kelamin,
				'kondisi_fisik_umur' => @$anjab->dataSyaratJabatan->umur,
				'kondisi_fisik_tinggi' => @$anjab->dataSyaratJabatan->tinggi_badan,
				'kondisi_fisik_berat' => @$anjab->dataSyaratJabatan->berat_badan,
				'kondisi_fisik_postur' => @$anjab->dataSyaratJabatan->postur_badan,
				'kondisi_fisik_penampilan' => @$anjab->dataSyaratJabatan->penampilan,
				'kelas_jabatan' => @$anjab->kelas_jabatan?$anjab->kelas_jabatan:'-'
	
			];
		}elseif ($anjab->jenis_jabatan == '2'){
			$data = [
				'nama_jabatan' => $anjab->jabatan->jabatan,
				'ikhtisar_jabatan' => $anjab->ikhtisar_jabatan,
				'unit_kerja_eselon2' => ucwords(strtolower(@$anjab->eselon2->unitkerja)), //kosongi
				'unit_kerja_eselon3' => ucwords(strtolower(@$anjab->eselon3->unitkerja)),
				'unit_kerja_eselon4' => ucwords(strtolower(@$anjab->eselon4->unitkerja)),
				'pangkat' => $anjab->m_jabatan_fungsional_jenjang,
				'pendidikan_formal' => $anjab->kualifikasi_pendidikan,
				'diklat_jenjang' => $anjab->diklat_penjenjangan,
				'diklat_teknis' => $anjab->diklat_teknis,
				'pengalaman_kerja' => $anjab->pengalaman_kerja,
				'pengetahuan_kerja' => $anjab->pengetahuan_kerja,
				'total_waktu_penyelesaian' => $totalWaktuPenyelesaian,
				'total_kebutuhan_pegawai' => $totalKebutuhanPegawai,
				'total2_kebutuhan_pegawai' => floor($totalKebutuhanPegawai),
				'lingkungan_tempat_kerja' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->tempat_kerja)),
				'lingkungan_suhu' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->suhu)),
				'lingkungan_udara' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->udara)),
				'lingkungan_keadaan_ruangan' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->keadaan_ruangan)),
				'lingkungan_letak' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->letak)),
				'lingkungan_penerangan' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->suara)),
				'lingkungan_suara' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->suara)),
				'lingkungan_keadaan' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->keadaan_tempat_kerja)),
				'lingkungan_getaran' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->getaran)),
				'keterampilan_kerja' => @$anjab->dataSyaratJabatan->keterampilan_kerja,
				'kondisi_fisik_jk' => @$anjab->dataSyaratJabatan->jenis_kelamin,
				'kondisi_fisik_umur' => @$anjab->dataSyaratJabatan->umur,
				'kondisi_fisik_tinggi' => @$anjab->dataSyaratJabatan->tinggi_badan,
				'kondisi_fisik_berat' => @$anjab->dataSyaratJabatan->berat_badan,
				'kondisi_fisik_postur' => @$anjab->dataSyaratJabatan->postur_badan,
				'kondisi_fisik_penampilan' => @$anjab->dataSyaratJabatan->penampilan,
				'kelas_jabatan' => @$anjab->kelas_jabatan?$anjab->kelas_jabatan:'-'
	
			];
		}elseif ($anjab->jenis_jabatan == '3'){
			$data = [
				'nama_jabatan' => $anjab->jabatan->jabatan,
				'ikhtisar_jabatan' => $anjab->ikhtisar_jabatan,
				'unit_kerja_eselon2' => ucwords(strtolower($unitKerjaEselon2 ?? 'tidak ditemukan')),
				'unit_kerja_eselon3' => ucwords(strtolower($anjab->dataUnitKerja->unitkerja)),
				'unit_kerja_eselon4' => ucwords(strtolower($anjab->dataUnitKerja->unit_kerja)),
				'pangkat' => (
					strpos(strtoupper($anjab->jabatan->kualifikasi_pendidikan), 'S-1') !== false ||
					strpos(strtoupper($anjab->jabatan->jenjang_pendidikan), 'S-1') !== false ||
					strpos(strtoupper($anjab->jabatan->kualifikasi_pendidikan), 'D-4') !== false ||
					strpos(strtoupper($anjab->jabatan->jenjang_pendidikan), 'D-4') !== false
				) ? 'IIIA' : (
					strpos(strtoupper($anjab->jabatan->kualifikasi_pendidikan), 'D-3') !== false ||
					strpos(strtoupper($anjab->jabatan->jenjang_pendidikan), 'D-3') !== false
				) ? 'IIC' : (
					strpos(strtoupper($anjab->jabatan->kualifikasi_pendidikan), 'D3') !== false ||
					strpos(strtoupper($anjab->jabatan->jenjang_pendidikan), 'D3') !== false
				) ? 'IIC' : (
					strpos(strtoupper($anjab->jabatan->kualifikasi_pendidikan), 'SLTA') !== false ||
					strpos(strtoupper($anjab->jabatan->jenjang_pendidikan), 'SLTA') !== false ||
					strpos(strtoupper($anjab->jabatan->kualifikasi_pendidikan), 'SMK') !== false ||
					strpos(strtoupper($anjab->jabatan->jenjang_pendidikan), 'SMK') !== false ||
					strpos(strtoupper($anjab->jabatan->kualifikasi_pendidikan), 'SMA') !== false ||
					strpos(strtoupper($anjab->jabatan->jenjang_pendidikan), 'SMA') !== false
				) ? 'IIA' : $anjab->jabatan->pangkat,
				'pendidikan_formal' => $anjab->jabatan->jenjang_pendidikan . ' - ' . $anjab->jabatan->kualifikasi_pendidikan,
				'diklat_jenjang' => $anjab->diklat_penjenjangan,
				'diklat_teknis' => $anjab->diklat_teknis,
				'pengalaman_kerja' => $anjab->pengalaman_kerja,
				'pengetahuan_kerja' => $anjab->pengetahuan_kerja,
				'total_waktu_penyelesaian' => $totalWaktuPenyelesaian,
				'total_kebutuhan_pegawai' => $totalKebutuhanPegawai,
				'total2_kebutuhan_pegawai' => floor($totalKebutuhanPegawai),
				'lingkungan_tempat_kerja' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->tempat_kerja)),
				'lingkungan_suhu' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->suhu)),
				'lingkungan_udara' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->udara)),
				'lingkungan_keadaan_ruangan' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->keadaan_ruangan)),
				'lingkungan_letak' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->letak)),
				'lingkungan_penerangan' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->suara)),
				'lingkungan_suara' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->suara)),
				'lingkungan_keadaan' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->keadaan_tempat_kerja)),
				'lingkungan_getaran' => ucfirst(str_replace("_"," ",@$anjab->dataLingkunganKerja->getaran)),
				'keterampilan_kerja' => @$anjab->dataSyaratJabatan->keterampilan_kerja,
				'kondisi_fisik_jk' => @$anjab->dataSyaratJabatan->jenis_kelamin,
				'kondisi_fisik_umur' => @$anjab->dataSyaratJabatan->umur,
				'kondisi_fisik_tinggi' => @$anjab->dataSyaratJabatan->tinggi_badan,
				'kondisi_fisik_berat' => @$anjab->dataSyaratJabatan->berat_badan,
				'kondisi_fisik_postur' => @$anjab->dataSyaratJabatan->postur_badan,
				'kondisi_fisik_penampilan' => @$anjab->dataSyaratJabatan->penampilan,
				'kelas_jabatan' => @$anjab->kelas_jabatan?$anjab->kelas_jabatan:'-'
	
			];
		}
		$templateProcessor->setValues($data);
		// return $data;
		$templateProcessor->saveAs('public/anjab/Anjab '.$anjab->jabatan->jabatan.' - '.$anjab->dataUnitKerja->unitkerja.'.docx');

		// header("Content-Disposition: attachment; filename='myFile.docx'");

		$headers = [
          	'Content-Disposition' => 'attachment',
          	'filename' => 'Anjab '.$anjab->jabatan->jabatan.' - '.$anjab->dataUnitKerja->unitkerja.'.docx'
       	];

       	// return redirect()->to(asset('anjab/Anjab '.$anjab->jabatan->jabatan.' - '.$anjab->dataUnitKerja->unitkerja.'.docx'));
		   return response()->download(public_path('anjab/Anjab '.$anjab->jabatan->jabatan.' - '.$anjab->dataUnitKerja->unitkerja.'.docx'));

		// return response()->download(public_path('anjab/Anjab '.$anjab->jabatan->nama.' - '.$anjab->dataOpd->unitkerja.'.docx'), 'Anjab '.$anjab->jabatan->nama.' - '.$anjab->dataOpd->unitkerja.'.docx', $headers);

    }

    function printAnjab($id_anjab){
    	$data['anjab']                                          = TrxAnjab::where('id',$id_anjab)->first();
       	$data['anjab_uraian']                                   = TrxAnjabUraianJabatan::where('trx_anjab_id', $id_anjab)->get();
        $data['anjab_abk']                                      = TrxAnjabAbk::where('trx_anjab_id', $id_anjab)->first();
        $data['anjab_bahan_kerja']                              = TrxAnjabBahanKerja::where('trx_anjab_id', $id_anjab)->get();
        $data['anjab_hasil_kerja']                              = TrxAnjabHasilKerja::where('trx_anjab_id', $id_anjab)->get();
        $data['anjab_korelasi_jabatan']                         = TrxAnjabKorelasiJabatan::where('trx_anjab_id', $id_anjab)->get();
        $data['anjab_lingkungan_kerja']                         = TrxAnjabLingkunganKerja::where('trx_anjab_id', $id_anjab)->first();
        $data['anjab_perangkat']                                = TrxAnjabPerangkat::where('trx_anjab_id', $id_anjab)->get();
        $data['anjab_prestasi_kerja']                           = TrxAnjabPrestasiKerja::where('trx_anjab_id', $id_anjab)->first();
        $data['anjab_resiko_bahaya']                            = TrxAnjabResikoBahaya::where('trx_anjab_id', $id_anjab)->get();
        $data['anjab_tanggung_jawab']                           = TrxAnjabTanggungJawab::where('trx_anjab_id', $id_anjab)->first();
        $data['anjab_wewenang']                                 = TrxAnjabWewenang::where('trx_anjab_id', $id_anjab)->first();
        $data['anjab_syarat_jabatan']                           = TrxAnjabSyaratJabatan::where('trx_anjab_id', $id_anjab)->first();

        $pdf                                                    = \PDF::loadView('anjab.form-view-print', $data);
        $customPaper                                            = array(0,0,595,935);
        $pdf->setPaper($customPaper);

        return $pdf->stream('Lembar Anjab');
    }

    function getJabatan(Request $request){

        $keyword                                                = $request->q;
        $jenis                                                  = $request->jenis;

		if ($keyword) {
            if($jenis == 3){
				$jabatan = MJabatanPelaksana::where('jabatan', 'LIKE', '%'.$keyword.'%')
											->whereYear('updated_at', 2024)
											->orWhereYear('updated_at', 2025)
											->get();
			}elseif($jenis == 2){
                // $jabatan                                        = MJabatanFungsional::where('jabatan', 'LIKE', '%'.$keyword.'%')->get();
				$jabatan = MJabatanFungsional::with('syarat')
							->where('jabatan', 'LIKE', '%'.$keyword.'%')
							->get();
            }elseif($jenis == 1){
				$jabatan = MJabatanStruktural::where(function($query) use ($keyword) {
					$query->where('jabatan', 'LIKE', '%'.$keyword.'%')
						  ->where(function($query) {
							  $query->where('catatan', '=', 'double')
									->orWhereNull('catatan');
						  });
				})->get();
			}

			// $arrJenis = ['', 'struktural', 'fungsional', 'pelaksana'];
			// $jabatan = MJabatan::where('jabatan', 'LIKE', '%'.$request->q.'%')->where('jenis', $arrJenis[$request->jenis])->get();

			if ($jabatan) {
				foreach ($jabatan as $key => $value) {
					// $pangkat = 0;

					// if($value['kualifikasi_pendidikan']){
					// 	if(str_starts_with($value['kualifikasi_pendidikan'], 'SLTA')){
					// 		$pangkat = 21;
					// 	}elseif(str_starts_with($value['kualifikasi_pendidikan'], 'D-3')){
					// 		$pangkat = 23;
					// 	}elseif(str_starts_with($value['kualifikasi_pendidikan'], 'D-4')){
					// 		$pangkat = 31;
					// 	}elseif(str_starts_with($value['kualifikasi_pendidikan'], 'S-1')){
					// 		$pangkat = 31;
					// 	}elseif(str_starts_with($value['kualifikasi_pendidikan'], 'S-2')){
					// 		$pangkat = 32;
					// 	}
					// }

	                $row['id']                                     = $value['id'];
	                $row['full_name']                              = $value['jabatan'];
	                $row['jabatan']                                = $value['jabatan'].' - '.$value['_kode'];
	                // $row['ikhtisar_jabatan'] = $value['tugas_jabatan'];
	                // $row['kualifikasi_pendidikan'] = $value['kualifikasi_pendidikan'];
	                // $row['diklat_penjenjangan'] = $value['diklat_penjenjangan'];
	                $row['kode_jabatan']                           = $value['_kode'];
					$row['kualifikasi_pendidikan'] = $value->syarat ? $value->syarat->pendidikan : '-';
					// $row['jenjang']                           	   = $value->jenjang['jenjang'];
	                // $row['pangkat'] = $pangkat;

	                $output[]                                      = $row;

	            }
	            $data['items']                                     = $output;
	            echo json_encode($data);
			}else{
				exit();
			}


		}
	}
	public function get_jenjang_selected($id, $selected){
		$jenjang = MJabatanFungsionalJenjang::where('id_m_jabatan_fungsional',$id)->get();
		
		// $html = '';
		// foreach ($jenjang as $item){
		// 	$data[] = '<option>'.$item->jenjang.'</option>';
		
		// // return $data;
		// $html = '
		// <div class="form-group">
		// 	<label for="exampleFormControlSelect1">Jenjang (Otomatis)</label>
		// 	<select class="form-control" id="exampleFormControlSelect1">
		// 	'.$data[0].'
		// 	'.$data[1].'
		// 	'.$data[2].'
		// 	</select>
		// </div>';
		// }
		// return $html;


		

		$html = '';

        foreach ($jenjang as $i) {
			if ($i->jenjang == $selected) {
				$html .= "<option selected value='".$i->jenjang."'>".$i->jenjang."</option>";
			} else {
				$html .= "<option value='".$i->jenjang."'>".$i->jenjang."</option>";
			}
        }
        
        echo $html;
	}
	
	public function get_jenjang($id){
		$jenjang = MJabatanFungsionalJenjang::where('id_m_jabatan_fungsional',$id)->get();
		// return $jenjang;
		
		// $html = '';
		// foreach ($jenjang as $item){
		// 	$data[] = '<option>'.$item->jenjang.'</option>';
		
		// // return $data;
		// $html = '
		// <div class="form-group">
		// 	<label for="exampleFormControlSelect1">Jenjang (Otomatis)</label>
		// 	<select class="form-control" id="exampleFormControlSelect1">
		// 	'.$data[0].'
		// 	'.$data[1].'
		// 	'.$data[2].'
		// 	</select>
		// </div>';
		// }
		// return $html;


		

		$html = '';
		if ($jenjang) {
			foreach ($jenjang as $i) {
                
				$html .= "<option value='".$i->jenjang."'>".$i->jenjang."</option>";
					
				}
		} 
        
        
        echo $html;
	}
	
	

   	function getUnitKerja(Request $request){
		if ($request->id) {
			// $kode_unit = substr($request->id, 0, 2);
			$kodeUnit                                                    = $request->id;

			if($kodeUnit == '01'){
				$lengthKode                                                 = strlen($kodeUnit)+4;
			}else{
				$lengthKode                                                 = strlen($kodeUnit)+2;
			}

			$unitKerja                                                   = MUnitKerja::where('kodeunit', 'like', $kodeUnit.'%')->whereRaw('LENGTH(kodeunit) = '.$lengthKode)->get();
			echo '<option value                                          = "" >-- PILIH UNIT KERJA --</option>';
			foreach($unitKerja as $key => $value) {
				echo '<option value                                         = "'.$value['kodeunit'].'">'.$value['unitkerja'].'</option>';
			}
		}
	}

	function getAtasanByOpd(Request $request, AnjabRepository $anjabRepo){
		$pejabatStruktural                                            = $anjabRepo->getStukturalAnjab($request->opd);

		echo "<select>";
		foreach($pejabatStruktural as $itemPejabatStruktural){
			echo "<option>".@$itemPejabatStruktural->jabatan->jabatan."</option>";
		}
		echo "</select>";

	}

	function getOrgConfig(request $request){
		$config = [
			'container' => "#basic-example",
			'connectors' => [
				'type' => 'step'
			],
			'node' => [
                'HTMLclass' => 'nodeExample1'
            ]

		];

		$ceo = [
	        'text' => [
	            'name' => "Mark Hill",
	            'title' => "Chief executive officer",
	            'contact' => "Tel: 01 213 123 134",
	        ],
	        'image' => "../headshots/2.jpg"
	    ];

        $data['config_option']                                  = json_encode(['config' => $config, 'ceo'=>$ceo]);

        return view('anjab.chart-org', $data);
	}

	function kunciAnjab(){
		\Redis::set('anjab:is_kunci', 1);
		return redirect()->back()->with('notify', 'Anjab Telah Dikunci');
	}

	function bukaKunciAnjab(){
		\Redis::del('anjab:is_kunci');
		return redirect()->back()->with('notify', 'Anjab Telah Dibuka');
	}

	function checkAnjab(Request $request){
		$jabatan                                                      = $request->jabatan;
		$jenis                                                        = $request->jenis;

		$trxJabatan                                                   = TrxAnjab::where('m_jabatan_id', $jabatan)->where('jenis_jabatan', $jenis)->first();

		if($trxJabatan == NULL){
			return response()->json(['status'=>'true']);
		}

		return response()->json(['status'=>'true', 'message' => 'Jabatan sudah dianalisis']);
	}

    function detail(Request $request, $id, AnjabRepository $anjabRepo){
        $anjab                                                  = $anjabRepo->getDetailAnjab($id);

		$bakatKerja                                                   = MBakatKerja::all();
        $upayaFisik                                             = MUpayaFisik::all();
        $temperamenKerja                                        = MTemperamenKerja::all();
        $minatKerja                                             = MMinatKerja::all();
        $fungsiPekerjaan                                        = MFungsiPekerjaan::all();

        $data = [
            'anjab'=> $anjab,
            'bakatKerja'=> $bakatKerja,
            'upayaFisik'=> $upayaFisik,
            'temperamenKerja'=> $temperamenKerja,
            'minatKerja'=> $minatKerja,
            'fungsiPekerjaan'=> $fungsiPekerjaan,
        ];
		// return $data;
        return view('anjab.ringkasan.browse', $data);
    }

    function loadModalTolak(Request $request){

        $data = [
            'id' => $request->id,
            'jenis' => $request->jenis
        ];

        return view('anjab.verifikasi.modal-tolak', $data);
    }

    function verifikasi(Request $request, AnjabRepository $anjabRepo){
		$jenis                                                        = $request->jenis;
        $id                                                     = $request->id;
        $status                                                 = $request->status;
        $keterangan                                             = $request->keterangan;

        $anjabRepo->verifikasi($jenis, $id, $status, $keterangan);

        return 1;


    }

	function duplicate(Request $request) {
		if ($request->jenis == '1') {
			$anjabDuplicate = TrxAnjab::find($request->id);
			$newAnjab 		= $anjabDuplicate->replicate();
			$newAnjab->user = \MojokertokabUser::getUser()->option->nip_baru;
			$newAnjab->status_data_jabatan = null;
			$newAnjab->status_tanggung_jawab = null;
			$newAnjab->status_wewenang = null;
			$newAnjab->status_korelasi_jabatan = null;
			$newAnjab->status_bahan_kerja = null;
			$newAnjab->status_perangkat = null;
			$newAnjab->status_hasil_kerja = null;
			$newAnjab->status_kondisi_lingkungan_kerja = null;
			$newAnjab->status_resiko_bahaya = null;
			$newAnjab->status_syarat_jabatan = null;
			$newAnjab->status_uraian_tugas = null;
			$newAnjab->status_prestasi_kerja = null;
			$duplikat 		= $newAnjab->save();

			$getIdDuplikat = $newAnjab->id;
			
			$anjabUraianJabatanDuplicate = TrxAnjabUraianJabatan::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabUraianJabatanDuplicate) ; $i++) { 
				$duplicateUraianJabatan 				= new TrxAnjabUraianJabatan;
				$duplicateUraianJabatan->trx_anjab_id 	= $getIdDuplikat;
				$duplicateUraianJabatan->uraian_jabatan = $anjabUraianJabatanDuplicate[$i]->uraian_jabatan;
				$duplicateUraianJabatan->save();
			}

			$anjabTanggungJawabDuplicate = TrxAnjabTanggungJawab::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabTanggungJawabDuplicate) ; $i++) { 
				$duplicateTanggungJawab 				= new TrxAnjabTanggungJawab;
				$duplicateTanggungJawab->trx_anjab_id 	= $getIdDuplikat;
				$duplicateTanggungJawab->tanggung_jawab = $anjabTanggungJawabDuplicate[$i]->tanggung_jawab;
				$duplicateTanggungJawab->save();
			}

			$anjabWewenangDuplicate = TrxAnjabWewenang::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabWewenangDuplicate) ; $i++) { 
				$duplicateWewenang 					= new TrxAnjabWewenang;
				$duplicateWewenang->trx_anjab_id 	= $getIdDuplikat;
				$duplicateWewenang->wewenang 		= $anjabWewenangDuplicate[$i]->wewenang;
				$duplicateWewenang->save();
			}

			$anjabKorelasiJabatanDuplicate = TrxAnjabKorelasiJabatan::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabKorelasiJabatanDuplicate) ; $i++) { 
				$duplicateKorelasiJabatan 				= new TrxAnjabKorelasiJabatan;
				$duplicateKorelasiJabatan->trx_anjab_id	= $getIdDuplikat;
				$duplicateKorelasiJabatan->jabatan 		= $anjabKorelasiJabatanDuplicate[$i]->jabatan;
				$duplicateKorelasiJabatan->unit_kerja	= $anjabKorelasiJabatanDuplicate[$i]->unit_kerja;
				$duplicateKorelasiJabatan->hal 			= $anjabKorelasiJabatanDuplicate[$i]->hal;
				$duplicateKorelasiJabatan->save();
			}

			$anjabBahanKerjaDuplicate = TrxAnjabBahanKerja::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabBahanKerjaDuplicate) ; $i++) { 
				$duplicateBahanKerja 							= new TrxAnjabBahanKerja;
				$duplicateBahanKerja->trx_anjab_id				= $getIdDuplikat;
				$duplicateBahanKerja->bahan_kerja 				= $anjabBahanKerjaDuplicate[$i]->bahan_kerja;
				$duplicateBahanKerja->penggunaan_dalam_tugas	= $anjabBahanKerjaDuplicate[$i]->penggunaan_dalam_tugas;
				$duplicateBahanKerja->save();
			}

			$anjabPerangkatDuplicate = TrxAnjabPerangkat::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabPerangkatDuplicate) ; $i++) { 
				$duplicatePerangkat 						= new TrxAnjabPerangkat;
				$duplicatePerangkat->trx_anjab_id			= $getIdDuplikat;
				$duplicatePerangkat->perangkat_kerja		= $anjabPerangkatDuplicate[$i]->perangkat_kerja;
				$duplicatePerangkat->digunakan_untuk_tugas	= $anjabPerangkatDuplicate[$i]->digunakan_untuk_tugas;
				$duplicatePerangkat->save();
			}

			$anjabHasilKerjaDuplicate = TrxAnjabHasilKerja::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabHasilKerjaDuplicate) ; $i++) { 
				$duplicateHasilKerja 				= new TrxAnjabHasilKerja;
				$duplicateHasilKerja->trx_anjab_id	= $getIdDuplikat;
				$duplicateHasilKerja->hasil_kerja	= $anjabHasilKerjaDuplicate[$i]->hasil_kerja;
				$duplicateHasilKerja->satuan_hasil	= $anjabHasilKerjaDuplicate[$i]->satuan_hasil;
				$duplicateHasilKerja->save();
			}

			$anjabLingkunganKerjaDuplicate = TrxAnjabLingkunganKerja::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabLingkunganKerjaDuplicate) ; $i++) { 
				$duplicateLingkunganKerja 						= new TrxAnjabLingkunganKerja;
				$duplicateLingkunganKerja->trx_anjab_id			= $getIdDuplikat;
				$duplicateLingkunganKerja->tempat_kerja			= $anjabLingkunganKerjaDuplicate[$i]->tempat_kerja;
				$duplicateLingkunganKerja->suhu					= $anjabLingkunganKerjaDuplicate[$i]->suhu;
				$duplicateLingkunganKerja->udara				= $anjabLingkunganKerjaDuplicate[$i]->udara;
				$duplicateLingkunganKerja->keadaan_ruangan		= $anjabLingkunganKerjaDuplicate[$i]->keadaan_ruangan;
				$duplicateLingkunganKerja->letak				= $anjabLingkunganKerjaDuplicate[$i]->letak;
				$duplicateLingkunganKerja->penerangan			= $anjabLingkunganKerjaDuplicate[$i]->penerangan;
				$duplicateLingkunganKerja->suara				= $anjabLingkunganKerjaDuplicate[$i]->suara;
				$duplicateLingkunganKerja->keadaan_tempat_kerja	= $anjabLingkunganKerjaDuplicate[$i]->keadaan_tempat_kerja;
				$duplicateLingkunganKerja->getaran				= $anjabLingkunganKerjaDuplicate[$i]->getaran;
				$duplicateLingkunganKerja->save();
			}

			$anjabResikoBahayaDuplicate = TrxAnjabResikoBahaya::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabResikoBahayaDuplicate) ; $i++) { 
				$duplicateResikoBahaya 					= new TrxAnjabResikoBahaya;
				$duplicateResikoBahaya->trx_anjab_id	= $getIdDuplikat;
				$duplicateResikoBahaya->resiko_bahaya	= $anjabResikoBahayaDuplicate[$i]->resiko_bahaya;
				$duplicateResikoBahaya->penyebab		= $anjabResikoBahayaDuplicate[$i]->penyebab;
				$duplicateResikoBahaya->save();
			}

			$anjabSyaratJabatanDuplicate = TrxAnjabSyaratJabatan::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabSyaratJabatanDuplicate) ; $i++) { 
				$duplicateSyaratJabatan 								= new TrxAnjabSyaratJabatan;
				$duplicateSyaratJabatan->trx_anjab_id					= $getIdDuplikat;
				$duplicateSyaratJabatan->keterampilan_kerja				= $anjabSyaratJabatanDuplicate[$i]->keterampilan_kerja;
				$duplicateSyaratJabatan->intelegensia					= $anjabSyaratJabatanDuplicate[$i]->intelegensia;
				$duplicateSyaratJabatan->bakat_verbal					= $anjabSyaratJabatanDuplicate[$i]->bakat_verbal;
				$duplicateSyaratJabatan->bakat_numerik					= $anjabSyaratJabatanDuplicate[$i]->bakat_numerik;
				$duplicateSyaratJabatan->bakat_ketelitian				= $anjabSyaratJabatanDuplicate[$i]->bakat_ketelitian;
				$duplicateSyaratJabatan->directing_control_planning		= $anjabSyaratJabatanDuplicate[$i]->directing_control_planning;
				$duplicateSyaratJabatan->feeling_idea_fact				= $anjabSyaratJabatanDuplicate[$i]->feeling_idea_fact;
				$duplicateSyaratJabatan->influencing					= $anjabSyaratJabatanDuplicate[$i]->influencing;
				$duplicateSyaratJabatan->measurable_verifiable_creteria	= $anjabSyaratJabatanDuplicate[$i]->measurable_verifiable_creteria;
				$duplicateSyaratJabatan->dealing_with_people			= $anjabSyaratJabatanDuplicate[$i]->dealing_with_people;
				$duplicateSyaratJabatan->repetitive_continuous			= $anjabSyaratJabatanDuplicate[$i]->repetitive_continuous;
				$duplicateSyaratJabatan->investigatif					= $anjabSyaratJabatanDuplicate[$i]->investigatif;
				$duplicateSyaratJabatan->konvensional					= $anjabSyaratJabatanDuplicate[$i]->konvensional;
				$duplicateSyaratJabatan->duduk							= $anjabSyaratJabatanDuplicate[$i]->duduk;
				$duplicateSyaratJabatan->berbicara						= $anjabSyaratJabatanDuplicate[$i]->berbicara;
				$duplicateSyaratJabatan->melihat						= $anjabSyaratJabatanDuplicate[$i]->melihat;
				$duplicateSyaratJabatan->jenis_kelamin					= $anjabSyaratJabatanDuplicate[$i]->jenis_kelamin;
				$duplicateSyaratJabatan->umur							= $anjabSyaratJabatanDuplicate[$i]->umur;
				$duplicateSyaratJabatan->tinggi_badan					= $anjabSyaratJabatanDuplicate[$i]->tinggi_badan;
				$duplicateSyaratJabatan->berat_badan					= $anjabSyaratJabatanDuplicate[$i]->berat_badan;
				$duplicateSyaratJabatan->postur_badan					= $anjabSyaratJabatanDuplicate[$i]->postur_badan;
				$duplicateSyaratJabatan->penampilan						= $anjabSyaratJabatanDuplicate[$i]->penampilan;
				$duplicateSyaratJabatan->mengkoordinasi_data			= $anjabSyaratJabatanDuplicate[$i]->mengkoordinasi_data;
				$duplicateSyaratJabatan->menganalisis_data				= $anjabSyaratJabatanDuplicate[$i]->menganalisis_data;
				$duplicateSyaratJabatan->menyusun_data					= $anjabSyaratJabatanDuplicate[$i]->menyusun_data;
				$duplicateSyaratJabatan->menasehati						= $anjabSyaratJabatanDuplicate[$i]->menasehati;
				$duplicateSyaratJabatan->berunding						= $anjabSyaratJabatanDuplicate[$i]->berunding;
				$duplicateSyaratJabatan->mengajar						= $anjabSyaratJabatanDuplicate[$i]->mengajar;
				$duplicateSyaratJabatan->menyelia						= $anjabSyaratJabatanDuplicate[$i]->menyelia;
				$duplicateSyaratJabatan->minat_kerja					= $anjabSyaratJabatanDuplicate[$i]->minat_kerja;
				$duplicateSyaratJabatan->temperamen_kerja				= $anjabSyaratJabatanDuplicate[$i]->temperamen_kerja;
				$duplicateSyaratJabatan->fungsi_pekerjaan				= $anjabSyaratJabatanDuplicate[$i]->fungsi_pekerjaan;
				$duplicateSyaratJabatan->upaya_fisik					= $anjabSyaratJabatanDuplicate[$i]->upaya_fisik;
				$duplicateSyaratJabatan->bakat_kerja					= $anjabSyaratJabatanDuplicate[$i]->bakat_kerja;
				$duplicateSyaratJabatan->save();
			}

			$anjabPrestasiKerjaDuplicate = TrxAnjabPrestasiKerja::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabPrestasiKerjaDuplicate) ; $i++) { 
				$duplicatePrestasiKerja 					= new TrxAnjabPrestasiKerja;
				$duplicatePrestasiKerja->trx_anjab_id		= $getIdDuplikat;
				$duplicatePrestasiKerja->prestasi_kerja		= $anjabPrestasiKerjaDuplicate[$i]->prestasi_kerja;
				$duplicatePrestasiKerja->save();
			}

			if ($duplikat) {
				return response()->json([
					'success' => true,
					'message' => 'Data Berhasil Diduplikasi'
				],200);
			} else {
				return response()->json([
					'success' => false,
					'message' => 'Data Gagal Diduplikasi'
				]);
			}
		}elseif ($request->jenis == '2') {
			$anjabDuplicate = TrxAnjab::find($request->id);
			$newAnjab 		= $anjabDuplicate->replicate();
			$newAnjab->user = \MojokertokabUser::getUser()->option->nip_baru;
			$newAnjab->status_data_jabatan = null;
			$newAnjab->status_tanggung_jawab = null;
			$newAnjab->status_wewenang = null;
			$newAnjab->status_korelasi_jabatan = null;
			$newAnjab->status_bahan_kerja = null;
			$newAnjab->status_perangkat = null;
			$newAnjab->status_hasil_kerja = null;
			$newAnjab->status_kondisi_lingkungan_kerja = null;
			$newAnjab->status_resiko_bahaya = null;
			$newAnjab->status_syarat_jabatan = null;
			$newAnjab->status_uraian_tugas = null;
			$newAnjab->status_prestasi_kerja = null;

			$duplikat 		= $newAnjab->save();

			$getIdDuplikat = $newAnjab->id;
			
			$anjabUraianJabatanDuplicate = TrxAnjabUraianJabatan::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabUraianJabatanDuplicate) ; $i++) { 
				$duplicateUraianJabatan 				= new TrxAnjabUraianJabatan;
				$duplicateUraianJabatan->trx_anjab_id 	= $getIdDuplikat;
				$duplicateUraianJabatan->uraian_jabatan = $anjabUraianJabatanDuplicate[$i]->uraian_jabatan;
				$duplicateUraianJabatan->save();
			}

			$anjabTanggungJawabDuplicate = TrxAnjabTanggungJawab::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabTanggungJawabDuplicate) ; $i++) { 
				$duplicateTanggungJawab 				= new TrxAnjabTanggungJawab;
				$duplicateTanggungJawab->trx_anjab_id 	= $getIdDuplikat;
				$duplicateTanggungJawab->tanggung_jawab = $anjabTanggungJawabDuplicate[$i]->tanggung_jawab;
				$duplicateTanggungJawab->save();
			}

			$anjabWewenangDuplicate = TrxAnjabWewenang::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabWewenangDuplicate) ; $i++) { 
				$duplicateWewenang 					= new TrxAnjabWewenang;
				$duplicateWewenang->trx_anjab_id 	= $getIdDuplikat;
				$duplicateWewenang->wewenang 		= $anjabWewenangDuplicate[$i]->wewenang;
				$duplicateWewenang->save();
			}

			$anjabKorelasiJabatanDuplicate = TrxAnjabKorelasiJabatan::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabKorelasiJabatanDuplicate) ; $i++) { 
				$duplicateKorelasiJabatan 				= new TrxAnjabKorelasiJabatan;
				$duplicateKorelasiJabatan->trx_anjab_id	= $getIdDuplikat;
				$duplicateKorelasiJabatan->jabatan 		= $anjabKorelasiJabatanDuplicate[$i]->jabatan;
				$duplicateKorelasiJabatan->unit_kerja	= $anjabKorelasiJabatanDuplicate[$i]->unit_kerja;
				$duplicateKorelasiJabatan->hal 			= $anjabKorelasiJabatanDuplicate[$i]->hal;
				$duplicateKorelasiJabatan->save();
			}

			$anjabBahanKerjaDuplicate = TrxAnjabBahanKerja::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabBahanKerjaDuplicate) ; $i++) { 
				$duplicateBahanKerja 							= new TrxAnjabBahanKerja;
				$duplicateBahanKerja->trx_anjab_id				= $getIdDuplikat;
				$duplicateBahanKerja->bahan_kerja 				= $anjabBahanKerjaDuplicate[$i]->bahan_kerja;
				$duplicateBahanKerja->penggunaan_dalam_tugas	= $anjabBahanKerjaDuplicate[$i]->penggunaan_dalam_tugas;
				$duplicateBahanKerja->save();
			}

			$anjabPerangkatDuplicate = TrxAnjabPerangkat::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabPerangkatDuplicate) ; $i++) { 
				$duplicatePerangkat 						= new TrxAnjabPerangkat;
				$duplicatePerangkat->trx_anjab_id			= $getIdDuplikat;
				$duplicatePerangkat->perangkat_kerja		= $anjabPerangkatDuplicate[$i]->perangkat_kerja;
				$duplicatePerangkat->digunakan_untuk_tugas	= $anjabPerangkatDuplicate[$i]->digunakan_untuk_tugas;
				$duplicatePerangkat->save();
			}

			$anjabHasilKerjaDuplicate = TrxAnjabHasilKerja::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabHasilKerjaDuplicate) ; $i++) { 
				$duplicateHasilKerja 				= new TrxAnjabHasilKerja;
				$duplicateHasilKerja->trx_anjab_id	= $getIdDuplikat;
				$duplicateHasilKerja->hasil_kerja	= $anjabHasilKerjaDuplicate[$i]->hasil_kerja;
				$duplicateHasilKerja->satuan_hasil	= $anjabHasilKerjaDuplicate[$i]->satuan_hasil;
				$duplicateHasilKerja->save();
			}

			$anjabLingkunganKerjaDuplicate = TrxAnjabLingkunganKerja::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabLingkunganKerjaDuplicate) ; $i++) { 
				$duplicateLingkunganKerja 						= new TrxAnjabLingkunganKerja;
				$duplicateLingkunganKerja->trx_anjab_id			= $getIdDuplikat;
				$duplicateLingkunganKerja->tempat_kerja			= $anjabLingkunganKerjaDuplicate[$i]->tempat_kerja;
				$duplicateLingkunganKerja->suhu					= $anjabLingkunganKerjaDuplicate[$i]->suhu;
				$duplicateLingkunganKerja->udara				= $anjabLingkunganKerjaDuplicate[$i]->udara;
				$duplicateLingkunganKerja->keadaan_ruangan		= $anjabLingkunganKerjaDuplicate[$i]->keadaan_ruangan;
				$duplicateLingkunganKerja->letak				= $anjabLingkunganKerjaDuplicate[$i]->letak;
				$duplicateLingkunganKerja->penerangan			= $anjabLingkunganKerjaDuplicate[$i]->penerangan;
				$duplicateLingkunganKerja->suara				= $anjabLingkunganKerjaDuplicate[$i]->suara;
				$duplicateLingkunganKerja->keadaan_tempat_kerja	= $anjabLingkunganKerjaDuplicate[$i]->keadaan_tempat_kerja;
				$duplicateLingkunganKerja->getaran				= $anjabLingkunganKerjaDuplicate[$i]->getaran;
				$duplicateLingkunganKerja->save();
			}

			$anjabResikoBahayaDuplicate = TrxAnjabResikoBahaya::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabResikoBahayaDuplicate) ; $i++) { 
				$duplicateResikoBahaya 					= new TrxAnjabResikoBahaya;
				$duplicateResikoBahaya->trx_anjab_id	= $getIdDuplikat;
				$duplicateResikoBahaya->resiko_bahaya	= $anjabResikoBahayaDuplicate[$i]->resiko_bahaya;
				$duplicateResikoBahaya->penyebab		= $anjabResikoBahayaDuplicate[$i]->penyebab;
				$duplicateResikoBahaya->save();
			}

			$anjabSyaratJabatanDuplicate = TrxAnjabSyaratJabatan::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabSyaratJabatanDuplicate) ; $i++) { 
				$duplicateSyaratJabatan 								= new TrxAnjabSyaratJabatan;
				$duplicateSyaratJabatan->trx_anjab_id					= $getIdDuplikat;
				$duplicateSyaratJabatan->keterampilan_kerja				= $anjabSyaratJabatanDuplicate[$i]->keterampilan_kerja;
				$duplicateSyaratJabatan->intelegensia					= $anjabSyaratJabatanDuplicate[$i]->intelegensia;
				$duplicateSyaratJabatan->bakat_verbal					= $anjabSyaratJabatanDuplicate[$i]->bakat_verbal;
				$duplicateSyaratJabatan->bakat_numerik					= $anjabSyaratJabatanDuplicate[$i]->bakat_numerik;
				$duplicateSyaratJabatan->bakat_ketelitian				= $anjabSyaratJabatanDuplicate[$i]->bakat_ketelitian;
				$duplicateSyaratJabatan->directing_control_planning		= $anjabSyaratJabatanDuplicate[$i]->directing_control_planning;
				$duplicateSyaratJabatan->feeling_idea_fact				= $anjabSyaratJabatanDuplicate[$i]->feeling_idea_fact;
				$duplicateSyaratJabatan->influencing					= $anjabSyaratJabatanDuplicate[$i]->influencing;
				$duplicateSyaratJabatan->measurable_verifiable_creteria	= $anjabSyaratJabatanDuplicate[$i]->measurable_verifiable_creteria;
				$duplicateSyaratJabatan->dealing_with_people			= $anjabSyaratJabatanDuplicate[$i]->dealing_with_people;
				$duplicateSyaratJabatan->repetitive_continuous			= $anjabSyaratJabatanDuplicate[$i]->repetitive_continuous;
				$duplicateSyaratJabatan->investigatif					= $anjabSyaratJabatanDuplicate[$i]->investigatif;
				$duplicateSyaratJabatan->konvensional					= $anjabSyaratJabatanDuplicate[$i]->konvensional;
				$duplicateSyaratJabatan->duduk							= $anjabSyaratJabatanDuplicate[$i]->duduk;
				$duplicateSyaratJabatan->berbicara						= $anjabSyaratJabatanDuplicate[$i]->berbicara;
				$duplicateSyaratJabatan->melihat						= $anjabSyaratJabatanDuplicate[$i]->melihat;
				$duplicateSyaratJabatan->jenis_kelamin					= $anjabSyaratJabatanDuplicate[$i]->jenis_kelamin;
				$duplicateSyaratJabatan->umur							= $anjabSyaratJabatanDuplicate[$i]->umur;
				$duplicateSyaratJabatan->tinggi_badan					= $anjabSyaratJabatanDuplicate[$i]->tinggi_badan;
				$duplicateSyaratJabatan->berat_badan					= $anjabSyaratJabatanDuplicate[$i]->berat_badan;
				$duplicateSyaratJabatan->postur_badan					= $anjabSyaratJabatanDuplicate[$i]->postur_badan;
				$duplicateSyaratJabatan->penampilan						= $anjabSyaratJabatanDuplicate[$i]->penampilan;
				$duplicateSyaratJabatan->mengkoordinasi_data			= $anjabSyaratJabatanDuplicate[$i]->mengkoordinasi_data;
				$duplicateSyaratJabatan->menganalisis_data				= $anjabSyaratJabatanDuplicate[$i]->menganalisis_data;
				$duplicateSyaratJabatan->menyusun_data					= $anjabSyaratJabatanDuplicate[$i]->menyusun_data;
				$duplicateSyaratJabatan->menasehati						= $anjabSyaratJabatanDuplicate[$i]->menasehati;
				$duplicateSyaratJabatan->berunding						= $anjabSyaratJabatanDuplicate[$i]->berunding;
				$duplicateSyaratJabatan->mengajar						= $anjabSyaratJabatanDuplicate[$i]->mengajar;
				$duplicateSyaratJabatan->menyelia						= $anjabSyaratJabatanDuplicate[$i]->menyelia;
				$duplicateSyaratJabatan->minat_kerja					= $anjabSyaratJabatanDuplicate[$i]->minat_kerja;
				$duplicateSyaratJabatan->temperamen_kerja				= $anjabSyaratJabatanDuplicate[$i]->temperamen_kerja;
				$duplicateSyaratJabatan->fungsi_pekerjaan				= $anjabSyaratJabatanDuplicate[$i]->fungsi_pekerjaan;
				$duplicateSyaratJabatan->upaya_fisik					= $anjabSyaratJabatanDuplicate[$i]->upaya_fisik;
				$duplicateSyaratJabatan->bakat_kerja					= $anjabSyaratJabatanDuplicate[$i]->bakat_kerja;
				$duplicateSyaratJabatan->save();
			}

			$anjabPrestasiKerjaDuplicate = TrxAnjabPrestasiKerja::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabPrestasiKerjaDuplicate) ; $i++) { 
				$duplicatePrestasiKerja 					= new TrxAnjabPrestasiKerja;
				$duplicatePrestasiKerja->trx_anjab_id		= $getIdDuplikat;
				$duplicatePrestasiKerja->prestasi_kerja		= $anjabPrestasiKerjaDuplicate[$i]->prestasi_kerja;
				$duplicatePrestasiKerja->save();
			}

			if ($duplikat) {
				return response()->json([
					'success' => true,
					'message' => 'Data Berhasil Diduplikasi'
				],200);
			} else {
				return response()->json([
					'success' => false,
					'message' => 'Data Gagal Diduplikasi'
				]);
			}
		}elseif ($request->jenis == '3') {
			$anjabDuplicate = TrxAnjab::find($request->id);
			$newAnjab 		= $anjabDuplicate->replicate();
			$newAnjab->user = \MojokertokabUser::getUser()->option->nip_baru;
			$newAnjab->status_data_jabatan = null;
			$newAnjab->status_tanggung_jawab = null;
			$newAnjab->status_wewenang = null;
			$newAnjab->status_korelasi_jabatan = null;
			$newAnjab->status_bahan_kerja = null;
			$newAnjab->status_perangkat = null;
			$newAnjab->status_hasil_kerja = null;
			$newAnjab->status_kondisi_lingkungan_kerja = null;
			$newAnjab->status_resiko_bahaya = null;
			$newAnjab->status_syarat_jabatan = null;
			$newAnjab->status_uraian_tugas = null;
			$newAnjab->status_prestasi_kerja = null;
			
			$duplikat 		= $newAnjab->save();

			$getIdDuplikat = $newAnjab->id;
			
			$anjabUraianJabatanDuplicate = TrxAnjabUraianJabatan::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabUraianJabatanDuplicate) ; $i++) { 
				$duplicateUraianJabatan 				= new TrxAnjabUraianJabatan;
				$duplicateUraianJabatan->trx_anjab_id 	= $getIdDuplikat;
				$duplicateUraianJabatan->uraian_jabatan = $anjabUraianJabatanDuplicate[$i]->uraian_jabatan;
				$duplicateUraianJabatan->save();
			}

			$anjabTanggungJawabDuplicate = TrxAnjabTanggungJawab::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabTanggungJawabDuplicate) ; $i++) { 
				$duplicateTanggungJawab 				= new TrxAnjabTanggungJawab;
				$duplicateTanggungJawab->trx_anjab_id 	= $getIdDuplikat;
				$duplicateTanggungJawab->tanggung_jawab = $anjabTanggungJawabDuplicate[$i]->tanggung_jawab;
				$duplicateTanggungJawab->save();
			}

			$anjabWewenangDuplicate = TrxAnjabWewenang::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabWewenangDuplicate) ; $i++) { 
				$duplicateWewenang 					= new TrxAnjabWewenang;
				$duplicateWewenang->trx_anjab_id 	= $getIdDuplikat;
				$duplicateWewenang->wewenang 		= $anjabWewenangDuplicate[$i]->wewenang;
				$duplicateWewenang->save();
			}

			$anjabKorelasiJabatanDuplicate = TrxAnjabKorelasiJabatan::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabKorelasiJabatanDuplicate) ; $i++) { 
				$duplicateKorelasiJabatan 				= new TrxAnjabKorelasiJabatan;
				$duplicateKorelasiJabatan->trx_anjab_id	= $getIdDuplikat;
				$duplicateKorelasiJabatan->jabatan 		= $anjabKorelasiJabatanDuplicate[$i]->jabatan;
				$duplicateKorelasiJabatan->unit_kerja	= $anjabKorelasiJabatanDuplicate[$i]->unit_kerja;
				$duplicateKorelasiJabatan->hal 			= $anjabKorelasiJabatanDuplicate[$i]->hal;
				$duplicateKorelasiJabatan->save();
			}

			$anjabBahanKerjaDuplicate = TrxAnjabBahanKerja::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabBahanKerjaDuplicate) ; $i++) { 
				$duplicateBahanKerja 							= new TrxAnjabBahanKerja;
				$duplicateBahanKerja->trx_anjab_id				= $getIdDuplikat;
				$duplicateBahanKerja->bahan_kerja 				= $anjabBahanKerjaDuplicate[$i]->bahan_kerja;
				$duplicateBahanKerja->penggunaan_dalam_tugas	= $anjabBahanKerjaDuplicate[$i]->penggunaan_dalam_tugas;
				$duplicateBahanKerja->save();
			}

			$anjabPerangkatDuplicate = TrxAnjabPerangkat::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabPerangkatDuplicate) ; $i++) { 
				$duplicatePerangkat 						= new TrxAnjabPerangkat;
				$duplicatePerangkat->trx_anjab_id			= $getIdDuplikat;
				$duplicatePerangkat->perangkat_kerja		= $anjabPerangkatDuplicate[$i]->perangkat_kerja;
				$duplicatePerangkat->digunakan_untuk_tugas	= $anjabPerangkatDuplicate[$i]->digunakan_untuk_tugas;
				$duplicatePerangkat->save();
			}

			$anjabHasilKerjaDuplicate = TrxAnjabHasilKerja::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabHasilKerjaDuplicate) ; $i++) { 
				$duplicateHasilKerja 				= new TrxAnjabHasilKerja;
				$duplicateHasilKerja->trx_anjab_id	= $getIdDuplikat;
				$duplicateHasilKerja->hasil_kerja	= $anjabHasilKerjaDuplicate[$i]->hasil_kerja;
				$duplicateHasilKerja->satuan_hasil	= $anjabHasilKerjaDuplicate[$i]->satuan_hasil;
				$duplicateHasilKerja->save();
			}

			$anjabLingkunganKerjaDuplicate = TrxAnjabLingkunganKerja::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabLingkunganKerjaDuplicate) ; $i++) { 
				$duplicateLingkunganKerja 						= new TrxAnjabLingkunganKerja;
				$duplicateLingkunganKerja->trx_anjab_id			= $getIdDuplikat;
				$duplicateLingkunganKerja->tempat_kerja			= $anjabLingkunganKerjaDuplicate[$i]->tempat_kerja;
				$duplicateLingkunganKerja->suhu					= $anjabLingkunganKerjaDuplicate[$i]->suhu;
				$duplicateLingkunganKerja->udara				= $anjabLingkunganKerjaDuplicate[$i]->udara;
				$duplicateLingkunganKerja->keadaan_ruangan		= $anjabLingkunganKerjaDuplicate[$i]->keadaan_ruangan;
				$duplicateLingkunganKerja->letak				= $anjabLingkunganKerjaDuplicate[$i]->letak;
				$duplicateLingkunganKerja->penerangan			= $anjabLingkunganKerjaDuplicate[$i]->penerangan;
				$duplicateLingkunganKerja->suara				= $anjabLingkunganKerjaDuplicate[$i]->suara;
				$duplicateLingkunganKerja->keadaan_tempat_kerja	= $anjabLingkunganKerjaDuplicate[$i]->keadaan_tempat_kerja;
				$duplicateLingkunganKerja->getaran				= $anjabLingkunganKerjaDuplicate[$i]->getaran;
				$duplicateLingkunganKerja->save();
			}

			$anjabResikoBahayaDuplicate = TrxAnjabResikoBahaya::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabResikoBahayaDuplicate) ; $i++) { 
				$duplicateResikoBahaya 					= new TrxAnjabResikoBahaya;
				$duplicateResikoBahaya->trx_anjab_id	= $getIdDuplikat;
				$duplicateResikoBahaya->resiko_bahaya	= $anjabResikoBahayaDuplicate[$i]->resiko_bahaya;
				$duplicateResikoBahaya->penyebab		= $anjabResikoBahayaDuplicate[$i]->penyebab;
				$duplicateResikoBahaya->save();
			}

			$anjabSyaratJabatanDuplicate = TrxAnjabSyaratJabatan::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabSyaratJabatanDuplicate) ; $i++) { 
				$duplicateSyaratJabatan 								= new TrxAnjabSyaratJabatan;
				$duplicateSyaratJabatan->trx_anjab_id					= $getIdDuplikat;
				$duplicateSyaratJabatan->keterampilan_kerja				= $anjabSyaratJabatanDuplicate[$i]->keterampilan_kerja;
				$duplicateSyaratJabatan->intelegensia					= $anjabSyaratJabatanDuplicate[$i]->intelegensia;
				$duplicateSyaratJabatan->bakat_verbal					= $anjabSyaratJabatanDuplicate[$i]->bakat_verbal;
				$duplicateSyaratJabatan->bakat_numerik					= $anjabSyaratJabatanDuplicate[$i]->bakat_numerik;
				$duplicateSyaratJabatan->bakat_ketelitian				= $anjabSyaratJabatanDuplicate[$i]->bakat_ketelitian;
				$duplicateSyaratJabatan->directing_control_planning		= $anjabSyaratJabatanDuplicate[$i]->directing_control_planning;
				$duplicateSyaratJabatan->feeling_idea_fact				= $anjabSyaratJabatanDuplicate[$i]->feeling_idea_fact;
				$duplicateSyaratJabatan->influencing					= $anjabSyaratJabatanDuplicate[$i]->influencing;
				$duplicateSyaratJabatan->measurable_verifiable_creteria	= $anjabSyaratJabatanDuplicate[$i]->measurable_verifiable_creteria;
				$duplicateSyaratJabatan->dealing_with_people			= $anjabSyaratJabatanDuplicate[$i]->dealing_with_people;
				$duplicateSyaratJabatan->repetitive_continuous			= $anjabSyaratJabatanDuplicate[$i]->repetitive_continuous;
				$duplicateSyaratJabatan->investigatif					= $anjabSyaratJabatanDuplicate[$i]->investigatif;
				$duplicateSyaratJabatan->konvensional					= $anjabSyaratJabatanDuplicate[$i]->konvensional;
				$duplicateSyaratJabatan->duduk							= $anjabSyaratJabatanDuplicate[$i]->duduk;
				$duplicateSyaratJabatan->berbicara						= $anjabSyaratJabatanDuplicate[$i]->berbicara;
				$duplicateSyaratJabatan->melihat						= $anjabSyaratJabatanDuplicate[$i]->melihat;
				$duplicateSyaratJabatan->jenis_kelamin					= $anjabSyaratJabatanDuplicate[$i]->jenis_kelamin;
				$duplicateSyaratJabatan->umur							= $anjabSyaratJabatanDuplicate[$i]->umur;
				$duplicateSyaratJabatan->tinggi_badan					= $anjabSyaratJabatanDuplicate[$i]->tinggi_badan;
				$duplicateSyaratJabatan->berat_badan					= $anjabSyaratJabatanDuplicate[$i]->berat_badan;
				$duplicateSyaratJabatan->postur_badan					= $anjabSyaratJabatanDuplicate[$i]->postur_badan;
				$duplicateSyaratJabatan->penampilan						= $anjabSyaratJabatanDuplicate[$i]->penampilan;
				$duplicateSyaratJabatan->mengkoordinasi_data			= $anjabSyaratJabatanDuplicate[$i]->mengkoordinasi_data;
				$duplicateSyaratJabatan->menganalisis_data				= $anjabSyaratJabatanDuplicate[$i]->menganalisis_data;
				$duplicateSyaratJabatan->menyusun_data					= $anjabSyaratJabatanDuplicate[$i]->menyusun_data;
				$duplicateSyaratJabatan->menasehati						= $anjabSyaratJabatanDuplicate[$i]->menasehati;
				$duplicateSyaratJabatan->berunding						= $anjabSyaratJabatanDuplicate[$i]->berunding;
				$duplicateSyaratJabatan->mengajar						= $anjabSyaratJabatanDuplicate[$i]->mengajar;
				$duplicateSyaratJabatan->menyelia						= $anjabSyaratJabatanDuplicate[$i]->menyelia;
				$duplicateSyaratJabatan->minat_kerja					= $anjabSyaratJabatanDuplicate[$i]->minat_kerja;
				$duplicateSyaratJabatan->temperamen_kerja				= $anjabSyaratJabatanDuplicate[$i]->temperamen_kerja;
				$duplicateSyaratJabatan->fungsi_pekerjaan				= $anjabSyaratJabatanDuplicate[$i]->fungsi_pekerjaan;
				$duplicateSyaratJabatan->upaya_fisik					= $anjabSyaratJabatanDuplicate[$i]->upaya_fisik;
				$duplicateSyaratJabatan->bakat_kerja					= $anjabSyaratJabatanDuplicate[$i]->bakat_kerja;
				$duplicateSyaratJabatan->save();
			}

			$anjabPrestasiKerjaDuplicate = TrxAnjabPrestasiKerja::where('trx_anjab_id',$request->id)->get();
			for ($i=0; $i <count($anjabPrestasiKerjaDuplicate) ; $i++) { 
				$duplicatePrestasiKerja 					= new TrxAnjabPrestasiKerja;
				$duplicatePrestasiKerja->trx_anjab_id		= $getIdDuplikat;
				$duplicatePrestasiKerja->prestasi_kerja		= $anjabPrestasiKerjaDuplicate[$i]->prestasi_kerja;
				$duplicatePrestasiKerja->save();
			}

			if ($duplikat) {
				return response()->json([
					'success' => true,
					'message' => 'Data Berhasil Diduplikasi'
				],200);
			} else {
				return response()->json([
					'success' => false,
					'message' => 'Data Gagal Diduplikasi'
				]);
			}
		} else {
			return response()->json([
				'success' => false,
				'message' => 'Fungsi Belum Tersedia'
			]);
		}
	}

}

