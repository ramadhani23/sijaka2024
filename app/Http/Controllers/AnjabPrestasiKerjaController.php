<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabPrestasiKerja;
use DB;

class AnjabPrestasiKerjaController extends Controller
{
    function browse($id){

    	$data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "Detail Anjab";
    	$data['pages'] = "detail_prestasi_kerja";
    	$data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['prestasi_kerja'] = TrxAnjabPrestasiKerja::where('trx_anjab_id', $id)->orderBy('id', 'asc')->paginate(10);
    	return view('anjab.anjab-detail-prestasi-kerja', $data);

    }

    function getForm(Request $request){
    	if($request->aksi == 'add-data'){
            $data['id'] = $request->id;
            
    		return view('anjab.prestasi_kerja.form-create-prestasi-kerja', $data);
    	}elseif ($request->aksi == 'edit-data') {
            $data['prestasi_kerja'] = TrxAnjabPrestasiKerja::findOrFail($request->id);
    		return view('anjab.prestasi_kerja.form-edit-prestasi-kerja', $data);
    	}
    }

     function save(Request $request){
        if($request->aksi == 'create'){
            $prestasi_kerja = new TrxAnjabPrestasiKerja;

            $prestasi_kerja->prestasi_kerja = $request->prestasi_kerja;
            $prestasi_kerja->trx_anjab_id = $request->id;
            $prestasi_kerja->created_at = date('Y-m-d H:i:s');
            $prestasi_kerja->save();
            return redirect()->route('anjab.browse-prestasi-kerja',$request->id)->with('notify', 'Prestasi Kerja berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {
            $prestasi_kerja = TrxAnjabPrestasiKerja::findOrFail($request->id);

            $prestasi_kerja->prestasi_kerja = $request->prestasi_kerja;

            $prestasi_kerja->update();
            return redirect()->route('anjab.browse-prestasi-kerja',$request->id_anjab)->with('notify', 'Prestasi Kerja berhasil diperbarui');
        }
    }

    function deletePrestasi(Request $request){
        $prestasi_kerja = TrxAnjabPrestasiKerja::findOrFail($request->id);
        $prestasi_kerja->delete();
        return redirect()->back()->with('notify', 'Prestasi Kerja berhasil dihapus');
    }
   
}
