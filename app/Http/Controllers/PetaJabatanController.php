<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\MUnitKerja;
use App\Models\TrxAnjab;
use App\Repositories\AnjabRepository;
use App\Repositories\UnitKerjaRepository;

class PetaJabatanController extends Controller
{
    function browse(Request $request, AnjabRepository $anjabRepo, UnitKerjaRepository $unitKerjaRepo){

        // $kodeopd = $request->input('unit_kerja'); // Ambil kodeopd dari request
        // $user = \MojokertokabUser::getUser();
    
        // // Jika kode_opd user login adalah '28' dan kodeopd request diawali dengan '28' serta memiliki panjang 4 karakter
        // // Cek jika user memiliki role ADMIN dan kode_opd diawali '28' dengan panjang 4 karakter
        // if ($user->role === 'ADMIN' && Str::startsWith($kodeopd, '28') && strlen($kodeopd) === 4) {
        //     return redirect()->back()->with('error', 'Silahkan Akses dari Dinas Kesehatan');
        // }
    
        // Aksi lain jika kondisi di atas tidak terpenuhi
        // Contoh: return response()->json(['message' => 'Kode OPD tidak sesuai']);
    	$data['section'] = "peta-jabatan";
    	$data['page_section'] = "peta-jabatan";
    	$data['page'] = "Data Peta Jabatan";

        
        // $data['unitKerja'] = MUnitkerja::whereRaw('LENGTH(kodeunit) = 6')->get();


        $unitKerja                                                 = $unitKerjaRepo->getActiveUnitKerja();
        $data['unitKerja'] = $unitKerja;
        // Jika kode_opd adalah '28', langsung ke else (bagian bawah)
        if (\MojokertokabUser::getUser()->kode_opd !== '28' && \MojokertokabUser::getUser()->role == 'PD') {
            // Blok untuk role 'PD' selain kode_opd '28'
            $satker = substr(\MojokertokabUser::getUser()->option->satker_id, 0, 2);

            if ($satker == '01') {
                $satker = substr(\MojokertokabUser::getUser()->option->satker_id, 0, 6);
            } elseif ($satker == '28') {
                $unitK = \MojokertokabUser::getUser()->option->satker_id;
                if ($unitK == '2801') {
                    $unitKerja = '28';
                } else {
                    $satker = substr(\MojokertokabUser::getUser()->option->satker_id, 0, 4);
                }
            }
            // return $satker;

        } else {
            // Masuk ke sini jika:
            // 1. kode_opd === '28'
            // 2. role !== 'PD'
            // return 'ok';
            if ($request->unit_kerja != null) {
                $satker = $request->unit_kerja;
                // return $satker;

            } else {
                $satker = substr(\MojokertokabUser::getUser()->option->satker_id, 0, 2);

                if ($satker == '01') {
                    $satker = substr(\MojokertokabUser::getUser()->option->satker_id, 0, 6);
                } elseif ($satker == '28') {
                    $unitK = \MojokertokabUser::getUser()->option->satker_id;
                    if ($unitK == '2801') {
                        $unitKerja = '28';
                    } else {
                        $satker = substr(\MojokertokabUser::getUser()->option->satker_id, 0, 4);
                    }
                }
                // return $satker;
            }
        }

        
        // return $satker;
        $whereSatker = MUnitKerja::where('kodeunit',$satker)->first();
		$data['satker'] = $whereSatker->unitkerja;

        $jenis  = 1;
        $anjab = $anjabRepo->getDataAnjab($satker, $jenis);
        foreach ($anjab as $item) {
            // Periksa apakah elemen memiliki properti 'parent'
            if (isset($item->parent)) {
                if (isset($item->jabatanStruktural->jabatan)) {
                    $nama_jabatan = $item->jabatanStruktural->jabatan;
                } else {
                    $nama_jabatan = '';
                }
                $parentValues[] = [
                    'id'        => $item->id,
                    'nama'      => $nama_jabatan,
                    'parent'    => $item->parent
                ];
            }
        }
        $hierarchy = [];

        foreach ($parentValues as $item) {
            $hierarchy[$item['parent']][] = $item;
        }
        $data['hierarchy'] = $hierarchy;
		$data['data_parent'] = $anjab[0]->parent;
        //jabatan fungsional
        $jenis                                                  = 2;
        $anjab                                                  = $anjabRepo->getDataAnjab($satker, $jenis);
        $data['anjabfungsional'] = $anjab;
        //jabatan pelaksana
        $jenis                                                  = 3;
        $anjab                                                  = $anjabRepo->getDataAnjab($satker, $jenis);
        $data['anjabpelaksana'] = $anjab;
        
        // Mengelompokkan data berdasarkan 'first' dan menghitung jumlahnya
        // $anjabcount = collect($anjab)->groupBy('first')->map(function($group) {
        //     return $group->count();
        // });
        // return $data;

        // return $anjab->count();
    	return view('petajabatan.browse', $data);
    }
}
