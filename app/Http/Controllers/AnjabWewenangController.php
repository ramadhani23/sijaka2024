<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabWewenang;
use DB;

class AnjabWewenangController extends Controller
{
    function browse($id){

    	$data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "Detail Anjab";
    	$data['pages'] = "detail_wewenang";
    	$data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['wewenang'] = TrxAnjabWewenang::where('trx_anjab_id', $id)->orderBy('id', 'asc')->paginate(10);
    	return view('anjab.anjab-detail-wewenang', $data);

    }

    function getForm(Request $request){
    	if($request->aksi == 'add-data'){
            $data['id'] = $request->id;
            
    		return view('anjab.wewenang.form-create-wewenang', $data);
    	}elseif ($request->aksi == 'edit-data') {
            $data['wewenang'] = TrxAnjabWewenang::findOrFail($request->id);
    		return view('anjab.wewenang.form-edit-wewenang', $data);
    	}
    }

     function save(Request $request){
        if($request->aksi == 'create'){
            $wewenang = new TrxAnjabWewenang;

            $wewenang->wewenang = $request->wewenang;
            $wewenang->trx_anjab_id = $request->id;
            $wewenang->created_at = date('Y-m-d H:i:s');
            $wewenang->save();
            return redirect()->route('anjab.browse-wewenang',$request->id)->with('notify', 'Wewenang berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {
            $wewenang = TrxAnjabWewenang::findOrFail($request->id);

            $wewenang->wewenang = $request->wewenang;

            $wewenang->update();
            return redirect()->route('anjab.browse-wewenang',$request->id_anjab)->with('notify', 'Wewenang berhasil diperbarui');
        }
    }

    function deleteWewenang(Request $request){
        $wewenang = TrxAnjabWewenang::findOrFail($request->id);
        $wewenang->delete();
        return redirect()->back()->with('notify', 'Wewenang berhasil dihapus');
    }
   
}
