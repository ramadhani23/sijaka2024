<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabAbk;
use App\Models\TrxAnjabUraianJabatan;

use DB;

class AnjabAbkController extends Controller
{
    function browse($id){

    	$data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "Detail ABK";
    	$data['pages'] = "detail_abk";
    	$data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['abk'] = TrxAnjabAbk::where('trx_anjab_id', $id)->orderBy('id', 'asc')->paginate(10);
    	return view('anjab.anjab-detail-abk', $data);

    }

    function getForm(Request $request){
    	if($request->aksi == 'add-data'){
            $tugasJabatan = TrxAnjabUraianJabatan::where('trx_anjab_id', $request->id)->get();
            $data['id'] = $request->id;
    		$data['tugasJabatan'] = $tugasJabatan;
            return view('anjab.abk.form-create-abk', $data);
    	}elseif ($request->aksi == 'edit-data') {
            $abk = TrxAnjabAbk::findOrFail($request->id);
            $tugasJabatan = TrxAnjabUraianJabatan::where('trx_anjab_id', $abk->trx_anjab_id)->get();

            $data['abk'] = $abk;
            $data['tugasJabatan'] = $tugasJabatan;
    		return view('anjab.abk.form-edit-abk', $data);
    	}
    }

     function save(Request $request){
        if($request->aksi == 'create'){
            $abk = new TrxAnjabAbk;

            $kebutuhan_pegawai = ($request->jumlah_hasil * $request->waktu_penyelesaian) / $request->waktu_efektif;

            $abk->trx_anjab_uraian_jabatan_id = $request->tugas_jabatan;
            $abk->hasil_kerja = $request->hasil_kerja;
            $abk->uraian_hasil = '';
            $abk->jumlah_hasil = $request->jumlah_hasil;
            $abk->waktu_penyelesaian = $request->waktu_penyelesaian;
            $abk->waktu_efektif = $request->waktu_efektif;
            $abk->kebutuhan_pegawai = number_format($kebutuhan_pegawai,2);
            $abk->trx_anjab_id = $request->id;
            $abk->created_at = date('Y-m-d H:i:s');
            $abk->save();
            return redirect()->route('abk.detail',$request->id)->with('notify', 'ABK berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {

            $abk = TrxAnjabAbk::findOrFail($request->id);

            $kebutuhan_pegawai = ($request->jumlah_hasil * $request->waktu_penyelesaian) / $request->waktu_efektif;

            $abk->trx_anjab_uraian_jabatan_id = $request->tugas_jabatan;
            $abk->hasil_kerja = $request->hasil_kerja;
            $abk->uraian_hasil = '';
            $abk->jumlah_hasil = $request->jumlah_hasil;
            $abk->waktu_penyelesaian = $request->waktu_penyelesaian;
            $abk->waktu_efektif = $request->waktu_efektif;
            $abk->kebutuhan_pegawai = number_format($kebutuhan_pegawai,2);

            $abk->update();
            return redirect()->back()->with('notify', 'ABK berhasil diperbarui');
        }
    }

    function deleteAbk (Request $request){
        $abk = TrxAnjabAbk::findOrFail($request->id);
        $abk->delete();
        return redirect()->back()->with('notify', 'Hasil Kerja berhasil dihapus');
    }
   
}
