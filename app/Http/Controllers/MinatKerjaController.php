<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MMinatKerja;

class MinatKerjaController extends Controller
{
    function browse(Request $request){

    	$data['section'] = "minat-kerja";
    	$data['page_section'] = "minat-kerja";
    	$data['page'] = "Data Minat Kerja";

    	$jabatan = MMinatKerja::query();

    	if($request->keyword){
    		$jabatan = $jabatan->where('nama', 'LIKE', '%'.$request->keyword.'%');
    	}

    	$jabatan = $jabatan->orderBy('nama', 'ASC')->paginate(20);

    	$data['jabatan'] = $jabatan;
    	return view('master_minat_kerja.browse', $data);
    }

    function getForm(Request $request){
    	if($request->aksi == 'create-jabatan'){
    		return view('master_minat_kerja.form-create');
    	}elseif($request->aksi == 'edit-jabatan'){
    		$jabatan = MMinatKerja::findOrFail($request->id);

    		$data = [
    			'jabatan' => $jabatan
    		];
    		return view('master_minat_kerja.form-edit', $data);
    	}
    }

    function save(Request $request){

    	if($request->aksi == 'add-jabatan'){
    		$jabatan = new MMinatKerja;

    		$jabatan->nama = $request->nama;
    		$jabatan->keterangan = $request->keterangan;

    		$jabatan->save();

    		return redirect()->back()->with('notify', 'Minat kerja berhasil ditambahkan');
    	}elseif ($request->aksi == 'update-jabatan') {
    		$jabatan = MMinatKerja::findOrFail($request->id);

    		$jabatan->nama = $request->nama;
    		$jabatan->keterangan = $request->keterangan;
    		$jabatan->update();

    		return redirect()->back()->with('notify', 'Minat kerja berhasil diperbarui');
    	}
    }

    function delete(Request $request){
    	$jabatan = MMinatKerja::findOrFail($request->id);

    	// if($jabatan->dataAnjab){
    	// 	return redirect()->back()->with('error', 'Jabatan tidak bisa dihapus, digunakan untuk Analisa Jabatan');
    	// }

    	$jabatan->delete();

    	return redirect()->back()->with('notify', 'Minat kerja berhasil dihapus');
    }
}
