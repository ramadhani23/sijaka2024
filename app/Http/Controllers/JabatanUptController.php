<?php

namespace App\Http\Controllers;
use App\Models\MJabatanUpt;
use App\Models\MUnitKerja;

use Illuminate\Http\Request;

class JabatanUptController extends Controller
{
    function browse(){
        $data['section'] = "jabatan-upt";
        $data['page_section'] = "jabatan-upt";
        $data['page'] = "Data Jabatan UPT";

        // Mengambil semua data dari tabel MJabatanUpt
        $data['jabatan'] = MJabatanUpt::with('unitKerja')->get();

        // return $jabatan->count();
        return view('master.jabatan-upt.browse', $data);
    }

    function getForm(Request $request){
        if($request->aksi == 'create-jabatan'){
            
            $data['unitKerja'] = MUnitKerja::whereRaw('LENGTH(kodeunit) = 4')
            ->where('jenis', 'UPT')
            ->where(function ($query) {
                $query->where('unitkerja', 'LIKE', '%puskesmas%')
                    ->orWhere('unitkerja', 'LIKE', '%laboratorium%');
            })
            ->get();
            // dd($data['unitKerja']); // Debugging untuk melihat hasil query

            return view('master.jabatan-upt.form-create', $data);
        } elseif($request->aksi == 'edit-jabatan'){

             $data['jabatan'] = MJabatanUpt::findOrFail($request->id); // Cari data berdasarkan ID
             $data['unitKerja'] = MUnitKerja::whereRaw('LENGTH(kodeunit) = 4')
             ->where('jenis', 'UPT')
             ->where(function ($query) {
                 $query->where('unitkerja', 'LIKE', '%puskesmas%')
                     ->orWhere('unitkerja', 'LIKE', '%laboratorium%');
             })
             ->get();

            return view('master.jabatan-upt.form-edit', $data);
        }
    }

    public function save(Request $request)
    {
        if($request->aksi == 'add-jabatan'){
        // return $request;
        // Validasi input
        $request->validate([
            'jabatan' => 'required|string|max:255',
            'instansi' => 'required|string|max:4',
        ]);

        // Simpan ke database
        // return $request;
        MJabatanUpt::create([
            'jabatan' => $request->jabatan,
            'kodeunit_instansi' => $request->instansi,
        ]);

        return redirect()->back()->with('notify', 'Jabatan berhasil ditambahkan');
    } else {
        // Validasi input
        $request->validate([
            'jabatan' => 'required|string|max:255',
            'instansi' => 'required|string|max:4',
        ]);

        // Cari data berdasarkan ID lalu update
        $jabatan = MJabatanUpt::findOrFail($request->id);
        $jabatan->update([
            'jabatan' => $request->jabatan,
            'kodeunit_instansi' => $request->instansi,
        ]);

        // Redirect dengan pesan sukses
        return redirect()->back()->with('notify', 'Jabatan berhasil diubah');
        }
    }
}
