<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabHasilKerja;
use DB;

class AnjabHasilKerjaController extends Controller
{
    function browse($id){

    	$data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "Detail Anjab";
    	$data['pages'] = "detail_hasil_kerja";
    	$data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['hasil_kerja'] = TrxAnjabHasilKerja::where('trx_anjab_id', $id)->orderBy('id', 'asc')->get();
    	return view('anjab.anjab-detail-hasil-kerja', $data);

    }

    function getForm(Request $request){
    	if($request->aksi == 'add-data'){
            $data['id'] = $request->id;
    		return view('anjab.hasil_kerja.form-create-hasil-kerja', $data);
    	}elseif ($request->aksi == 'edit-data') {
            $data['hasil_kerja'] = TrxAnjabHasilKerja::findOrFail($request->id);
    		return view('anjab.hasil_kerja.form-edit-hasil-kerja', $data);
    	}
    }

     function save(Request $request){
        if($request->aksi == 'create'){
            $hasil_kerja = new TrxAnjabHasilKerja;


            $hasil_kerja->hasil_kerja = $request->hasil_kerja;
            $hasil_kerja->satuan_hasil = $request->satuan_hasil;
            $hasil_kerja->trx_anjab_id = $request->id;
            $hasil_kerja->created_at = date('Y-m-d H:i:s');
            $hasil_kerja->save();
            return redirect()->route('anjab.browse-hasil-kerja',$request->id)->with('notify', 'Hasil Kerja berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {
            $hasil_kerja = TrxAnjabHasilKerja::findOrFail($request->id);

            $hasil_kerja->hasil_kerja = $request->hasil_kerja;
            $hasil_kerja->satuan_hasil = $request->satuan_hasil;

            $hasil_kerja->update();
            return redirect()->route('anjab.browse-hasil-kerja',$request->id_anjab)->with('notify', 'Hasil Kerja berhasil diperbarui');
        }
    }

    function deleteHasilKerja(Request $request){
        $hasil_kerja = TrxAnjabHasilKerja::findOrFail($request->id);
        $hasil_kerja->delete();
        return redirect()->back()->with('notify', 'Hasil Kerja berhasil dihapus');
    }
   
}
