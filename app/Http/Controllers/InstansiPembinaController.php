<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MJabatanFungsionalInstansiPembina;

class InstansiPembinaController extends Controller
{
    function browse(Request $request){

    	$data['section'] = "instansi-pembina";
    	$data['page_section'] = "instansi-pembina";
    	$data['page'] = "Data Instansi Pembina";

    	$instansi = MJabatanFungsionalInstansiPembina::query();

    	if($request->keyword){
    		$instansi = $instansi->where('nama', 'LIKE', '%'.$request->keyword.'%');
    	}

    	$instansi = $instansi->orderBy('nama', 'ASC')->paginate(20);

    	$data['instansi'] = $instansi;
    	return view('master_instansi_pembina.browse', $data);
    }

    function getForm(Request $request){
    	if($request->aksi == 'create-instansi'){
    		return view('master_instansi_pembina.form-create');
    	}elseif($request->aksi == 'edit-instansi'){
    		$instansi = MJabatanFungsionalInstansiPembina::findOrFail($request->id);

    		$data = [
    			'instansi' => $instansi
    		];
    		return view('master_instansi_pembina.form-edit', $data);
    	}
    }

    function save(Request $request){

    	if($request->aksi == 'add-instansi'){
    		$instansi = new MJabatanFungsionalInstansiPembina;

    		$instansi->nama = $request->nama;

    		$instansi->save();

    		return redirect()->back()->with('notify', 'Instansi Pembina berhasil ditambahkan');
    	}elseif ($request->aksi == 'update-instansi') {
    		$instansi = MJabatanFungsionalInstansiPembina::findOrFail($request->id);

    		$instansi->nama = $request->nama;
    		$instansi->update();

    		return redirect()->back()->with('notify', 'Instansi Pembina berhasil diperbarui');
    	}
    }

    function delete(Request $request){
    	$instansi = MJabatanFungsionalInstansiPembina::findOrFail($request->id);

    	$instansi->delete();

    	return redirect()->back()->with('notify', 'Instansi Pembina berhasil dihapus');
    }
}
