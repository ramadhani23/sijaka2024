<?php

namespace App\Http\Controllers;

use App\Serializers\CustomArraySerializer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabPerangkat;
use DB;

class AnjabPerangkatController extends Controller
{
    function browse($id){

    	$data['section'] = "anjab";
    	$data['page_section'] = "anjab";
    	$data['page'] = "Detail Anjab";
    	$data['pages'] = "detail_perangkat";
    	$data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['perangkat'] = TrxAnjabPerangkat::where('trx_anjab_id', $id)->orderBy('id', 'asc')->paginate(10);
    	return view('anjab.anjab-detail-perangkat', $data);

    }

    function getForm(Request $request){
    	if($request->aksi == 'add-data'){
            $data['id'] = $request->id;
            
    		return view('anjab.perangkat.form-create-perangkat', $data);
    	}elseif ($request->aksi == 'edit-data') {
            $data['perangkat'] = TrxAnjabPerangkat::findOrFail($request->id);
    		return view('anjab.perangkat.form-edit-perangkat', $data);
    	}
    }

     function save(Request $request){
        if($request->aksi == 'create'){
            $perangkat = new TrxAnjabPerangkat;

            $perangkat->perangkat_kerja = $request->perangkat;
            $perangkat->digunakan_untuk_tugas = $request->digunakan_untuk_tugas;
            $perangkat->trx_anjab_id = $request->id;
            $perangkat->created_at = date('Y-m-d H:i:s');
            $perangkat->save();
            return redirect()->route('anjab.browse-perangkat',$request->id)->with('notify', 'Anjab berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {
            $perangkat = TrxAnjabPerangkat::findOrFail($request->id);

            $perangkat->perangkat_kerja = $request->perangkat;
            $perangkat->digunakan_untuk_tugas = $request->digunakan_untuk_tugas;

            $perangkat->update();
            return redirect()->route('anjab.browse-perangkat',$request->id_anjab)->with('notify', 'Anjab berhasil diperbarui');
        }
    }

    function deletePerangkat (Request $request){
        $perangkat = TrxAnjabPerangkat::findOrFail($request->id);
        $perangkat->delete();
        return redirect()->back()->with('notify', 'perangkat berhasil dihapus');
    }
   
}
