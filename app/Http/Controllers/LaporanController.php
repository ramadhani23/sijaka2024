<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabAbk;
use App\Models\TrxAnjabEvajabStruktural;	
use App\Models\TrxAnjabEvajabFungsional;	

class LaporanController extends Controller
{
    function browseKelasJabatan(){
        $data['section'] = "rekap";
        
        $data['page'] = "Rekap Kelas Jabatan";
        $data['pages'] = "rekap";
        $data['page_section'] = "rekap-kelas-jabatan";

        $anjabStruktural = TrxAnjab::whereHas('dataEvajabStruktural')->paginate(25);
       
        $data['anjabStruktural'] = $anjabStruktural;
        return view('laporan.rekap-kelas-jabatan.browse', $data);        
    }

    function browseJabatanStruktural(){
    	$data['section'] = "rekap";
    	
    	$data['page'] = "Rekap Jabatan Struktural";
    	$data['pages'] = "rekap";
    	$data['page_section'] = "rekap-jabatan-struktural";

        $anjab = TrxAnjab::where('jenis_jabatan', '1')->whereDoesntHave('dataEvajabStruktural')->get();
        $evjab = TrxAnjabEvajabStruktural::paginate(25);
        $data['evjab'] = $evjab;
        $data['anjab'] = $anjab;

        return view('laporan.rekap-jabatan-struktural.browse', $data);

    }

    function browseJabatanFungsional(){
    	$data['section'] = "rekap";
    	
    	$data['page'] = "Rekap Jabatan Fungsional";
    	$data['pages'] = "rekap";
    	$data['page_section'] = "rekap-jabatan-fungsional";

        $anjab = TrxAnjab::where('jenis_jabatan', '!=', '1')->whereDoesntHave('dataEvajabFungsional')->get();
        $evjab = TrxAnjabEvajabFungsional::paginate(25);
        $data['evjab'] = $evjab;
        $data['anjab'] = $anjab;

        return view('laporan.rekap-jabatan-fungsional.browse', $data);

    }

    function browseEvjabStruktural(){
    	$data['section'] = "rekap";
    	
    	$data['page'] = "Rekap Evaluasi Jabatan Struktural";
    	$data['pages'] = "rekap";
    	$data['page_section'] = "rekap-evjab-struktural";

        $anjab = TrxAnjab::where('jenis_jabatan', '1')->whereDoesntHave('dataEvajabStruktural')->get();
        $evjab = TrxAnjabEvajabStruktural::paginate(25)->sortByDesc('kelas_jabatan');;
        $data['evjab'] = $evjab;
        $data['anjab'] = $anjab;

        return view('laporan.rekap-evjab-jabatan-struktural.browse', $data);

    }

    function browseEvjabFungsional(){
    	$data['section'] = "rekap";
    	
    	$data['page'] = "Rekap Evaluasi Jabatan Fungsional";
    	$data['pages'] = "rekap";
    	$data['page_section'] = "rekap-evjab-fungsional";

        $anjab = TrxAnjab::where('jenis_jabatan','!=', '1')->whereDoesntHave('dataEvajabFungsional')->get();
        $evjab = TrxAnjabEvajabStruktural::paginate(25)->sortByDesc('kelas_jabatan');;
        $data['evjab'] = $evjab;
        $data['anjab'] = $anjab;

        return view('laporan.rekap-evjab-jabatan-fungsional.browse', $data);

    }
}
