<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TrxAnjab;
use App\Models\TrxAnjabUraianJabatan;
use App\Models\TrxAnjabUraianJabatanTahapan;

class AnjabUraianTugasController extends Controller
{
    function browse($id){

        $data['section'] = "anjab";
        $data['page_section'] = "anjab";
        $data['page'] = "Uraian Tugas";
        $data['pages'] = "detail_uraian_tugas";
        $data['anjab'] = TrxAnjab::where('id', $id)->first();
        $data['anjab_uraian'] = TrxAnjabUraianJabatan::where('trx_anjab_id', $id)->orderBy('id', 'asc')->paginate(10);
        return view('anjab.anjab-detail-uraian-tugas', $data);
    }

    function getFormUraian(Request $request){
        if($request->aksi == 'add-data'){
            $data['id'] = $request->id;

            return view('anjab.uraian.form-create-anjab-uraian', $data);
        }elseif ($request->aksi == 'edit-uraian') {
            $data['anjab_uraian'] = TrxAnjabUraianJabatan::findOrFail($request->id);
            return view('anjab.uraian.form-edit-anjab-uraian', $data);
        }
    }

    function saveUraian(Request $request){
        if($request->aksi == 'create'){

            $id = $request->id;
            $cek_key = TrxAnjabUraianJabatan::where('trx_anjab_id',$id)
                ->withTrashed()
                ->latest()
                ->first();
            if ($cek_key == null) {
                $value_key = 0;
            } else {
                $value_key = $cek_key->key + 1;
            }

            $anjab_uraian = new TrxAnjabUraianJabatan;

            $anjab_uraian->trx_anjab_id = $request->id;
            $anjab_uraian->uraian_jabatan = $request->uraian_tugas;
            $anjab_uraian->key  = $value_key;
            $anjab_uraian->save();
            return redirect()->back()->with('notify', 'Uraian berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {
            $anjab_uraian = TrxAnjabUraianJabatan::findOrFail($request->id);

            $anjab_uraian->uraian_jabatan = $request->uraian_tugas;

            $anjab_uraian->update();
            return redirect()->back()->with('notify', 'Uraian berhasil diperbarui');
        }
    }

    function deleteUraian(Request $request){
        $anjab_uraian = TrxAnjabUraianJabatan::findOrFail($request->id);
        $anjab_uraian->delete();
        return redirect()->back()->with('notify', 'Uraian berhasil dihapus');
    }

    function getFormUraianTahapan(Request $request){
        if($request->aksi == 'add-tahapan'){
            $data['id_anjab'] = $request->id_anjab;
            $data['id_uraian'] = $request->id_uraian;
            return view('anjab.uraian.form-create-anjab-uraian-tahapan', $data);
        }elseif ($request->aksi == 'edit-uraian') {
            $data['anjab_uraian'] = TrxAnjabUraianJabatan::findOrFail($request->id);
            return view('anjab.form-edit-anjab-uraian-tahapan', $data);
        }
    }

    function saveUraianTahapan(Request $request){
        if($request->aksi == 'create'){
            $anjab_uraian_tahapan = new TrxAnjabUraianJabatanTahapan;

            $anjab_uraian_tahapan->trx_anjab_id = $request->id_anjab;
            $anjab_uraian_tahapan->trx_anjab_uraian_jabatan_id = $request->id_uraian;
            $anjab_uraian_tahapan->uraian = $request->uraian_tahapan;
            $anjab_uraian_tahapan->save();
            return redirect()->back()->with('notify', 'Tahapan berhasil ditambahkan');

        }elseif ($request->aksi == 'update') {
            $anjab_uraian_tahapan = TrxAnjabUraianJabatanTahapan::findOrFail($request->id);

            $anjab_uraian_tahapan->uraian = $request->uraian_tahapan;

            $anjab_uraian_tahapan->update();
            return redirect()->back()->with('notify', 'Tahapan berhasil diperbarui');
        }
    }
}
