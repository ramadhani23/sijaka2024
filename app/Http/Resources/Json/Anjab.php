<?php

namespace App\Http\Resources\Json;

use Illuminate\Http\Resources\Json\JsonResource;

class Anjab extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'nama' => strtoupper($this->jabatan->jabatan),
            'jenis_jabatan' => $this->text_jenis_jabatan,
            'kode' => $this->jabatan->{'_kode'},
            'kode_bkpp' => $this->jabatan->kode_bkpp,
            'kebutuhan_pegawai' => $this->total_kebutuhan_pegawai,

        ];
    }
}
