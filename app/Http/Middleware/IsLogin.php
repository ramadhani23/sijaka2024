<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Nowakowskir\JWT\JWT;
use Nowakowskir\JWT\TokenDecoded;
use App\Vendorku\TokenEncoded;
use Cookie;
use Illuminate\Support\Facades\Redis;
use Nowakowskir\JWT\Exceptions\TokenExpiredException;

class IsLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $authToken = $request->cookie('AUTH-TOKEN');

        if($authToken == NULL){
            return redirect()->route('login-view')->with('notify', 'Token tidak ditemukan');
        }

        \Log::info('Isi Cookie AUTH-TOKEN:', ['token' => $authToken]);
        $tokenEncoded = new TokenEncoded($request->cookie('AUTH-TOKEN'));
        $header = $tokenEncoded->decode()->getHeader();

        try {
            $tokenEncoded->validate(env('KEYCLOAK_PUBLIC_KEY'), JWT::ALGORITHM_RS256);
        } catch(Exception $e) {
            return redirect()->route('login-view')->with('notify', 'Token Validation Gagal');
        } catch(TokenExpiredException $e){
            return redirect()->route('login-view')->with('notify', 'Login Expired');
        }

        return $next($request);
    }
    
}

