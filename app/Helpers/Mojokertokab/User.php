<?php
namespace App\Helpers\Mojokertokab;

use Nowakowskir\JWT\JWT;
use Nowakowskir\JWT\TokenDecoded;
use App\Vendorku\TokenEncoded;
use Cookie;
use Nowakowskir\JWT\Exceptions\TokenExpiredException;

use Illuminate\Support\Facades\Redis;

class User {
    /**
     * @param int $user_id User-id
     *
     * @return string
     */
    public static function getUser() {
    	$tokenEncoded = new TokenEncoded(request()->cookie('AUTH-TOKEN'));
    	$tokenDecoded = $tokenEncoded->decode()->getPayload();
    	$username = $tokenDecoded['preferred_username'];
        return json_decode(Redis::get('auth:user_'.$username));
    }
    public static function getUsername() {
    	$tokenEncoded = new TokenEncoded(request()->cookie('AUTH-TOKEN'));
    	$tokenDecoded = $tokenEncoded->decode()->getPayload();
    	return $username = $tokenDecoded['preferred_username'];
    }
}
