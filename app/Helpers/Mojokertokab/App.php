<?php
namespace App\Helpers\Mojokertokab;

use App\Models\CApp;
use App\Models\MPeriode;

class App {

	public static function isKunciApp() {
		return CApp::where('key', 'kunci_app')->first()->value;
	}

	public static function allowChangingData(){
		if(\MojokertokabUser::getUser()->role == 'ADMIN' || ( (\MojokertokabUser::getUser()->role == 'PD') && \MojokertokabApp::isKunciApp() == 0) ){
			return 1;
		}else{
			return 0;
		}
	}

	public static function getActivePeriod(){
		$periodeAktif = MPeriode::where('is_aktif', '1')->first();
		return $periodeAktif;
	}

	public static function getJenjangFungsional(){
		return [
			'pemula' => 'Pemula',
			// 'pelaksana_pemula' => 'Pelaksana Pemula',
			'terampil' => 'Terampil',
			// 'pelaksana' => 'Pelaksana',
			'mahir' => 'Mahir',
			// 'pelaksana_lanjutan' => 'Pelaksana Lanjutan',
			'penyelia' => 'Penyelia',
			// 'pertama' => 'Pertama',
			// 'muda' => 'Muda',
			// 'madya' => 'Madya',
			// 'utama' => 'Utama',
			'ahli_pertama' => 'Ahli Pertama',
			'ahli_muda' => 'Ahli Muda',
			'ahli_madya' => 'Ahli Madya',
			'ahli_utama' => 'Ahli Utama'
		];
	}

}