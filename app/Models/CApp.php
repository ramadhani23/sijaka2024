<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CApp extends Model
{
    use HasFactory;

    protected $table = 'c_app';
}
