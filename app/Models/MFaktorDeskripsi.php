<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MFaktorDeskripsi extends Model
{
    use HasFactory;
    protected $table = 'm_deskripsi_faktor';

}
