<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrxKomjabTeknisIndikator extends Model
{
    use HasFactory;
    protected $table = 'trx_komjab_teknis_indikator';
}
