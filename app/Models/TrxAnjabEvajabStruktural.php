<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TrxAnjabEvajabStruktural extends Model
{
    use SoftDeletes;

    protected $table = 'trx_anjab_evajab_struktural';

    function faktorLevel1(){
    	return $this->belongsTo('App\Models\MLevelFaktor', 'faktor1', 'id');
    }

    function faktorLevel2(){
    	return $this->belongsTo('App\Models\MLevelFaktor', 'faktor2', 'id');
    }

    function faktorLevel3(){
    	return $this->belongsTo('App\Models\MLevelFaktor', 'faktor3', 'id');
    }

    function faktorLevel4A(){
    	return $this->belongsTo('App\Models\MLevelFaktor', 'faktor4A', 'id');
    }

    function faktorLevel4B(){
    	return $this->belongsTo('App\Models\MLevelFaktor', 'faktor4B', 'id');
    }

    function faktorLevel5(){
    	return $this->belongsTo('App\Models\MLevelFaktor', 'faktor5', 'id');
    }

    function faktorLevel6(){
    	return $this->belongsTo('App\Models\MLevelFaktor', 'faktor6', 'id');
    }

    function faktor($id){
    	return $this->belongsTo('App\Models\MLevelFaktor', $id, 'id');
    }

    function getTotalNilaiAttribute(){
        $nilaiFaktor1 = 0;
        if($this->faktorLevel1) {
            $nilaiFaktor1 = $this->faktorLevel1->nilai;
        }
        $nilaiFaktor2 = 0;
        if($this->faktorLevel2) {
            $nilaiFaktor2 = $this->faktorLevel2->nilai;
        }
        $nilaiFaktor3 = 0;
        if($this->faktorLevel3) {
            $nilaiFaktor3 = $this->faktorLevel3->nilai;
        }
        $nilaiFaktor4A = 0;
        if($this->faktorLevel4A) {
            $nilaiFaktor4A = $this->faktorLevel4A->nilai;
        }
        $nilaiFaktor4B = 0;
        if($this->faktorLevel4B) {
            $nilaiFaktor4B = $this->faktorLevel4B->nilai;
        }
        $nilaiFaktor5 = 0;
        if($this->faktorLevel5) {
            $nilaiFaktor5 = $this->faktorLevel5->nilai;
        }
        $nilaiFaktor6 = 0;
        if($this->faktorLevel6) {
            $nilaiFaktor6 = $this->faktorLevel6->nilai;
        } 

        $totalNilai = $nilaiFaktor1 + $nilaiFaktor2 + $nilaiFaktor3 + $nilaiFaktor4A + $nilaiFaktor4B + $nilaiFaktor5 + $nilaiFaktor6;

        return $totalNilai;
    }

    function getKelasJabatanAttribute(){
        $totalNilai = $this->total_nilai;

        $kelasJabatan = 0;

        if($totalNilai < 190){
            $kelasJabatan = 0;
        }elseif($totalNilai <= 240){
            $kelasJabatan =  1;
        }elseif($totalNilai <= 300){
            $kelasJabatan =  2;
        }elseif($totalNilai <= 370){
            $kelasJabatan =  3;
        }elseif($totalNilai <= 450){
            $kelasJabatan =  4;
        }elseif($totalNilai <= 650){
            $kelasJabatan =  5;
        }elseif($totalNilai <= 850){
            $kelasJabatan =  6;
        }elseif($totalNilai <= 1100){
            $kelasJabatan =  7;
        }elseif($totalNilai <= 1350){
            $kelasJabatan =  8;
        }elseif($totalNilai <= 1600){
            $kelasJabatan =  9;
        }elseif($totalNilai <= 1850){
            $kelasJabatan =  10;
        }elseif($totalNilai <= 2100){
            $kelasJabatan =  11;
        }elseif($totalNilai <= 2350){
            $kelasJabatan =  12;
        }elseif($totalNilai <= 2750){
            $kelasJabatan =  13;
        }elseif($totalNilai <= 3150){
            $kelasJabatan =  14;
        }elseif($totalNilai <= 3600){
            $kelasJabatan =  15;
        }elseif($totalNilai <= 4050){
            $kelasJabatan =  16;
        }elseif($totalNilai >= 4055){
            $kelasJabatan =  17;
        }

        return $kelasJabatan;

    }


    function getTingkatFaktor1Attribute(){
        $levelFaktor = $this->faktorLevel1;
        $faktor = \App\Models\MFaktor::find($levelFaktor->m_faktor_id);
        return explode('Faktor ', $faktor->nama)[1].'-'.explode('Level ', $levelFaktor->nama)[1];
    }
    function getTingkatFaktor2Attribute(){
        $levelFaktor = $this->faktorLevel2;
        $faktor = \App\Models\MFaktor::find($levelFaktor->m_faktor_id);
        return explode('Faktor ', $faktor->nama)[1].'-'.explode('Level ', $levelFaktor->nama)[1];
    }
    function getTingkatFaktor3Attribute(){
        $levelFaktor = $this->faktorLevel3;
        $faktor = \App\Models\MFaktor::find($levelFaktor->m_faktor_id);
        return explode('Faktor ', $faktor->nama)[1].'-'.explode('Level ', $levelFaktor->nama)[1];
    }
    function getTingkatFaktor4AAttribute(){
        $levelFaktor = $this->faktorLevel4A;
        $faktor = \App\Models\MFaktor::find($levelFaktor->m_faktor_id);
        return explode('Faktor ', $faktor->nama)[1].'-'.explode('Level ', $levelFaktor->nama)[1];
    }
    function getTingkatFaktor4BAttribute(){
        $levelFaktor = $this->faktorLevel4B;
        $faktor = \App\Models\MFaktor::find($levelFaktor->m_faktor_id);
        return explode('Faktor ', $faktor->nama)[1].'-'.explode('Level ', $levelFaktor->nama)[1];
    }
    function getTingkatFaktor5Attribute(){
        $levelFaktor = $this->faktorLevel5;
        $faktor = \App\Models\MFaktor::find($levelFaktor->m_faktor_id);
        return explode('Faktor ', $faktor->nama)[1].'-'.explode('Level ', $levelFaktor->nama)[1];
    }
    function getTingkatFaktor6Attribute(){
        $levelFaktor = $this->faktorLevel6;
        $faktor = \App\Models\MFaktor::find($levelFaktor->m_faktor_id);
        return explode('Faktor ', $faktor->nama)[1].'-'.explode('Level ', $levelFaktor->nama)[1];
    }

    function dataAnjab(){
        return $this->belongsTo('App\Models\TrxAnjab',  'trx_anjab_id', 'id');
    }
}
