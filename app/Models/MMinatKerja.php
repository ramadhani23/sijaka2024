<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MMinatKerja extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'm_minat_kerja';
}
