<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MKamusKompetensiLevel extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'm_kamus_kompetensi_level';

    function dataIndikator(){
        return $this->hasMany('App\Models\MKamusKompetensiLevelIndikator', 'id_m_kamus_kompetensi_level', 'id');
    }
}
