<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MJabatan extends Model
{

    use SoftDeletes;
    
    protected $table = 'm_jabatan_struktural';

    function dataAnjab(){
    	return $this->belongsTo('App\Models\TrxAnjab', 'id', 'm_jabatan_id');
    }

    function getKodeAttribute($value){
        if($this->jenis == 'pelaksana'){
            return $this->kode_urusan_pemerintahan.'-'.$value;
        }else{
            return $value;
        }
    }

    function getTextDasarHukumAttribute(){
    	$dasarHukum = $this->attributes['dasar_hukum'];

    	if($dasarHukum == 'sk-bupati-315-2020'){
    		return 'KEPUTUSAN BUPATI MOJOKERTO NOMOR 188.45/315/HK/416-012/2020 TAHUN 2020';
    	}else{
    		return 'Tidak Ada Aturan';
    	}
    }
}

