<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MTrxKamusKompetensi extends Model
{
    use HasFactory;
    use SoftDeletes;
    
    protected $table = 'm_kamus_kompetensi';

    function dataLevel(){
        return $this->hasMany('App\Models\MKamusKompetensiLevel', 'id_m_kamus_kompetensi', 'id');
    }
}
