<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrxKomjabPersyaratanJabatanTeknis extends Model
{
    use HasFactory;
    protected $table ='trx_komjab_persyaratan_jabatan_teknis';

    function getBadgeStatusAttribute(){
        $status = $this->attributes['status_teknis'];

        if($status == 'mutlak'){
            return 'MUTLAK';
        }elseif($status == 'penting'){
            return 'PENTING';   
        }elseif($status == 'perlu'){
            return 'PERLU';   
        }
    }
}
