<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TrxAnjabPerangkat extends Model
{

    use SoftDeletes;
    protected $table = 'trx_anjab_perangkat';

    function dataPerangkat(){
        return $this->hasMany('App\Models\TrxAnjab', 'id', 'trx_anjab_id');
    }


}
