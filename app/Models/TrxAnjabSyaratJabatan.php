<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TrxAnjabSyaratJabatan extends Model
{

    use SoftDeletes;
    protected $table = 'trx_anjab_syarat_jabatan';

    function setMinatKerjaAttribute($value){
    	$this->attributes['minat_kerja'] = json_encode($value);
    }
    function setTemperamenKerjaAttribute($value){
    	$this->attributes['temperamen_kerja'] = json_encode($value);
    }
    function setFungsiPekerjaanAttribute($value){
    	$this->attributes['fungsi_pekerjaan'] = json_encode($value);
    }
    function setUpayaFisikAttribute($value){
    	$this->attributes['upaya_fisik'] = json_encode($value);
    }
    function setBakatKerjaAttribute($value){
    	$this->attributes['bakat_kerja'] = json_encode($value);
    }

    function getMinatKerjaAttribute(){
    	return json_decode($this->attributes['minat_kerja']);
    }
    function getTemperamenKerjaAttribute(){
    	return json_decode($this->attributes['temperamen_kerja']);
    }
    function getFungsiPekerjaanAttribute(){
    	return json_decode($this->attributes['fungsi_pekerjaan']);
    }
    function getUpayaFisikAttribute(){
    	return json_decode($this->attributes['upaya_fisik']);
    }
    function getBakatKerjaAttribute(){
    	return json_decode($this->attributes['bakat_kerja']);
    }

    function getTextUmurAttribute(){
        $umur = $this->umur;

        if($umur == 0){
            $textUmur = '-';
        }else{
            $textUmur = $umur.' tahun';
        }

        return $textUmur;
    }

    function getTextTinggiBadanAttribute(){
        $tinggiBadan = $this->tinggi_badan;

        if($tinggiBadan == 0){
            $textTinggiBadan = '-';
        }else{
            $textTinggiBadan = $tinggiBadan.' cm';
        }

        return $textTinggiBadan;
    }

    function getTextBeratBadanAttribute(){
        $beratBadan = $this->berat_badan;

        if($beratBadan == 0){
            $textBeratBadan = '-';
        }else{
            $textBeratBadan = $beratBadan.' kg';
        }

        return $textBeratBadan;
    }

    function getTextJenisKelaminAttribute(){
        $jenkel = $this->jenis_kelamin;
        switch ($jenkel) {
            case 'LAKI_LAKI':
                return "Laki-Laki";
                break;
            case 'PEREMPUAN':
                return "Perempuan";
                break;
            case 'LAKI_LAKI_PEREMPUAN':
                return "Laki-Laki / Perempuan";
                break;
            
            default:
                // code...
                break;
        }
    }

}
