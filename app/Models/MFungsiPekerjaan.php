<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MFungsiPekerjaan extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'm_fungsi_pekerjaan';
}
