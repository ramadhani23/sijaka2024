<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MUnitKerja extends Model
{

    use SoftDeletes;

    protected $table = 'm_unit_kerja_2';
    // public $primaryKey  = 'kodeunit';

    public function scopeDinas($query)
    {
        return $query->where('jenis', 'DINAS');
    }

    public function scopeBadan($query)
    {
        return $query->where('jenis', 'BADAN');
    }

    public function scopeBagianSetda($query)
    {
        return $query->where('jenis', 'BAGIAN');
    }

    public function scopeKecamatan($query)
    {
        return $query->where('jenis', 'KECAMATAN');
    }

    public function scopeRumahSakit($query)
    {
        return $query->where('jenis', 'RUMAH_SAKIT');
    }

    public function scopeUpt($query)
    {   
        return $query->whereRaw('SUBSTR(kodeunit, 1, 2) != "16"')->where('jenis', 'UPT');
    }

    public function scopeSekolah($query)
    {
        return $query->whereRaw('SUBSTR(kodeunit, 1, 2) = "16"')->where('jenis', 'UPT');
    }

    public function scopeKorwil($query)
    {
        return $query->whereRaw('SUBSTR(kodeunit, 1, 2) = "16"')->where('jenis', 'KORWIL_DISPENDIK');
    }
    public function scopeWilker($query)
    {
        return $query->whereRaw('SUBSTR(kodeunit, 1, 2) = "16"')->where('jenis', 'WILKER_DISPENDIK');
    }
    public function scopepuskesmas($query)
    {
        return $query->whereRaw('SUBSTR(kodeunit, 1, 2) = "28"')->where('jenis', 'UPT');
    }

}
