<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrxAnjabWewenang extends Model
{

    use SoftDeletes;

    protected $table = 'trx_anjab_wewenang';

    function dataWewenang(){
        return $this->hasMany('App\Models\TrxAnjab', 'id', 'trx_anjab_id');
    }

}
