<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrxAnjabUraianJabatan extends Model
{

    use SoftDeletes;
    
    protected $table = 'trx_anjab_uraian_jabatan';

    function dataUraianJabatan(){
        return $this->hasMany('App\Models\TrxAnjab', 'id', 'trx_anjab_id');
    }

    function tahapan(){
    	return $this->hasMany('App\Models\TrxAnjabUraianJabatanTahapan', 'trx_anjab_uraian_jabatan_id', 'id');
    }

    function dataAbk(){
    	return $this->hasOne('App\Models\TrxAnjabAbk', 'trx_anjab_uraian_jabatan_id', 'id');
    }

}
