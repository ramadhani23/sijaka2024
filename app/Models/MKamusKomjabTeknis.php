<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MKamusKomjabTeknis extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'trx_komjab_teknis';

}
