<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrxKomjabPersyaratanJabatanPengalamanKerja extends Model
{
    use HasFactory;
    protected $table ='trx_komjab_persyaratan_jabatan_pengalaman_kerja';

    function getBadgeStatusAttribute(){
        $status = $this->attributes['status_pengalaman_kerja'];

        if($status == 'mutlak'){
            return 'MUTLAK';
        }elseif($status == 'penting'){
            return 'PENTING';   
        }elseif($status == 'perlu'){
            return 'PERLU';   
        }
    }
}
