<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TrxAnjabBahanKerja extends Model
{
    use SoftDeletes;
    protected $table = 'trx_anjab_bahan_kerja';

    function dataBahanKerja(){
        return $this->hasMany('App\Models\TrxAnjab', 'id', 'trx_anjab_id');
    }

}
