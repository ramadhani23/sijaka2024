<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MJabatanFungsionalJenjang extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'm_jabatan_fungsional_jenjang';
    protected $dates = ['deleted_at'];

    function dataInstansiPembina(){
        return $this->hasOne('App\Models\MJabatanFungsionalInstansiPembina', 'id', 'm_jabatan_fungsional_instansi_pembina_id');
    }

    function dataRumpun(){
        return $this->hasOne('App\Models\MJabatanFungsionalRumpun', 'id', 'm_jabatan_fungsional_rumpun_id');
    }

}
