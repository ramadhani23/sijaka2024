<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MKonjabPelaksana extends Model
{
    use HasFactory;

    protected $table = 'm_konjab_pelaksana';
}
