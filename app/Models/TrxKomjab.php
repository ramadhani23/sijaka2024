<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrxKomjab extends Model
{
    use HasFactory;
    protected $table = 'trx_komjab';

    function komjabTeknis(){
        return $this->hasMany('App\Models\TrxKomjabTeknis', 'id_trx_komjab', 'id');
    }

    function komjabPersyaratanJabatanTeknis(){
        return $this->hasMany('App\Models\TrxKomjabPersyaratanJabatanTeknis', 'id_trx_komjab', 'id');
    }

    function komjabPersyaratanJabatanPengalamanKerja(){
        return $this->hasMany('App\Models\TrxKomjabPersyaratanJabatanPengalamanKerja', 'id_trx_komjab', 'id');
    }

     function komjabPersyaratanJabatanIndikatorKinerja(){
        return $this->hasMany('App\Models\TrxKomjabPersyaratanJabatanIndikatorKinerja', 'id_trx_komjab', 'id');
    }

    function dataLevelIntegritas(){
        return $this->belongsTo('App\Models\MKamusKompetensiLevel', 'id_m_kompetensi_level_integritas', 'id');
    }

    function dataLevelKerjasama(){
        return $this->belongsTo('App\Models\MKamusKompetensiLevel', 'id_m_kompetensi_level_kerjasama', 'id');
    }

    function dataLevelKomunikasi(){
        return $this->belongsTo('App\Models\MKamusKompetensiLevel', 'id_m_kompetensi_level_komunikasi', 'id');
    }

    function dataLevelOrientasi(){
        return $this->belongsTo('App\Models\MKamusKompetensiLevel', 'id_m_kompetensi_level_orientasi', 'id');
    }

    function dataLevelPelayanan(){
        return $this->belongsTo('App\Models\MKamusKompetensiLevel', 'id_m_kompetensi_level_pelayanan', 'id');
    }

    function dataLevelPengembangan(){
        return $this->belongsTo('App\Models\MKamusKompetensiLevel', 'id_m_kompetensi_level_pengembangan', 'id');
    }
    function dataLevelPerubahan(){
        return $this->belongsTo('App\Models\MKamusKompetensiLevel', 'id_m_kompetensi_level_perubahan', 'id');
    }
    function dataLevelKeputusan(){
        return $this->belongsTo('App\Models\MKamusKompetensiLevel', 'id_m_kompetensi_level_keputusan', 'id');
    }
    function dataLevelPerekatBangsa(){
        return $this->belongsTo('App\Models\MKamusKompetensiLevel', 'id_m_kompetensi_level_perekat_bangsa', 'id');
    }

    function dataAnjab(){
        return $this->belongsTo('App\Models\TrxAnjab',  'trx_anjab_id', 'id');
    }
}
