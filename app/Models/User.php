<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

     protected $fillable = [
        'option',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    function getOptionAttribute(){
        return json_decode($this->attributes['option']);
    }

    function getTextRoleAttribute(){
        $role = $this->attributes['role'];
        if($role == 'ADMIN'){
            return 'Admin Aplikasi';
        }elseif ($role == 'PD') {
            return 'Operator PD';
        }
    }

    function dataOpd(){
        return $this->belongsTo('App\Models\MUnitKerja', 'kode_opd', 'kodeunit');
    }

    function getTextOpdAttribute(){
        if($this->attributes['kode_opd'] == null){
            return 'PD Kabupaten Mojokerto';
        }else{
            return $this->dataOpd->unitkerja;
        }
    }
}
