<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TrxAnjabTanggungJawab extends Model
{

    use SoftDeletes;

    protected $table = 'trx_anjab_tanggung_jawab';

    function datatanggungJawab(){
        return $this->hasMany('App\Models\TrxAnjab', 'id', 'trx_anjab_id');
    }

}
