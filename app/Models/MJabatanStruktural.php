<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\MJabatanPelaksana;


class MJabatanStruktural extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'm_jabatan_struktural';

    public function anjab()
    {
        return $this->hasMany(TrxAnjab::class, 'm_jabatan_id', 'id');
    }
}
