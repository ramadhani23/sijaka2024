<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrxAnjabKorelasiJabatan extends Model
{

    use SoftDeletes;
    protected $table = 'trx_anjab_korelasi_jabatan';

    function datakorelasiJabatan(){
        return $this->hasMany('App\Models\TrxAnjab', 'id', 'trx_anjab_id');
    }

}
