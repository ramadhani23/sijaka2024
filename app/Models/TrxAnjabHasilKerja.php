<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TrxAnjabHasilKerja extends Model
{
    use SoftDeletes;
    protected $table = 'trx_anjab_hasil_kerja';

    function datahasilKerja(){
        return $this->hasMany('App\Models\TrxAnjab', 'id', 'trx_anjab_id');
    }

}
