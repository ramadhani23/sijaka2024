<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MLevelFaktor extends Model
{
    use SoftDeletes;
    
    protected $table = 'm_level_faktor';

    function dataFaktor(){
        return $this->belongsTo('App\Models\MFaktor', 'm_faktor_id', 'id');
    }



}

