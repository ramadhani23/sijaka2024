<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MJabatanFungsionalRumpun extends Model
{
    use HasFactory;

    protected $table = 'm_jabatan_fungsional_rumpun';
}
