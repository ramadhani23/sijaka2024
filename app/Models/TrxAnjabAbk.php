<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrxAnjabAbk extends Model

{
    use SoftDeletes;
    protected $table = 'trx_anjab_abk';

    function dataAnjab(){
        return $this->belongsTo('App\Models\TrxAnjab', 'trx_anjab_id', 'id');
    }

    function dataAbk(){
        return $this->hasMany('App\Models\TrxAnjab', 'id', 'trx_anjab_id');
    }

    function hasilKerja(){
        return $this->belongsTo('App\Models\TrxAnjabHasilKerja', 'trx_anjab_id', 'trx_anjab_id');
    }

    function dataTugasJabatan(){
    	return $this->belongsTo('App\Models\TrxAnjabUraianJabatan', 'trx_anjab_uraian_jabatan_id', 'id');
    }

    function getKebutuhanPegawaiAttribute(){
    	return number_format(($this->jumlah_hasil * $this->waktu_penyelesaian) / $this->waktu_efektif, 2);
    }

}
