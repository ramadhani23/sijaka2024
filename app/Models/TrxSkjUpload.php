<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrxSkjUpload extends Model
{
    use HasFactory;
    protected $table = 'trx_skj';

    function getTextOpdAttribute(){
        return $this->dataOpd->unitkerja;
    }

    function scopeUnitKerja($query, $unit){
        // return $query->where('unit_kerja_eselon2', 'LIKE', $unit.'%')->orWhere('unit_kerja_eselon3', 'LIKE', $unit.'%')->orWhere('unit_kerja_eselon4', 'LIKE', $unit.'%');

        return $query->where('unit_kerja', 'LIKE', $unit.'%');
    }

    function dataBahanKerja(){
        return $this->hasMany('App\Models\TrxAnjabBahanKerja', 'trx_anjab_id', 'id');
    }
    function dataHasilKerja(){
        return $this->hasMany('App\Models\TrxAnjabHasilKerja', 'trx_anjab_id', 'id');
    }
    function dataPrestasiKerja(){
        return $this->hasMany('App\Models\TrxAnjabPrestasiKerja', 'trx_anjab_id', 'id');
    }
    function dataKorelasiJabatan(){
        return $this->hasMany('App\Models\TrxAnjabKorelasiJabatan', 'trx_anjab_id', 'id');
    }
    function dataPerangkat(){
        return $this->hasMany('App\Models\TrxAnjabPerangkat', 'trx_anjab_id', 'id');
    }
    function dataResikoBahaya(){
        return $this->hasMany('App\Models\TrxAnjabResikoBahaya', 'trx_anjab_id', 'id');
    }
    function dataWewenang(){
        return $this->hasMany('App\Models\TrxAnjabWewenang', 'trx_anjab_id', 'id');
    }
    function dataSyaratJabatan(){
        return $this->belongsTo('App\Models\TrxAnjabSyaratJabatan', 'id', 'trx_anjab_id');
    }
    function dataLingkunganKerja(){
        return $this->belongsTo('App\Models\TrxAnjabLingkunganKerja', 'id', 'trx_anjab_id');
    }
    function dataTanggungJawab(){
        return $this->hasMany('App\Models\TrxAnjabTanggungJawab', 'trx_anjab_id', 'id');
    }
    function dataAbk(){
        return $this->hasMany('App\Models\TrxAnjabAbk', 'trx_anjab_id', 'id');
    }
    function dataAbkKhusus(){
        return $this->belongsTo('App\Models\TrxAbkKhusus', 'id', 'trx_anjab_id');
    }
    function dataKomjab(){
        return $this->hasMany('App\Models\MTrxKomjab', 'trx_anjab_id', 'id');
    }
    function dataEvajabFungsional(){
        return $this->hasMany('App\Models\TrxAnjabEvajabFungsional', 'trx_anjab_id', 'id');
    }
    function dataEvajabStruktural(){
        return $this->hasMany('App\Models\TrxAnjabEvajabStruktural', 'trx_anjab_id', 'id');
    }
    function dataUraianJabatan(){
        return $this->hasMany('App\Models\TrxAnjabUraianJabatan', 'trx_anjab_id', 'id');
    }

    function dataEvajab(){
        if($this->attributes['jenis_jabatan'] == 1){
            return $this->hasOne('App\Models\TrxAnjabEvajabStruktural', 'trx_anjab_id', 'id');
        }elseif ($this->attributes['jenis_jabatan'] == 2 || $this->attributes['jenis_jabatan'] == 3) {
            return $this->hasOne('App\Models\TrxAnjabEvajabFungsional', 'trx_anjab_id', 'id');
        }
    }

    function getKelasJabatanAttribute(){
        // $kelasJabatan = $this->dataEvajab->
        $dataEvajab = $this->dataEvajab;
        $totalNilai = 0;
        // foreach($dataEvajab as $itemDataEvajab){
        //     $totalNilai += $itemDataEvajab->total_nilai;
        // }
        $totalNilai = $dataEvajab->total_nilai;

        $kelasJabatan = 0;

        if($totalNilai < 190){
            $kelasJabatan = 0;
        }elseif($totalNilai <= 240){
            $kelasJabatan =  1;
        }elseif($totalNilai <= 300){
            $kelasJabatan =  2;
        }elseif($totalNilai <= 370){
            $kelasJabatan =  3;
        }elseif($totalNilai <= 450){
            $kelasJabatan =  4;
        }elseif($totalNilai <= 650){
            $kelasJabatan =  5;
        }elseif($totalNilai <= 850){
            $kelasJabatan =  6;
        }elseif($totalNilai <= 1100){
            $kelasJabatan =  7;
        }elseif($totalNilai <= 1350){
            $kelasJabatan =  8;
        }elseif($totalNilai <= 1600){
            $kelasJabatan =  9;
        }elseif($totalNilai <= 1850){
            $kelasJabatan =  10;
        }elseif($totalNilai <= 2100){
            $kelasJabatan =  11;
        }elseif($totalNilai <= 2350){
            $kelasJabatan =  12;
        }elseif($totalNilai <= 2750){
            $kelasJabatan =  13;
        }elseif($totalNilai <= 3150){
            $kelasJabatan =  14;
        }elseif($totalNilai <= 3600){
            $kelasJabatan =  15;
        }elseif($totalNilai <= 4050){
            $kelasJabatan =  16;
        }elseif($totalNilai >= 4055){
            $kelasJabatan =  17;
        }

        return $kelasJabatan;

    }


    function getTotalKebutuhanPegawaiAttribute(){
        if($this->dataAbkKhusus){
            return number_format($this->dataAbkKhusus->kebutuhan);
        }
        return number_format($this->dataAbk->sum('kebutuhan_pegawai'));
    }
    function getTotalWaktuEfektifAttribute(){
        return number_format($this->dataAbk->sum('waktu_efektif'));
    }

    function dataOpd(){
        return $this->belongsTo('App\Models\MKelembagaan', 'kode_opd_2', 'kode_bkpp');
    }

    function getKodeOpd2Attribute(){
        return substr($this->attributes['unit_kerja'], 0, 2);
    }

    function dataUser(){
        return $this->belongsTo('App\Models\User', 'user', 'username');
    }


    function jabatan(){
        $jenisJabatan = $this->jenis_jabatan;
        if($jenisJabatan == '1'){
            return $this->belongsTo('App\Models\MJabatanStruktural', '_kode_jabatan', '_kode');
        }elseif ($jenisJabatan == '3') {
            return $this->belongsTo('App\Models\MJabatanPelaksana', '_kode_jabatan', '_kode');
        }elseif ($jenisJabatan == '2') {
            return $this->belongsTo('App\Models\MJabatanFungsional', '_kode_jabatan', '_kode');
        }


    }

    // function jabatanPelaksana(){
    //     return $this->belongsTo('App\Models\MJabatanPelaksana', '_kode_jabatan', '_kode');
    // }

    function dataUnitKerja(){
        return $this->belongsTo('App\Models\MUnitKerja', 'unit_kerja', 'kodeunit');
    }

    function parentJabatan(){
        return $this->belongsTo('App\Models\TrxAnjab', 'parent', 'id');
    }

    function eselon2(){
    	return $this->belongsTo('App\Models\MUnitKerja', 'unit_kerja_eselon2', 'kodeunit');
    }

    function eselon3(){
    	return $this->belongsTo('App\Models\MUnitKerja', 'unit_kerja_eselon3', 'kodeunit');
    }

    function eselon4(){
    	return $this->belongsTo('App\Models\MUnitKerja', 'unit_kerja_eselon4', 'kodeunit');
    }


    function getTextJenisJabatanAttribute(){
        switch ($this->jenis_jabatan) {
            case '1':
                return 'Struktural';
                break;
            case '2':
                return 'Fungsional';
                break;
            case '3':
                return 'Pelaksana';
                break;
            default:
                return 'Tidak Ditemukan';
                break;
        }
    }

    function getTextPangkatAttribute(){

        $gol = $this->pangkat;

        switch ($gol) {
            case  11:
                return 'Juru Muda';
                break;
            case 12:
                return 'Juru Muda Tk. I';
                break;
            case 13:
                return 'Juru';
                break;
            case 14:
                return 'Juru Tk. I';
                break;

            case 21:
                return 'Pengatur Muda';
                break;
            case 22:
                return 'Pengatur Muda Tk. I';
                break;
            case 23:
                return 'Pengatur';
                break;
            case 24:
                return 'Pengatur Tk. I';
                break;

            case 31:
                return 'Penata Muda';
                break;
            case 32:
                return 'Penata Muda Tk. I';
                break;
            case 33:
                return 'Penata';
                break;
            case 34:
                return 'Penata Tingkat I';
                break;

            case 41:
                return 'Pembina';
                break;
            case 42:
                return 'Pembina Tk. I';
                break;
            case 43:
                return 'Pembina Utama Muda';
                break;
            case 44:
                return 'Pembina Utama Madya';
                break;
            case 45:
                return 'Pembina Utama';
                break;
            default:
                return "Pangkat Tidak Ditemukan";
                break;
        }
    }

    function getTextEselonJabatanAttribute(){
        if($this->eselon_jabatan){
            switch ($this->eselon_jabatan) {
                case 11:
                    return 'Ia';
                    break;
                case 21:
                    return 'IIa';
                    break;
                case 31:
                    return 'IIIa';
                    break;
                case 41:
                    return 'IVa';
                    break;
                case 12:
                    return 'Ib';
                    break;
                case 22:
                    return 'IIb';
                    break;
                case 32:
                    return 'IIIb';
                    break;
                case 42:
                    return 'IVb';
                    break;

                default:
                    # code...
                    break;
            }
            return $this->eselon_jabatan;
        }else{
            return '-';
        }
    }

    function getTextKodeJabatanAttribute(){
        if($this->kode_jabatan){
            return $this->kode_jabatan;
        }else{
            return '-';
        }
    }

    function getStatusVerificationAttribute(){
        $sumStatus = $this->sum_status_verification;
        $successItem = 12;
        if($sumStatus[2] == $successItem){
            return 1;
        }elseif($sumStatus[1] == $successItem){
            return 2;
        }else{
            return 0;
        }
    }

    function getSumStatusVerificationAttribute(){
        $jumlahMenunggu = 0;
        $jumlahDitolak = 0;
        $jumlahDiterima = 0;
        $arrItem = ['status_data_jabatan', 'status_tanggung_jawab', 'status_wewenang', 'status_korelasi_jabatan', 'status_bahan_kerja', 'status_perangkat', 'status_hasil_kerja', 'status_kondisi_lingkungan_kerja', 'status_resiko_bahaya', 'status_syarat_jabatan', 'status_prestasi_kerja', 'status_uraian_tugas'];
        foreach ($arrItem as $item) {
            if($this->attributes[$item] == 'menunggu_verifikasi' || $this->attributes[$item] == NULL){
                $jumlahMenunggu++;
            }elseif($this->attributes[$item] == 'ditolak'){
                $jumlahDitolak++;
            }elseif($this->attributes[$item] == 'verifikasi'){
                $jumlahDiterima++;
            }
        }

        return [$jumlahMenunggu, $jumlahDitolak, $jumlahDiterima];
    }
}
