<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MJabatanBkpp extends Model
{
    use HasFactory;

    protected $table = 'jabatan_bkpp';
}
