<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TrxAnjabPrestasiKerja extends Model
{

    use SoftDeletes;

    protected $table = 'trx_anjab_prestasi_kerja';

    function dataPrestasiKerja(){
        return $this->hasMany('App\Models\TrxAnjab', 'id', 'trx_anjab_id');
    }

}
