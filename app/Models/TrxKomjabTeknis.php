<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrxKomjabTeknis extends Model
{
    use HasFactory;
    protected $table ='trx_komjab_teknis';


    function indikatorTeknis(){
        return $this->hasMany('App\Models\MTrxKomjabTeknisIndikator', 'id_trx_komjab_teknis', 'id');
    }
}
