<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MJabatanUpt extends Model
{
    use HasFactory;
    protected $table = 'm_jabatan_upt';
    protected $fillable = ['jabatan', 'kodeunit_instansi']; // Kolom yang bisa diisi

    public function unitKerja()
    {
        return $this->belongsTo(MUnitKerja::class, 'kodeunit_instansi', 'kodeunit');
    }

}
