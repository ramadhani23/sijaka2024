<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RkpKebutuhanPegawai extends Model
{
    use HasFactory;

    protected $table = 'rkp_kebutuhan_pegawai';
}
