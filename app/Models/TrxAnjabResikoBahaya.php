<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TrxAnjabResikoBahaya extends Model
{

    use SoftDeletes;
    protected $table = 'trx_anjab_resiko_bahaya';

    function dataresikoBahaya(){
        return $this->hasMany('App\Models\TrxAnjab', 'id', 'trx_anjab_id');
    }

}
