<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TrxAnjabUraianJabatanTahapan extends Model
{
    use SoftDeletes;
    protected $table = 'trx_anjab_uraian_jabatan_tahapan';

}
