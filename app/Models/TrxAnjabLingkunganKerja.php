<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TrxAnjabLingkunganKerja extends Model
{

    use SoftDeletes;

    protected $table = 'trx_anjab_lingkungan_kerja';

    function datalingkunganKerja(){
        return $this->hasMany('App\Models\TrxAnjab', 'id', 'trx_anjab_id');
    }


}
