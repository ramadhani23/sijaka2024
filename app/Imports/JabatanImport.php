<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class JabatanImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
    	$no = 1;
        foreach ($rows as $row) 
        {

        	if($row[0] != 'Jabatan' || ($row[0] != '' && $row[1] != '' && $row[2] != '')){
        		echo "<table>
	         		<tr>
	         			<td>".$no."</td>
	         			<td>".$row[0]."</td>
	         			<td>".$row[1]."</td>
	         			<td>".$row[2]."</td>
	         		</tr>
	         	</table>";	
        	}
        	$no++;
         	   
        }
    }
}
