<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('login2');
})->name('login-view');
Route::get('/get-data-jenjang/{id}','AnjabController@get_jenjang')->name('get-jenjang');
Route::get('/get-data-jenjang/{id}/{selected}','AnjabController@get_jenjang_selected')->name('get-jenjang-seletced');


Route::get('login', 'AuthController@auth')->name('login');
Route::get('callback', 'AuthController@callback');

Route::get('data-jabatan', 'TrialController@import');

Route::get('sync-jabatan-lama', function(){
	$jabatanPelaksanaBaru = DB::table('m_jabatan_2')->get();
	// dd($jabatanPelaksanaBaru);
	foreach ($jabatanPelaksanaBaru as $itemJabatanBaru) {
		$jabatanLama = DB::table('m_jabatan')->where('nama', 'LIKE', '%'.$itemJabatanBaru->jabatan.'%')->first();
		// dd($jabatanLama);
		if($jabatanLama){
			$jabatanBaru = \App\Models\MJabatan::find($itemJabatanBaru->id);
			$jabatanBaru->kode_bkpp = $jabatanLama->kode_jabatan;
			$jabatanBaru->update();
		}


		// $jabatanBaru->update(['kode_bkpp'=>$jabatanLama->kode_jabatan]);
	}
});

Route::get('sync-kode2-jabatan', function(){
	$jabatan = \App\Models\MJabatan::where('jenis', 'pelaksana')->get();
	foreach($jabatan as $itemJabatan){
		$itemJabatan->kode_2 = $itemJabatan->kode;
		$itemJabatan->update();
	}
});

Route::get('sync-kode_jabatan2_anjab', function(){
	$anjab = \App\Models\TrxAnjab::all();
	foreach($anjab as $itemAnjab){

		if($itemAnjab->m_jabatan_id_2 != NULL){
			$jabatan = \App\Models\MJabatan::find($itemAnjab->m_jabatan_id_2);

			if($jabatan){
				$itemAnjab->kode_jabatan_2 = $jabatan->kode_2;
				$itemAnjab->update();
			}else{
				echo "Tidak Ada Jabatan ";
				dd($itemAnjab);
			}
		}
	}
});

Route::get('sync-anjab-lama', function(){
	$listAnjab = \App\Models\TrxAnjab::where('kode_jabatan', '!=', NULL)->get();
	foreach($listAnjab as $itemAnjab){
		$jabatan = \App\Models\MJabatan::where('kode_bkpp', $itemAnjab->kode_jabatan)->first();
		if($jabatan){
			// dd($jabatan);
			$itemAnjab->m_jabatan_id_2 = $jabatan->id;
			$itemAnjab->update();
		}

	}
});


Route::get('strfirst-jabatan', function(){
	$listJabatan = \App\Models\MJabatan::where('jenis', 'struktural')->get();

	foreach($listJabatan as $itemJabatan){
		$itemJabatan->jabatan =ucwords(strtolower($itemJabatan->jabatan));
		$itemJabatan->update();
	}

});
Route::get('get-hasil-kerja', 'AbkController@getHasilKerja')->name('abk.get-hasil-kerja');
Route::get('/get-jabatan', 'AnjabController@getJabatan')->name('anjab.get-jabatan');

Route::group(['prefix'=>'mojokertoku', 'middleware'=>'is.login'], function(){
	Route::get('/', 'DashboardController@index')->name('mojokertoku.dashboard');
	Route::get('/get-struktur-organisasi', 'DashboardController@getStrukturOrganisasi')->name('mojokertoku.get-struktur-organisasi');
	Route::get('/clear-token', 'AuthController@clearToken')->name('mojokertoku.clear-token');
	Route::get('/kunci-anjab', 'AnjabController@kunciAnjab')->name('mojokertoku.kunci-anjab');
	Route::get('/buka-kunci-anjab', 'AnjabController@bukaKunciAnjab')->name('mojokertoku.buka-kunci-anjab');


	Route::group(['prefix'=>'anjab'], function(){
		Route::get('/', 'AnjabController@browse')->name('anjab.browse');
		Route::get('/table', 'AnjabController@anjab_table')->name('anjab.table');
		Route::get('/get-form', 'AnjabController@getForm')->name('anjab.get-form');

		Route::get('/get-unit-kerja', 'AnjabController@getUnitKerja')->name('anjab.get-unit-kerja');
		Route::get('/view-print', 'AnjabController@viewPrint')->name('anjab.view-print');
		Route::post('/simpan', 'AnjabController@save')->name('anjab.save');
		Route::post('/copy', 'AnjabController@copy')->name('anjab.copy');
		Route::post('/delete', 'AnjabController@delete')->name('anjab.delete');

		Route::get('/get-org-config', 'AnjabController@getOrgConfig')->name('anjab.get-org-config');

		Route::get('/check-anjab', 'AnjabController@checkAnjab')->name('anjab.check-anjab');
		Route::get('/get-atasan', 'AnjabController@getAtasanByOpd')->name('anjab.get-atasan-by-opd');
		Route::post('/duplicate','AnjabController@duplicate')->name('anjab.duplicate');




		Route::get('/print/{id_anjab}', 'AnjabController@printAnjab')->name('anjab.print');
		Route::get('/print-word/{id_anjab}', 'AnjabController@printWord')->name('anjab.print-word');

		Route::group(['prefix'=>'uraian'], function(){

		});



		Route::group(['prefix'=>'uraian-tugas'], function(){

			Route::get('/get-form-uraian', 'AnjabUraianTugasController@getFormUraian')->name('anjab.get-form-uraian');
			Route::get('/get-form-uraian-tahapan', 'AnjabUraianTugasController@getFormUraianTahapan')->name('anjab.get-form-uraian-tahapan');
			Route::post('/simpan-uraian', 'AnjabUraianTugasController@saveUraian')->name('anjab.save-uraian');
			Route::post('/simpan-uraian-tahapan', 'AnjabUraianTugasController@saveUraianTahapan')->name('anjab.save-uraian-tahapan');
			Route::post('/delete', 'AnjabUraianTugasController@deleteUraian')->name('anjab.delete-uraian');
			Route::get('/{id}', 'AnjabUraianTugasController@browse')->name('anjab.uraian-tugas');
		});

		Route::group(['prefix'=>'verifikasi'], function(){
			Route::post('/', 'AnjabController@verifikasi')->name('anjab.verifikasi');
			Route::get('/modal-tolak', 'AnjabController@loadModalTolak')->name('anjab.load-modal-tolak');
			Route::get('{id}', 'AnjabController@getVerifikasi')->name('anjab.get-verifikasi');

		});

		Route::get('detail/{id}', 'AnjabController@detail')->name('anjab.detail');

	});



	Route::group(['prefix'=>'anjab-tanggung-jawab'], function(){
		Route::get('/get-form', 'AnjabTanggungJawabController@getForm')->name('anjab.get-form-tanggung-jawab');
		Route::post('/simpan', 'AnjabTanggungJawabController@save')->name('anjab.save-tanggung-jawab');
		Route::post('/delete', 'AnjabTanggungJawabController@deleteTanggungJawab')->name('anjab.delete-tanggung-jawab');
		Route::get('/{id}', 'AnjabTanggungJawabController@browse')->name('anjab.browse-tanggung-jawab');
	});

	Route::group(['prefix'=>'anjab-wewenang'], function(){
		Route::get('/get-form', 'AnjabWewenangController@getForm')->name('anjab.get-form-wewenang');
		Route::post('/simpan', 'AnjabWewenangController@save')->name('anjab.save-wewenang');
		Route::post('/delete', 'AnjabWewenangController@deleteWewenang')->name('anjab.delete-wewenang');
		Route::get('/{id}', 'AnjabWewenangController@browse')->name('anjab.browse-wewenang');
	});

	Route::group(['prefix'=>'anjab-bahan-kerja'], function(){
		Route::get('/get-form', 'AnjabBahanKerjaController@getForm')->name('anjab.get-form-bahan-kerja');
		Route::post('/simpan', 'AnjabBahanKerjaController@save')->name('anjab.save-bahan-kerja');
		Route::post('/delete', 'AnjabBahanKerjaController@deleteBahanKerja')->name('anjab.delete-bahan-kerja');
		Route::get('/{id}', 'AnjabBahanKerjaController@browse')->name('anjab.browse-bahan-kerja');
	});

	Route::group(['prefix'=>'anjab-perangkat'], function(){
		Route::get('/get-form', 'AnjabPerangkatController@getForm')->name('anjab.get-form-perangkat');
		Route::post('/simpan', 'AnjabPerangkatController@save')->name('anjab.save-perangkat');
		Route::post('/delete', 'AnjabPerangkatController@deletePerangkat')->name('anjab.delete-perangkat');
		Route::get('/{id}', 'AnjabPerangkatController@browse')->name('anjab.browse-perangkat');
	});

	Route::group(['prefix'=>'anjab-hasil-kerja'], function(){
		Route::get('/get-form', 'AnjabHasilKerjaController@getForm')->name('anjab.get-form-hasil-kerja');
		Route::post('/simpan', 'AnjabHasilKerjaController@save')->name('anjab.save-hasil-kerja');
		Route::post('/delete', 'AnjabHasilKerjaController@deleteHasilKerja')->name('anjab.delete-hasil-kerja');
		Route::get('/{id}', 'AnjabHasilKerjaController@browse')->name('anjab.browse-hasil-kerja');
	});

	Route::group(['prefix'=>'anjab-lingkungan-kerja'], function(){
		Route::get('/get-form', 'AnjabLingkunganKerjaController@getForm')->name('anjab.get-form-lingkungan-kerja');
		Route::post('/simpan', 'AnjabLingkunganKerjaController@save')->name('anjab.save-lingkungan-kerja');
		Route::post('/delete', 'AnjabLingkunganKerjaController@deleteLingkunganKerja')->name('anjab.delete-lingkungan-kerja');
		Route::get('/{id}', 'AnjabLingkunganKerjaController@browse')->name('anjab.browse-lingkungan-kerja');
	});

	Route::group(['prefix'=>'anjab-resiko'], function(){
		Route::get('/get-form', 'AnjabResikoBahayaController@getForm')->name('anjab.get-form-resiko-bahaya');
		Route::post('/simpan', 'AnjabResikoBahayaController@save')->name('anjab.save-resiko-bahaya');
		Route::post('/delete', 'AnjabResikoBahayaController@deleteResiko')->name('anjab.delete-resiko-bahaya');
		Route::get('/{id}', 'AnjabResikoBahayaController@browse')->name('anjab.browse-resiko-bahaya');
	});
	Route::group(['prefix'=>'anjab-korelasi-jabatan'], function(){
		Route::get('/get-form', 'AnjabKorelasiJabatanController@getForm')->name('anjab.get-form-korelasi-jabatan');
		Route::post('/simpan', 'AnjabKorelasiJabatanController@save')->name('anjab.save-korelasi-jabatan');
		Route::post('/delete', 'AnjabKorelasiJabatanController@deleteKorelasi')->name('anjab.delete-korelasi-jabatan');
		Route::get('/{id}', 'AnjabKorelasiJabatanController@browse')->name('anjab.browse-korelasi-jabatan');
	});
	Route::group(['prefix'=>'anjab-syarat-jabatan'], function(){
		Route::get('/get-form', 'AnjabSyaratJabatanController@getForm')->name('anjab.get-form-syarat-jabatan');
		Route::post('/simpan', 'AnjabSyaratJabatanController@save')->name('anjab.save-syarat-jabatan');
		Route::post('/delete', 'AnjabSyaratJabatanController@deleteKorelasi')->name('anjab.delete-syarat-jabatan');
		Route::get('/{id}', 'AnjabSyaratJabatanController@browse')->name('anjab.browse-syarat-jabatan');
	});
	Route::group(['prefix'=>'anjab-prestasi-kerja'], function(){
		Route::get('/get-form', 'AnjabPrestasiKerjaController@getForm')->name('anjab.get-form-prestasi-kerja');
		Route::post('/simpan', 'AnjabPrestasiKerjaController@save')->name('anjab.save-prestasi-kerja');
		Route::post('/delete', 'AnjabPrestasiKerjaController@deletePrestasi')->name('anjab.delete-prestasi-kerja');
		Route::get('/{id}', 'AnjabPrestasiKerjaController@browse')->name('anjab.browse-prestasi-kerja');
	});
	Route::group(['prefix'=>'anjab-abk'], function(){
		Route::get('/get-form', 'AnjabAbkController@getForm')->name('anjab.get-form-abk');
		Route::post('/simpan', 'AnjabAbkController@save')->name('anjab.save-abk');
		Route::post('/delete', 'AnjabAbkController@deleteAbk')->name('anjab.delete-abk');
		Route::get('/{id}', 'AnjabAbkController@browse')->name('anjab.browse-abk');
	});

	Route::group(['prefix'=>'anjab-abk'], function(){
		Route::get('/get-form', 'AnjabAbkController@getForm')->name('anjab.get-form-abk');
		Route::post('/simpan', 'AnjabAbkController@save')->name('anjab.save-abk');
		Route::post('/delete', 'AnjabAbkController@deleteAbk')->name('anjab.delete-abk');
		Route::get('/{id}', 'AnjabAbkController@browse')->name('anjab.browse-abk');
	});

	// Route::group(['prefix'=>'anjab-evajab'], function(){
	// 	Route::get('/get-level-faktor', 'AnjabInputEvajabController@getLevelFaktor')->name('anjab.get-level-faktor');
	// 	Route::post('/simpan', 'AnjabInputEvajabController@save')->name('anjab.save-evajab');
	// 	Route::post('/update', 'AnjabInputEvajabController@update')->name('anjab.update-evajab');
	// 	Route::get('/{id}', 'AnjabInputEvajabController@input')->name('anjab.input-evajab');
	// });

	Route::group(['prefix'=>'evajab'], function(){
		Route::get('/get-level-faktor', 'EvajabController@getLevelFaktor')->name('evajab.get-level-faktor');
		Route::get('/get-form', 'EvajabController@getForm')->name('evajab.get-form');
		Route::get('/pilih-anjab', 'EvajabController@pilihAnjab')->name('evjab.pilih-anjab');
		Route::get('/get-jabatan', 'EvajabController@getJabatan')->name('evajab.get-jabatan');
		Route::get('/get-unit-kerja', 'EvajabController@getUnitKerja')->name('evajab.get-unit-kerja');
		Route::get('/view-print', 'EvajabController@viewPrint')->name('evajab.view-print');
		Route::post('/simpan', 'EvajabController@save')->name('evajab.save');
		Route::post('/delete', 'EvajabController@deleteEvajab')->name('evajab.delete');
		Route::post('/update', 'EvajabController@update')->name('evajab.update');
		Route::get('/{jenis}', 'EvajabController@browse')->name('evajab.browse');
		Route::get('/detail/{id}', 'EvajabController@detail')->name('evajab.detail');
		Route::get('/print/{jabatan}/{unitkerja}/{anjab_id}', 'EvajabController@printEvajab')->name('evajab.print');
		Route::get('/verifikasi/{id}', 'EvajabController@verifikasi')->name('evajab.verifikasi');
		Route::get('/batalverif/{id}', 'EvajabController@batalverifikasi')->name('evajab.batal-verifikasi');


	});

	Route::group(['prefix'=>'abk'], function(){
		// Route::get('/get-jabatan', 'EvajabController@getJabatan')->name('evajab.get-jabatan');
		// Route::get('/get-unit-kerja', 'EvajabController@getUnitKerja')->name('evajab.get-unit-kerja');
		// Route::get('/view-print', 'EvajabController@viewPrint')->name('evajab.view-print');
		// Route::post('/simpan', 'EvajabController@save')->name('evajab.save');
		// Route::post('/update', 'EvajabController@update')->name('evajab.update');
		// Route::get('/print/{jabatan}/{unitkerja}', 'EvajabController@printEvajab')->name('evajab.print');
		Route::get('/get-form', 'AbkController@getForm')->name('abk.get-form-abk');
		Route::get('/pilih-anjab', 'AbkController@pilihAnjab')->name('abk.pilih-anjab');
		Route::post('/simpan', 'AbkController@save')->name('abk.save-abk');
		Route::post('/update', 'AbkController@update')->name('abk.update-abk');
		Route::post('/delete', 'AbkController@deleteAbk')->name('abk.delete-abk');
		Route::post('/verif', 'AbkController@verifikasiStrukturalPelaksana')->name('abk.verifikasi-abk-struktural-pelaksana');
		Route::get('/detail/{id}', 'AbkController@detail')->name('abk.detail');
		Route::get('/{jenis}', 'AbkController@browse')->name('abk.browse');
		Route::post('/delete-abk-khusus', 'AbkController@deleteAbkKhusus')->name('abk.delete-abk-khusus');
		Route::post('/verif-khusus', 'AbkController@verifikasi')->name('abk.verifikasi-abk-khusus');
		Route::post('/batalverif-khusus', 'AbkController@batalverifikasi')->name('abk.batal-verifikasi-abk-khusus');
		Route::get('/cek-relasi', 'AbkController@cekRelasi')->name('anjab.cek-relasi');
		
	});



	Route::group(['prefix'=>'komjab'], function(){

		Route::get('pilih-anjab', 'KomjabController@pilihAnjab')->name('komjab.pilih-anjab');
		Route::get('/get-data', 'KomjabController@getData')->name('komjab.get-data');
		Route::get('/get-form', 'KomjabController@getForm')->name('komjab.get-form');
		Route::get('/get-data/teknis', 'KomjabController@getDataTeknis')->name('komjab.get-data-teknis');
		Route::post('/save', 'KomjabController@save')->name('komjab.save');
		Route::post('/update', 'KomjabController@save')->name('komjab.update');
		Route::post('/delete', 'KomjabController@delete')->name('komjab.delete');
		Route::get('/{jenis}', 'KomjabController@browse')->name('komjab.browse');
		Route::get('detail/{id}', 'KomjabController@detail')->name('komjab.detail');
		Route::get('/print-word/{id_komjab}', 'KomjabController@printWord')->name('komjab.print-word');

		// Route::post('/save', 'AnjabInputKomjabController@save')->name('komjab.save');
		// Route::post('/update', 'AnjabInputKomjabController@save')->name('komjab.update');
		// Route::get('/get-data', 'AnjabInputKomjabController@getData')->name('komjab.get-data');
		// Route::get('/get-form', 'AnjabInputKomjabController@getForm')->name('komjab.get-form');
		// Route::get('/get-data/teknis', 'AnjabInputKomjabController@getDataTeknis')->name('komjab.get-data-teknis');
		// Route::post('/save', 'AnjabInputKomjabController@save')->name('komjab.save');
		// Route::post('/update', 'AnjabInputKomjabController@save')->name('komjab.update');
		// Route::post('/delete', 'AnjabInputKomjabController@delete')->name('komjab.delete');
		// Route::get('/print-word/{id_komjab}', 'AnjabInputKomjabController@printWord')->name('komjab.print-word');
		// Route::get('/{id}', 'AnjabInputKomjabController@input')->name('komjab.input');
	});

	Route::group(['prefix'=>'jabatan'], function(){

		Route::group(['prefix'=>'struktural'], function(){
			Route::get('/', 'JabatanStrukturalController@browse')->name('jabatan-struktural.browse');
			Route::get('/get-form', 'JabatanStrukturalController@getForm')->name('jabatan-struktural.get-form');
			Route::post('/save', 'JabatanStrukturalController@save')->name('jabatan-struktural.save');
			Route::post('/delete', 'JabatanStrukturalController@delete')->name('jabatan-struktural.delete');
		});
		Route::group(['prefix'=>'fungsional'], function(){
			Route::get('/', 'JabatanFungsionalController@browse')->name('jabatan-fungsional.browse');
			Route::get('/get-form', 'JabatanFungsionalController@getForm')->name('jabatan-fungsional.get-form');
			Route::get('/detail/{id}', 'JabatanFungsionalController@detail')->name('jabatan-fungsional.detail');
			Route::post('/save', 'JabatanFungsionalController@save')->name('jabatan-fungsional.save');
			Route::post('/delete', 'JabatanFungsionalController@delete')->name('jabatan-fungsional.delete');
			Route::post('/delete-jabatan', 'JabatanFungsionalController@deleteJenjang')->name('jabatan-fungsional.delete-jenjang');
			Route::post('/delete-syarat', 'JabatanFungsionalController@deleteSyarat')->name('jabatan-fungsional.delete-syarat');
		});
		Route::group(['prefix'=>'pelaksana'], function(){
			Route::get('/', 'JabatanPelaksanaController@browse')->name('jabatan-pelaksana.browse');
			Route::get('/get-form', 'JabatanPelaksanaController@getForm')->name('jabatan-pelaksana.get-form');
			Route::post('/save', 'JabatanPelaksanaController@save')->name('jabatan-pelaksana.save');
			Route::post('/delete', 'JabatanPelaksanaController@delete')->name('jabatan-pelaksana.delete');
		});
		Route::group(['prefix'=>'upt'], function(){
			Route::get('/', 'JabatanUptController@browse')->name('jabatan-upt.browse');
			Route::get('/get-form', 'JabatanUptController@getForm')->name('jabatan-upt.get-form');
			Route::post('/save', 'JabatanUptController@save')->name('jabatan-upt.save');
			Route::post('/delete', 'JabatanUptController@delete')->name('jabatan-upt.delete');
		});
	});
	Route::group(['prefix'=>'fungsi-pekerjaan'], function(){
		Route::get('/', 'FungsiPekerjaanController@browse')->name('fungsi-pekerjaan.browse');
		Route::get('/get-form', 'FungsiPekerjaanController@getForm')->name('fungsi-pekerjaan.get-form');
		Route::post('/save', 'FungsiPekerjaanController@save')->name('fungsi-pekerjaan.save');
		Route::post('/delete', 'FungsiPekerjaanController@delete')->name('fungsi-pekerjaan.delete');
	});
	Route::group(['prefix'=>'minat-kerja'], function(){
		Route::get('/', 'MinatKerjaController@browse')->name('minat-kerja.browse');
		Route::get('/get-form', 'MinatKerjaController@getForm')->name('minat-kerja.get-form');
		Route::post('/save', 'MinatKerjaController@save')->name('minat-kerja.save');
		Route::post('/delete', 'MinatKerjaController@delete')->name('minat-kerja.delete');
	});
	Route::group(['prefix'=>'instansi-pembina'], function(){
		Route::get('/', 'InstansiPembinaController@browse')->name('instansi-pembina.browse');
		Route::get('/get-form', 'InstansiPembinaController@getForm')->name('instansi-pembina.get-form');
		Route::post('/save', 'InstansiPembinaController@save')->name('instansi-pembina.save');
		Route::post('/delete', 'InstansiPembinaController@delete')->name('instansi-pembina.delete');
	});
	Route::group(['prefix'=>'tempramen-kerja'], function(){
		Route::get('/', 'TempramenKerjaController@browse')->name('tempramen-kerja.browse');
		Route::get('/get-form', 'TempramenKerjaController@getForm')->name('tempramen-kerja.get-form');
		Route::post('/save', 'TempramenKerjaController@save')->name('tempramen-kerja.save');
		Route::post('/delete', 'TempramenKerjaController@delete')->name('tempramen-kerja.delete');
	});
	Route::group(['prefix'=>'unit-kerja'], function(){
		Route::get('/', 'UnitKerjaController@browse')->name('unit-kerja.browse');
		Route::get('/get-form', 'UnitKerjaController@getForm')->name('unit-kerja.get-form');
		Route::post('/save', 'UnitKerjaController@save')->name('unit-kerja.save');
		Route::post('/delete', 'UnitKerjaController@delete')->name('unit-kerja.delete');
	});
	Route::group(['prefix'=>'bakat-kerja'], function(){
		Route::get('/', 'BakatKerjaController@browse')->name('bakat-kerja.browse');
		Route::get('/get-form', 'BakatKerjaController@getForm')->name('bakat-kerja.get-form');
		Route::post('/save', 'BakatKerjaController@save')->name('bakat-kerja.save');
		Route::post('/delete', 'BakatKerjaController@delete')->name('bakat-kerja.delete');
	});
	Route::group(['prefix'=>'upaya-fisik'], function(){
		Route::get('/', 'UpayaFisikController@browse')->name('upaya-fisik.browse');
		Route::get('/get-form', 'UpayaFisikController@getForm')->name('upaya-fisik.get-form');
		Route::post('/save', 'UpayaFisikController@save')->name('upaya-fisik.save');
		Route::post('/delete', 'UpayaFisikController@delete')->name('upaya-fisik.delete');
	});
	Route::group(['prefix'=>'users'], function(){
		Route::get('/', 'UserController@browse')->name('user.browse');
		Route::get('/get-form', 'UserController@getForm')->name('user.get-form');
		Route::get('/get-pegawai-from-simpeg', 'UserController@getPegawaiFromSimpeg')->name('user.get-pegawai-from-simpeg');
		Route::get('/get-pegawai-satker-from-simpeg', 'UserController@getPegawaiSatkerFromSimpeg')->name('user.get-pegawai-satker-from-simpeg');
		Route::post('/save', 'UserController@save')->name('user.save');
		Route::post('/delete', 'UserController@delete')->name('user.delete');
		
		Route::get('/cek', 'UserController@getdb');
	});

	Route::group(['prefix'=>'periode'], function(){
		Route::get('/', 'PeriodeController@browse')->name('periode.browse');
		Route::get('/get-form', 'PeriodeController@getForm')->name('periode.get-form');
		Route::post('/save', 'PeriodeController@save')->name('periode.save');
		Route::post('/delete', 'PeriodeController@delete')->name('periode.delete');
	});

	Route::get('/compare-data', function(){
		$jabatanBkpp = \App\Models\MJabatanBkpp::all();

		foreach($jabatanBkpp as $itemJabatanBkpp){
			$jabatan = \App\Models\MJabatan::whereRaw('LOWER(nama) = ?', strtolower($itemJabatanBkpp->nama_jabatan))->first();
			if($jabatan){
				echo $jabatan->nama;
				$jabatan->kode_jabatan = $itemJabatanBkpp->kode_jabatan;
				$jabatan->update();
			}else{
				$newJabatan = new \App\Models\MJabatan();
				$newJabatan->nama = $itemJabatanBkpp->nama_jabatan;
				$newJabatan->kode_jabatan = $itemJabatanBkpp->kode_jabatan;
				if($itemJabatanBkpp->jenis == 'JST'){
					$newJabatan->jenis_jabatan = 'struktural';
				}elseif($itemJabatanBkpp->jenis == 'JFU'){
					$newJabatan->jenis_jabatan = 'pelaksana';
				}elseif($itemJabatanBkpp->jenis == 'JFT'){
					$newJabatan->jenis_jabatan = 'fungsional';
				}

				$newJabatan->save();
			}
		}

	});

	Route::group(['prefix'=>'laporan'], function(){
		Route::group(['prefix'=>'rekap-kelas-jabatan'], function(){
			Route::get('/', 'LaporanController@browseKelasJabatan')->name('laporan.rekap-kelas-jabatan.browse');
		});
		Route::group(['prefix'=>'rekap-jabatan-struktural'], function(){
			Route::get('/', 'LaporanController@browseJabatanStruktural')->name('laporan.rekap-jabatan-struktural.browse');
		});
		Route::group(['prefix'=>'rekap-jabatan-fungsional'], function(){
			Route::get('/', 'LaporanController@browseJabatanFungsional')->name('laporan.rekap-jabatan-fungsional.browse');
		});
		Route::group(['prefix'=>'rekap-evjab-jabatan-struktural'], function(){
			Route::get('/', 'LaporanController@browseEvjabStruktural')->name('laporan.rekap-evjab-jabatan-struktural.browse');
		});
		Route::group(['prefix'=>'rekap-evjab-jabatan-fungsional'], function(){
			Route::get('/', 'LaporanController@browseEvjabFungsional')->name('laporan.rekap-evjab-jabatan-fungsional.browse');
		});
	});

	Route::group(['prefix'=>'skjupload'], function(){
		Route::get('/{jenis}', 'SkjUploadController@browse')->name('skjupload.browse');
		Route::get('/get-form', 'SkjUploadController@getForm')->name('skjupload.getForm');
		Route::post('/save', 'SkjUploadController@save')->name('skjupload.save');
		Route::post('/edit', 'SkjUploadController@edit')->name('skjupload.edit');
		// Route::post('edit', 'SkjUploadController@edit')->name('skjupload.edit');
		Route::post('/delete', 'SkjUploadController@delete')->name('skjupload.delete');
		
	});

	// fitur2 baru

	Route::group(['prefix'=>'petajabatan'], function(){
		Route::get('/', 'PetaJabatanController@browse')->name('petajabatan.browse');
	});
	Route::group(['prefix'=>'petajabatanupt'], function(){
		Route::get('/', 'PetaJabatanUptController@browse')->name('petajabatanupt.browse');
	});

	Route::group(['prefix'=>'masterevjab'], function(){
		Route::get('/struktural', 'EvajabController@browseFaktorStruktural')->name('faktorstruktural.browse');
		Route::get('/fungsional', 'EvajabController@browseFaktorFungsional')->name('faktorfungsional.browse');
		Route::get('/get-form', 'EvajabController@getFormFaktor')->name('faktor.get-form');
		Route::post('/save', 'EvajabController@saveFaktor')->name('faktor.save');
		Route::post('/edit ', 'EvajabController@editFaktor')->name('faktor.edit');
		Route::post('/delete', 'EvajabController@deleteFaktor')->name('faktor.delete');
	});

	Route::group(['prefix'=>'konjabpelaksana'], function(){
		Route::get('/get-form', 'KonjabPelaksanaController@getForm')->name('konjab-pelaksana.get-form');
		Route::post('/simpan', 'KonjabPelaksanaController@save')->name('konjab-pelaksana.save');
		Route::post('/delete', 'KonjabPelaksanaController@delete')->name('konjab-pelaksana.delete');
		Route::get('/', 'KonjabPelaksanaController@browse')->name('konjab-pelaksana.browse');
	});
	Route::get('/cek-relasi', 'AbkController@cekRelasi')->name('anjab.cek-relasi');
});
