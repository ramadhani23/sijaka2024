<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AbkController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('satker/detail/{kodeOpd}', 'SatkerController@getDetailSatker');
Route::get('satker/detail', 'SatkerController@getDetailSatker2');
Route::get('satker', 'SatkerController@getSatker');
Route::get('abk', [AbkController::class, 'getAbk']);
// Route::get('satker', 'Api\AnjabController@getAnjab');
