<div class="row">
	<div class="col-12">
		<input type="hidden" value="update-jabatan" name="aksi">
		<input type="hidden" value="{{ $jabatan->id }}" name="id">

		<div class="form-group">
			<label>Nama</label>
			<input value="{{ $jabatan->jabatan }}" class="form-control" type="text" name="nama" required="">
		</div>

		<div class="form-group">
			<label>Diklat Penjenjangan</label>
			<input value="{{ $jabatan->diklat_penjenjangan }}" class="form-control" type="text" name="diklat_penjenjangan" required="">
		</div>

		<div class="form-group">
			<label>Ikhtisar Jabatan</label>
			<textarea class="form-control" name="tugas_jabatan">{{ $jabatan->tugas_jabatan }}</textarea>
		</div>

		<div class="form-group">
			<label>Kualifikasi Pendidikan</label>
			<textarea class="form-control" name="kualifikasi_pendidikan">{{ $jabatan->kualifikasi_pendidikan }}</textarea>
		</div>
		
		<div class="form-group">
			<label>Dasar Hukum</label>
			<select class="form-control" name="dasar_hukum">
				<option selected="" disabled="">-- PILIH Dasar Hukum --</option>
				<option {{ $jabatan->dasar_hukum == 'permenpan-41-2018'?'selected':'' }} value="permenpan-41-2018">Permenpan No 41 Tahun 2018</option>
				<option {{ $jabatan->dasar_hukum == 'sk-bupati-315-2020'?'selected':'' }} value="sk-bupati-315-2020">SK Bupati No 315 Tahun 2020</option>
			</select>
		</div>

	</div>
	
</div>