<div class="row">
	<div class="col-12">
		<input type="hidden" value="add-jabatan" name="aksi">

		<div class="form-group">
			<label>Nama</label>
			<input class="form-control" type="text" name="nama" required="">
		</div>

		<div class="form-group">
			<label>Diklat Penjenjangan</label>
			<input class="form-control" type="text" name="diklat_penjenjangan" required="">
		</div>

		<div class="form-group">
			<label>Ikhtisar Jabatan</label>
			<textarea class="form-control" name="tugas_jabatan"></textarea>
		</div>

		<div class="form-group">
			<label>Kualifikasi Pendidikan</label>
			<textarea class="form-control" name="kualifikasi_pendidikan"></textarea>
		</div>
		
		<div class="form-group">
			<label>Dasar Hukum</label>
			<select class="form-control" name="dasar_hukum">
				<option selected="" disabled="">-- PILIH Dasar Hukum --</option>
				<option value="permenpan-41-2018">Permenpan No 41 Tahun 2018</option>
				<option value="sk-bupati-315-2020">SK Bupati No 315 Tahun 2020</option>
			</select>
		</div>

	</div>
	
</div>