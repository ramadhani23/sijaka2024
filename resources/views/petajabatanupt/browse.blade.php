@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
  caption {
    font-size: 1.5em;
    margin: 10px;
  }
</style>

<!--begin::Container-->
<div class="container">
    <div class="mb-3">
        <a href="{{ url()->previous() }}" class="btn btn-primary">
            ← Kembali
        </a>
    </div>
    {{-- <div class="card mb-3">
        @if(\MojokertokabUser::getUser()->role == 'ADMIN' || 
                    (\MojokertokabUser::getUser()->role == 'PD' && \MojokertokabUser::getUser()->kode_opd === '28'))
        <div class="card-header flex-wrap py-2">
            <form method="GET">
                <div class="form-group row align-items-center">
                    <label class="col-1 col-form-label">Filter</label>
                    <div class="col-3">
                        <select required class="form-control select2" name="kode_opd">
                            <option selected disabled>-- PILIH Unit Kerja --</option>
                            @foreach($unitKerja as $itemUnitkerja)
                                <option 
                                    @if(@$unit_kerja == $itemUnitkerja->kodeunit) selected @endif 
                                    value="{{ $itemUnitkerja->kodeunit }}" 
                                    {{ old('unit_kerja') == $itemUnitkerja->kodeunit ? 'selected' : '' }}
                                >
                                    {{ $itemUnitkerja->unitkerja }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-1">
                        <button class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </form>
        </div>
        @endif
    </div> --}}

    <!--begin::Dashboard-->
    <div class="row flex-center">
        <div class="card card-custom" style="justify-content: center; display: flex">
            <h1 style="text-align: center">Peta Jabatan</h1>
            <div class="card-toolbar mr-3" style="text-align: right">
                <button class="bg-blue-500 text-black font-semibold py-2 px-4 rounded hover:bg-blue-700 transition ease-in-out duration-300 print-button" onclick="window.print()">Print Page</button>
            </div>

            {{-- <!-- Tree Hierarchy -->
            <div class="tf-tree mt-3" style="justify-content: flex-start; display: flex">
                <ul>
                    <li>
                        <span class="tf-nc">
                            {{ optional($kepala_puskesmas)['jabatan'] }}
                        </span>
                        <ul>
                            @foreach ($jabatan as $item)
                                <li>
                                    <span class="tf-nc">{{ $item->nama_jabatan }}</span>
                                    <div>{{ $item->jenis_jabatan == 2 ? 'Fungsional' : 'Pelaksana' }}</div>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div> --}}
            <!-- Tree Hierarchy -->
            <div class="tf-tree mt-3" style="justify-content: flex-start; display: flex">
                <ul>
                    @if ($unitKerja == '2835')
                        <li>
                            <span class="tf-nc">Kepala Dinas Kesehatan</span>
                            <ul>
                                <li>
                                    <span class="tf-nc">{{ optional($kepala_puskesmas)['jabatan'] }}</span>
                                    <ul>
                                        @foreach ($jabatan as $jenis => $items)
                                            @foreach ($items as $item)
                                                <li>
                                                    <span class="tf-nc">
                                                        {{ ucwords(strtolower($item->nama_jabatan)) }}
                                                        @if(!empty($item->jenjang))
                                                            - {{ ucwords(str_replace('_', ' ', strtolower($item->jenjang))) }}
                                                        @endif
                                                    </span>
                                                    <div class="jabatan-jenis">
                                                        {{ $jenis == 2 ? 'Fungsional' : 'Pelaksana' }}
                                                    </div>
                                                </li>
                                            @endforeach
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li>
                            <span class="tf-nc">{{ optional($kepala_puskesmas)['jabatan'] }}</span>
                            <ul>
                                @foreach ($jabatan as $jenis => $items)
                                    @foreach ($items as $item)
                                        <li>
                                            <span class="tf-nc">
                                                {{ ucwords(strtolower($item->nama_jabatan)) }}
                                                @if(!empty($item->jenjang))
                                                    - {{ ucwords(str_replace('_', ' ', strtolower($item->jenjang))) }}
                                                @endif
                                            </span>
                                            <div class="jabatan-jenis">
                                                {{ $jenis == 2 ? 'Fungsional' : 'Pelaksana' }}
                                            </div>
                                        </li>
                                    @endforeach
                                @endforeach
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
            
            


            <!-- Displaying Jabatan Fungsional and Pelaksana -->
            {{-- <div class="row">
                    <div class="col-6">
                        <h3 style="text-align: center">Jabatan Fungsional</h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col" style="text-align: center">No</th>
                                    <th scope="col" style="text-align: center">Nama Jabatan</th>
                                    <th scope="col" style="text-align: center">Jenis Jabatan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($jabatan[2] ?? [] as $item)   
                                <tr>
                                    <th scope="row" style="text-align: center">{{ $loop->iteration }}</th>
                                    <td style="text-align: center">{{ $item->nama_jabatan }}</td>
                                    <td style="text-align: center">Fungsional</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                
                    <div class="col-6">
                        <h3 style="text-align: center">Jabatan Pelaksana</h3>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col" style="text-align: center">No</th>
                                    <th scope="col" style="text-align: center">Nama Jabatan</th>
                                    <th scope="col" style="text-align: center">Jenis Jabatan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($jabatan[3] ?? [] as $item)   
                                <tr>
                                    <th scope="row" style="text-align: center">{{ $loop->iteration }}</th>
                                    <td style="text-align: center">{{ $item->nama_jabatan }}</td>
                                    <td style="text-align: center">Pelaksana</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                
            </div> --}}
        </div>
    </div>
    <!--end::Card-->
</div>
<!--end::Container-->
@endsection

@section('js')
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
@endsection

@section('page-script')
<script type="text/javascript">
    $('.select2').select2();
</script>
@endsection

<style>
    .form-group.row {
        display: flex;
        align-items: center;
    }

    .form-group.row .col-form-label,
    .form-group.row select,
    .form-group.row button {
        margin: 0;
    }

    .form-group.row select {
        padding: 5px;
    }

    .form-group.row button {
        padding: 5px 10px;
        background-color: #007bff;
        color: white;
        border: none;
        border-radius: 3px;
    }

    .form-group {
        margin-bottom: 1.7rem;
        margin-top: 1.7rem;
    }

    /* Garis penghubung */


    .tf-tree li:only-child::before, .tf-tree li:only-child::after {
        display: none;
    }

    /* Kotak jabatan */
    /* .tf-nc {
        padding: 10px 15px;
        border: 1px solid #000;
        border-radius: 8px;
        display: inline-block;
        background-color: #f0f0f0;
        font-weight: bold;
    } */

    /* Jenis jabatan tanpa garis penghubung */
    .jabatan-jenis {
        margin-top: 5px;
        font-size: 14px;
        font-style: italic;
        color: #555;
    }

    /* Hapus garis vertikal ke bawah dari kotak jabatan ke jenis jabatan */
    .tf-tree li > .tf-nc + .jabatan-jenis::before {
        display: none;
    }
    
</style>
