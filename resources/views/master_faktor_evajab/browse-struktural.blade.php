@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      <a href="javascript:;" class="btn btn-primary btn-sm font-weight-bolder add-faktorstruktural">
                       <i class="far fa-plus-square"></i>Tambah Data</a>
                      <!--end::Button-->
                    </div>
                  </div>
                  <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table" id="anjab_tabel" style="margin-top: 13px !important">
                      <thead class="thead-light">
                        <tr>
                          <th>No</th>
                          <th>Nama Faktor</th>
                          <th>Level</th>
                          <th>Nilai</th>
                          <th>Keterangan</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @php
                            $previousValue = null;
                        @endphp
                       @foreach($faktor as $itemfaktorstruktural)
                       <tr>
                         <td>{{ $loop->iteration }}</td>
                         @if($previousValue != $itemfaktorstruktural->dataFaktor->nama)
                            <td>{{ $itemfaktorstruktural->dataFaktor->nama }}</td>
                            @php
                                $previousValue = $itemfaktorstruktural->dataFaktor->nama;
                            @endphp
                        @else
                            <td></td>
                        @endif
                         <td>{{ $itemfaktorstruktural->nama }}</td>
                         <td>{{ $itemfaktorstruktural->nilai }}</td>
                         <td style="width: 50%">{!! $itemfaktorstruktural->uraian  !!}</td>
                         <td>
                           <div class="btn-group dropleft" style="justify-content: flex-end;">
                              <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Menu
                              </button>
                              <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                <!--begin::Naviigation-->
                                <ul class="navi">
                                 
                                  <li class="navi-item">
                                    <a data-id="{{ $itemfaktorstruktural->id }}"  href="javascript:;" class="navi-link edit-faktorstruktural">
                                      <span class="navi-icon">
                                        <i class="navi-icon flaticon-edit-1"></i>
                                      </span>
                                      <span class="navi-text">Edit</span>
                                    </a>
                                  </li>

                                  <li class="navi-item">
                                    <a data-id="{{ $itemfaktorstruktural->id }}"  href="javascript:;" class="navi-link delete-faktorstruktural">
                                      <span class="navi-icon">
                                        <i class="navi-icon flaticon-delete-1"></i>
                                      </span>
                                      <span class="navi-text">Hapus</span>
                                    </a>
                                  </li>
                                </ul>
                                <!--end::Naviigation-->
                              </div>
                          </div>
                         </td>
                       </tr>
                       @endforeach
                      </tbody>
                      
                    </table>
                    <!--end: Datatable-->
                     {{-- {{ $faktor->links() }} --}}
                  </div>
                </div>
                <!--end::Card-->

                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

            <div id="modal-faktorstruktural" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5> 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('faktor.save') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                      
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          <form id="form-delete-faktorstruktural" method="POST" action="{{ route('faktor.delete') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/38.1.1/classic/ckeditor.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">

            ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
  initEdit();
  initHapus();

  $('.add-faktorstruktural').on('click', function(){
    $.ajax({
      url:'{{ route("faktor.get-form") }}',
      method:'GET',
      data:{aksi:'create-faktorstruktural'},
      success:function(result){
        $('#modal-faktorstruktural .modal-title').text("Tambah Deskripsi Faktor");
        $('#modal-faktorstruktural .modal-body').html(result);
        $('#modal-faktorstruktural').modal('show');
        $('.select2').select2();
      }
    });
  });

  function initEdit(){
    $('.edit-faktorstruktural').on('click', function(){
      var id = $(this).data('id');
      $.ajax({
        url:'{{ route("faktor.get-form") }}',
        method:'GET',
        data:{aksi:'edit-faktorstruktural',id:id },
        success:function(result){
          $('#modal-faktorstruktural .modal-title').text("Edit Faktor");
          $('#modal-faktorstruktural .modal-body').html(result);
          $('#modal-faktorstruktural').modal('show');
          $('.select2').select2();
        }
      });
    });
  }

  function initHapus(){
    $('.delete-faktorstruktural').on('click', function(){
      var id = $(this).data('id');
          swal.fire({
              title: 'Apakah anda yakin?',
              text: "Data yang sudah dihapus, tidak dapat dikembalikan",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Hapus Data',
              cancelButtonText: 'Batal',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-warning',
              buttonsStyling: false
          }).then(function (status) {
            if(status.value){
              $('form[id=form-delete-faktorstruktural] input[name=id]').val(id);
                  $('form[id=form-delete-faktorstruktural').submit();
            }
          });
    });
  }

    
        


  

</script>
@endsection