<div class="row">
	<div class="col-12">
		<input type="hidden" value="edit-faktorfungsional" name="aksi">
		<input type="hidden" value="{{ $faktor->id }}" name="id">

		<div class="form-group">
			<label>Nilai</label>
			<input value="{{ $faktor->nilai }}" class="form-control" type="text" name="nilai" required="">
		</div>

		<div class="form-group">
			<label>Uraian</label>
			<textarea class="form-control" id="editort" name="uraian">{{ $faktor->uraian }}</textarea>
		</div>

	</div>
	
</div>

<script src="https://cdn.ckeditor.com/ckeditor5/38.1.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
</script>