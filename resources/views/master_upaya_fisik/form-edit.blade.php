<div class="row">
	<div class="col-12">
		<input type="hidden" value="update-upayafisik" name="aksi">
		<input type="hidden" value="{{ $upayafisik->id }}" name="id">

		<div class="form-group">
			<label>Nama</label>
			<input value="{{ $upayafisik->nama }}" class="form-control" type="text" name="nama" required="">
		</div>

		<div class="form-group">
			<label>Keterangan</label>
			<textarea class="form-control" name="keterangan">{{ $upayafisik->keterangan }}</textarea>
		</div>
	</div>
	
</div>