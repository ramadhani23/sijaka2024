<input type="hidden" value="create" name="aksi">
<input type="hidden" value="{{ $id }}" name="id">

<div class="form-group">
	<label>Tugas Jabatan</label>
	<select style="width: 100%" onchange="selectionChanged1()" id="selectHasil" class="form-control select2" name="tugas_jabatan" required="">
		<option value="" selected disabled>-- PILIH TUGAS JABATAN --</option>
		@foreach($tugasJabatan as $key => $itemTugasJabatan)
			<option value="{{ $itemTugasJabatan->id.'-'.$itemTugasJabatan->key }}">{{ $itemTugasJabatan->uraian_jabatan.'-'.$itemTugasJabatan->key }}</option>
		@endforeach
	</select>
</div>
{{-- <div class="form-group">
    <label>Tugas Jabatan</label>
    <select style="width: 100%" onchange="selectionChanged1()" id="selectHasil" class="form-control select2" name="tugas_jabatan" required="">
        <option value="" selected disabled>-- PILIH TUGAS JABATAN --</option>
        @foreach($tugasJabatan as $itemTugasJabatan)
            <option value="{{ $itemTugasJabatan->id.'-'.$itemTugasJabatan->new_index }}">
                {{ $itemTugasJabatan->uraian_jabatan.'-'.$itemTugasJabatan->new_index }}
            </option>
        @endforeach
    </select>
</div> --}}

{{-- <div class="form-group">
	<label>Uraian Hasil</label>
	<textarea required="" name="uraian_hasil" class="form-control" rows="1"></textarea> 
</div> --}}
{{-- <div class="form-group">
	<label>Hasil Kerja </label>
	<select onchange="selectionChanged()" id="mySelect" style="width: 100%" class="form-control select2" name="hasil_kerja" required="">
		<option selected disabled>-- PILIH HASIL KERJA --</option>
		@foreach($hasilKerja as $itemhasil)
			<option value="{{ $itemhasil->hasil_kerja }}">{{ $itemhasil->hasil_kerja }}</option>
		@endforeach
	</select>
</div> --}}
<div class="form-group">
	<label>Hasil Kerja</label>
	<input type="text" required class="form-control" id="hasil_kerja" readonly name="hasil_kerja" >
</div>	
<div class="form-group">
	<label>Satuan Hasil</label>
	<input type="text" required class="form-control" readonly name="satuan_hasil" >
</div>	
<div class="form-group">
	<label>Jumlah Hasil </label>
	<input type="text" required class="form-control" name="jumlah_hasil" >
</div>
<div class="form-group">
	<label>Waktu Penyelesaian (Jam) </label>
	<input type="text" required class="form-control" name="waktu_penyelesaian" >
</div>
<div class="form-group">
	<label>Waktu Efektif </label>
	<input value="1250" readonly="" type="text" required class="form-control" name="waktu_efektif" >
</div>
{{-- <div class="form-group">
	<label>Kebutuhan Pegawai(MENPAN RB)</label>
	<input type="text" required class="form-control" name="waktu_penyelesaian" >
</div>
<div class="form-group">
	<label>Kebutuhan Pegawai(KEMENDAGRI)</label>
	<input type="text" required class="form-control" name="waktu_penyelesaian" >
</div> --}}

<script>
	function selectionChanged() {
		var selectElement = document.getElementById('mySelect');
		var selectedValue = selectElement.value;
		alert(selectedValue);
		die();

		$.ajax({
			url: "{{ route('abk.get-hasil-kerja') }}",
			type: 'GET',
			data: {
				hasilkerja:selectedValue
			},
			success: function(data) {
				document.getElementsByName('satuan_hasil')[0].value = data.satuan_hasil;
			},
			error: function(error) {
				console.error('Error fetching details:', error);
			}
		});
	}
	
	function selectionChanged1() {
		var selectElement = document.getElementById('selectHasil');
		var selectedValue = selectElement.value;
		var key = document.getElementById('id').value;

		console.log("Selected Value:", selectedValue); // Cek nilai yang dikirim dari select
		console.log("Key:", key); // Cek nilai key yang diambil dari input id
		// console.log('oke');
		// alert(key);
		// die();

		$.ajax({
			url: "{{ route('abk.get-hasil-kerja') }}",
			type: 'GET',
			data: {
				key:selectedValue,
				hasilkerja:key
			},
			success: function(data) {
				console.log("Response dari server:", data); // Tambahkan ini untuk cek data di console
				document.getElementsByName('hasil_kerja')[0].value = data.hasil_kerja;
				document.getElementsByName('satuan_hasil')[0].value = data.satuan_hasil;
			},
			error: function(error) {
				console.error('Error fetching details:', error);
				// console.log('oke');
			}
		});
	}
</script>