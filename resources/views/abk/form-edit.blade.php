<input type="hidden" value="update" name="aksi">
<input type="hidden" value="{{ $abk->id }}" name="id">
<input type="hidden" value="{{ $abk->trx_anjab_id }}" name="id_anjab">

{{-- <div class="form-group">
	<label>Tugas Jabatan</label>
	<select style="width: 100%; background: #EEEEEE " onchange="selectionChanged1()" id="selectHasil" class="form-control select2"	 name="tugas_jabatan" required="">
		<option value="" selected disabled>-- PILIH TUGAS JABATAN --</option>
		@foreach($tugasJabatan as $key => $itemTugasJabatan)
		<option {{ $abk->trx_anjab_uraian_jabatan_id == $itemTugasJabatan->id?'selected':''}} value="{{ $itemTugasJabatan->id }}">{{ $itemTugasJabatan->uraian_jabatan }}</option>
		@endforeach
	</select>
</div> --}}

{{-- <div class="form-group">
	<label>Uraian Hasil</label>
	<textarea required="" name="uraian_hasil" class="form-control" rows="10">{{ $abk->uraian_hasil }}</textarea> 
</div> --}}
<div class="form-group">
	<label>Tugas Jabatan </label>
	<input type="text" style="background: #EEEEEE" required readonly class="form-control" value="{{ $tugasJabatan[0]['uraian_jabatan'] }}">
</div>
{{-- <div class="form-group">
	<label>Hasil Kerja </label>
	<input type="text" style="background: #EEEEEE" required readonly class="form-control" name="hasil_kerja" value="{{ $abk->hasil_kerja }}">
</div>
<div class="form-group">
	<label>Satuan Hasil </label>
	<input type="text" required readonly class="form-control" name="satuan_hasil" value="{{ $abk->satuan_hasil }}">
</div> --}}
<div class="form-group">
	<label>Jumlah Hasil </label>
	<input type="text" required class="form-control" name="jumlah_hasil" value="{{ $abk->jumlah_hasil }}">
</div>
<div class="form-group">
	<label>Waktu Penyelesaian (Jam) </label>
	<input type="text" required class="form-control" name="waktu_penyelesaian" value="{{ $abk->waktu_penyelesaian }}">
</div>
<div class="form-group">
	<label>Waktu Efektif </label>
	<input type="text" readonly="" required class="form-control" name="waktu_efektif" value="{{ $abk->waktu_efektif }}">
</div>

<script>
function selectionChanged1() {
	var selectElement = document.getElementById('selectHasil');
	var selectedValue = selectElement.value;
	var key = document.getElementById('id').value;
	// console.log('oke');
	// alert(key);
	// die();

	$.ajax({
		url: "{{ route('abk.get-hasil-kerja') }}",
		type: 'GET',
		data: {
			key:selectedValue,
			hasilkerja:key
		},
		success: function(data) {
			document.getElementsByName('hasil_kerja')[0].value = data.hasil_kerja;
			document.getElementsByName('satuan_hasil')[0].value = data.satuan_hasil;
		},
		error: function(error) {
			console.error('Error fetching details:', error);
			// console.log('oke');
		}
	});
}
</script>


