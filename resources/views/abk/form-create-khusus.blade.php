<div class="col-12">
  <input type="hidden" value="create-khusus" name="aksi">
  {{-- <input type="hidden" value="{{ $itemAnjab->id }}" name="id"> --}}
  <div class="form-group">
    <label>Jabatan</label>
    <select style="width: 100%" class="form-control select2" id="jabatan" name="jabatan">
      <option value="" selected disabled>-- Pilih Jabatan --</option>
     
      @foreach($anjab as $itemAnjab)
      <option value="{{ $itemAnjab->id }}">{{ $itemAnjab->jabatan->nama }} - {{ $itemAnjab->dataOpd->unitkerja }}</option>
      @endforeach
    </select>
  </div>

  <div class="form-group">
    <label>Dokumen ABK</label>
    <input type="file" required="" class="form-control" name="dokumen">
  </div>

   <div class="form-group">
    <label>Jumlah Kebutuhan</label>
    <input type="number" required="" class="form-control" name="kebutuhan">
  </div>
</div>