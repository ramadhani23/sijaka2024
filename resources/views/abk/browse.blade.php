@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>

<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    @if(\MojokertokabApp::allowChangingData())
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      {{-- <a href="javascript:;" class="btn btn-sm btn-primary font-weight-bolder add-abk">
                       <i class="far fa-plus-square"></i>Tambah Analisa Beban Kerja</a> --}}
                          {{-- @if ( $jenis == 'fungsional')
                          <a href="javascript:;" class="ml-1 btn btn-sm btn-success font-weight-bolder unggah-abk">
                            <i class="fas fa-upload"></i>Unggah Analisa Beban Kerja
                          </a>
                          @else
                              
                          @endif --}}
                        
                      <!--end::Button-->
                    </div>
                    @endif
                  </div>
                  <div class="card-body">
                    @if(\MojokertokabUser::getUser()->role == 'ADMIN' || \MojokertokabUser::getUser()->role == 'KEMENDAGRI' || (\MojokertokabUser::getUser()->role == 'PD' && \MojokertokabUser::getUser()->kode_opd === '28')
)
                    <form method="GET">
                      <div class="form-group row">
                        <label class="col-1 col-form-label">Filter</label>
                        {{-- <div class="col-4">
                          <input class="form-control" type="text" name="jabatan" placeholder="Nama Jabatan ...">
                        </div> --}}

                        <div class="col-3">
                          <select required class="form-control select2" name="unit_kerja">
                            <option selected="" disabled="">-- PILIH Unit Kerja --</option>
                            @foreach($unitKerja as $itemUnitkerja)
                            {{-- <option  value="{{ $itemUnitkerja->kodeunit }}">{{ $itemUnitkerja->unitkerja }}</option> --}}
                            <option @if(@$unit_kerja == $itemUnitkerja['kodeunit'] ) selected @endif value="{{ $itemUnitkerja['kodeunit'] }}" {{ old('unit_kerja') == $itemUnitkerja['kodeunit'] ? "selected" :""}}>{{  $itemUnitkerja['unitkerja'] }}</option>
                            @endforeach
                          </select>
                        </div>

                        <div class="col-3">
                          <select class="form-control select2" name="jenis_jabatan" disabled>
                            <option selected="" disabled="">-- PILIH Jenis Jabatan --</option>
                            <option value="1" @if ($jenis == 'struktural') selected @endif>STRUKTURAL</option>
                            <option value="2" @if ($jenis == 'fungsional') selected @endif>FUNGSIONAL</option>
                            <option value="3" @if ($jenis == 'pelaksana') selected @endif>PELAKSANA</option>
                          </select>
                        </div>

                        <div class="col-1">
                          <button class="btn btn-primary">Filter</button>
                        </div>
                      </div>
                    </form>
                    @endif

                    <!--begin: Datatable-->
                    @if((request()->unit_kerja && (\MojokertokabUser::getUser()->role == 'ADMIN' || \MojokertokabUser::getUser()->role == 'KEMENDAGRI')) || \MojokertokabUser::getUser()->role == 'PD' && \MojokertokabUser::getUser()->kode_opd !== '28' || \MojokertokabUser::getUser()->role == 'PD' && \MojokertokabUser::getUser()->kode_opd === '28' && request()->unit_kerja)
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-top: 13px !important">
                          <thead class="thead-light">
                            <tr>
                              <th>No</th>
                              <th>Jabatan</th>
                              <th>Unit Kerja</th>
                              <th>Kebutuhan Pegawai</th>
                              <th></th>
                              @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                              <th>Verifikasi</th>
                              @endif
                            </tr>
                          </thead>
                          <tbody>
                            @if($anjab->count() == 0)
                            <tr>
                              <td class="text-center" colspan="6">Tidak ada data</td>
                            </tr>
                            @endif
                            @php
                              $totalKebutuhan = 0;
                            @endphp
                            @foreach($anjab as $itemAnjab)
                            {{-- {{ dd($itemAnjab) }} --}}
                            <tr class="@if(@$itemAnjab->dataAbk[0]->status == 'verifikasi') table-success @else @endif">
                              <td class="text-center">{{ $loop->iteration }}</td>
                              <td>{{ \Illuminate\Support\Str::title(str_replace(['_', '-'], ' ', @$itemAnjab->jabatan->jabatan)) }}  {{ ucwords(str_replace('_', ' ', @$itemAnjab->m_jabatan_fungsional_jenjang)) }}
                                <div> 
                                  @if($itemAnjab->dataAbkKhusus && $itemAnjab->dataAbkKhusus->status == null) 
                                  <a target="_blank" href="{{ asset('abk-khusus/'.$itemAnjab->dataAbkKhusus->dokumen_pendukung) }}" class="btn btn-primary btn-sm">Lihat Dokumen ABK</a>
                                  @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                  <a data-id="{{ $itemAnjab->dataAbkKhusus->id }}" href="javascript:;" class="btn btn-success btn-sm verifikasi-abk-khusus">Verifikasi</a>
                                  @endif
                                  @endif
                                </div>
                                <div>
                                  @if($itemAnjab->dataAbkKhusus && $itemAnjab->dataAbkKhusus->status == 'verifikasi') 
                                  <a target="_blank" href="{{ asset('abk-khusus/'.$itemAnjab->dataAbkKhusus->dokumen_pendukung) }}" class="btn btn-primary btn-sm">Lihat Dokumen ABK</a>
                                  @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                  <a data-id="{{ $itemAnjab->dataAbkKhusus->id }}" href="javascript:;" class="btn btn-danger btn-sm batal-verifikasi-abk-khusus">Batal Verifikasi</a>
                                  @endif
                                  @endif
                                </div>
                                <div>
                                  @if($itemAnjab->dataAbkKhusus && $itemAnjab->dataAbkKhusus->status == 'belum_verifikasi') 
                                  <a target="_blank" href="{{ asset('abk-khusus/'.$itemAnjab->dataAbkKhusus->dokumen_pendukung) }}" class="btn btn-primary btn-sm">Lihat Dokumen ABK</a>
                                  @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                  <a data-id="{{ $itemAnjab->dataAbkKhusus->id }}" href="javascript:;" class="btn btn-success btn-sm verifikasi-abk-khusus">Verifikasi</a>
                                  @endif
                                  @endif
                                </div>
                              </td>
                              {{-- <td>{{ @$itemAnjab->m_jabatan_fungsional_jenjang }}</td> --}}
                              <td>{{ @$itemAnjab->dataUnitKerja->unitkerja }}</td>
                              {{-- @php
                              $totalWaktuPenyelesaian = $itemAnjab->dataAbk->waktu_penyelesaian * $itemAnjab->dataAbk->jumlah_hasil;
                              $kemendagri = $totalWaktuPenyelesaian / 1350;
                              @endphp --}}
                              {{-- <td>
                                  <a target="_blank" href=""><i class="fas fa-download"></i></a>
                              </td> --}}
                              <td class="text-center">{{ floor($itemAnjab->total_kebutuhan_pegawai) }}</td>
                              
                              {{-- <td>
                                @if ( $jenis == 'fungsional')
                                <a data-id="{{ $itemAnjab->id }}" href="javascript:;" class="ml-1 btn btn-sm btn-success font-weight-bolder edit-abk">
                                  <i class="fas fa-upload"></i>Edit Analisa Beban Kerja
                                </a>
                                @else
                                    
                                @endif
                              </td> --}}
                              <td>
                                @if(\MojokertokabApp::allowChangingData() or \MojokertokabUser::getUser()->role == 'KEMENDAGRI') 
                                  @php
@endphp
                                  @if($jenis == 'fungsional')
                                      @if($itemAnjab->dataAbkKhusus && $itemAnjab->dataAbkKhusus->status == 'belum_verifikasi')
                                          @if(\MojokertokabUser::getUser()->role != 'KEMENDAGRI')
                                              <a href="javascript:;" data-id="{{ $itemAnjab->dataAbkKhusus->id }}" class="btn btn-sm btn-icon btn-warning edit-unggah-abk"><i class="flaticon-edit"></i></a>
                                              <a data-id="{{ $itemAnjab->dataAbkKhusus->id }}" href="javascript:;" class="btn btn-danger btn-icon btn-sm delete-abk-khusus"><i class="flaticon-delete"></i></a>
                                          @endif
                                      @elseif($itemAnjab->dataAbkKhusus && $itemAnjab->dataAbkKhusus->status == null)
                                          @if(\MojokertokabUser::getUser()->role != 'KEMENDAGRI')
                                              <a href="javascript:;" data-id="{{ $itemAnjab->dataAbkKhusus->id }}" class="btn btn-sm btn-icon btn-warning edit-unggah-abk"><i class="flaticon-edit"></i></a>
                                              <a data-id="{{ $itemAnjab->dataAbkKhusus->id }}" href="javascript:;" class="btn btn-danger btn-icon btn-sm delete-abk-khusus"><i class="flaticon-delete"></i></a>
                                          @endif
                                      @elseif($itemAnjab->dataAbkKhusus && $itemAnjab->dataAbkKhusus->status == 'verifikasi')
                                          Tidak Bisa Edit/Hapus
                                      @else
                                      @if(
                                              \MojokertokabUser::getUser()->role != 'KEMENDAGRI' && 
                                              // $itemAnjab->parent === null && 
                                              (
                                                  \MojokertokabUser::getUser()->kode_opd === $itemAnjab->unit_kerja || 
                                                  !(
                                                      \MojokertokabUser::getUser()->kode_opd === '28' && 
                                                      $itemAnjab->unit_kerja === '2835' || $itemAnjab->parent === null
                                                  )
                                              )
                                          )
                                              <a href="javascript:;" data-id="{{ $itemAnjab->id }}" class="btn btn-sm btn-icon btn-warning unggah-abk"><i class="flaticon-edit"></i></a>
                                      @endif
                                  @endif
                                @elseif($jenis == 'fungsional' and \MojokertokabUser::getUser()->role == 'KEMENDAGRI')
                                <p>TIDAK BISA UBAH DATA</p>
                                @else
                                  
                                      @if(
                                        (substr($itemAnjab->dataUnitKerja->kodeunit, 0, 2) == '29' || 
                                        substr($itemAnjab->dataUnitKerja->kodeunit, 0, 2) == '30' || 
                                        substr($itemAnjab->dataUnitKerja->kodeunit, 0, 2) == '28') &&
                                        !(substr($itemAnjab->dataUnitKerja->kodeunit, 0, 2) == '28' && 
                                          strlen($itemAnjab->dataUnitKerja->kodeunit) == 4 && 
                                          $itemAnjab->dataUnitKerja->kodeunit != '2801')
                                    )
                                        @if(\MojokertokabUser::getUser()->role != 'KEMENDAGRI' && $itemAnjab->jenis_jabatan == 'fungsional')
                                            <a href="javascript:;" data-id="{{ $itemAnjab->id }}" class="btn btn-sm btn-icon btn-info unggah-abk"><i class="flaticon-upload"></i></a>
                                        @endif
                                    @endif
                                    <a href="{{ route('abk.detail', $itemAnjab->id) }}" class="btn btn-sm btn-icon btn-warning">
                                      @if(\MojokertokabUser::getUser()->role == 'KEMENDAGRI')
                                          <i class="flaticon-eye"></i>
                                      @else
                                          <i class="flaticon-edit"></i>
                                      @endif
                                  </a>

                                @endif

                                  

                                @endif
                              </td>
                              @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                              <td>
                                @if (@$itemAnjab->dataAbk[0]->status == null)
                                  <form action="{{ route('abk.verifikasi-abk-struktural-pelaksana') }}" method="POST">
                                      @csrf
                                      <input type="hidden" name="id" value="{{ $itemAnjab->id}}">
                                      <input type="hidden" name="aksi" value="verifikasi">
                                      <button type="submit" class="btn btn-success">
                                          Verifikasi
                                      </button>
                                  </form>
                                @else
                                <form action="{{ route('abk.verifikasi-abk-struktural-pelaksana') }}" method="POST">
                                  @csrf
                                  <input type="hidden" name="id" value="{{ $itemAnjab->id}}">
                                  <input type="hidden" name="aksi" value="batal">
                                  <button type="submit" class="btn btn-danger">
                                      Batal Verifikasi
                                  </button>
                              </form>
                                @endif
                              </td>
                              @endif
                            </tr>
                            @php $totalKebutuhan += $itemAnjab->total_kebutuhan_pegawai  @endphp
                            @endforeach
                            <tr>
                              <td class="text-right" colspan="3">Total Kebutuhan Pegawai</td>
                              <td class="text-center">{{ floor($totalKebutuhan) }}</td>
                              
                            </tr>
                          </tbody>
                          <td>

                          </td>

                        </table>

                    </div>
                    <!--end: Datatable-->
                    @endif
                  </div>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

          <form id="form-delete-anjab" method="POST" action="{{ route('anjab.delete') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>

          <form id="form-delete-abk-khusus" method="POST" action="{{ route('abk.delete-abk-khusus') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>

          <form id="form-verifikasi-abk-khusus" method="POST" action="{{ route('abk.verifikasi-abk-khusus') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>

          <form id="form-batal-verifikasi-abk-khusus" method="POST" action="{{ route('abk.batal-verifikasi-abk-khusus') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>

          <div id="modal-unggah-abk" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form action="{{ route('abk.save-abk') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-12">
                        <input type="hidden" value="create-khusus" name="aksi">
                        <input type="hidden" value="" id="coba" name="id">

                        {{-- <input type="text" value="{{ @$itemAnjab->id }}" name="id"> --}}
                        {{-- <div class="form-group">
                          <label>Jabatan2</label>
                          <select required style="width: 100%" class="form-control select2" id="jabatan" name="jabatan">
                            <option value="" selected disabled>-- Pilih Jabatan --</option>
                            {{ dd($anjab) }}
                              @foreach($anjab as $itemAnjab)
                                <option value="{{ $itemAnjab->id }}">{{ @$itemAnjab->jabatan->jabatan }} - {{ @$itemAnjab->dataOpd->unitkerja }}</option>
                                <option value="{{ $itemAnjab->id }}">{{ @$itemAnjab->jabatan->jabatan }} - {{ @$itemAnjab->dataOpd->nama }} - {{ @$itemAnjab->m_jabatan_fungsional_jenjang }}</option>
                              @endforeach
                          </select>
                        </div> --}}

                        <div class="form-group">
                          <label>Dokumen ABK</label>
                          <input type="file" required="" class="form-control" name="dokumen">
                        </div>

                         <div class="form-group">
                          <label>Jumlah Kebutuhan</label>
                          <input type="number" required="" class="form-control" name="kebutuhan">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>

            <div id="modal-edit-abk" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form action="{{ route('abk.save-abk') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                  <div class="modal-body">
                    <div class="row">
                      
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>

            {{-- <div id="modal-abk" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form action="{{ route('abk.pilih-anjab') }}" method="GET">
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-12">
                        <input type="hidden" value="create" name="aksi">
                        <input type="hidden" value="{{ $itemAnjab->id }}" name="id">
                        <div class="form-group">
                          <label>Jabatan</label>
                          <select style="width: 100%" class="form-control select2" id="jabatan" name="jabatan">
                            <option value="" selected disabled>-- Pilih Jabatan --</option>
                            @foreach($anjab as $itemAnjab)
                            <option value="{{ $itemAnjab->id }}">{{ @$itemAnjab->jabatan->jabatan }} - {{ $itemAnjab->dataOpd->unitkerja }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success btn-sm">Analisa</button>
                  </div>
                  </form>
                </div>
              </div>
            </div> --}}


@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
  $('.select2').select2();

  $('.add-abk').on('click', function(){
    $('#modal-abk .modal-title').text("Tambah Analisa Beban Kerja");
    $('#modal-abk').modal('show');
    $('#modal-abk .select2').select2();
  });

  $('.unggah-abk').on('click', function(){
    var id = $(this).data('id')
    $('#coba').val(id)
    // alert(id)
    $('#modal-unggah-abk .modal-title').text("Unggah Analisa Beban Kerja");
    $('#modal-unggah-abk .select2').select2();
    $('#modal-unggah-abk').modal('show');
  });

    $('.edit-unggah-abk').on('click', function(){
      var id = $(this).data('id');
      // alert(id)
        $.ajax({
          url:'{{ route("abk.get-form-abk") }}',
          method:'GET',
          data:{aksi:'edit-abk-khusus',id:id, 'jenis':'{{ $jenis }}'},
          success:function(result){
            $('#modal-edit-abk .modal-title').text("Edit Analisa Beban Kerja");
            $('#modal-edit-abk .modal-body').html(result);
            $('#modal-edit-abk .select2').select2();
            $('#modal-edit-abk').modal('show');
          }
        });
    });

    function initEditFungsional(){
    $('.edit-abk').on('click', function(){
      var id = $(this).data('id');
      $.ajax({
        url:'{{ route("abk.get-form-abk") }}',
        method:'GET',
        data:{aksi:'edit-abk',id:id },
        success:function(result){
          $('#modal-unggah-abk .modal-title').text("Edit ABK");
          $('#modal-unggah-abk .modal-body').html(result);
          $('#modal-unggah-abk').modal('show');
          $('.select2').select2();
        }
      });
    });
  }

function initEdit(){
    $('.edit-anjabs').on('click', function(){
      var id = $(this).data('id');
      $.ajax({
        url:'{{ route("anjab.get-form") }}',
        method:'GET',
        data:{aksi:'edit-anjab',id:id },
        success:function(result){
          $('#modal-anjab .modal-title').text("Edit Analisa Beban Kerja");
          $('#modal-anjab .modal-body').html(result);
          $('#modal-anjab').modal('show');
          $('.select2').select2();
        }
      });
    });
  }

  $('.delete-abk-khusus').on('click', function(){
    var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin?',
            // text: "Anda tidak dapat membatalkan pengajuan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus Data',
            cancelButtonText: 'Batal',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-warning',
            buttonsStyling: false
        }).then(function (status) {
          if(status.value){
            $('form[id=form-delete-abk-khusus] input[name=id]').val(id);
            $('form[id=form-delete-abk-khusus').submit();
          }
        });
  });

  $('.verifikasi-abk-khusus').on('click', function(){
    var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin?',
            // text: "Anda tidak dapat membatalkan pengajuan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Verifikasi Data',
            cancelButtonText: 'Batal',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-warning',
            buttonsStyling: false
        }).then(function (status) {
          if(status.value){
            $('form[id=form-verifikasi-abk-khusus] input[name=id]').val(id);
            $('form[id=form-verifikasi-abk-khusus').submit();
          }
        });
  });

  $('.batal-verifikasi-abk-khusus').on('click', function(){
    var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin?',
            // text: "Anda tidak dapat membatalkan pengajuan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Batal Verifikasi Data',
            cancelButtonText: 'Batal',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-warning',
            buttonsStyling: false
        }).then(function (status) {
          if(status.value){
            $('form[id=form-batal-verifikasi-abk-khusus] input[name=id]').val(id);
            $('form[id=form-batal-verifikasi-abk-khusus').submit();
          }
        });
  });

  function initHapus(){
    $('.delete-anjab').on('click', function(){
      var id = $(this).data('id');
          swal.fire({
              title: 'Apakah anda yakin?',
              // text: "Anda tidak dapat membatalkan pengajuan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Hapus Data',
              cancelButtonText: 'Batal',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-warning',
              buttonsStyling: false
          }).then(function (status) {
            if(status.value){
              $('form[id=form-delete-anjab] input[name=id]').val(id);
                  $('form[id=form-delete-anjab').submit();
            }
          });
    });
  }



</script>
@endsection
