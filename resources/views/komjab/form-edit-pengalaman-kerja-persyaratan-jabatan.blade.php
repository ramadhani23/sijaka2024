<input type="hidden" value="update-pengalaman-kerja-persyaratan-jabatan" name="aksi">
<input type="hidden" value="{{ $id_komjab }}" name="id_komjab">
<input type="hidden" value="{{ $id_anjab }}" name="id_anjab">	
<input type="hidden" value="{{ $id }}" name="id">
<div class="row">
	<div class="col-12">
	
		<div class="form-group">
			<label class="col-sm-12 control-label">Pengalaman Kerja</label>
			<div class="col-sm-12">
			<textarea class="form-control" name="pengalaman_kerja" rows="5">{{ $persyaratanJabatanPengalamanKerja->pengalaman_kerja }}</textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-12 control-label">Tingkat Kepentingan Pada Jabatan</label>
			<div class="col-sm-12">
			<select  class="form-control" name="status_pengalaman_kerja" id="status_pengalaman_kerja">
               <option value="" disabled="" selected="">-- Pilih Tingkat Kepentingan Pada Jabatan--</option>
               <option value="mutlak" {{ $persyaratanJabatanPengalamanKerja->status_pengalaman_kerja == 'mutlak'?'selected':'' }}>Mutlak</option>
               <option value="penting" {{ $persyaratanJabatanPengalamanKerja->status_pengalaman_kerja == 'penting'?'selected':'' }}>Penting</option>
               <option value="perlu" {{ $persyaratanJabatanPengalamanKerja->status_pengalaman_kerja == 'perlu'?'selected':'' }}>Perlu</option>
            </select>
			</div>
		</div>
	</div>
</div>



