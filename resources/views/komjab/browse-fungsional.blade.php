@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      <a href="javascript:;" class="btn btn-primary font-weight-bolder add-komjab">
                       <i class="far fa-plus-square"></i>Tambah Data</a>
                      <!--end::Button-->
                    </div>
                  </div>
                  <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                          <!--begin: Datatable-->
                    <table class="table" id="anjab_tabel" style="margin-top: 13px !important">
                      <thead class="thead-light">
                        <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>PD</th>
                          <th>Jenis</th>
                          <th>Eselon</th>
                          <th>Kode</th>
                          
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($komjab as $itemKomjab)
                        <tr>
                          <td>{{ $loop->iteration + $komjab->firstItem()-1 }}</td>
                          <td>{{ $itemKomjab->jabatan->nama }} <span></span></td>
                          <td>
                            @if($itemKomjab->dataOpd)
                            {{ $itemKomjab->dataOpd->unitkerja }}
                            @else
                            -
                            @endif
                          </td>
                          <td>{{ $itemKomjab->text_jenis_jabatan }}</td>
                          <td>{{ $itemKomjab->text_eselon_jabatan }}</td>
                          <td>{{ $itemKomjab->kode_jabatan }}</td>
                          <td>

                            <div class="btn-group dropleft">
                              <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Menu
                              </button>
                              <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                <!--begin::Naviigation-->
                                <ul class="navi">
                                  
                                  <li class="navi-item">
                                    <a  href="{{ route('komjab.detail', $itemKomjab->id) }}" class="navi-link">
                                      <span class="navi-icon">
                                        <i class="navi-icon flaticon-eye"></i>
                                      </span>
                                      <span class="navi-text">Kompetensi Jabatan</span>
                                    </a>
                                  </li>
                                  
                                </ul>
                                <!--end::Naviigation-->
                              </div>
                          </div>

                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                      
                    </table>
                    <!--end: Datatable-->
                    {{ $komjab->links() }}
                    </div>
                    <!--end: Datatable-->
                  </div>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

             <div id="modal-komjab" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title"></h5> 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form action="{{ route('komjab.pilih-anjab') }}" method="GET">
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-12">
                        <input type="hidden" value="create" name="aksi">
                        {{-- <input type="hidden" value="{{ $itemAnjab->id }}" name="id"> --}}
                        <div class="form-group">
                          <label>Jabatan</label>
                          <select style="width: 100%" class="form-control select2" id="jabatan" name="jabatan">
                            @foreach($anjab as $itemAnjab)
                            <option value="{{ $itemAnjab->id }}">{{ $itemAnjab->jabatan->nama }} - {{ $itemAnjab->dataOpd->unitkerja }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success btn-sm">Analisis</button>
                  </div>
                  </form>
                </div>
              </div>
          </div>
          
@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
   
  $('.add-komjab').on('click', function(){
    $('#modal-komjab .modal-title').text("Tambah Kompetensi Jabatan");
    $('#modal-komjab').modal('show');
    $('.select2').select2();
  });

  

</script>
@endsection