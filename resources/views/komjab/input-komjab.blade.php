@extends('layout')

@section('content')
<!--begin::Container-->
              <div class="container">
              <form action="{{route('komjab.save')}}" enctype="multipart/form-data" method="post">
                @csrf
                <!--begin::Dashboard-->
                  <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h3 class="card-label text-center">{{ isset($page) ? $page : 'Dashboard' }}</h3>
                    </div>
                  </div>
                  <div class="card-body">
                  
                      <input type="text" hidden name="type" value="save">

                      <div class="mb-15">
                        <div class="form-group row">
                          <label class="col-lg-2 col-form-label text-right">Nama Jabatan</label>
                          <div class="col-lg-8">
                            <select required="" class="form-control" name="id_jabatan" id="jabatan">
                              <option value="" disabled="" selected="">-- PILIH JABATAN--</option>
                              @foreach($anjabData as $anj)
                                  <option value="{{ $anj->id }}">{{ $anj->jabatan->nama }}- {{ $anj->dataOpd->unitkerja }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Kelompok Jabatan :</label>
                          <div class="col-lg-8">
                            <select required="" class="form-control" name="kelompok_jabatan" id="kelompok_jabatan">
                              <option value="" disabled="" selected="">-- PILIH KELOMPOK JABATAN--</option>
                                  <option  value="Jabatan Pimpinan Tinggi Pratama">Jabatan Pimpinan Tinggi Pratama</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Urusan Pemerintah :</label>
                          <div class="col-lg-8">
                            <select required="" class="form-control" name="urusan_pemerintah" id="urusan_pemerintah">
                              <option value="" disabled="" selected="">-- PILIH URUSAN PEMERINTAH--</option>
                                  <option  value="Kesekretariatan">Kesekretariatan</option>
                                  <option value="Bidang Komunikasi dan Informatika, Bidang Persandian dan Bidang Statistik">Bidang Komunikasi dan Informatika, Bidang Persandian dan Bidang Statistik</option>
                            </select>
                          </div>
                      </div>
                  </div>
                </div>
                <!--end::Card-->
                <br>

                <div class="card card-custom">
                  <div class="card-body">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Ikhtisar Jabatan :</label>
                        <div class="col-lg-8">
                          <textarea readonly id="ikhtisar" name="ikhtisar" required="" class="form-control" rows="10">{{ $anjab->ikhtisar_jabatan != null ? $anjab->ikhtisar_jabatan :''}}</textarea>
                        </div>
                    </div>
                  </div>
                </div>
                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Manajerial</h2>
                    </div>
                  </div>
                  <div class="card-body">
                    
                    <input type="hidden" name="id_anjab" id="id_anjab" value="{{ $anjab->id }}">
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Integeritas</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">level</label>
                        <div class="col-lg-6">
                          <select  class="form-control" name="level_integeritas" id="level_integeritas">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_integeritas as $level)
                                <option data-kompetensi="Integeritas" data-urutan="1" value="{{ $level->id }}">{{ $level->level }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_integeritas">

                      </div>
                    </div>
                      
                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Kerjasama</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_kerjasama" id="level_kerjasama">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_kerjasama as $level)
                                <option data-kompetensi="Kerjasama" data-urutan="2"  value="{{ $level->id }}">{{ $level->level }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_kerjasama">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Komunikasi</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_komunikasi" id="level_komunikasi">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_komunikasi as $level)
                                <option data-kompetensi="Komunikasi" data-urutan="3"  value="{{ $level->id }}">{{ $level->level }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_komunikasi">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Orientasi Pada Hasil</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_orientasi" id="level_orientasi">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_orientasi as $level)
                                <option data-kompetensi="Orientasi Pada Hasil" data-urutan="4" value="{{ $level->id }}">{{ $level->level }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_orientasi">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pelayanan Publik</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_pelayanan" id="level_pelayanan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_pelayanan as $level)
                                <option data-kompetensi="Pelayanan Publik" data-urutan="5" value="{{ $level->id }}">{{ $level->level }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_pelayanan">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pengembangan Diri dan Orang Lain</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_pengembangan" id="level_pengembangan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_pengembangan as $level)
                                <option data-kompetensi="Pengembangan Diri dan Orang Lain" data-urutan="6" value="{{ $level->id }}">{{ $level->level }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_pengembangan">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Mengelola Perubahan</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_perubahan" id="level_perubahan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_perubahan as $level)
                                <option data-kompetensi="Mengelola Perubahan" data-urutan="7" value="{{ $level->id }}">{{ $level->level }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_perubahan">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pengambilan Keputusan</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_keputusan" id="level_keputusan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_keputusan as $level)
                                <option data-kompetensi="Pengambilan Keputusan" data-urutan="8" value="{{ $level->id }}">{{ $level->level }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_keputusan">

                      </div>
                    </div>
                    
                  </div>

                </div>

                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Sosial Kultural</h2>
                    </div>
                  </div>
                  <div class="card-body">
                    
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Perekat Bangsa</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">level</label>
                        <div class="col-lg-8">
                          <select  class="form-control" name="level_perekat" id="level_perekat">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_perekat as $level)
                                <option data-kompetensi="Perekat Bangsa" data-urutan="9" value="{{ $level->id }}">{{ $level->level }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_perekat">

                      </div>
                    </div>
                      
                    
                    
                  </div>
                  <div class="card-footer">
                    <div class="row">
                      <div class="col text-right">
                          <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                      </div>
                    </div>
                  </div>
                </div>
                <br>
               
              </div>
            </form>
              <!--end::Container-->
              <!-- <input type="text" class="form-control" name="indikator_kompetensi[]" id="indikator_kompetensi[]"> -->

            <div class="modal fade text-left" id="modal" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content" style="width: 1200px;">
                      <div class="modal-header">
                          <h4 class="modal-title"></h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <form method="POST" action="{{ route('komjab.save') }}">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                      </form>
                    </div>
                  </div>
              </div>

       
          </div>
@endsection

@section('js')
{{-- js files --}}
 <script src="{{ asset('') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script src="//cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script>
 
  var options = {
    filebrowserImageBrowseUrl: '{{ URL::to("laravel-filemanager?type=Images") }}',
    filebrowserImageUploadUrl: '{{ URL::to("laravel-filemanager/upload?type=Images&_token=") }}',
    filebrowserBrowseUrl: '{{ URL::to("/laravel-filemanager?type=Files") }}',
    filebrowserUploadUrl: '{{ URL::to("/laravel-filemanager/upload?type=Files&_token=") }}',
  };
</script>

<script>

        $('.add-data-teknis').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'add-teknis',id_anjab:id_anjab,id_komjab:id_komjab },
            success:function(result){
              $('#modal .modal-title').text("Tambah Teknis");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        }); 

        $('.edit-teknis').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          var id = $(this).data('id');
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'edit-teknis',id_anjab:id_anjab,id_komjab:id_komjab,id:id },
            success:function(result){
              $('#modal .modal-title').text("Edit Teknis");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        $('.delete-teknis').on('click', function(){
          var id = $(this).data('id');
              swal.fire({
                  title: 'Apakah anda yakin?',
                  // text: "Anda tidak dapat membatalkan pengajuan!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Hapus Data',
                  cancelButtonText: 'Batal',
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-warning',
                  buttonsStyling: false
              }).then(function (status) {
                if(status.value){
                  $('form[id=form-delete-teknis] input[name=id]').val(id);
                      $('form[id=form-delete-teknis]').submit();
                }
              });
        });

        $('.add-indikator').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          var id_teknis = $(this).data('id_teknis');
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'add-indikator',id_anjab:id_anjab,id_komjab:id_komjab,id_teknis:id_teknis },
            success:function(result){
              $('#modal .modal-title').text("Edit Teknis");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        $("#nama_jabatan").change(function() {
          var ikhtisar = $(this).find(':selected').data('ikhtisar');
          $('#ikhtisar').text(ikhtisar);
          $('#id').val(id);
        });

        $("#level_integeritas").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          console.log(varkompetensi);
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_integeritas").html(html);
            }
          });
        });
        $("#level_kerjasama").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_kerjasama").html(html);
            }
          });
        });
        $("#level_komunikasi").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_komunikasi").html(html);
            }
          });
        });
        $("#level_orientasi").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_orientasi").html(html);
            }
          });
        });
        $("#level_pelayanan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_pelayanan").html(html);
            }
          });
        });
        $("#level_pengembangan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_pengembangan").html(html);
            }
          });
        });
        $("#level_perubahan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_perubahan").html(html);
            }
          });
        });
        $("#level_keputusan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_keputusan").html(html);
            }
          });
        });
        $("#level_perekat").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_perekat").html(html);
            }
          });
        });
        // $("#level_teknis").change(function() {
        //   var dats = $(this).val();
        //   var id = $(this).find(':selected').data('id');
        //   $.ajax({
        //     type: "GET",
        //     url: '{{ route("komjab.get-data-teknis") }}',
        //     data: {
        //           level :  dats,
        //           id_komjab : id
        //     },
        //     cache: false,
        //     success: function(html) {
        //       $("#teknis").html(html);
        //     }
        //   });
        // });


</script>
@endsection