<input type="hidden" value="create-teknis-persyaratan-jabatan" name="aksi">
<input type="hidden" value="{{ $id_komjab }}" name="id_komjab">
<input type="hidden" value="{{ $id_anjab }}" name="id_anjab">	
<div class="row">
	<div class="col-12">
	
		<div class="form-group">
			<label class="col-sm-12 control-label">Teknis</label>
			<div class="col-sm-12">
			<textarea class="form-control" name="teknis" rows="5"></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-12 control-label">Tingkat Kepentingan Pada Jabatan</label>
			<div class="col-sm-12">
			<select  class="form-control" name="status_teknis" id="status_teknis">
               <option value="" disabled="" selected="">-- Pilih Tingkat Kepentingan Pada Jabatan--</option>
               <option value="mutlak">Mutlak</option>
               <option value="penting">Penting</option>
               <option value="perlu">Perlu</option>
            </select>
			</div>
		</div>
	</div>
</div>



