@extends('layout')

@section('content')
<!--begin::Container-->
              <div class="container">
              <form action="{{route('komjab.save')}}" enctype="multipart/form-data" method="post">
                      @csrf
                <!--begin::Dashboard-->
                  <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h3 class="card-label text-center">{{ isset($page) ? $page : 'Dashboard' }}</h3>
                        @if(isset($komjab))
                          <div class="text-right">
                            <a href="{{ route('komjab.print-word', $komjab->id) }}"  class="btn btn-success btn-sm font-weight-bolder add-anjab">
                            <i class="flaticon2-print"></i>Unduh Kompetensi Jabatan</a>
                          </div>
                        @endif
                    </div>
                  </div>
                  <div class="card-body">
                  
                    
                      @if(isset($komjab))
                        <input type="text" hidden name="type" value="update">
                        <input type="text" hidden name="id_komjab" id="id_komjab" value="{{$komjab->id}}">
                      @else
                        <input type="text" hidden name="type" value="save">
                      @endif

                      <div class="mb-15">
                        <div class="form-group row">
                          <label class="col-lg-2 col-form-label text-right">Nama Jabatan</label>
                          <div class="col-lg-8">
                            <p class="form-control-plaintext">{{ $anjab->nama }}</p>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Kelompok Jabatan :</label>
                          <div class="col-lg-8">
                            <select required="" class="form-control" name="kelompok_jabatan" id="kelompok_jabatan">
                              <option value="" disabled="" selected="">-- PILIH KELOMPOK JABATAN--</option>
                                  <option {{ isset($komjab) && $komjab->kelompok_jabatan == 'Jabatan Pimpinan Tinggi Pratama' ? 'selected':'' }} value="Jabatan Pimpinan Tinggi Pratama">Jabatan Pimpinan Tinggi Pratama</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Urusan Pemerintah :</label>
                          <div class="col-lg-8">
                            <select required="" class="form-control" name="urusan_pemerintah" id="urusan_pemerintah">
                              <option value="" disabled="" selected="">-- PILIH URUSAN PEMERINTAH--</option>
                                  <option {{ isset($komjab) && $komjab->urusan_pemerintah == 'Kesekretariatan' ? 'selected' : ''}} value="Kesekretariatan">Kesekretariatan</option>
                                  <option {{ isset($komjab) && $komjab->urusan_pemerintah == 'Bidang Komunikasi dan Informatika, Bidang Persandian dan Bidang Statistik' ? 'selected' : ''}} value="Bidang Komunikasi dan Informatika, Bidang Persandian dan Bidang Statistik">Bidang Komunikasi dan Informatika, Bidang Persandian dan Bidang Statistik</option>
                            </select>
                          </div>
                      </div>
                  </div>
                </div>
                <!--end::Card-->
                <br>

                <div class="card card-custom">
                  <div class="card-body">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Ikhtisar Jabatan :</label>
                        <div class="col-lg-8">
                          <p class="form-control-plaintext">{{ $anjab->ikhtisar_jabatan != null ? $anjab->ikhtisar_jabatan :''}}</p>
                        </div>
                    </div>
                  </div>
                </div>
                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Manajerial</h2>
                    </div>
                  </div>
                  <div class="card-body">
                    
                    <input type="hidden" name="id_anjab" id="id_anjab" value="{{ $anjab->id }}">
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Integeritas</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">level</label>
                        <div class="col-lg-3">
                          <select  class="form-control" name="level_integeritas" id="level_integeritas">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_integeritas as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Integeritas" data-urutan="1" {{$level->id == $komjab->id_m_kompetensi_level_integritas? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Integeritas" data-urutan="1" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_integeritas">
                        @if($komjab)
                        <div class="form-group row">
                          <div class="col-lg-2"></div>
                          <div class="col-lg-8">
                              <table class="table table-bordered" id="anjab_tabel" style="margin-top: 13px !important">
                                  <thead>
                                      <tr>
                                          <th class="text-center">No</th>
                                          <th class="text-center">Kompetensi</th>
                                          <th class="text-center">Diskripsi</th>
                                          <th class="text-center">Indikator Kompetensi</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>1</td>
                                          <td>Integritas</td>
                                          <td width="30%" class="text-left">{{ $komjab->dataLevelIntegritas->deskripsi }}</td>
                                          <td>
                                              <ul>
                                                @foreach($komjab->dataLevelIntegritas->dataIndikator as $indikator)
                                                    <li>
                                                        {{$indikator->indikator}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                          </td>
                                              
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                        </div>
                        @endif
                      </div>
                    </div>
                      
                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Kerjasama</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                        <select  class="form-control" name="level_kerjasama" id="level_kerjasama">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_kerjasama as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Kerjasama" data-urutan="2" {{$level->id == $komjab->id_m_kompetensi_level_kerjasama? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Kerjasama" data-urutan="2"  value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_kerjasama">
                        @if($komjab)
                        <div class="form-group row">
                          <div class="col-lg-2"></div>
                          <div class="col-lg-8">
                              <table class="table table-bordered" id="anjab_tabel" style="margin-top: 13px !important">
                                  <thead>
                                      <tr>
                                          <th class="text-center">No</th>
                                          <th class="text-center">Kompetensi</th>
                                          <th class="text-center">Diskripsi</th>
                                          <th class="text-center">Indikator Kompetensi</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>1</td>
                                          <td>Kerjasama</td>
                                          <td width="30%" class="text-left">{{ $komjab->dataLevelKerjasama->deskripsi }}</td>
                                          <td>
                                              <ul>
                                                @foreach($komjab->dataLevelKerjasama->dataIndikator as $indikator)
                                                    <li>
                                                        {{$indikator->indikator}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                          </td>
                                              
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                        </div>
                        @endif
                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Komunikasi</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                        <select  class="form-control" name="level_komunikasi" id="level_komunikasi">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_komunikasi as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Komunikasi" data-urutan="3" {{$level->id == $komjab->id_m_kompetensi_level_komunikasi? 'selected':''}}  value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Komunikasi" data-urutan="3"  value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_komunikasi">
                         @if($komjab)
                        <div class="form-group row">
                          <div class="col-lg-2"></div>
                          <div class="col-lg-8">
                              <table class="table table-bordered" id="anjab_tabel" style="margin-top: 13px !important">
                                  <thead>
                                      <tr>
                                          <th class="text-center">No</th>
                                          <th class="text-center">Kompetensi</th>
                                          <th class="text-center">Diskripsi</th>
                                          <th class="text-center">Indikator Kompetensi</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>1</td>
                                          <td>Kerjasama</td>
                                          <td width="30%" class="text-left">{{ $komjab->dataLevelKomunikasi->deskripsi }}</td>
                                          <td>
                                              <ul>
                                                @foreach($komjab->dataLevelKomunikasi->dataIndikator as $indikator)
                                                    <li>
                                                        {{$indikator->indikator}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                          </td>
                                              
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                        </div>
                        @endif
                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Orientasi Pada Hasil</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                        <select  class="form-control" name="level_orientasi" id="level_orientasi">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_orientasi as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Orientasi Pada Hasil" data-urutan="4" {{$level->id == $komjab->id_m_kompetensi_level_orientasi? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Orientasi Pada Hasil" data-urutan="4" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_orientasi">
                        @if($komjab)
                        <div class="form-group row">
                          <div class="col-lg-2"></div>
                          <div class="col-lg-8">
                              <table class="table table-bordered" id="anjab_tabel" style="margin-top: 13px !important">
                                  <thead>
                                      <tr>
                                          <th class="text-center">No</th>
                                          <th class="text-center">Kompetensi</th>
                                          <th class="text-center">Diskripsi</th>
                                          <th class="text-center">Indikator Kompetensi</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>4</td>
                                          <td>Orientasi Pada Hasil</td>
                                          <td width="30%" class="text-left">{{ $komjab->dataLevelOrientasi->deskripsi }}</td>
                                          <td>
                                              <ul>
                                                @foreach($komjab->dataLevelOrientasi->dataIndikator as $indikator)
                                                    <li>
                                                        {{$indikator->indikator}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                          </td>
                                              
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                        </div>
                        @endif
                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pelayanan Publik</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                        <select  class="form-control" name="level_pelayanan" id="level_pelayanan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_pelayanan as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Pelayanan Publik" data-urutan="5" {{$level->id == $komjab->id_m_kompetensi_level_pelayanan? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Pelayanan Publik" data-urutan="5" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_pelayanan">
                        @if($komjab)
                        <div class="form-group row">
                          <div class="col-lg-2"></div>
                          <div class="col-lg-8">
                              <table class="table table-bordered" id="anjab_tabel" style="margin-top: 13px !important">
                                  <thead>
                                      <tr>
                                          <th class="text-center">No</th>
                                          <th class="text-center">Kompetensi</th>
                                          <th class="text-center">Diskripsi</th>
                                          <th class="text-center">Indikator Kompetensi</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>5</td>
                                          <td>Pelayanan Publik</td>
                                          <td width="30%" class="text-left">{{ $komjab->dataLevelPelayanan->deskripsi }}</td>
                                          <td>
                                              <ul>
                                                @foreach($komjab->dataLevelPelayanan->dataIndikator as $indikator)
                                                    <li>
                                                        {{$indikator->indikator}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                          </td>
                                              
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                        </div>
                        @endif
                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pengembangan Diri dan Orang Lain</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                        <select  class="form-control" name="level_pengembangan" id="level_pengembangan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_pengembangan as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Pengembangan Diri dan Orang Lain" data-urutan="6" {{$level->id == $komjab->id_m_kompetensi_level_pengembangan? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Pengembangan Diri dan Orang Lain" data-urutan="6" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_pengembangan">
                         @if($komjab)
                        <div class="form-group row">
                          <div class="col-lg-2"></div>
                          <div class="col-lg-8">
                              <table class="table table-bordered" id="anjab_tabel" style="margin-top: 13px !important">
                                  <thead>
                                      <tr>
                                          <th class="text-center">No</th>
                                          <th class="text-center">Kompetensi</th>
                                          <th class="text-center">Diskripsi</th>
                                          <th class="text-center">Indikator Kompetensi</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>6</td>
                                          <td>Pengembangan diri dan orang lain</td>
                                          <td width="30%" class="text-left">{{ $komjab->dataLevelPengembangan->deskripsi }}</td>
                                          <td>
                                              <ul>
                                                @foreach($komjab->dataLevelPengembangan->dataIndikator as $indikator)
                                                    <li>
                                                        {{$indikator->indikator}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                          </td>
                                              
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                        </div>
                        @endif
                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Mengelola Perubahan</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                        <select  class="form-control" name="level_perubahan" id="level_perubahan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_perubahan as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Mengelola Perubahan" data-urutan="7" {{$level->id == $komjab->id_m_kompetensi_level_perubahan? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Mengelola Perubahan" data-urutan="7" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_perubahan">
                         @if($komjab)
                        <div class="form-group row">
                          <div class="col-lg-2"></div>
                          <div class="col-lg-8">
                              <table class="table table-bordered" id="anjab_tabel" style="margin-top: 13px !important">
                                  <thead>
                                      <tr>
                                          <th class="text-center">No</th>
                                          <th class="text-center">Kompetensi</th>
                                          <th class="text-center">Diskripsi</th>
                                          <th class="text-center">Indikator Kompetensi</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>7</td>
                                          <td>Mengelola Perubahan</td>
                                          <td width="30%" class="text-left">{{ $komjab->dataLevelPerubahan->deskripsi }}</td>
                                          <td>
                                              <ul>
                                                @foreach($komjab->dataLevelPerubahan->dataIndikator as $indikator)
                                                    <li>
                                                        {{$indikator->indikator}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                          </td>
                                              
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                        </div>
                        @endif
                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pengambilan Keputusan</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                        <select  class="form-control" name="level_keputusan" id="level_keputusan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_keputusan as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Pengambilan Keputusan" data-urutan="8" {{$level->id == $komjab->id_m_kompetensi_level_keputusan? 'selected':''}}  value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Pengambilan Keputusan" data-urutan="8" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_keputusan">
                         @if($komjab)
                        <div class="form-group row">
                          <div class="col-lg-2"></div>
                          <div class="col-lg-8">
                              <table class="table table-bordered" id="anjab_tabel" style="margin-top: 13px !important">
                                  <thead>
                                      <tr>
                                          <th class="text-center">No</th>
                                          <th class="text-center">Kompetensi</th>
                                          <th class="text-center">Diskripsi</th>
                                          <th class="text-center">Indikator Kompetensi</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>8</td>
                                          <td>Pengambilan Keputusan</td>
                                          <td width="30%" class="text-left">{{ $komjab->dataLevelKeputusan->deskripsi }}</td>
                                          <td>
                                              <ul>
                                                @foreach($komjab->dataLevelKeputusan->dataIndikator as $indikator)
                                                    <li>
                                                        {{$indikator->indikator}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                          </td>
                                              
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                        </div>
                        @endif
                      </div>
                    </div>
                    
                  </div>

                </div>

                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Sosial Kultural</h2>
                    </div>
                  </div>
                  <div class="card-body">
                    
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Perekat Bangsa</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">level</label>
                        <div class="col-lg-3">
                          <select  class="form-control" name="level_perekat" id="level_perekat">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_perekat as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Perekat Bangsa" data-urutan="9" {{$level->id == $komjab->id_m_kompetensi_level_perekat_bangsa? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Perekat Bangsa" data-urutan="9" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_perekat">
                         @if($komjab)
                        <div class="form-group row">
                          <div class="col-lg-2"></div>
                          <div class="col-lg-8">
                              <table class="table table-bordered" id="anjab_tabel" style="margin-top: 13px !important">
                                  <thead>
                                      <tr>
                                          <th class="text-center">No</th>
                                          <th class="text-center">Kompetensi</th>
                                          <th class="text-center">Diskripsi</th>
                                          <th class="text-center">Indikator Kompetensi</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>9</td>
                                          <td>Perekat Bangsa</td>
                                          <td width="30%" class="text-left">{{ $komjab->dataLevelPerekatBangsa->deskripsi }}</td>
                                          <td>
                                              <ul>
                                                @foreach($komjab->dataLevelPerekatBangsa->dataIndikator as $indikator)
                                                    <li>
                                                        {{$indikator->indikator}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                          </td>
                                              
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                        </div>
                        @endif
                      </div>
                    </div>
                      
                    
                    
                  </div>
                  <div class="card-footer">
                    <div class="row">
                      <div class="col text-right">
                        @if(!isset($komjab))
                          <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        @endif
                        
                        @if(isset($komjab))
                          <button type="submit" class="btn btn-success mr-2">Update</button>
                        @endif
                     
                      </div>
                    </div>
                  </div>
                </div>
                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Teknis</h2>
                    </div>
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      @if(isset($komjab))
                      <a href="javascript:;" class="btn btn-primary btn-sm add-data-teknis">
                       <i class="far fa-plus-square"></i>Teknis</a>
                      @endif
                      <!--end::Button-->
                    </div>

                  </div>

                  <div class="card-body">
                    <div class="mb-15">
                      <table class="table table-separate table-bordered" >
                        <thead>
                          <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Level</th>
                            <th class="text-center">Kompetensi</th>
                            <th class="text-center">Deskripsi</th>
                            <th class="text-center">Indikator Kompetensi</th>
                            <th class="text-center">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($komjab->komjabTeknis->count() == 0)
                          <tr>
                            <td colspan="6" class="text-center">Tidak Ada Data</td>
                          </tr>
                        @endif
                        @php
                          $komjabTeknis = $komjab->komjabTeknis;
                        @endphp
                        @foreach($komjabTeknis as $kom)
                        @php
                          $indikatorTeknis = $kom->indikatorTeknis;
                        @endphp
                          <tr>
                              <td width="5%" class="text-center" ># </td>
                              <td class="text-center" >{{ $kom->level }}</td>
                              <td>{{ $kom->nama_kompetensi != null ? $kom->nama_kompetensi:'' }}</td>
                              <td>{{ $kom->deskripsi != null ? $kom->deskripsi :'' }}</td>
                              <td>
                                @if($indikatorTeknis->count() != 0)
                                <br>
                                <table class="table table-borderless">
                                  <tbody>
                                      @foreach($indikatorTeknis as $indikator)
                                      <tr>
                                        <td class="text-center">{{ $loop->iteration }} </td>
                                        <td class="text-left">{{ $indikator->indikator }}</td>
                                         <td>
                                          <div class="btn-group dropleft">
                                             <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Menu
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                              <!--begin::Naviigation-->
                                              <ul class="navi">
                                                <li class="navi-item">
                                                  <a data-id="{{ $indikator->id }}"  href="javascript:;" class="navi-link edit-indikator">
                                                    <span class="navi-icon">
                                                      <i class="navi-icon flaticon2-edit"></i>
                                                    </span>
                                                    <span class="navi-text">Edit Indikator</span>
                                                  </a>
                                                </li>

                                                 <li class="navi-item">
                                                  <a data-id="{{ $indikator->id }}" data-id_teknis="{{ $kom->id }}"  href="javascript:;" class="navi-link delete-indikator">
                                                    <span class="navi-icon">
                                                      <i class="navi-icon flaticon-delete-1"></i>
                                                    </span>
                                                    <span class="navi-text">Hapus Indikator</span>
                                                  </a>
                                                </li>
                                              </ul>
                                              <!--end::Naviigation-->
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      @endforeach
                                  </tbody>
                                </table>

                                @endif
                              </td>
                              <td>
                                <div class="btn-group dropleft">
                                   <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Menu
                                  </button>
                                  <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                    <!--begin::Naviigation-->
                                    <ul class="navi">
                                      <li class="navi-item">
                                        <a data-id="{{ $kom->id }}"  href="javascript:;" class="navi-link edit-teknis">
                                          <span class="navi-icon">
                                            <i class="navi-icon flaticon2-edit"></i>
                                          </span>
                                          <span class="navi-text">Edit Teknis</span>
                                        </a>
                                      </li>

                                       <li class="navi-item">
                                        <a data-id="{{ $kom->id }}"  href="javascript:;" class="navi-link delete-teknis">
                                          <span class="navi-icon">
                                            <i class="navi-icon flaticon-delete-1"></i>
                                          </span>
                                          <span class="navi-text">Hapus Teknis</span>
                                        </a>
                                      </li>

                                      <li class="navi-item">
                                        <a data-id_teknis="{{ $kom->id }}"  href="javascript:;" class="navi-link add-indikator">
                                          <span class="navi-icon">
                                            <i class="navi-icon flaticon2-plus-1"></i>
                                          </span>
                                          <span class="navi-text">Tambah Indikator</span>
                                        </a>
                                      </li>
                                      
                                      
                                     
                                    </ul>
                                    <!--end::Naviigation-->
                                  </div>
                                </div>
                              </td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>

                   

                  </div>

                  
                </div>
                <!--end::Card-->
                </form>
                <br>
                    <!--begin::Card-->
                @if(isset($komjab))
                <form action="{{route('komjab.save')}}" enctype="multipart/form-data" method="post">
                @csrf
                  <input type="text" hidden name="id_komjab" id="id_komjab" value="{{$komjab->id}}">
                  <input type="hidden" name="id_anjab" id="id_anjab" value="{{ $anjab->id }}">
                  <input type="text" hidden name="type_syarat_jabatan" value="update-persyaratan-jabatan">
                  <div class="card card-custom">
                   
                       <div class="card-header">
                        <div class="card-title">
                          <h2 class="card-label">Persyaratan Jabatan</h2>
                        </div>
                        <div class="card-toolbar">
                          <!--begin::Button-->
                           <button type="submit" class="btn btn-success">Update</button>
                          <!--end::Button-->
                        </div>

                      </div>
                     
                      <div class="card-body">
                        <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pendidikan</h2>
                        <div class="mb-15">
                          <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-right">Jenjang</label>
                            <div class="col-lg-3">
                              <input type="text" class="form-control" name="jenjang" value="{{$komjab->jenjang}}">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-right">Bidang Ilmu</label>
                            <div class="col-lg-4">
                              <textarea class="form-control" name="bidang_ilmu" rows="5">{{$komjab->bidang_ilmu}}</textarea>
                            </div>
                          </div>

                          <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pangkat</h2>
                          <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-right">Pangkat</label>
                            <div class="col-lg-3">
                              <input type="text" class="form-control" name="pangkat" value="{{$komjab->pangkat}}">
                            </div>
                          </div>

                        </div>

                        <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pelatihan</h2>
                        <div class="mb-15">
                          <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-right">Manajerial</label>
                            <div class="col-lg-3">
                              <input type="text" class="form-control" name="manajerial" value="{{$komjab->manajerial}}">
                            </div>
                            <label class="col-lg-2 col-form-label text-right">Tingkat Kepentingan Pada Jabatan</label>
                            <div class="col-lg-3">
                              <select  class="form-control" name="status_manajerial" id="status_manajerial">
                                 <option value="" disabled="" selected="">-- Pilih Tingkat Kepentingan Pada Jabatan--</option>
                                 <option value="mutlak" {{ $komjab->status_manajerial == 'mutlak'?'selected':'' }}>Mutlak</option>
                                 <option value="penting" {{ $komjab->status_manajerial == 'penting'?'selected':'' }}>Penting</option>
                                 <option value="perlu" {{ $komjab->status_manajerial == 'perlu'?'selected':'' }}>Perlu</option>
                              </select>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-lg-2 col-form-label text-right">Teknis</label>
                            <div class="col-lg-8">
                              <a href="javascript:;" class="btn btn-primary btn-sm add-teknis-persyaratan-jabatan">
                              <i class="far fa-plus-square"></i>Teknis</a>
                              <br><br>
                              <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Teknis</th>
                                        <th class="text-center">Tingkat Kepentingan Pada Jabatan</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @if($komjab->komjabPersyaratanJabatanTeknis->count() == 0)
                                    <tr>
                                      <td colspan="6" class="text-center">Tidak Ada Data</td>
                                    </tr>
                                  @endif
                                  @php
                                    $komjabPersyaratanJabatanTeknis = $komjab->komjabPersyaratanJabatanTeknis;
                                  @endphp
                                  @foreach($komjabPersyaratanJabatanTeknis as $komPersyaratanJabatan)
                                    <tr>
                                      <td width="5%" class="text-center" ># </td>
                                      <td class="text-center">{{ $komPersyaratanJabatan->teknis }}</td>
                                      <td class="text-center">{{ $komPersyaratanJabatan->badge_status }}</td>
                                      <td>
                                        <div class="btn-group dropleft">
                                           <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              Menu
                                          </button>
                                          <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                            <!--begin::Naviigation-->
                                            <ul class="navi">
                                              <li class="navi-item">
                                                <a data-id="{{ $komPersyaratanJabatan->id }}"  href="javascript:;" class="navi-link edit-teknis-persyaratan-jabatan">
                                                  <span class="navi-icon">
                                                    <i class="navi-icon flaticon2-edit"></i>
                                                  </span>
                                                  <span class="navi-text">Edit </span>
                                                </a>
                                              </li>

                                               <li class="navi-item">
                                                <a data-id="{{ $komPersyaratanJabatan->id }}"  href="javascript:;" class="navi-link delete-teknis-persyaratan-jabatan">
                                                  <span class="navi-icon">
                                                    <i class="navi-icon flaticon-delete-1"></i>
                                                  </span>
                                                  <span class="navi-text">Hapus</span>
                                                </a>
                                              </li>
                                            </ul>
                                            <!--end::Naviigation-->
                                          </div>
                                      </td>
                                    </tr>
                                  @endforeach
                                </tbody>
                              </table>
                            </div>
                          </div>
                         
                        </div>

                        <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pengalaman Kerja</h2>
                        <div class="mb-15">
                            <a href="javascript:;" class="btn btn-primary btn-sm add-pengalaman-kerja-persyaratan-jabatan">
                              <i class="far fa-plus-square"></i>Pemgalaman Kerja</a>
                            <br><br>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Pengalaman Kerja</th>
                                        <th class="text-center">Tingkat Kepentingan Pada Jabatan</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    @if($komjab->komjabPersyaratanJabatanPengalamanKerja->count() == 0)
                                      <tr>
                                        <td colspan="6" class="text-center">Tidak Ada Data</td>
                                      </tr>
                                    @endif
                                    @php
                                      $komjabPersyaratanJabatanPengalamanKerja = $komjab->komjabPersyaratanJabatanPengalamanKerja;
                                    @endphp
                                    @foreach($komjabPersyaratanJabatanPengalamanKerja as $komPersyaratanJabatanPengalaman)
                                      <tr>
                                        <td width="5%" class="text-center" ># </td>
                                        <td class="text-center">{{ $komPersyaratanJabatanPengalaman->pengalaman_kerja }}</td>
                                        <td class="text-center">{{ $komPersyaratanJabatanPengalaman->badge_status }}</td>
                                        <td>
                                          <div class="btn-group dropleft">
                                             <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Menu
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                              <!--begin::Naviigation-->
                                              <ul class="navi">
                                                <li class="navi-item">
                                                  <a data-id="{{ $komPersyaratanJabatanPengalaman->id }}"  href="javascript:;" class="navi-link edit-pengalaman-kerja-persyaratan-jabatan">
                                                    <span class="navi-icon">
                                                      <i class="navi-icon flaticon2-edit"></i>
                                                    </span>
                                                    <span class="navi-text">Edit </span>
                                                  </a>
                                                </li>

                                                 <li class="navi-item">
                                                  <a data-id="{{ $komPersyaratanJabatanPengalaman->id }}"  href="javascript:;" class="navi-link delete-pengalaman-kerja-persyaratan-jabatan">
                                                    <span class="navi-icon">
                                                      <i class="navi-icon flaticon-delete-1"></i>
                                                    </span>
                                                    <span class="navi-text">Hapus</span>
                                                  </a>
                                                </li>
                                              </ul>
                                              <!--end::Naviigation-->
                                            </div>
                                        </td>
                                      </tr>
                                    @endforeach
                                  </tr>
                                </tbody>
                            </table>

                        </div>

                        <h2 class="font-size-lg text-dark font-weight-bold mb-6">Indikator Kinerja Jabatan</h2>
                        <div class="mb-15">
                          <a href="javascript:;" class="btn btn-primary btn-sm add-indikator-kinerja-persyaratan-jabatan">
                              <i class="far fa-plus-square"></i>Indikator Kinerja</a>
                          <br><br>
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Indikator Kinerja</th>
                                <th class="text-center">Aksi</th>
                              </tr>
                            </thead>
                            <tbody>
                              @if($komjab->komjabPersyaratanJabatanIndikatorKinerja->count() == 0)
                                <tr>
                                  <td colspan="6" class="text-center">Tidak Ada Data</td>
                                </tr>
                              @endif
                              @php
                                $komjabPersyaratanJabatanIndikatorKinerja = $komjab->komjabPersyaratanJabatanIndikatorKinerja;
                              @endphp
                              @foreach($komjabPersyaratanJabatanIndikatorKinerja as $komPersyaratanJabatanIndikator)
                                <tr>
                                  <td width="5%" class="text-center" ># </td>
                                  <td class="text-center">{{ $komPersyaratanJabatanIndikator->indikator_kinerja }}</td>
                                  <td>
                                    <div class="btn-group dropleft">
                                       <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          Menu
                                      </button>
                                      <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                        <!--begin::Naviigation-->
                                        <ul class="navi">
                                          <li class="navi-item">
                                            <a data-id="{{ $komPersyaratanJabatanIndikator->id }}"  href="javascript:;" class="navi-link edit-indikator-kinerja-persyaratan-jabatan">
                                              <span class="navi-icon">
                                                <i class="navi-icon flaticon2-edit"></i>
                                              </span>
                                              <span class="navi-text">Edit </span>
                                            </a>
                                          </li>

                                           <li class="navi-item">
                                            <a data-id="{{ $komPersyaratanJabatanIndikator->id }}"  href="javascript:;" class="navi-link delete-indikator-kinerja-persyaratan-jabatan">
                                              <span class="navi-icon">
                                                <i class="navi-icon flaticon-delete-1"></i>
                                              </span>
                                              <span class="navi-text">Hapus</span>
                                            </a>
                                          </li>
                                        </ul>
                                        <!--end::Naviigation-->
                                      </div>
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                          
                  </div>
                  </form>  
                  @endif
                
              
              </div>
            
              <!--end::Container-->
              <!-- <input type="text" class="form-control" name="indikator_kompetensi[]" id="indikator_kompetensi[]"> -->

            <div class="modal fade text-left" id="modal" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h4 class="modal-title"></h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <form method="POST" action="{{ route('komjab.save') }}">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                        </div>
                      </form>
                  </div>
              </div>
          </div>

           <form id="form-delete-teknis" method="POST" action="{{ route('komjab.delete') }}">
              {{ csrf_field() }}
              <input type="hidden" value="" name="id">
              <input type="hidden" value="delete-teknis" name="aksi">
            </form>

            <form id="form-delete-indikator" method="POST" action="{{ route('komjab.delete') }}">
              {{ csrf_field() }}
              <input type="hidden" value="" name="id">
              <input type="hidden" value="delete-indikator" name="aksi">
            </form>

            <form id="form-delete-teknis-persyaratan-jabatan" method="POST" action="{{ route('komjab.delete') }}">
              {{ csrf_field() }}
              <input type="hidden" value="" name="id">
              <input type="hidden" value="delete-teknis-persyaratan-jabatan" name="aksi">
            </form>
@endsection

@section('js')
{{-- js files --}}
 <script src="{{ asset('') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script src="//cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script>
 
  var options = {
    filebrowserImageBrowseUrl: '{{ URL::to("laravel-filemanager?type=Images") }}',
    filebrowserImageUploadUrl: '{{ URL::to("laravel-filemanager/upload?type=Images&_token=") }}',
    filebrowserBrowseUrl: '{{ URL::to("/laravel-filemanager?type=Files") }}',
    filebrowserUploadUrl: '{{ URL::to("/laravel-filemanager/upload?type=Files&_token=") }}',
  };
</script>

<script>

        $('.add-data-teknis').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'add-teknis',id_anjab:id_anjab,id_komjab:id_komjab },
            success:function(result){
              $('#modal .modal-title').text("Tambah Teknis");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        }); 

        $('.edit-teknis').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          var id = $(this).data('id');
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'edit-teknis',id_anjab:id_anjab,id_komjab:id_komjab,id:id },
            success:function(result){
              $('#modal .modal-title').text("Edit Teknis");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        $('.delete-teknis').on('click', function(){
          var id = $(this).data('id');
              swal.fire({
                  title: 'Apakah anda yakin?',
                  // text: "Anda tidak dapat membatalkan pengajuan!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Hapus Data',
                  cancelButtonText: 'Batal',
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-warning',
                  buttonsStyling: false
              }).then(function (status) {
                if(status.value){
                  $('form[id=form-delete-teknis] input[name=id]').val(id);
                      $('form[id=form-delete-teknis]').submit();
                }
              });
        });

        $('.delete-indikator').on('click', function(){
          var id = $(this).data('id');
              swal.fire({
                  title: 'Apakah anda yakin?',
                  // text: "Anda tidak dapat membatalkan pengajuan!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Hapus Data',
                  cancelButtonText: 'Batal',
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-warning',
                  buttonsStyling: false
              }).then(function (status) {
                if(status.value){
                  $('form[id=form-delete-indikator] input[name=id]').val(id);
                      $('form[id=form-delete-indikator]').submit();
                }
              });
        });

        $('.delete-teknis-persyaratan-jabatan').on('click', function(){
          var id = $(this).data('id');
              swal.fire({
                  title: 'Apakah anda yakin?',
                  // text: "Anda tidak dapat membatalkan pengajuan!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Hapus Data',
                  cancelButtonText: 'Batal',
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-warning',
                  buttonsStyling: false
              }).then(function (status) {
                if(status.value){
                  $('form[id=form-delete-teknis-persyaratan-jabatan] input[name=id]').val(id);
                      $('form[id=form-delete-teknis-persyaratan-jabatan]').submit();
                }
              });
        });

        $('.delete-indikator-kinerja-persyaratan-jabatan').on('click', function(){
          var id = $(this).data('id');
              swal.fire({
                  title: 'Apakah anda yakin?',
                  // text: "Anda tidak dapat membatalkan pengajuan!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Hapus Data',
                  cancelButtonText: 'Batal',
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-warning',
                  buttonsStyling: false
              }).then(function (status) {
                if(status.value){
                  $('form[id=form-delete-teknis-persyaratan-jabatan] input[name=id]').val(id);
                      $('form[id=form-delete-teknis-persyaratan-jabatan]').submit();
                }
              });
        });

        $('.add-indikator').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          var id_teknis = $(this).data('id_teknis');
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'add-indikator',id_anjab:id_anjab,id_komjab:id_komjab,id_teknis:id_teknis },
            success:function(result){
              $('#modal .modal-title').text("Tambah Indikator");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        $('.edit-indikator').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          var id_teknis = $(this).data('id_teknis');
          var id = $(this).data('id');
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'edit-indikator',id:id,id_anjab:id_anjab,id_komjab:id_komjab,id_teknis:id_teknis },
            success:function(result){
              $('#modal .modal-title').text("Edit Indikator");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        $("#nama_jabatan").change(function() {
          var ikhtisar = $(this).find(':selected').data('ikhtisar');
          $('#ikhtisar').text(ikhtisar);
          $('#id').val(id);
        });

        $("#level_integeritas").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          console.log(varkompetensi);
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_integeritas").html(html);
            }
          });
        });
        $("#level_kerjasama").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_kerjasama").html(html);
            }
          });
        });
        $("#level_komunikasi").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_komunikasi").html(html);
            }
          });
        });
        $("#level_orientasi").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_orientasi").html(html);
            }
          });
        });
        $("#level_pelayanan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_pelayanan").html(html);
            }
          });
        });
        $("#level_pengembangan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_pengembangan").html(html);
            }
          });
        });
        $("#level_perubahan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_perubahan").html(html);
            }
          });
        });
        $("#level_keputusan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_keputusan").html(html);
            }
          });
        });
        $("#level_perekat").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_perekat").html(html);
            }
          });
        });

        $('.add-teknis-persyaratan-jabatan').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'add-teknis-persyaratan-jabatan',id_anjab:id_anjab,id_komjab:id_komjab },
            success:function(result){
              $('#modal .modal-title').text("Tambah Teknis Persyaratan Jabatan");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        }); 

        $('.edit-teknis-persyaratan-jabatan').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          var id = $(this).data('id');
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'edit-teknis-persyaratan-jabatan',id_anjab:id_anjab,id_komjab:id_komjab,id:id },
            success:function(result){
              $('#modal .modal-title').text("Edit Teknis");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        $('.add-pengalaman-kerja-persyaratan-jabatan').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'add-pengalaman-kerja-persyaratan-jabatan',id_anjab:id_anjab,id_komjab:id_komjab },
            success:function(result){
              $('#modal .modal-title').text("Tambah Pengalaman Kerja Persyaratan Jabatan");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        $('.edit-pengalaman-kerja-persyaratan-jabatan').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          var id = $(this).data('id');
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'edit-pengalaman-kerja-persyaratan-jabatan',id_anjab:id_anjab,id_komjab:id_komjab,id:id },
            success:function(result){
              $('#modal .modal-title').text("Edit Pengalaman Kerja Persyaratan Jabatan");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        $('.add-indikator-kinerja-persyaratan-jabatan').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'add-indikator-kinerja-persyaratan-jabatan',id_anjab:id_anjab,id_komjab:id_komjab },
            success:function(result){
              $('#modal .modal-title').text("Tambah Indikator Kinerja Persyaratan Jabatan");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        $('.edit-indikator-kinerja-persyaratan-jabatan').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          var id = $(this).data('id');
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'edit-indikator-kinerja-persyaratan-jabatan',id_anjab:id_anjab,id_komjab:id_komjab,id:id },
            success:function(result){
              $('#modal .modal-title').text("Edit Indikator Kinerja Persyaratan Jabatan");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        // $("#level_teknis").change(function() {
        //   var dats = $(this).val();
        //   var id = $(this).find(':selected').data('id');
        //   $.ajax({
        //     type: "GET",
        //     url: '{{ route("komjab.get-data-teknis") }}',
        //     data: {
        //           level :  dats,
        //           id_komjab : id
        //     },
        //     cache: false,
        //     success: function(html) {
        //       $("#teknis").html(html);
        //     }
        //   });
        // });


</script>
@endsection