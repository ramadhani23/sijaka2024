@extends('layout')

@section('content')
<!--begin::Container-->
              <div class="container">
              <form action="{{route('komjab.save')}}" enctype="multipart/form-data" method="post">
                      @csrf
                <!--begin::Dashboard-->
                  <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h3 class="card-label text-center">{{ isset($page) ? $page : 'Dashboard' }}</h3>
                        @if(isset($komjab))
                          <div class="text-right">
                            <a href="{{ route('komjab.print-word', $komjab->id) }}"  class="btn btn-success btn-sm font-weight-bolder add-anjab">
                            <i class="flaticon2-print"></i>Unduh Hasil Analisa</a>
                          </div>
                        @endif
                    </div>
                  </div>
                  <div class="card-body">
                  
                    
                      @if(isset($komjab))
                        <input type="text" hidden name="type" value="update">
                        <input type="text" hidden name="id_komjab" id="id_komjab" value="{{$komjab->id}}">
                      @else
                        <input type="text" hidden name="type" value="save">
                      @endif

                      <div class="mb-15">
                        <div class="form-group row">
                          <label class="col-lg-2 col-form-label text-right">Nama Jabatan</label>
                          <div class="col-lg-8">
                            <select required="" class="form-control" name="id_jabatan" id="jabatan">
                              <option value="" disabled="" selected="">-- PILIH JABATAN--</option>
                              @foreach($anjabAll as $anj)
                                @if(isset($komjab))
                                  <option {{$anj->id == $komjab->trx_anjab_id ? 'selected':''}} data-ikhtisar="{{$anj->ikhtisar_jabatan}}" value="{{ $anj->id }}">{{ $anj->jabatan->nama }}</option>
                                @else
                                  <option data-ikhtisar="{{$anj->ikhtisar_jabatan}}" value="{{ $anj->id }}">{{ $anj->jabatan->nama }}</option>
                                @endif
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Kelompok Jabatan :</label>
                          <div class="col-lg-8">
                            <select required="" class="form-control" name="kelompok_jabatan" id="kelompok_jabatan">
                              <option value="" disabled="" selected="">-- PILIH KELOMPOK JABATAN--</option>
                                  <option {{ isset($komjab) && $komjab->kelompok_jabatan == 'Jabatan Pimpinan Tinggi Pratama' ? 'selected':'' }} value="Jabatan Pimpinan Tinggi Pratama">Jabatan Pimpinan Tinggi Pratama</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Urusan Pemerintah :</label>
                          <div class="col-lg-8">
                            <select required="" class="form-control" name="urusan_pemerintah" id="urusan_pemerintah">
                              <option value="" disabled="" selected="">-- PILIH URUSAN PEMERINTAH--</option>
                                  <option {{ isset($komjab) && $komjab->urusan_pemerintah == 'Kesekretariatan' ? 'selected' : ''}} value="Kesekretariatan">Kesekretariatan</option>
                            </select>
                          </div>
                      </div>
                  </div>
                </div>
                <!--end::Card-->
                <br>

                <div class="card card-custom">
                  <div class="card-body">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Ikhtisar Jabatan :</label>
                        <div class="col-lg-8">
                          <textarea readonly id="ikhtisar" name="ikhtisar" required="" class="form-control" rows="10">{{ $anjab->ikhtisar_jabatan != null ? $anjab->ikhtisar_jabatan :''}}</textarea>
                        </div>
                    </div>
                  </div>
                </div>
                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Manajerial</h2>
                    </div>
                  </div>
                  <div class="card-body">
                    
                    <input type="hidden" name="id_anjab" id="id_anjab" value="{{ $anjab->id }}">
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Integeritas</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">level</label>
                        <div class="col-lg-6">
                          <select  class="form-control" name="level_integeritas" id="level_integeritas">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_integeritas as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Integeritas" data-urutan="1" {{$level->id == $komjab->id_m_kompetensi_level_integritas? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Integeritas" data-urutan="1" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_integeritas">

                      </div>
                    </div>
                      
                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Kerjasama</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_kerjasama" id="level_kerjasama">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_kerjasama as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Kerjasama" data-urutan="2" {{$level->id == $komjab->id_m_kompetensi_level_kerjasama? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Kerjasama" data-urutan="2"  value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_kerjasama">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Komunikasi</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_komunikasi" id="level_komunikasi">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_komunikasi as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Komunikasi" data-urutan="3" {{$level->id == $komjab->id_m_kompetensi_level_komunikasi? 'selected':''}}  value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Komunikasi" data-urutan="3"  value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_komunikasi">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Orientasi Pada Hasil</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_orientasi" id="level_orientasi">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_orientasi as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Orientasi Pada Hasil" data-urutan="4" {{$level->id == $komjab->id_m_kompetensi_level_orientasi? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Orientasi Pada Hasil" data-urutan="4" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_orientasi">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pelayanan Publik</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_pelayanan" id="level_pelayanan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_pelayanan as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Pelayanan Publik" data-urutan="5" {{$level->id == $komjab->id_m_kompetensi_level_pelayanan? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Pelayanan Publik" data-urutan="5" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_pelayanan">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pengembangan Diri dan Orang Lain</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_pengembangan" id="level_pengembangan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_pengembangan as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Pengembangan Diri dan Orang Lain" data-urutan="6" {{$level->id == $komjab->id_m_kompetensi_level_pengembangan? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Pengembangan Diri dan Orang Lain" data-urutan="6" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_pengembangan">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Mengelola Perubahan</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_perubahan" id="level_perubahan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_perubahan as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Mengelola Perubahan" data-urutan="7" {{$level->id == $komjab->id_m_kompetensi_level_perubahan? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Mengelola Perubahan" data-urutan="7" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_perubahan">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pengambilan Keputusan</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_keputusan" id="level_keputusan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_keputusan as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Pengambilan Keputusan" data-urutan="8" {{$level->id == $komjab->id_m_kompetensi_level_keputusan? 'selected':''}}  value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Pengambilan Keputusan" data-urutan="8" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_keputusan">

                      </div>
                    </div>
                    
                  </div>

                </div>

                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Sosial Kultural</h2>
                    </div>
                  </div>
                  <div class="card-body">
                    
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Perekat Bangsa</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">level</label>
                        <div class="col-lg-8">
                          <select  class="form-control" name="level_perekat" id="level_perekat">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_perekat as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Perekat Bangsa" data-urutan="9" {{$level->id == $komjab->id_m_kompetensi_level_perekat_bangsa? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Perekat Bangsa" data-urutan="9" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_perekat">

                      </div>
                    </div>
                      
                    
                    
                  </div>
                  <div class="card-footer">
                    <div class="row">
                      <div class="col text-right">
                        @if(!isset($komjab))
                          <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Teknis</h2>
                    </div>
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      @if(isset($komjab))
                      <a href="javascript:;" class="btn btn-primary btn-sm add-data-teknis">
                       <i class="far fa-plus-square"></i>Teknis</a>
                      @endif
                      <!--end::Button-->
                    </div>

                  </div>

                  <div class="card-body">
                    <div class="mb-15">
                      <table class="table table-separate table-bordered" >
                        <thead>
                          <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Level</th>
                            <th class="text-center">Kompetensi</th>
                            <th class="text-center">Deskripsi</th>
                            <th class="text-center">Indikator Kompetensi</th>
                            <th class="text-center">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($komjab->komjabTeknis->count() == 0)
                          <tr>
                            <td colspan="6" class="text-center">Tidak Ada Data</td>
                          </tr>
                        @endif
                        @php
                          $komjabTeknis = $komjab->komjabTeknis;
                        @endphp
                        @foreach($komjabTeknis as $kom)
                        @php
                          $indikatorTeknis = $kom->indikatorTeknis;
                        @endphp
                          <tr>
                              <td width="10%" class="text-center" ># </td>
                              <td class="text-center" >{{ $kom->level }}</td>
                              <td>{{ $kom->nama_kompetensi != null ? $kom->nama_kompetensi:'' }}</td>
                              <td>{{ $kom->deskripsi != null ? $kom->deskripsi :'' }}</td>
                              <td>
                                @if($indikatorTeknis->count() != 0)
                                <br>
                                <table class="table table-bordered">
                                  <thead>
                                    <tr>
                                      <th class="text-center">No</th>
                                      <th class="text-center">Indikator Kompetensi</th>
                                      <th class="text-center">Aksi</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                      @foreach($indikatorTeknis as $indikator)
                                      <tr>
                                        <td class="text-center">{{ $loop->iteration }} </td>
                                        <td class="text-center">{{ $indikator->indikator }}</td>
                                         <td>
                                          <div class="btn-group dropleft">
                                             <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Menu
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                              <!--begin::Naviigation-->
                                              <ul class="navi">
                                                <li class="navi-item">
                                                  <a data-id="{{ $indikator->id }}"  data-id_teknis="{{ $kom->id }}"   href="javascript:;" class="navi-link edit-indikator">
                                                    <span class="navi-icon">
                                                      <i class="navi-icon flaticon2-edit"></i>
                                                    </span>
                                                    <span class="navi-text">Edit Indikator</span>
                                                  </a>
                                                </li>

                                                 <li class="navi-item">
                                                  <a data-id="{{ $indikator->id }}" href="javascript:;" class="navi-link delete-indikator">
                                                    <span class="navi-icon">
                                                      <i class="navi-icon flaticon-delete-1"></i>
                                                    </span>
                                                    <span class="navi-text">Hapus Indikator</span>
                                                  </a>
                                                </li>
                                              </ul>
                                              <!--end::Naviigation-->
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      @endforeach
                                  </tbody>
                                </table>

                                @endif
                              </td>
                              <td>
                                <div class="btn-group dropleft">
                                   <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Menu
                                  </button>
                                  <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                    <!--begin::Naviigation-->
                                    <ul class="navi">
                                      <li class="navi-item">
                                        <a data-id="{{ $kom->id }}"  href="javascript:;" class="navi-link edit-teknis">
                                          <span class="navi-icon">
                                            <i class="navi-icon flaticon2-edit"></i>
                                          </span>
                                          <span class="navi-text">Edit Teknis</span>
                                        </a>
                                      </li>

                                       <li class="navi-item">
                                        <a data-id="{{ $kom->id }}"  href="javascript:;" class="navi-link delete-teknis">
                                          <span class="navi-icon">
                                            <i class="navi-icon flaticon-delete-1"></i>
                                          </span>
                                          <span class="navi-text">Hapus Teknis</span>
                                        </a>
                                      </li>

                                      <li class="navi-item">
                                        <a data-id_teknis="{{ $kom->id }}"  href="javascript:;" class="navi-link add-indikator">
                                          <span class="navi-icon">
                                            <i class="navi-icon flaticon2-plus-1"></i>
                                          </span>
                                          <span class="navi-text">Tambah Indikator</span>
                                        </a>
                                      </li>
                                      
                                      
                                     
                                    </ul>
                                    <!--end::Naviigation-->
                                  </div>
                                </div>
                              </td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>

                   

                  </div>

                  
                </div>
                <!--end::Card-->

                  <div class="card-footer">
                    <div class="row">
                      <div class="col text-right">
                        @if(isset($komjab))
                          <button type="submit" class="btn btn-success mr-2">Update</button>
                        @endif
                      </div>
                    </div>
                  </div>
                
              </div>
            </form>
              <!--end::Container-->
              <!-- <input type="text" class="form-control" name="indikator_kompetensi[]" id="indikator_kompetensi[]"> -->

            <div class="modal fade text-left" id="modal" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content" style="width: 1200px;">
                      <div class="modal-header">
                          <h4 class="modal-title"></h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <form method="POST" action="{{ route('komjab.save') }}">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                      </form>
                    </div>
                  </div>
              </div>

              <form id="form-delete-teknis" method="POST" action="{{ route('komjab.delete') }}">
                {{ csrf_field() }}
                <input type="hidden" value="" name="id">
                <input type="hidden" value="delete-teknis" name="aksi">
              </form>

          </div>
@endsection

@section('js')
{{-- js files --}}
 <script src="{{ asset('') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script src="//cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script>
 
  var options = {
    filebrowserImageBrowseUrl: '{{ URL::to("laravel-filemanager?type=Images") }}',
    filebrowserImageUploadUrl: '{{ URL::to("laravel-filemanager/upload?type=Images&_token=") }}',
    filebrowserBrowseUrl: '{{ URL::to("/laravel-filemanager?type=Files") }}',
    filebrowserUploadUrl: '{{ URL::to("/laravel-filemanager/upload?type=Files&_token=") }}',
  };
</script>

<script>

        $('.add-data-teknis').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'add-teknis',id_anjab:id_anjab,id_komjab:id_komjab },
            success:function(result){
              $('#modal .modal-title').text("Tambah Teknis");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        }); 

        $('.edit-teknis').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          var id = $(this).data('id');
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'edit-teknis',id_anjab:id_anjab,id_komjab:id_komjab,id:id },
            success:function(result){
              $('#modal .modal-title').text("Edit Teknis");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        $('.delete-teknis').on('click', function(){
          var id = $(this).data('id');
              swal.fire({
                  title: 'Apakah anda yakin?',
                  // text: "Anda tidak dapat membatalkan pengajuan!",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Hapus Data',
                  cancelButtonText: 'Batal',
                  confirmButtonClass: 'btn btn-success',
                  cancelButtonClass: 'btn btn-warning',
                  buttonsStyling: false
              }).then(function (status) {
                if(status.value){
                  $('form[id=form-delete-teknis] input[name=id]').val(id);
                      $('form[id=form-delete-teknis]').submit();
                }
              });
        });

        $('.add-indikator').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          var id_teknis = $(this).data('id_teknis');
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'add-indikator',id_anjab:id_anjab,id_komjab:id_komjab,id_teknis:id_teknis },
            success:function(result){
              $('#modal .modal-title').text("Tambah Indikator");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        $('.edit-indikator').on('click', function(){
          var id_komjab = $('#id_komjab').val();
          var id_anjab = $('#id_anjab').val();
          var id_teknis = $(this).data('id_teknis');
          var id = $(this).data('id');
          $.ajax({
            url:'{{ route("komjab.get-form") }}',
            method:'GET',
            data:{aksi:'edit-indikator',id:id,id_anjab:id_anjab,id_komjab:id_komjab,id_teknis:id_teknis },
            success:function(result){
              $('#modal .modal-title').text("Edit Indikator");
              $('#modal .modal-body').html(result);
              $('#modal').modal('show');
            }
          });
        });

        $("#nama_jabatan").change(function() {
          var ikhtisar = $(this).find(':selected').data('ikhtisar');
          $('#ikhtisar').text(ikhtisar);
          $('#id').val(id);
        });

        $("#level_integeritas").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          console.log(varkompetensi);
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_integeritas").html(html);
            }
          });
        });
        $("#level_kerjasama").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_kerjasama").html(html);
            }
          });
        });
        $("#level_komunikasi").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_komunikasi").html(html);
            }
          });
        });
        $("#level_orientasi").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_orientasi").html(html);
            }
          });
        });
        $("#level_pelayanan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_pelayanan").html(html);
            }
          });
        });
        $("#level_pengembangan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_pengembangan").html(html);
            }
          });
        });
        $("#level_perubahan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_perubahan").html(html);
            }
          });
        });
        $("#level_keputusan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_keputusan").html(html);
            }
          });
        });
        $("#level_perekat").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_perekat").html(html);
            }
          });
        });
        // $("#level_teknis").change(function() {
        //   var dats = $(this).val();
        //   var id = $(this).find(':selected').data('id');
        //   $.ajax({
        //     type: "GET",
        //     url: '{{ route("komjab.get-data-teknis") }}',
        //     data: {
        //           level :  dats,
        //           id_komjab : id
        //     },
        //     cache: false,
        //     success: function(html) {
        //       $("#teknis").html(html);
        //     }
        //   });
        // });


</script>
@endsection