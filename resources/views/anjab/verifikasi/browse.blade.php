@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">       
                @include('kunci')
                {{-- @include('anjab.card-anjab') --}}

                <br><br>
                @if(0 != 1)
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-body">
                    <!--begin::Example-->
                        <div class="example mb-10">
                          
                          <div class="example-preview">
                            <div class="row">
                              <div class="col-3 border-right">
                                @include('anjab.step-indicator')
                              </div>
                              <div class="col-9">

                                  <div class="card card-custom gutter-b">
                                      <div class="card-header">
                                        <div class="card-title">
                                          <h3 class="card-label"><i class="flaticon-event-calendar-symbol"></i> RINGKASAN</h3>
                                        </div>
                                        @if(\MojokertokabApp::allowChangingData())
                                        <div class="card-toolbar">
                                          
                                        </div>
                                        @endif
                                      </div>
                                      <div class="card-body">
                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">1. Data Jabatan:</h3>
                                        <div class="mb-15">
                                          <table class="table table-bordered table-striped " id="anjab_tabel" style="margin-top: 13px !important">
                                            <tbody>
                                              <tr>
                                                <td>Nama Jabatan </td>
                                                <td>{{ $anjab->jabatan->jabatan }} </td>
                                              </tr>
                                              <tr>
                                                <td>Jenis Jabatan </td>
                                                <td>{{ $anjab->text_jenis_jabatan }} </td>
                                              </tr>
                                              <tr>
                                                <td>Eselon Jabatan </td>
                                                <td>{{ $anjab->text_eselon_jabatan }} </td>
                                              </tr>
                                              <tr>
                                                <td>Kode Jabatan </td>
                                                <td>{{ $anjab->text_kode_jabatan }} </td>
                                              </tr>
                                              <tr>
                                                <td>Unit Kerja Eselon 2 </td>
                                                <td>{{ @$anjab->eselon2->unitkerja }} </td>
                                              </tr>
                                              <tr>
                                                <td>Unit Kerja Eselon 3 </td>
                                                <td>{{ @$anjab->eselon3->unitkerja }} </td>
                                              </tr>
                                              <tr>
                                                <td>Unit Kerja Eselon 4 </td>
                                                <td>{{ @$anjab->eselon4->unitkerja }} </td>
                                              </tr>
                                              <tr>
                                                <td>Ikhtisar Jabatan </td>
                                                <td width="50%" style="text-align: justify;">{{ $anjab->ikhtisar_jabatan }} </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>

                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">2. Uraian Tugas:</h3>
                                        <div class="mb-3">

                                          <div id="status-uraian-tugas">
                                            @if($anjab->status_uraian_tugas == 'verifikasi')
                                            <div class="alert alert-success" role="alert">
                                              Tanggung Jawab telah disetujui.
                                            </div>
                                            @elseif($anjab->status_uraian_tugas == 'ditolak')
                                            <div class="alert alert-danger" role="alert">
                                              Tanggung Jawab telah ditolak. {{ $anjab->keterangan_uraian_tugas }}
                                            </div>
                                            @else
                                            <div class="alert alert-warning" role="alert">
                                              Menunggu Verifikasi
                                            </div>
                                            @endif
                                          </div>

                                         <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th class="text-center">No</th>
                                              <th class="text-center">Uraian</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            @if($anjab->dataUraianJabatan->count() == 0)
                                              <tr>
                                                <td colspan="2" class="text-center">Tidak Ada Data</td>
                                              </tr>
                                            @endif
                                            @foreach($anjab->dataUraianJabatan as $anjabs)
                                            <?php $tahapan = $anjabs->tahapan ?>
                                            <tr>
                                              <td width="10%" >{{ $loop->iteration }} </td>
                                              <td>
                                                {{ $anjabs->uraian_jabatan }}
                                                @if($tahapan->count() != 0)
                                                <br>
                                                <table class="table table-bordered">
                                                  <thead>
                                                    <tr>
                                                      <th class="text-center">No</th>
                                                      <th class="text-center">Uraian Tahapan</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                      @foreach($tahapan as $tahapans)
                                                      <tr>
                                                        <td class="text-center">{{ $loop->iteration }} </td>
                                                        <td class="text-center">{{ $tahapans->uraian }}</td>
                                                      </tr>
                                                      @endforeach
                                                  </tbody>
                                                </table>
                                                @endif
                                              </td>
                                            </tr>
                                            @endforeach
                                          </tbody>
                                         </table>

                                         @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                         <div class="text-right mt-1">
                                           <a data-anjab-id="{{ $anjab->id }}" data-jenis="uraian-tugas" class="btn btn-success btn-sm btn-setujui">Setujui</a>
                                           <a data-anjab-id="{{ $anjab->id }}" data-jenis="uraian-tugas" id="btn-tolak-uraian-tugas" class="btn btn-danger btn-sm btn-tolak">Tolak</a>
                                         </div>
                                         @endif

                                        </div>

                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6 mt-6">3. Tanggung Jawab:</h3>
                                        <div class="mb-3">

                                          <div id="status-tanggung-jawab">
                                            @if($anjab->status_tanggung_jawab == 'verifikasi')
                                            <div class="alert alert-success" role="alert">
                                              Tanggung Jawab telah disetujui.
                                            </div>
                                            @elseif($anjab->status_tanggung_jawab == 'ditolak')
                                            <div class="alert alert-danger" role="alert">
                                              Tanggung Jawab telah ditolak. {{ $anjab->keterangan_tanggung_jawab }}
                                            </div>
                                            @else
                                            <div class="alert alert-warning" role="alert">
                                              Menunggu Verifikasi
                                            </div>
                                            @endif
                                          </div>

                                         <table class="table table-bordered">
                                            @foreach($anjab->dataTanggungJawab as $itemTanggungJawab)
                                            <tr>
                                             <td>{{ $itemTanggungJawab->tanggung_jawab }}</td>
                                            </tr>
                                            @endforeach
                                         </table>

                                         @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                         <div class="text-right mt-1">
                                           <a data-anjab-id="{{ $anjab->id }}" data-jenis="tanggung-jawab" class="btn btn-success btn-sm btn-setujui">Setujui</a>
                                           <a data-anjab-id="{{ $anjab->id }}" data-jenis="tanggung-jawab" id="btn-tolak-tanggung-jawab" class="btn btn-danger btn-sm btn-tolak">Tolak</a>
                                         </div>
                                         @endif

                                        </div>

                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">4. Wewenang:</h3>
                                        <div class="mb-3">

                                          <div id="status-wewenang">
                                            @if($anjab->status_wewenang == 'verifikasi')
                                            <div class="alert alert-success" role="alert">
                                              Wewenang telah disetujui.
                                            </div>
                                            @elseif($anjab->status_wewenang == 'ditolak')
                                            <div class="alert alert-danger" role="alert">
                                              Wewenang telah ditolak. {{ $anjab->keterangan_wewenang }}
                                            </div>
                                            @endif
                                          </div>
                                          <table class="table table-bordered">
                                              @foreach($anjab->dataWewenang as $itemWewenang)
                                              <tr>
                                              <td>{{ $itemWewenang->wewenang }}</td>
                                              </tr>
                                              @endforeach
                                          </table>

                                          @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                           <div class="text-right mt-1">
                                             <a data-anjab-id="{{ $anjab->id }}" data-jenis="wewenang" class="btn btn-success btn-sm btn-setujui">Setujui</a>
                                             <a data-anjab-id="{{ $anjab->id }}" data-jenis="wewenang" id="btn-tolak-wewenang" class="btn btn-danger btn-sm btn-tolak">Tolak</a>
                                           </div>
                                          @endif

                                        </div>

                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">5. Korelasi Jabatan:</h3>
                                        <div class="mb-3">
                                          <div id="status-korelasi-jabatan">
                                            @if($anjab->status_korelasi_jabatan == 'verifikasi')
                                            <div class="alert alert-success" role="alert">
                                              Korelasi jabatan telah disetujui.
                                            </div>
                                            @elseif($anjab->status_korelasi_jabatan == 'ditolak')
                                            <div class="alert alert-danger" role="alert">
                                              Korelasi jabatan telah ditolak. {{ $anjab->keterangan_korelasi_jabatan }}
                                            </div>
                                            @else
                                            <div class="alert alert-warning" role="alert">
                                              Menunggu Verifikasi
                                            </div>
                                            @endif
                                          </div>
                                         <table class="table table-bordered">
                                          <tr>
                                            <th>Jabatan</th>
                                            <th>Unit Kerja</th>
                                          </tr>
                                            @foreach($anjab->dataKorelasiJabatan as $itemKorelasiJabatan)
                                            <tr>
                                             <td>{{ $itemKorelasiJabatan->jabatan }}</td>
                                             <td>{{ $itemKorelasiJabatan->unit_kerja }}</td>
                                            </tr>
                                            @endforeach
                                         </table>
                                         <div class="collapse" id="alasan_tolak_korelasi_jabatan">
                                            <textarea class="form-control" placeholder="Alasan ditolak"></textarea>
                                          </div>

                                          @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                          <div class="text-right mt-1">
                                            
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="korelasi-jabatan" class="btn btn-success btn-sm btn-setujui">Setujui</a>
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="korelasi-jabatan" id="btn-tolak-korelasi-jabatan" class="btn btn-danger btn-sm btn-tolak">Tolak</a>
                                          </div>
                                          @endif
                                        </div>

                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">6. Bahan Kerja:</h3>
                                        <div class="mb-3">
                                          <div id="status-bahan-kerja">
                                            @if($anjab->status_bahan_kerja == 'verifikasi')
                                            <div class="alert alert-success" role="alert">
                                              Bahan kerja telah disetujui.
                                            </div>
                                            @elseif($anjab->status_bahan_kerja == 'ditolak')
                                            <div class="alert alert-danger" role="alert">
                                              Bahan kerja telah ditolak. {{ $anjab->keterangan_bahan_kerja }}
                                            </div>
                                            @else
                                            <div class="alert alert-warning" role="alert">
                                              Menunggu Verifikasi
                                            </div>
                                            @endif
                                          </div>
                                         <table class="table table-bordered">
                                          <tr>
                                            <th>Bahan Kerja</th>
                                            <th>Penggunaan</th>
                                          </tr>
                                            @foreach($anjab->dataBahanKerja as $itemBahanKerja)
                                            <tr>
                                             <td>{{ $itemBahanKerja->bahan_kerja }}</td>
                                             <td>{{ $itemBahanKerja->penggunaan_dalam_tugas }}</td>
                                            </tr>
                                            @endforeach
                                         </table>

                                         @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                         <div class="text-right mt-1">
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="bahan-kerja" class="btn btn-success btn-sm btn-setujui">Setujui</a>
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="bahan-kerja" id="btn-tolak-bahan-kerja" class="btn btn-danger btn-sm btn-tolak">Tolak</a>
                                         </div>
                                         @endif

                                        </div>

                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">7. Perangkat / Alat kerja:</h3>
                                        <div class="mb-3">
                                          <div id="status-perangkat">
                                            @if($anjab->status_perangkat == 'verifikasi')
                                            <div class="alert alert-success" role="alert">
                                              Perangkat / Alat kerja telah disetujui.
                                            </div>
                                            @elseif($anjab->status_perangkat == 'ditolak')
                                            <div class="alert alert-danger" role="alert">
                                              Perangkat / Alat kerja telah ditolak. {{ $anjab->keterangan_perangkat }}
                                            </div>
                                            @else
                                            <div class="alert alert-warning" role="alert">
                                              Menunggu Verifikasi
                                            </div>
                                            @endif
                                          </div>
                                         <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th>No</th>
                                              <th>Perangkat / Alat Kerja</th>
                                              <th>Digunakan Untuk Tugas</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                             @if($anjab->dataPerangkat->count() == 0)
                                              <tr>
                                                <td colspan="3" class="text-center">Tidak Ada Data</td>
                                              </tr>
                                            @endif
                                            @foreach($anjab->dataPerangkat as $perangkats)
                                              <tr>
                                                <td>{{ $loop->iteration }} </td>
                                                <td>{{ $perangkats->perangkat_kerja }}</td>
                                                <td>{{ $perangkats->digunakan_untuk_tugas }}</td>
                                              </tr>
                                            @endforeach
                                          </tbody>
                                         </table>

                                         @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                         <div class="text-right mt-1">
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="perangkat" class="btn btn-success btn-sm btn-setujui">Setujui</a>
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="perangkat" id="btn-tolak-perangkat" class="btn btn-danger btn-sm btn-tolak">Tolak</a>
                                         </div>
                                         @endif
                                        </div>

                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">8. Hasil kerja:</h3>
                                        <div class="mb-3">
                                          <div id="status-hasil-kerja">
                                            @if($anjab->status_hasil_kerja == 'verifikasi')
                                            <div class="alert alert-success" role="alert">
                                              Bahan kerja telah disetujui.
                                            </div>
                                            @elseif($anjab->status_hasil_kerja == 'ditolak')
                                            <div class="alert alert-danger" role="alert">
                                              Bahan kerja telah ditolak. {{ $anjab->keterangan_hasil_kerja }}
                                            </div>
                                            @else
                                            <div class="alert alert-warning" role="alert">
                                              Menunggu Verifikasi
                                            </div>
                                            @endif
                                          </div>
                                         <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th class="text-center">No</th>
                                              <th class="text-center">Hasil Kerja</th>
                                              <th class="text-center">Satuan Hasil</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                             @if($anjab->dataHasilKerja->count() == 0)
                                              <tr>
                                                <td colspan="8" class="text-center">Tidak Ada Data</td>
                                              </tr>
                                            @endif
                                            @foreach($anjab->dataHasilKerja as $hasil_kerjas)
                                              <tr>
                                                <td class="text-center">{{ $loop->iteration}} </td>
                                                <td>{{ $hasil_kerjas->hasil_kerja }}</td>
                                                <td class="text-center">{{ $hasil_kerjas->satuan_hasil }}</td>
                                              </tr>
                                            @endforeach
                                          </tbody>
                                         </table>

                                         @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                         <div class="text-right mt-1">
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="hasil-kerja" class="btn btn-success btn-sm btn-setujui">Setujui</a>
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="hasil-kerja" id="btn-tolak-hasil-kerja" class="btn btn-danger btn-sm btn-tolak">Tolak</a>
                                         </div>
                                         @endif

                                        </div>

                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">9. Kondisi Lingkungan Kerja:</h3>
                                        <div class="mb-3">
                                          <div id="status-kondisi-lingkungan-kerja">
                                            @if($anjab->status_kondisi_lingkungan_kerja == 'verifikasi')
                                            <div class="alert alert-success" role="alert">
                                              Bahan kerja telah disetujui.
                                            </div>
                                            @elseif($anjab->status_kondisi_lingkungan_kerja == 'ditolak')
                                            <div class="alert alert-danger" role="alert">
                                              Bahan kerja telah ditolak. {{ $anjab->keterangan_kondisi_lingkungan_kerja }}
                                            </div>
                                            @else
                                            <div class="alert alert-warning" role="alert">
                                              Menunggu Verifikasi
                                            </div>
                                            @endif
                                          </div>
                                         <table class="table table-bordered">
                                          <tbody>
                                            <tr>
                                              <td width="5%">a.</td>
                                              <td width="25%">Tempat Kerja</td>
                                              <td width="25%">
                                                @isset($anjab->dataLingkunganKerja)
                                                  {{ ucfirst(str_replace("_"," ",$anjab->dataLingkunganKerja->tempat_kerja)) }}
                                                @endisset
                                              </td>
                                            </tr>
                                            <tr>
                                              <td width="5%">b.</td>
                                               <td width="25%">Suhu</td>
                                               <td width="25%">
                                                 @isset($anjab->dataLingkunganKerja)
                                                  {{ ucfirst(str_replace("_"," ",$anjab->dataLingkunganKerja->suhu)) }}
                                                 @endisset
                                               </td>
                                            </tr>
                                            <tr>
                                              <td width="5%">c.</td>
                                              <td width="25%">Udara</td>
                                              <td width="25%">
                                                @isset($anjab->dataLingkunganKerja)
                                                  {{ ucfirst(str_replace("_"," ",$anjab->dataLingkunganKerja->udara)) }}
                                                @endisset
                                              </td>
                                            </tr>
                                            <tr>
                                              <td width="5%">d.</td>
                                              <td width="25%">Keadaan Ruangan</td>
                                              <td width="25%">
                                                @isset($anjab->dataLingkunganKerja)
                                                  {{ ucfirst(str_replace("_"," ",$anjab->dataLingkunganKerja->keadaan_ruangan)) }}
                                                @endisset
                                              </td>
                                            </tr>
                                            <tr>
                                              <td width="5%">e.</td>
                                              <td width="25%">Letak</td>
                                              <td width="25%">
                                                @isset($anjab->dataLingkunganKerja)
                                                  {{ ucfirst(str_replace("_"," ",$anjab->dataLingkunganKerja->letak)) }}
                                                @endisset
                                              </td>
                                            </tr>
                                            <tr>
                                              <td width="5%">f.</td>
                                              <td width="25%">Penerangan</td>
                                              <td width="25%">
                                                @isset($anjab->dataLingkunganKerja)
                                                  {{ ucfirst(str_replace("_"," ",$anjab->dataLingkunganKerja->penerangan)) }}
                                                @endisset
                                               </td>
                                            </tr>  
                                            <tr>
                                              <td width="5%">g.</td>
                                              <td width="25%">Suara</td>
                                              <td width="25%">
                                                @isset($anjab->dataLingkunganKerja)
                                                  {{ ucfirst(str_replace("_"," ",$anjab->dataLingkunganKerja->suara)) }}
                                                @endisset
                                              </td>
                                            </tr>  
                                            <tr>
                                              <td width="5%">h.</td>
                                              <td width="25%">Keadaan Tempat Kerja</td>
                                              <td width="25%">
                                                @isset($anjab->dataLingkunganKerja)
                                                  {{ ucfirst(str_replace("_"," ",$anjab->dataLingkunganKerja->keadaan_tempat_kerja)) }}
                                                @endisset
                                              </td>
                                            </tr>
                                            <tr>
                                              <td width="5%">i.</td>
                                              <td width="25%">Getaran</td>
                                              <td width="25%">
                                                @isset($anjab->dataLingkunganKerja)
                                                  {{ ucfirst(str_replace("_"," ",$anjab->dataLingkunganKerja->getaran)) }}
                                                @endisset
                                              </td>
                                            </tr>
                                            </tr>
                                          </tbody>
                                         </table>

                                         @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                         <div class="text-right mt-1">
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="kondisi-lingkungan-kerja" class="btn btn-success btn-sm btn-setujui">Setujui</a>
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="kondisi-lingkungan-kerja" id="btn-tolak-kondisi-lingkungan-kerja" class="btn btn-danger btn-sm btn-tolak">Tolak</a>
                                         </div>
                                         @endif

                                        </div>

                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">10. Resiko Bahaya:</h3>
                                        <div class="mb-3">
                                          <div id="status-resiko-bahaya">
                                            @if($anjab->status_resiko_bahaya == 'verifikasi')
                                            <div class="alert alert-success" role="alert">
                                              Bahan kerja telah disetujui.
                                            </div>
                                            @elseif($anjab->status_resiko_bahaya == 'ditolak')
                                            <div class="alert alert-danger" role="alert">
                                              Bahan kerja telah ditolak. {{ $anjab->keterangan_resiko_bahaya }}
                                            </div>
                                            @else
                                            <div class="alert alert-warning" role="alert">
                                              Menunggu Verifikasi
                                            </div>
                                            @endif
                                          </div>
                                         <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th class="text-center">No</th>
                                              <th class="text-center">Resiko Bahaya</th>
                                              <th class="text-center">Penyebab</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                             @if($anjab->dataResikoBahaya->count() == 0)
                                              <tr>
                                                <td colspan="8" class="text-center">Tidak Ada Data</td>
                                              </tr>
                                            @endif
                                            @foreach($anjab->dataResikoBahaya as $resiko_bahayas)
                                              <tr>
                                                <td class="text-center">{{ $loop->iteration }} </td>
                                                <td>{{ $resiko_bahayas->resiko_bahaya }}</td>
                                                <td>{{ $resiko_bahayas->penyebab }}</td>
                                              </tr>
                                            @endforeach
                                          </tbody>
                                         </table>

                                         @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                         <div class="text-right mt-1">
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="resiko-bahaya" class="btn btn-success btn-sm btn-setujui">Setujui</a>
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="resiko-bahaya" id="btn-tolak-resiko-bahaya" class="btn btn-danger btn-sm btn-tolak">Tolak</a>
                                         </div>
                                         @endif
                                        </div>

                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">11. Syarat Jabatan:</h3>
                                        <div class="mb-3">
                                          <div id="status-syarat-jabatan">
                                            @if($anjab->status_syarat_jabatan == 'verifikasi')
                                            <div class="alert alert-success" role="alert">
                                              Telah disetujui.
                                            </div>
                                            @elseif($anjab->status_syarat_jabatan == 'ditolak')
                                            <div class="alert alert-danger" role="alert">
                                              Ditolak. {{ $anjab->keterangan_syarat_jabatan }}
                                            </div>
                                            @else
                                            <div class="alert alert-warning" role="alert">
                                              Menunggu Verifikasi
                                            </div>
                                            @endif
                                          </div>
                                          <div class="form-group">
                                            <label>Keterampilan Kerja</label>
                                            <textarea disabled class="form-control" rows="4">@isset($anjab->dataSyaratJabatan){{ $anjab->dataSyaratJabatan->keterampilan_kerja}}@endisset
                                            </textarea> 
                                          </div>
                                          <hr>
                                            <p>Bakat Kerja :</p>
                                            <table class="table">
                                              <thead class="thead-light">
                                                <tr>
                                                  <th colspan="2" class="text-center">Bakat Kerja</th>
                                                  <th>&#10003;</th>
                                                </tr>
                                              </thead>
                                              <tbody>
  
                                                @if(empty($anjab->dataSyaratJabatan->bakat_kerja) || $anjab->dataSyaratJabatan == null)
                                                <tr>
                                                  <td class="text-center" colspan="3">Tidak Ada Data</td>
                                                </tr>
                                                @else
                                                  @foreach($bakatKerja as $itemBakatKerja)
                                                  @if(@in_array($itemBakatKerja->id, $anjab->dataSyaratJabatan->bakat_kerja))
                                                  <tr>
                                                    <td>{{ $itemBakatKerja->nama }}</td>
                                                    <td>{{ $itemBakatKerja->keterangan }}</td>
                                                    <td>&#10003;
                                                    </td>
                                                  </tr>
                                                  @endif
                                                  @endforeach
  
                                                @endif
                                                
                                              </tbody>
                                            </table>
                                            <br>
                                            <p>Tempramen Kerja :</p>
                                            <table class="table">
                                              <thead class="thead-light">
                                                <tr>
                                                  <th colspan="2" class="text-center">Temperamen Kerja</th>
                                                  <th>&#10003;</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @if(empty($anjab->dataSyaratJabatan->temperamen_kerja))
                                                <tr>
                                                  <td class="text-center" colspan="3">Tidak Ada Data</td>
                                                </tr>
                                                @else
                                                  @foreach($temperamenKerja as $itemTemperamenKerja)
                                                @if(@in_array($itemTemperamenKerja->id, $anjab->dataSyaratJabatan->temperamen_kerja))
                                                <tr>
                                                  <td>{{ $itemTemperamenKerja->nama  }}</td>
                                                  <td>{{ $itemTemperamenKerja->keterangan  }}</td>
                                                  <td>&#10003;</td>
                                                </tr>
                                                @endif
                                                @endforeach
                                                @endif
                                                
                                                
                                              </tbody>
                                            </table>
  
                                            <br>
                                            <p>Minat Kerja :</p>
                                            <table class="table">
                                              <thead class="thead-light">
                                                <tr>
                                                  <th colspan="2" class="text-center">Minat Kerja</th>
                                                  <th>&#10003;</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @if(empty($anjab->dataSyaratJabatan->minat_kerja))
                                                <tr>
                                                  <td class="text-center" colspan="3">Tidak Ada Data</td>
                                                </tr>
                                                @else
                                                  @foreach($minatKerja as $itemMinatKerja)
                                                @if(@in_array($itemMinatKerja->id, $anjab->dataSyaratJabatan->minat_kerja))
                                                <tr>
                                                  <td>{{ $itemMinatKerja->nama }}</td>
                                                  <td>{{ $itemMinatKerja->keterangan }}</td>
                                                  <td>&#10003;</td>
                                                </tr>
                                                 @endif
                                                @endforeach
                                                @endif
                                                
                                                
                                              </tbody>
                                            </table>
                                            <p>Kondisi Fisik :</p>
                                            <table class="table table-striped">
                                              <tbody>
                                                <tr>
                                                  <td>
                                                    Jenis Kelamin
                                                  </td>
                                                  <td>
                                                    :
                                                  </td>
                                                  <td>
                                                    @isset($anjab->dataSyaratJabatan)
                                                    {{ $anjab->dataSyaratJabatan->text_jenis_kelamin }}
                                                     @endisset
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    Umur
                                                  </td>
                                                  <td>
                                                    :
                                                  </td>
                                                  <td>
                                                    @isset($anjab->dataSyaratJabatan)
                                                    {{ $anjab->dataSyaratJabatan->text_umur }}
                                                     @endisset
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    Tinggi Badan
                                                  </td>
                                                  <td>
                                                    :
                                                  </td>
                                                  <td>
                                                    @isset($anjab->dataSyaratJabatan)
                                                    {{ $anjab->dataSyaratJabatan->text_tinggi_badan }}
                                                     @endisset
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    Berat Badan
                                                  </td>
                                                  <td>
                                                    :
                                                  </td>
                                                  <td>
                                                    @isset($anjab->dataSyaratJabatan)
                                                    {{ $anjab->dataSyaratJabatan->text_berat_badan }}
                                                     @endisset
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    Postur Badan
                                                  </td>
                                                  <td>
                                                    :
                                                  </td>
                                                  <td>
                                                    @isset($anjab->dataSyaratJabatan)
                                                    {{ ucfirst($anjab->dataSyaratJabatan->postur_badan) }}
                                                     @endisset
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    Penampilan
                                                  </td>
                                                  <td>
                                                    :
                                                  </td>
                                                  <td>
                                                    @isset($anjab->dataSyaratJabatan)
                                                    {{ ucfirst($anjab->dataSyaratJabatan->penampilan) }}
                                                     @endisset
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
  
                                            <hr>
                                            <p>Upaya Fisik  :</p>
                                            <table class="table">
                                              <thead class="thead-light">
                                                <tr>
                                                  <th colspan="2" class="text-center">Upaya Fisik</th>
                                                  <th>&#10003;</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @if(empty($anjab->dataSyaratJabatan->upaya_fisik))
                                                <tr>
                                                  <td class="text-center" colspan="3">Tidak Ada Data</td>
                                                </tr>
                                                @else
                                                  @foreach($upayaFisik as $itemUpayaFisik)
                                                @if(@in_array($itemUpayaFisik->id, $anjab->dataSyaratJabatan->upaya_fisik))
                                                <tr>
                                                  <td>{{ $itemUpayaFisik->nama }}</td>
                                                  <td>{{ $itemUpayaFisik->keterangan }}.</td>
                                                  <td>
                                                      &#10003;
                                                  </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                                @endif
                                                
                                                
                                              </tbody>
                                            </table>
  
                                            <br>
                                            <p>Fungsi Pekerjaan  :</p>
                                            <table class="table">
                                              <thead class="thead-light">
                                                <tr>
                                                  <th colspan="2" class="text-center">Fungsi Pekerjaan</th>
                                                  <th>&#10003;</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @if(empty($anjab->dataSyaratJabatan->fungsi_pekerjaan))
                                                <tr>
                                                  <td class="text-center" colspan="3">Tidak Ada Data</td>
                                                </tr>
                                                @else
                                                  @foreach($fungsiPekerjaan as $itemFungsiPekerjaan)
                                                @if(@in_array($itemFungsiPekerjaan->id, $anjab->dataSyaratJabatan->fungsi_pekerjaan))
                                                <tr>
                                                  <td>{{ $itemFungsiPekerjaan->nama }}</td>
                                                  <td>
                                                    {{ $itemFungsiPekerjaan->keterangan }}
                                                  </td>
                                                  <td>&#10003;</td>
                                                </tr>
                                                @endif
                                                @endforeach
                                                @endif
                                                
                                                
                                              </tbody>
                                            </table>

                                          @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                           <div class="text-right mt-1">
                                            <a data-anjab-id="{{ $anjab->id }}" data-jenis="syarat-jabatan" class="btn btn-success btn-sm btn-setujui">Setujui</a>
                                            <a data-anjab-id="{{ $anjab->id }}" data-jenis="syarat-jabatan" id="btn-tolak-syarat-jabatan" class="btn btn-danger btn-sm btn-tolak">Tolak</a>
                                           </div>
                                          @endif
                                        </div>

                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">12. Prestasi Kerja:</h3>
                                        <div class="mb-3">
                                          <div id="status-prestasi-kerja">
                                            @if($anjab->status_prestasi_kerja == 'verifikasi')
                                            <div class="alert alert-success" role="alert">
                                              Telah disetujui.
                                            </div>
                                            @elseif($anjab->status_prestasi_kerja == 'ditolak')
                                            <div class="alert alert-danger" role="alert">
                                              Ditolak. {{ $anjab->keterangan_prestasi_kerja }}
                                            </div>
                                            @else
                                            <div class="alert alert-warning" role="alert">
                                              Menunggu Verifikasi
                                            </div>
                                            @endif
                                          </div>
                                         <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th>No</th>
                                              <th>Prestasi Kerja</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                             @if($anjab->dataPrestasiKerja->count() == 0)
                                              <tr>
                                                <td colspan="3" class="text-center">Tidak Ada Data</td>
                                              </tr>
                                            @endif
                                            @foreach($anjab->dataPrestasiKerja as $prestasi_kerjas)
                                              <tr>
                                                <td>{{ $loop->iteration }} </td>
                                                <td>{{ $prestasi_kerjas->prestasi_kerja }}</td>
                                              </tr>
                                            @endforeach
                                          </tbody>
                                         </table>


                                         @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                         <div class="text-right mt-1">
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="prestasi-kerja" class="btn btn-success btn-sm btn-setujui">Setujui</a>
                                          <a data-anjab-id="{{ $anjab->id }}" data-jenis="prestasi-kerja" id="btn-tolak-prestasi-kerja" class="btn btn-danger btn-sm btn-tolak">Tolak</a>
                                         </div>
                                         @endif

                                        </div>

                                        


                                       </div>
                                    </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--end::Example-->
                  </div>
                </div>
                <!--end::Card-->
                @endif
              </div>
              <!--end::Container-->

              <div id="modal-tolak" class="modal fade">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title text-white"></h5> 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="">
                    {{ csrf_field() }}
                    <div class="modal-body">
                      
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button id="btn-simpan-tolak" type="button" class="btn btn-danger btn-sm">Tolak</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div id="modal-tahapan" class="modal fade">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5> 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('anjab.save-uraian-tahapan') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                      
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>


          <form id="form-delete-uraian" method="POST" action="{{ route('anjab.delete-uraian') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>

          <div class="modal fade text-left" id="modal" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-xl" role="document">
                  <div class="modal-content" style="width: 1200px;">
                      <div class="modal-header">
                          <h4 class="modal-title"></h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div class="modal-body">
                          
                      </div>
                      
                  </div>
              </div>
          </div>
@endsection

@section('js')
{{-- js files --}}
 <script src="{{ asset('') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">

  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

  var verifikasi = function(jenis, id, status, keterangan){
    $.ajax({
      url:'{{ route("anjab.verifikasi") }}',
      method:'POST',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},  
      data:{jenis:jenis, id:id, status:status, keterangan:keterangan},
      success: function(result){
        switch(status) {
          case 'verifikasi':
            toastr.success("Data telah tersimpan");
            $('#status-'+jenis).html('<div class="alert alert-success" role="alert">'+jenis+' telah disetujui.</div>');
            break;
          case 'ditolak':
          toastr.success("Data berhahsil ditolak");
            $('#status-'+jenis).html('<div class="alert alert-danger" role="alert">'+jenis+' telah ditolak. '+keterangan+'</div>');
            break;
          default:
            // code block
        }
      },
      error: function(e){
        toastr.error("Terjadi Kesalahan Pada Server. Harap Hubungi Admin Aplikasi");
      }
    });
  }

  $('.btn-setujui').on('click', function(){
    var jenis = $(this).data('jenis');
    var id = $(this).data('anjab-id');
    var status = 'verifikasi';

    verifikasi(jenis, id, status, '');
    
  });

  $('#btn-simpan-tolak').on('click', function(){
    var id = $('#modal-tolak input[name=id]').val();
    var jenis = $('#modal-tolak input[name=jenis]').val();
    var status = 'ditolak';
    var keterangan = $('#modal-tolak textarea[name=keterangan]').val();

    verifikasi(jenis, id, status, keterangan);
    $('#modal-tolak').modal('hide');
  });

  $('.btn-tolak').on('click', function(){

    var jenis = $(this).data('jenis');
    var id = $(this).data('anjab-id');

    $.ajax({
      url:'{{ route('anjab.load-modal-tolak') }}',
      data:{id:id, jenis:jenis},
      method:'GET',
      success: function(result){
        $('#modal-tolak .modal-title').text("Tolak");
        $('#modal-tolak .modal-body').html(result);
        $('#modal-tolak').modal('show');
      }
    })
    
  });

  $('.add-data').on('click', function(){
    var id = $('#id').val();
    $.ajax({
      url:'{{ route("anjab.get-form-uraian") }}',
      method:'GET',
      data:{aksi:'add-data',id:id },
      success:function(result){
        $('#modal-uraian .modal-title').text("Tambah Anjab Uraian");
        $('#modal-uraian .modal-body').html(result);
        $('#modal-uraian').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.add-tahapan').on('click', function(){
    var id_anjab = $('#id').val();
    var id_uraian = $(this).data('id');
    $.ajax({
      url:'{{ route("anjab.get-form-uraian-tahapan") }}',
      method:'GET',
      data:{aksi:'add-tahapan',id_anjab:id_anjab,id_uraian:id_uraian },
      success:function(result){
        $('#modal-tahapan .modal-title').text("Tambah Anjab Tahapan ");
        $('#modal-tahapan .modal-body').html(result);
        $('#modal-tahapan').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.edit-uraian').on('click', function(){
    var id = $(this).data('id');
    $.ajax({
      url:'{{ route("anjab.get-form-uraian") }}',
      method:'GET',
      data:{aksi:'edit-uraian',id:id },
      success:function(result){
        $('#modal-uraian .modal-title').text("Edit Uraian Anjab");
        $('#modal-uraian .modal-body').html(result);
        $('#modal-uraian').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.delete-uraian').on('click', function(){
    var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin?',
            // text: "Anda tidak dapat membatalkan pengajuan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus Data',
            cancelButtonText: 'Batal',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-warning',
            buttonsStyling: false
        }).then(function (status) {
          if(status.value){
            $('form[id=form-delete-uraian] input[name=id]').val(id);
                $('form[id=form-delete-uraian]').submit();
          }
        });
  });
</script>
@endsection