{{-- <style type="text/css">
	.select2-selection--single{
		min-width: 450px;
	}
</style> --}}

<div class="row">
	<div style="display: none;" id="alert-anjab" class="col-12">
		<div class="alert alert-danger">
			
		</div>
	</div>
	<div class="col-4">
		<input type="hidden" value="create" name="aksi">
		<input type="hidden" name="pd" value="{{ \MojokertokabUser::getUser()->kode_opd }}">

		<div class="form-group">
			<label>Jenis Jabatan</label>
			{{-- <input type="text" class="form-control" name="jenis_jabatan" required=""> --}}
			<select id="jenis" required="" class="form-control" name="jenis_jabatan">
				<option selected="" disabled="" value="">PILIH JENIS JABATAN</option>
				<option value="1">Struktural</option>
				<option value="2">Fungsional</option>
				<option value="3">Pelaksana</option>
			</select>
		</div>

		<div class="form-group">
			<label>Nama Jabatan</label>
			<select disabled style="width: 100%" class="form-control select2" id="jabatan" name="jabatan">
				<option label="Label"></option>
			</select>
		</div>

		{{-- <div class="form-group">
			<label>Unit Kerja</label>
			<select disabled style="width: 100%" required="" name="pd" class="form-control select2">
				<option value="" disabled="" selected="">-- PILIH PERANGKAT DAERAH --</option>
				@foreach($unitKerja as $unitKerjas)
				<option value="{{ $unitKerjas->kodeunit }}">{{ $unitKerjas->unitkerja }}</option>
				@endforeach
			</select>
			<p class="form-control-plaintext">{{ $unitKerja->unitkerja }}</p>
		</div> --}}

		

		<div class="form-group">
			<label>Eselon Jabatan (Optional)</label>
			<br>
			<select disabled class="form-control " name="eselon_jabatan">
				<option value="" selected="">-- TANPA ESELON --</option>
				<option value="11">Ia</option>
				<option value="12">Ib</option>
				<option value="21">IIa</option>
				<option value="22">IIb</option>
				<option value="31">IIIa</option>
				<option value="32">IIIb</option>
				<option value="41">IVa</option>
				<option value="42">IVb</option>
			</select>
		</div>

		<div class="form-group">
			<label>Kode Jabatan (Otomatis)</label>
			<input readonly type="text" class="form-control" name="kode_jabatan">
		</div>

	</div>
	<div class="col-4">

		<div class="form-group">
			<label>Atasan Langsung</label>
			<select disabled style="width: 100%" required="" name="atasan_langsung" class="form-control" id="atasan_langsung">
				<option value="" disabled="" selected="">-- PILIH ATASAN --</option>
				@foreach($pejabatStruktural as $itemPejabat)
				<option value="{{ $itemPejabat->id }}">{{ @$itemPejabat->jabatan->jabatan }}</option>
				@endforeach
			</select>
		</div>

		@if(MojokertokabUser::getUser()->kode_opd >= 31 && MojokertokabUser::getUser()->kode_opd <= 48)
		<div class="form-group">
			<label>Unit Kerja Eselon 3 (Optional)</label>
			<select disabled style="width: 100%" required name="unit_kerja_eselon3" class="form-control select2" id="unit_kerja_eselon3" >
				<option value="" disabled="" selected="">-- PILIH UNIT KERJA ESELON 3 --</option>
				@foreach($unitKerja as $unitKerjas)
				<option value="{{ $unitKerjas->kodeunit }}">{{ $unitKerjas->unitkerja }}</option>
				@endforeach
			</select>
			<!-- <input name="unit_kerja_eselon3" class="form-control"> -->
		</div>

		<div class="form-group">
			<label>Unit Kerja Eselon 4 (Optional)</label>
			<select disabled style="width: 100%" name="unit_kerja_eselon4" class="form-control select2" id="unit_kerja_eselon4" >
				<option value="" disabled="" selected="">-- PILIH UNIT KERJA ESELON 4 --</option>
			</select>
			<!-- <input name="unit_kerja_eselon4" class="form-control"> -->
		</div>
		@else
		<div class="form-group">
			<label>Unit Kerja Eselon 2</label>
			<input type="hidden" value="{{ $unitKerja->kodeunit }}" name="unit_kerja_eselon2">
			{{-- <select disabled style="width: 100%" required="" name="unit_kerja_eselon2" class="form-control select2" id="unit_kerja_eselon2">
				<option value="" disabled="" selected="">-- PILIH UNIT KERJA ESELON 2 --</option>
				@foreach($unitKerja as $unitKerjas)
				<option value="{{ $unitKerjas->kodeunit }}">{{ $unitKerjas->unitkerja }}</option>
				@endforeach
			</select> --}}
			<p class="form-control-plaintext">{{ $unitKerja->unitkerja }}</p>
		</div>
		<div class="form-group">
			<label>Unit Kerja Eselon 3 (Optional)</label>
			<select disabled style="width: 100%" name="unit_kerja_eselon3" class="form-control select2" id="unit_kerja_eselon3" >
				<option value="" selected="">-- PILIH UNIT KERJA ESELON 3 --</option>
				@foreach($unitKerjaEselon3 as $itemUnitkerja)
				<option value="{{ $itemUnitkerja->kodeunit }}">{{ $itemUnitkerja->unitkerja }}</option>
				@endforeach
			</select>
			<!-- <input name="unit_kerja_eselon3" class="form-control"> -->
		</div>

		<div class="form-group">
			<label>Unit Kerja Eselon 4 (Optional)</label>
			<select disabled style="width: 100%" name="unit_kerja_eselon4" class="form-control select2" id="unit_kerja_eselon4" >
				<option value="" selected="">-- PILIH UNIT KERJA ESELON 4 --</option>
			</select>
			<!-- <input name="unit_kerja_eselon4" class="form-control"> -->
		</div>
		@endif
		
	</div>

	<div class="col-4">
		<div class="form-group">
			<label>Pangkat / Golongan</label>
			<br>
			<select disabled required="" class="form-control " name="pangkat">
				<option value="" disabled="" selected="">-- PANGKAT GOLONGAN --</option>
				<option value="11">Ia</option>
				<option value="12">Ib</option>
				<option value="13">Ic</option>
				<option value="14">Id</option>
				<option value="21">IIa</option>
				<option value="22">IIb</option>
				<option value="23">IIc</option>
				<option value="24">IId</option>
				<option value="31">IIIa</option>
				<option value="32">IIIb</option>
				<option value="33">IIIc</option>
				<option value="33">IIId</option>
				<option value="41">IVa</option>
				<option value="42">IVb</option>
				<option value="43">IVc</option>
				<option value="44">IVd</option>
				<option value="45">IVe</option>
			</select>
		</div>

		<div class="form-group">
			<label>Kualifikasi Pendidikan</label>
			<textarea disabled required="" rows="3" required="" name="kualifikasi_pendidikan" class="form-control"></textarea> 
		</div>

		<div class="form-group">
			<label>Diklat Penjenjangan</label>
			<textarea disabled required="" rows="2" required="" name="diklat_penjenjangan" class="form-control"></textarea> 
		</div>

		<div class="form-group">
			<label>Diklat Teknis</label>
			<textarea disabled required="" rows="2" required="" name="diklat_teknis" class="form-control"></textarea> 
		</div>

		<div class="form-group">
			<label>Pengalaman Kerja</label>
			<textarea disabled required="" rows="2" required="" name="pengalaman_kerja" class="form-control"></textarea> 
		</div>

		<div class="form-group">
			<label>Pengetahuan Kerja</label>
			<textarea disabled required="" rows="2" required="" name="pengetahuan_kerja" class="form-control"></textarea> 
		</div>

	</div>

	<div class="col-12">
		<div class="form-group">
			<label>Ikhtisar Jabatan</label>
			<textarea rows="6" required="" name="ikhtisar_jabatan" class="form-control"></textarea> 
		</div>
	</div>

</div>

<script type="text/javascript">

	$('#jenis').on('change', function(){

		var jenis = $(this).val();
		if(jenis == 3){
			$('textarea[name=diklat_penjenjangan]').prop('disabled', false);
			$('select[name=pangkat]').prop('disabled', true);
		}else if(jenis == 2){
			$('textarea[name=kualifikasi_pendidikan]').prop('disabled', false);
			$('textarea[name=diklat_penjenjangan]').prop('disabled', false);
			$('select[name=pangkat]').prop('disabled', false);
		}else if(jenis == 1){
			$('textarea[name=kualifikasi_pendidikan]').prop('disabled', false);
			$('textarea[name=diklat_penjenjangan]').prop('disabled', true);
			$('select[name=pangkat]').prop('disabled', false);
		}

		
		$('textarea[name=diklat_teknis]').prop('disabled', false);
		$('textarea[name=pengalaman_kerja]').prop('disabled', false);
		$('textarea[name=pengetahuan_kerja]').prop('disabled', false);

		$('#jabatan').prop('disabled', false);
		$('select[name=pd]').prop('disabled', false);
		$('select[name=eselon_jabatan]').prop('disabled', false);
		$('select[name=unit_kerja_eselon3]').prop('disabled', false);
		$('select[name=atasan_langsung]').prop('disabled', false);
		$('textarea[name=ikhtisar_jabatan]').prop('disabled', false);
		
	});

	$('select[name=pd]').on('change', function(){
		var kode_opd = $(this).val();
		$.ajax({
			url:'{{ route("anjab.get-atasan-by-opd") }}',
			method:'GET',
			data:{opd: kode_opd},
			success: function(result){
				$('#atasan_langsung').prop('disabled', false);
				$('#atasan_langsung').html(result);
			}
		});
	});

	$("#unit_kerja_eselon2").change(function() {
      var dats = 'id='+ $(this).val();
      $.ajax({
        type: "GET",
        url: '{{ route("anjab.get-unit-kerja") }}',
        data: dats,
        cache: false,
        success: function(html) {
          $("#unit_kerja_eselon3").html(html);
        }
      });
    });

    $("#unit_kerja_eselon3").change(function() {
      var dats = 'id='+ $(this).val();
      $.ajax({
        type: "GET",
        url: '{{ route("anjab.get-unit-kerja") }}',
        data: dats,
        cache: false,
        success: function(html) {
          $("#unit_kerja_eselon4").html(html);
          $('select[name=unit_kerja_eselon4]').prop('disabled', false);
        }
      });
    });

    // Initialization
	jQuery(document).ready(function() {

		$('#jabatan').on('select2:select', function (e) {
		    var data = e.params.data;
		    console.log(data);
		    $('textarea[name=ikhtisar_jabatan]').val(data.ikhtisar_jabatan);
		    $('textarea[name=kualifikasi_pendidikan]').val(data.kualifikasi_pendidikan);
		    $('textarea[name=diklat_penjenjangan]').val(data.diklat_penjenjangan);
		    $('input[name=kode_jabatan]').val(data.kode_jabatan);

		    if($('#jenis').val() == 3){
		    	$('select[name=pangkat]').val(data.pangkat);
		    }
		});

	    function formatRepo(repo) {
	        if (repo.loading) return repo.text;

	        var markup =
	          "<div class='select2-result-repository clearfix'>" +
	            "<div class='select2-result-repository__meta'>" +
	            "<div class='select2-result-repository__title'>" +
	            "<div>"+repo.jabatan+"</div>"+
	          '</div></div></div>';

	        return markup;
	    }

	    function formatRepoSelection(repo) {
	        return repo.full_name || repo.text;
	    }

	    $("#jabatan").select2({
	        placeholder: "Cari Jabatan",
	        allowClear: true,
	        ajax: {
	            url: '{{ route("anjab.get-jabatan") }}',
	            dataType: 'json',
	            delay: 250,
	            data: function(params) {
	                return {
	                		jenis: $('#jenis').val(),
	                    q: params.term, // search term
	                    page: params.page
	                };
	            },
	            processResults: function(data, params) {
	                // parse the results into the format expected by Select2
	                // since we are using custom formatting functions we do not need to
	                // alter the remote JSON data, except to indicate that infinite
	                // scrolling can be used
	                params.page = params.page || 1;

	                return {
	                    results: data.items,
	                    pagination: {
	                        more: (params.page * 30) < data.total_count
	                    }
	                };
	            },
	            cache: true
	        },
	        escapeMarkup: function(markup) {
	            return markup;
	        }, // let our custom formatter work
	        minimumInputLength: 1,
	        templateResult: formatRepo, // omitted for brevity, see the source of this page
	        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
	    });
	});

</script>