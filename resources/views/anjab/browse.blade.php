@extends('layout')

@section('content')

<link rel="stylesheet" href="{{ asset('treant-js') }}/Treant.css">
<link rel="stylesheet" href="{{ asset('treant-js') }}/examples/basic-example/basic-example.css">

<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->

                @include('kunci')

                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                          <ul class="nav nav-light-danger nav-bold nav-pills">
                            @if(Str::startsWith(@$pd->kodeunit, '28') && Str::contains(strtolower(@$pd->unitkerja), 'puskesmas'))
                                <!-- Hanya Menampilkan Fungsional & Pelaksana -->
                                <li class="nav-item">
                                    <a class="nav-link {{ $jenis == '2'?'active':'' }}" href="{{ route('anjab.browse', ['jenis'=>'2']) }}">
                                        <span class="nav-icon">
                                            <i class="flaticon2-drop"></i>
                                        </span>
                                        <span class="nav-text">Fungsional &nbsp;</span>
                                        {{-- <span class="badge badge-warning">Baru</span> --}}
                                      </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ $jenis == '3'?'active':'' }}" href="{{ route('anjab.browse', ['jenis'=>'3']) }}">
                                        <span class="nav-icon">
                                            <i class="flaticon2-drop"></i>
                                        </span>
                                        <span class="nav-text">Pelaksana</span>
                                    </a>
                                </li>
                            @else
                                <!-- Menampilkan Semua Menu -->
                                <li class="nav-item">
                                    <a class="nav-link {{ $jenis == '1'?'active':'' }}" href="{{ route('anjab.browse', ['jenis'=>'1']) }}">
                                        <span class="nav-icon">
                                            <i class="flaticon2-chat-1"></i>
                                        </span>
                                        <span class="nav-text">Struktural</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ $jenis == '2'?'active':'' }}" href="{{ route('anjab.browse', ['jenis'=>'2']) }}">
                                        <span class="nav-icon">
                                            <i class="flaticon2-drop"></i>
                                        </span>
                                        <span class="nav-text">Fungsional &nbsp;</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ $jenis == '3'?'active':'' }}" href="{{ route('anjab.browse', ['jenis'=>'3']) }}">
                                        <span class="nav-icon">
                                            <i class="flaticon2-drop"></i>
                                        </span>
                                        <span class="nav-text">Pelaksana</span>
                                    </a>
                                </li>
                            @endif

                            {{-- <li class="nav-item dropdown">
                              <a class="nav-link {{ $jenis == '2'?'active':'' }}" href="{{ route('anjab.browse', ['jenis'=>'2']) }}" role="button">
                                <span class="nav-icon">
                                  <i class="flaticon2-gear"></i>
                                </span>
                                <span class="nav-text">Fungsional</span>
                              </a>
                            </li> --}}
                          </ul>
                        </div>
                    <div class="card-toolbar">
                     
                      @if(\MojokertokabApp::allowChangingData())
                      <!--begin::Button-->
                      
                        {{-- @if($selectedUnitKerja == null)
                        @else --}}
                        {{-- menghilangkan button jika memilih puskesmas --}}
                        @if(Str::startsWith(@$pd->kodeunit, '28') && Str::contains(strtolower(@$pd->unitkerja), 'puskesmas') && $jenis == '1')
                        @else
                            @if(empty($selectedPuskesmas) || $selectedPuskesmas === '28')
                              <a data-jenis-jabatan="{{ $jenis }}" data-kode-opd="{{ @$pd->kodeunit }}" href="javascript:;" class="btn btn-primary btn-sm font-weight-bolder add-anjab" id="btnTambahAnalis">
                                  <i class="far fa-plus-square"></i> Tambah Analisis Jabatan
                                  @if($jenis == '1') Struktural
                                  @elseif($jenis == '2') Fungsional
                                  @elseif($jenis == '3') Pelaksana
                                  @endif
                              </a>
                            @endif
                          @endif
                        {{-- @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                        @else
                        {{ 'Masih Perbaikan Sebentar Nggih..' }}
                        @endif --}}
  
                        {{-- @endif --}}

                      {{-- <a data-jenis-jabatan="{{ $jenis }}" data-kode-opd="{{ @$pd->kodeunit }}" href="javascript:;" class="btn btn-primary btn-sm font-weight-bolder add-anjab" id="btnTambahAnalis">
                       <i class="far fa-plus-square"></i>Tambah Analisis Jabatan
                       @if($jenis == '1') Struktural
                       @elseif($jenis == '2') Fungsional
                       @elseif($jenis == '3') Pelaksana
                       @endif</a> --}}
                          
                      @endif
                      <!--end::Button-->
                     
                      {{-- <a href="javascript:;" class="btn btn-success ml-1">Peta Jabatan</a> --}}
                    </div>
                  </div>
                  <div class="card-body">

                    {{-- <div class="row">
                      <div class="col-lg-2">
                        <div class="card card-custom bg-danger gutter-b" style="height: 130px">
                          <!--begin::Body-->
                          <div class="card-body d-flex flex-column p-0">
                            <!--begin::Stats-->
                            <div class="flex-grow-1 card-spacer-x pt-6">
                              <div class="text-inverse-danger font-weight-bold">Jabatan Struktural</div>
                              <div class="text-inverse-danger font-weight-bolder font-size-h3">3,620</div>
                            </div>
                            <!--end::Stats-->
                            <!--begin::Chart-->
                            <div id="kt_tiles_widget_2_chart" class="card-rounded-bottom" style="height: 50px"></div>
                            <!--end::Chart-->
                          </div>
                          <!--end::Body-->
                        </div>
                      </div>

                      <div class="col-lg-2">
                        <div class="card card-custom bg-danger gutter-b" style="height: 130px">
                          <!--begin::Body-->
                          <div class="card-body d-flex flex-column p-0">
                            <!--begin::Stats-->
                            <div class="flex-grow-1 card-spacer-x pt-6">
                              <div class="text-inverse-danger font-weight-bold">Jabatan Struktural</div>
                              <div class="text-inverse-danger font-weight-bolder font-size-h3">3,620</div>
                            </div>
                            <!--end::Stats-->
                            <!--begin::Chart-->
                            <div id="kt_tiles_widget_2_chart" class="card-rounded-bottom" style="height: 50px"></div>
                            <!--end::Chart-->
                          </div>
                          <!--end::Body-->
                        </div>
                      </div>

                      <div class="col-lg-2">
                        <div class="card card-custom bg-danger gutter-b" style="height: 130px">
                          <!--begin::Body-->
                          <div class="card-body d-flex flex-column p-0">
                            <!--begin::Stats-->
                            <div class="flex-grow-1 card-spacer-x pt-6">
                              <div class="text-inverse-danger font-weight-bold">Jabatan Struktural</div>
                              <div class="text-inverse-danger font-weight-bolder font-size-h3">3,620</div>
                            </div>
                            <!--end::Stats-->
                            <!--begin::Chart-->
                            <div id="kt_tiles_widget_2_chart" class="card-rounded-bottom" style="height: 50px"></div>
                            <!--end::Chart-->
                          </div>
                          <!--end::Body-->
                        </div>
                      </div>

                      <div class="col-lg-2">
                        <div class="card card-custom bg-success gutter-b" style="height: 130px">
                          <!--begin::Body-->
                          <div class="card-body d-flex flex-column p-0">
                            <!--begin::Stats-->
                            <div class="flex-grow-1 card-spacer-x pt-6">
                              <div class="text-inverse-danger font-weight-bold">Anjab Menunggu Verfikasi</div>
                              <div class="text-inverse-danger font-weight-bolder font-size-h4">3,620</div>
                            </div>
                            <!--end::Stats-->
                            <!--begin::Chart-->
                            <div id="kt_tiles_widget_2_chart" class="card-rounded-bottom" style="height: 50px"></div>
                            <!--end::Chart-->
                          </div>
                          <!--end::Body-->
                        </div>
                      </div>

                    </div> --}}

                    @if(\MojokertokabUser::getUser()->role == 'ADMIN' || 
                    (\MojokertokabUser::getUser()->role == 'PD' && \MojokertokabUser::getUser()->kode_opd === '28'))

                    
                    <form method="GET">
                      <input type="hidden" name="jenis" value="{{ $jenis }}">
                      <div class="form-group row">
                        <label class="col-1 col-form-label">Filter</label>
                    
                        @if(\MojokertokabUser::getUser()->role == 'PD' && \MojokertokabUser::getUser()->kode_opd === '28')

                          <!-- Jika role adalah PD dan kode OPD dimulai dengan 28, hanya tampilkan filter puskesmas -->
                          <div class="col-4">
                            <select class="form-control select2" name="puskesmas">
                              <option value="">Pilih Puskesmas</option>
                              @if(!empty($puskesmas))
                                @foreach($puskesmas as $itemPuskesmas)
                                  <option {{ $selectedPuskesmas == $itemPuskesmas->kodeunit ? 'selected' : '' }} value="{{ $itemPuskesmas->kodeunit }}">{{ $itemPuskesmas->unitkerja }}</option>
                                @endforeach
                              @else
                                <option value="">-- No Puskesmas available --</option>
                              @endif
                            </select>
                          </div>
                        @else
                          <!-- Jika role bukan PD atau kode OPD tidak dimulai dengan 28, tampilkan filter unit kerja -->
                          <div class="col-4">
                            <select class="form-control select2" name="unit_kerja">
                              <option value="">-- PILIH UNIT KERJA --</option>
                              @if(!empty($unitKerja))
                                @foreach($unitKerja as $itemUnitkerja)
                                  <option {{ $selectedUnitKerja == $itemUnitkerja->kodeunit ? 'selected' : '' }} value="{{ $itemUnitkerja->kodeunit }}">{{ $itemUnitkerja->unitkerja }}</option>
                                @endforeach
                              @else
                                <option value="">-- No Unit Kerja available --</option>
                              @endif
                            </select>
                          </div>
                        @endif
                    
                        <div class="col-1">
                          <button class="btn btn-primary" id="filterunitkerja">Filter</button>
                        </div>
                      </div>
                    </form>
                    
                    
                    @endif

                    <div class="alert alert-danger">
                        Apabila pada daftar jabatan ada keterangan "Harus Diperbarui" maka Admin OPD harus melakukan perbaruan data dengan klik Menu > Edit > Perbaiki Data > Simpan
                    </div>

                  </div>
                  <div class="table-responsive" style="overflow-x: visible;">
                      <!--begin: Datatable-->
                      <table class="table" id="anjab_tabel" style="margin-top: 13px !important">
                        <thead class="thead-light">
                          <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">Nama</th>
                            <th rowspan="2">Unit Kerja</th>
                            <th rowspan="2">Jenis Jabatan</th>
                            <th rowspan="2">Jenjang</th>
                            <th rowspan="2">Eselon</th>
                            <th rowspan="2">Kode</th>
                            <th style="width: 150px;" rowspan="2">Status (12)</th>
                            {{-- <th class="text-center" colspan="11">Analisis</th> --}}
                            <th rowspan="2"></th>
                          </tr>
                          {{-- <tr>
                            <th>UT</th>
                            <th>TJ</th>
                            <th>W</th>
                            <th>KJ</th>
                            <th>BK</th>
                            <th>AK</th>
                            <th>HK</th>
                            <th>KLK</th>
                            <th>RB</th>
                            <th>SJ</th>
                            <th>PK</th>
                          </tr> --}}
                        </thead>
                        <tbody>

                          @if(count($anjab) > 0)
                          {{-- {{ dd($anj) }} --}}
                            @foreach($anjab as $itemAnjab)
                              @if($itemAnjab->jenis_jabatan == '2' && $itemAnjab->jenjang == NULL)

                              @else

                              @endif

                            <tr class="@if($itemAnjab->status_verification == 1) table-success @elseif($itemAnjab->status_verification == 2) table-danger @else table-warning @endif">
                              <td>{{ $loop->iteration }}</td>
                              {{-- <td>{{ $itemAnjab->status_verification}}</td> --}}
                              <td>
                                @if($itemAnjab->jabatan)
                                {{ \Illuminate\Support\Str::title($itemAnjab->jabatan->jabatan) }}
                                @else
                                <i class="flaticon-danger text-danger"></i>{{ @$itemAnjab->nama }} (Harus Diperbarui)
                                @endif

                                <div style="font-size: 10px;">Oleh : <span><b>{{ @$itemAnjab->dataUser->option->namalengkap }}</b></span>  </div>
                                @if($itemAnjab->status_data_jabatan == 'ditolak')
                                <div style="font-size: 10px;background: red;color: white; padding: 3px;">Kesalahan : {{ $itemAnjab->keterangan_data_jabatan }}</div>
                                @endif
                              </td>
                              <td>
                                {{ @$itemAnjab->dataUnitKerja->unitkerja }}
                                {{-- <div>JPT Pratama : {{ $itemAnjab->unit_eselon_2 }}</div>
                                <div>Administrator : {{ $itemAnjab->unit_eselon_3 }}</div>
                                <div>Pengawas : {{ $itemAnjab->unit_eselon_4 }}</div> --}}
                              </td>
                              <td>{{ $itemAnjab->text_jenis_jabatan }} @if(@$itemAnjab->jabatan->jenis != strtolower($itemAnjab->text_jenis_jabatan))   @endif</td>
                              <td>
                                @if(array_key_exists($itemAnjab->m_jabatan_fungsional_jenjang, \MojokertokabApp::getJenjangFungsional()))
                                  {{ @\MojokertokabApp::getJenjangFungsional()[$itemAnjab->m_jabatan_fungsional_jenjang] }}    
                                @else
                                  {{ $itemAnjab->m_jabatan_fungsional_jenjang }} *
                                @endif
                              </td>
                              <td>{{ $itemAnjab->text_eselon_jabatan }}</td>
                              {{-- <td>{{ $itemAnjab->kode_jabatan }}</td> --}}
                              <td>{{ @$itemAnjab->jabatan->_kode }}</td>
                              <td>
                                <span class="badge badge-warning text-white">{{ $itemAnjab->sum_status_verification[0] }}</span>
                                <span class="badge badge-success text-white">{{ $itemAnjab->sum_status_verification[2] }}</span>
                                <span class="badge badge-danger text-white">{{ $itemAnjab->sum_status_verification[1] }}</span>
                              </td>
                              {{-- <td>
                                @if($itemAnjab->num_uraian_jabatan > 0)
                                <span class="badge badge-warning text-white">{{ $itemAnjab->num_uraian_jabatan }}</span>
                                @else
                                <span class="badge badge-danger text-white">{{ $itemAnjab->num_uraian_jabatan }}</span>
                                @endif
                              </td>
                              <td>
                                @if($itemAnjab->num_tanggung_jawab > 0)
                                <span class="badge badge-warning text-white">{{ $itemAnjab->num_tanggung_jawab }}</span>
                                @else
                                <span class="badge badge-danger text-white">{{ $itemAnjab->num_tanggung_jawab }}</span>
                                @endif
                              </td>
                              <td>
                                @if($itemAnjab->num_wewenang > 0)
                                <span class="badge badge-warning text-white">{{ $itemAnjab->num_wewenang }}</span>
                                @else
                                <span class="badge badge-danger text-white">{{ $itemAnjab->num_wewenang }}</span>
                                @endif
                              </td>
                              <td>
                                @if($itemAnjab->num_korelasi_jabatan > 0)
                                <span class="badge badge-warning text-white">{{ $itemAnjab->num_korelasi_jabatan }}</span>
                                @else
                                <span class="badge badge-danger text-white">{{ $itemAnjab->num_korelasi_jabatan }}</span>
                                @endif
                              </td>
                              <td>
                                @if($itemAnjab->num_bahan_kerja > 0)
                                <span class="badge badge-warning text-white">{{ $itemAnjab->num_bahan_kerja }}</span>
                                @else
                                <span class="badge badge-danger text-white">{{ $itemAnjab->num_bahan_kerja }}</span>
                                @endif
                              </td>

                              <td>
                                @if($itemAnjab->num_alat_kerja > 0)
                                <span class="badge badge-warning text-white">{{ $itemAnjab->num_alat_kerja }}</span>
                                @else
                                <span class="badge badge-danger text-white">{{ $itemAnjab->num_alat_kerja }}</span>
                                @endif
                              </td>

                              <td>
                                @if($itemAnjab->num_hasil_kerja > 0)
                                <span class="badge badge-warning text-white">{{ $itemAnjab->num_hasil_kerja }}</span>
                                @else
                                <span class="badge badge-danger text-white">{{ $itemAnjab->num_hasil_kerja }}</span>
                                @endif
                              </td>

                              <td>
                                @if($itemAnjab->kondisi_lingkungan_kerja)
                                <span class="badge badge-success text-white">OK</span>
                                @else
                                <span class="badge badge-danger text-white">BELUM</span>
                                @endif
                              </td>

                              <td>
                                @if($itemAnjab->num_resiko_bahaya > 0)
                                <span class="badge badge-warning text-white">{{ $itemAnjab->num_resiko_bahaya }}</span>
                                @else
                                <span class="badge badge-danger text-white">{{ $itemAnjab->num_resiko_bahaya }}</span>
                                @endif
                              </td>
                              <td>
                                @if($itemAnjab->syarat_jabatan)
                                <span class="badge badge-success text-white">OK</span>
                                @else
                                <span class="badge badge-danger text-white">BELUM</span>
                                @endif
                              </td>
                              <td>
                                @if($itemAnjab->num_prestasi_kerja > 0)
                                <span class="badge badge-warning text-white">{{ $itemAnjab->num_prestasi_kerja }}</span>
                                @else
                                <span class="badge badge-danger text-white">{{ $itemAnjab->num_prestasi_kerja }}</span>
                                @endif
                              </td> --}}

                              <td>

                                <div class="btn-group dropleft">
                                  <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Menu
                                  </button>
                                  <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                    <!--begin::Naviigation-->
                                    <ul class="navi">
                                      @if($itemAnjab->jabatan)
                                      <li class="navi-item">
                                        <a  href="{{ route('anjab.detail', $itemAnjab->id) }}" class="navi-link">
                                          <span class="navi-icon">
                                            <i class="navi-icon flaticon-eye"></i>
                                          </span>
                                          <span class="navi-text">Detail</span>
                                        </a>
                                      </li>
                                      @endif



                                      @if(
                                          \MojokertokabApp::allowChangingData() &&
                                          !(\MojokertokabUser::getUser()->kode_opd === '28' && $itemAnjab->unit_kerja === '2835') &&
                                          (
                                              \MojokertokabUser::getUser()->kode_opd !== '28' ||
                                              ($itemAnjab->parent !== null)
                                          )
                                      )

                                      <li class="navi-item">
                                        <a href="javascript:;" data-jenis="{{ $jenis }}" data-id="{{ $itemAnjab->id }}" class="navi-link duplicate">
                                          <span class="navi-icon">
                                            <i class="navi-icon flaticon2-copy"></i>
                                          </span>
                                          <span class="navi-text">Duplikat Anjab</span>
                                        </a>
                                      </li>
                                      <li class="navi-item">
                                        <a 
                                            data-jenis-jabatan="{{ $jenis }}" 
                                            data-kode-opd="{{ @$pd->kodeunit }}" 
                                            data-id="{{ $itemAnjab->id }}"  
                                            href="javascript:;" 
                                            class="navi-link edit-anjabs @if($itemAnjab->status_verification == 1) disabled-link @endif"
                                            @if($itemAnjab->status_verification == 1) 
                                                style="pointer-events: none; cursor: default;" 
                                            @endif
                                        >
                                            <span class="navi-icon">
                                                <i class="navi-icon flaticon-edit-1"></i>
                                            </span>
                                            <span class="navi-text">Edit</span>
                                        </a>
                                      </li>

                                      <li class="navi-item">
                                        <a data-id="{{ $itemAnjab->id }}" 
                                           href="javascript:;" 
                                           class="navi-link delete-anjab @if($itemAnjab->status_verification == 1) disabled-link @endif">
                                          <span class="navi-icon">
                                            <i class="navi-icon flaticon-delete-1"></i>
                                          </span>
                                          <span class="navi-text">Hapus</span>
                                        </a>
                                      </li>
                                      @endif
                                      @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                                      {{-- <li class="navi-item">
                                        <a data-id="{{ $itemAnjab->id }}"  href="javascript:;" class="navi-link copy-anjab">
                                          <span class="navi-icon">
                                            <i class="navi-icon flaticon2-copy"></i>
                                          </span>
                                          <span class="navi-text">Copy</span>
                                        </a>
                                      </li> --}}
                                      @endif
                                    </ul>
                                    <!--end::Naviigation-->
                                  </div>
                              </div>

                              </td>
                            </tr>
                            @endforeach
                          @else
                            <tr>
                              <td class="text-center" colspan="19">TIDAK ADA DATA ANALISIS JABATAN</td>
                            </tr>
                          @endif


                        </tbody>

                      </table>
                      <!--end: Datatable-->
                    </div>

                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

            <div id="modal-anjab" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-xl">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('anjab.save') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">

                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button id="btn-submit-anjab" type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            @if(\MojokertokabUser::getUser()->role == 'ADMIN')

            <div id="modal-copy-anjab" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('anjab.copy') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">

                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Copy <i class="icon-copy"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            @endif

            <div id="modal-org" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-xl">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">

                  </div>

                  <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </div>

          <form id="form-delete-anjab" method="POST" action="{{ route('anjab.delete') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
  $('.select2').select2();

  initEdit();
  initHapus();
  initCopy();

  toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};

  $('#modal-anjab form').submit(function(e){
    // alert("Submit");
    e.preventDefault(); // avoid to execute the actual submit of the form.

    $.ajax({
      url: '{{ route('anjab.save') }}',
      method: 'POST',
      data:$('#modal-anjab form').serialize(),
      success: function(e){
        // console.log($('#modal-anjab form').serialize());
        // $('#modal-anjab').modal('hide');
        // alert("Analisis jabatan berhasil disimpan");
        toastr.success("Data Jabatan berhasil disimpan!");
        location.reload();
      },
      error: function(e){
        switch(e.status){
          case 409:
            alert("Data sudah terinput");
        }
      }
    })
  });

  $('.add-anjab').on('click', function(){

    var jenis_jabatan = $(this).data('jenis-jabatan');
    var kode_opd = $(this).data('kode-opd');

    $.ajax({
      url:'{{ route("anjab.get-form") }}',
      method:'GET',
      data:{aksi:'add-anjab', jenis_jabatan: jenis_jabatan, kode_opd:kode_opd},
      success:function(result){
        $('#modal-anjab .modal-title').text("Tambah Analisis Jabatan");
        $('#modal-anjab .modal-body').html(result);
        $('#modal-anjab').modal('show');
        // $('#btn-submit-anjab').attr('disabled', true);
        $('.select2').select2();

        // pilih_jabatan();
      }
    });
  });

  $('.btn-view-org').on('click', function(){
    $.ajax({
      url:'{{ route("anjab.get-org-config") }}',
      method:'GET',
      data:{},
      success: function(result){
        $('#modal-org .modal-body').html(result);
        $('#modal-org').modal('show');
      }
    })

  });

  $('#modal-anjab').on('shown.bs.modal', function (e) {
      init_check_anjab();
  })

  // function pilih_jabatan(){
  //   $('select[name=jabatan]').on('change', function(){
  //     $.ajax({
  //       url:'',
  //       method:'get',
  //       data:'',
  //       success: function(result){
  //         $('textarea[name=ikhtisar_jabatan]').val(result);
  //       }
  //     });
  //   });
  // }


  function init_check_anjab(){
    $('#jabatan').on('change', function(){
      var jabatan = $('#jabatan').val();
      var jenis = $('#jenis').val();

      $.ajax({
        url: '{{ route("anjab.check-anjab") }}',
        method:'GET',
        data:{jabatan:jabatan, jenis:jenis},
        success: function(result){
          if(result.status === "false"){
            $('#alert-anjab').show();
            $('#alert-anjab .alert').text(result.message);
            $('#btn-submit-anjab').attr('disabled', 'disabled');
          }else{
            $('#alert-anjab').hide();
            $('#btn-submit-anjab').removeAttr('disabled');
          }
        }
      });
    });

    $('#jenis').on('change', function(){
      var jabatan = $('#jabatan').val();
      var jenis = $('#jenis').val();

      $.ajax({
        url: '{{ route("anjab.check-anjab") }}',
        method:'GET',
        data:{jabatan:jabatan, jenis:jenis},
        success: function(result){
          if(result.status === "false"){
            $('#alert-anjab').show();
            $('#alert-anjab .alert').text(result.message);
            $('#btn-submit-anjab').attr('disabled', 'disabled');
          }else{
            $('#alert-anjab').hide();
            $('#btn-submit-anjab').removeAttr('disabled');
          }
        }
      });
    });
  }

  function initEdit(){
    $('.edit-anjabs').on('click', function(){

      var jenis_jabatan = $(this).data('jenis-jabatan');
      var kode_opd = $(this).data('kode-opd');

      var id = $(this).data('id');
      $.ajax({
        url:'{{ route("anjab.get-form") }}',
        method:'GET',
        data:{aksi:'edit-anjab',id:id, jenis_jabatan: jenis_jabatan, kode_opd:kode_opd },
        success:function(result){
          $('#modal-anjab .modal-title').text("Edit Analisis Jabatan");
          $('#modal-anjab .modal-body').html(result);
          $('#modal-anjab').modal('show');
          $('.select2').select2();
        }
      });
    });
  }

  function initCopy(){
    $('.copy-anjab').on('click', function(){
      var id = $(this).data('id');
      $.ajax({
        url:'{{ route("anjab.get-form") }}',
        method:'GET',
        data:{aksi:'copy-anjab',id:id },
        success:function(result){
          $('#modal-copy-anjab .modal-title').text("Copy Anjab");
          $('#modal-copy-anjab .modal-body').html(result);
          $('#modal-copy-anjab').modal('show');
          $('.select2').select2();
        }
      });
    })
  }

  function initHapus(){
    $('.delete-anjab').on('click', function(){
      var id = $(this).data('id');
          swal.fire({
              title: 'Apakah anda yakin?',
              text: "Menghapus Analisa Jabatan, akan menghapus ABK dan Evjab. Data yang sudah dihapus, tidak dapat dikembalikan",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Hapus Data',
              cancelButtonText: 'Batal',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-warning',
              buttonsStyling: false
          }).then(function (status) {
            if(status.value){
              $('form[id=form-delete-anjab] input[name=id]').val(id);
                  $('form[id=form-delete-anjab').submit();
            }
          });
    });
  }

  $('body').on('click', '.duplicate', function(){
    var id = $(this).data('id')
    var jenis = $(this).data('jenis')
    swal.fire({
        title: 'Apakah anda yakin?',
        text: "Menduplikat Data Anjab",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Duplikat Data',
        cancelButtonText: 'Batal',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-warning',
        buttonsStyling: false
    }).then(function (e) {
      if (e.value === true) {
        let _url = "{{ route('anjab.duplicate') }}"
        $.ajax({
            method: 'POST',
            url: _url,
            data: {
                _token: "{{csrf_token()}}",
                id:id,
                jenis:jenis
            },
            success: function (resp) {
              // console.log(resp);
                if (resp.success) {
                    swal.fire("Sukses", resp.message, "success").then(function(){
                        location.reload();
                    })
                } else {
                    swal.fire("Error!", resp.message, "error");
                }
            },
            error: function (resp) {
              console.log(resp);
                swal.fire("Error!", resp.message, "error");
            }
        });
        } else {
          e.dismiss;
        }
    });
  })



</script>

<script>
  // $(document).ready( function() {
  //   $('#btnTambahAnalis').hide()
  // }) 
  
  // var abc = 
  // $('body').on('click', '#filterunitkerja', function(){
  //   $('#btnTambahAnalis').show()
  // })

  // $('#filterunitkerja').hide()
</script>
@endsection
