
<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <title> Basic example </title>
    <link rel="stylesheet" href="{{ asset('treant-js') }}/Treant.css">
    <link rel="stylesheet" href="{{ asset('treant-js') }}/examples/basic-example/basic-example.css">
    
</head>
<body>
    <div class="chart" id="basic-example"></div>
    <script src="{{ asset('treant-js') }}/vendor/raphael.js"></script>
    <script src="{{ asset('treant-js') }}/Treant.js"></script>
    
    <!-- <script src="{{ asset('treant-js') }}/examples/basic-example/basic-example.js"></script> -->
    <script>

        var config = {!! $config !!}
        var ceo = {!! $ceo !!}
        var chart_config = [
            config_option,
            ceo
        ]
        

        new Treant( chart_config );
    </script>
</body>
</html>