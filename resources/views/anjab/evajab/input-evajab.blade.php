@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                  <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h3 class="card-label">Faktor Jabatan :  {{ $anjab->jabatan->nama }}</h3>
                    </div>
                  </div>
                  <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table" id="anjab_tabel" style="margin-top: 13px !important">
                      <thead>
                        <tr>
                          <th class="text-center">No</th>
                          <th class="text-center">Faktor Evaluasi</th>
                          <th class="text-center">Nilai Yang Diberikan</th>
                          <th class="text-center" style="width: 20%">Standar Jabatan Struktual Yang Digunakan (Jika Ada) </th>
                          <th class="text-center">Keterangan</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php
                          if(@$faktorEvajab){
                            $nilaiFaktor1 = $faktorEvajab->faktor_level1->nilai;
                            $nilaiFaktor2 = $faktorEvajab->faktor_level2->nilai;
                            $nilaiFaktor3 = $faktorEvajab->faktor_level3->nilai;
                            $nilaiFaktor4A = $faktorEvajab->faktor_level4A->nilai;
                            $nilaiFaktor4B = $faktorEvajab->faktor_level4B->nilai;
                            $nilaiFaktor5 = $faktorEvajab->faktor_level5->nilai;
                            $nilaiFaktor6 = $faktorEvajab->faktor_level6->nilai;

                            $total_nilai = $nilaiFaktor1 + $nilaiFaktor2 + $nilaiFaktor3 + $nilaiFaktor4A + $nilaiFaktor4B + $nilaiFaktor5 + $nilaiFaktor6;
                          }else{
                            $total_nilai = '';
                          }
                          
                        @endphp
                        <tr>
                          <td>1</td>
                          <td>Faktor 1 : Ruang Lingkup dan Dampak Program</td>
                          <td class="text-center">{{ isset($faktorEvajab) ? $faktorEvajab->faktor_level1->nilai : '-' }}</td>
                          <td width="20%"></td>
                          <td class="text-center">Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_1 }}</td>
                        </tr>
                      <tr>
                          <td>2</td>
                          <td>Faktor 2 : Pengaturan Organisasi</td>
                          <td class="text-center">{{ isset($faktorEvajab) ? $faktorEvajab->faktor_level2->nilai : '-' }}</td>
                          <td width="20%"></td>
                          <td class="text-center">Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_2 }}</td>
                        </tr>
                      <tr>
                          <td>3</td>
                          <td>Faktor 3 : Wewenang Penyeliaan Dan Manajerial</td>
                          <td class="text-center">{{  isset($faktorEvajab) ? $faktorEvajab->faktor_level3->nilai : '-' }}</td>
                          <td width="20%"></td>
                          <td class="text-center">Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_3 }}</td>
                        </tr>
                      <tr>
                          <td>4</td>
                          <td>Faktor 4 : Hubungan Personal
                            <br>
                            a. Sifat Hubungan <br>
                            b. Tujuan Hubungan
                          </td>
                          <td class="text-center">
                            <br>
                            a. {{ @$faktorEvajab->faktor_level4A->nilai }} <br>
                            b. {{ @$faktorEvajab->faktor_level4B->nilai }}
                          </td>
                          
                          <td width="20%">
                           
                          </td>
                          <td class="text-center">
                            <br>
                            a. Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_4A }} <br>
                            b. Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_4B }}
                          </td>
                        </tr>
                        <tr>
                          <td>5</td>
                          <td>Faktor 5 : Kesulitan Pengarahan Pekerjaan</td>
                          <td class="text-center">{{ isset($faktorEvajab) ? $faktorEvajab->faktor_level5->nilai :'-' }}</td>
                          <td width="20%">
                            
                          </td>
                          <td class="text-center">Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_5 }}</td>
                        </tr>
                        <tr>
                          <td>6</td>
                          <td>Faktor 6 : Kondisi Lain</td>
                          <td class="text-center">{{ isset($faktorEvajab) ? $faktorEvajab->faktor_level6->nilai :'-' }}</td>
                          <td width="20%">
                            
                          </td>
                          <td class="text-center">Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_6 }}</td>
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th class="text-center" colspan="2">Total Nilai</th>
                          <th class="text-center" >{{ $total_nilai }}</th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <th class="text-center" colspan="2">Kelas Jabatan</th>
                          <th class="text-center">{{ $faktorEvajab->kelas_jabatan }}</th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </tfoot>
                    </table>
                    <!--end: Datatable-->
                  </div>
                </div>
                <!--end::Card-->
                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Faktor Jabatan :  {{ $anjab->jabatan->nama }}</h2>
                    </div>
                  </div>
                  @if(isset($faktorEvajab))
                  <form id="form-input-evajab" method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('anjab.update-evajab')}}">
                  {{csrf_field()}}

                  <input type="hidden" name="id_evajab" id="id" value="{{ $faktorEvajab->id }}">
                  @else
                  <form id="form-input-evajab" method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('anjab.save-evajab')}}">
                    {{csrf_field()}}
                  @endif
                  <div class="card-body">
                    
                    <input type="hidden" name="id" id="id" value="{{ $anjab->id }}">
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 1 :</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-8">
                          <select required="" class="form-control" name="level_faktor1" id="level_faktor1">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_faktor1 as $level_faktors1)
                            <option {{ @$faktorEvajab->level_faktor1 == $level_faktors1->id? 'selected' : '' }}  value="{{ $level_faktors1->id }}">{{ $level_faktors1->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor1">
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Ruang Lingkup dan Dampak Program :</label>
                        <div class="col-lg-8">
                          <textarea id="ruang_lingkup" name="ruang_lingkup" required="" rows="10" cols="100">{{ isset($faktorEvajab->ruang_lingkup) ? $faktorEvajab->ruang_lingkup : '' }}</textarea>
                        </div>
                      </div>
                     {{--  <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Dampak Program :</label>
                        <div class="col-lg-6">
                          <textarea id="dampak_program" name="dampak_program" required="" rows="10" cols="100">{{ isset($faktorEvajab->dampak_program) ? $faktorEvajab->dampak_program : '' }}</textarea>
                        </div>
                      </div>
                    </div> --}}
                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 2 :</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                          <select required="" class="form-control" name="level_faktor2" id="level_faktor2">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_faktor2 as $level_faktors2)
                            <option {{ @$faktorEvajab->level_faktor2 == $level_faktors2->id? 'selected' : '' }} value="{{ $level_faktors2->id }}">{{ $level_faktors2->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor2">
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Pengaturan Organisasi :</label>
                        <div class="col-lg-6">
                          <textarea id="pengaturan_organisasi" name="pengaturan_organisasi" required="" rows="10" cols="100">{{ isset($faktorEvajab->pengaturan_organisasi) ? $faktorEvajab->pengaturan_organisasi : '' }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 3 :</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                          <select required="" class="form-control" name="level_faktor3" id="level_faktor3">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_faktor3 as $level_faktors3)
                            <option {{ @$faktorEvajab->level_faktor3 == $level_faktors3->id? 'selected' : '' }} value="{{ $level_faktors3->id }}">{{ $level_faktors3->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor3">
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Wewenang Penyeliaan Dan Manajerial :</label>
                        <div class="col-lg-6">
                          <textarea id="wewenang_penyeliaan_manajerial" name="wewenang_penyeliaan_manajerial" required="" rows="10" cols="100">{{ isset($faktorEvajab->wewenang_penyeliaan_manajerial) ? $faktorEvajab->wewenang_penyeliaan_manajerial : '' }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="separator separator-dashed my-5"></div>
                    <div class="form-group ">
                      <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 4 :</h2>
                      <h2 class="font-size-lg text-dark font-weight-bold mb-6">Hubungan Personal </h2>
                    </div>
                    <div class="mb-15">
                      <div class="form-group ">
                        <h2 class="col-lg-3 col-form-label text-right"><b><u>Sifat Hubungan :</u></b></h2>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 text-right">Level :</label>
                        <div class="col-lg-6">
                          <select required="" class="form-control" name="level_faktor4A" id="level_faktor4A">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_faktor4A as $level_faktors4A)
                            <option {{ @$faktorEvajab->level_faktor4A == $level_faktors4A->id? 'selected' : '' }} value="{{ $level_faktors4A->id }}">{{ $level_faktors4A->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor4A">
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Sifat Hubungan :</label>
                        <div class="col-lg-6">
                          <textarea id="sifat_hubungan" name="sifat_hubungan" required="" rows="10" cols="100">{{ isset($faktorEvajab->sifat_hubungan) ? $faktorEvajab->sifat_hubungan: '' }}</textarea>
                        </div>
                      </div>
                      <div class="form-group ">
                        <h2 class="col-lg-3 col-form-label text-right"><b><u>Tujuan Hubungan :</u></b></h2>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 text-right">Level :</label>
                        <div class="col-lg-6">
                          <select required="" class="form-control" name="level_faktor4B" id="level_faktor4B">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_faktor4B as $level_faktors4B)
                            <option {{ @$faktorEvajab->level_faktor4B == $level_faktors4B->id? 'selected' : '' }} value="{{ $level_faktors4B->id }}">{{ $level_faktors4B->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor4B">
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Tujuan Hubungan :</label>
                        <div class="col-lg-6">
                          <textarea id="tujuan_hubungan" name="tujuan_hubungan" required="" rows="10" cols="100">{{ isset($faktorEvajab->tujuan_hubungan) ? $faktorEvajab->tujuan_hubungan: '' }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 5 :</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                          <select required="" class="form-control" name="level_faktor5" id="level_faktor5">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_faktor5 as $level_faktors5)
                            <option {{ @$faktorEvajab->level_faktor5 == $level_faktors5->id? 'selected' : '' }} value="{{ $level_faktors5->id }}">{{ $level_faktors5->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor5">
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Kesulitan Pengarahan :</label>
                        <div class="col-lg-6">
                          <textarea id="kesulitan_pengarahan" name="kesulitan_pengarahan" required="" rows="10" cols="100">{{ isset($faktorEvajab->kesulitan_pengarahan) ? $faktorEvajab->kesulitan_pengarahan: '' }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 6 :</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                          <select required="" class="form-control" name="level_faktor6" id="level_faktor6">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                             @foreach($level_faktor6 as $level_faktors6)
                            <option {{ @$faktorEvajab->level_faktor6 == $level_faktors6->id? 'selected' : '' }} value="{{ $level_faktors6->id }}">{{ $level_faktors6->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor6">
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Kondisi Lain:</label>
                        <div class="col-lg-6">
                          <textarea id="kondisi_lain" name="kondisi_lain" required="" rows="10" cols="100">{{ isset($faktorEvajab->kondisi_lain) ? $faktorEvajab->kondisi_lain: '' }}</textarea>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  <div class="card-footer">
                    <div class="row">
                      <div class="col text-right">
                        @if(isset($faktorEvajab))
                        <button type="submit" class="btn btn-success mr-2">Update</button>
                        @else
                        <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        @endif
                      </div>
                    </div>
                  </div>
                  </form>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

              
@endsection

@section('js')
{{-- js files --}}
 <script src="{{ asset('') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script src="//cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script>
  var options = {
    filebrowserImageBrowseUrl: '{{ URL::to("laravel-filemanager?type=Images") }}',
    filebrowserImageUploadUrl: '{{ URL::to("laravel-filemanager/upload?type=Images&_token=") }}',
    filebrowserBrowseUrl: '{{ URL::to("/laravel-filemanager?type=Files") }}',
    filebrowserUploadUrl: '{{ URL::to("/laravel-filemanager/upload?type=Files&_token=") }}',
  };
</script>

<script>

        CKEDITOR.replace( 'ruang_lingkup', options);
        // CKEDITOR.replace( 'dampak_program', options);
        CKEDITOR.replace( 'pengaturan_organisasi', options);
        CKEDITOR.replace( 'wewenang_penyeliaan_manajerial', options);
        CKEDITOR.replace( 'sifat_hubungan', options);
        CKEDITOR.replace( 'tujuan_hubungan', options);
        CKEDITOR.replace( 'kesulitan_pengarahan', options);
        CKEDITOR.replace( 'kondisi_lain', options);

        $("#level_faktor1").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("anjab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 1
            },
            cache: false,
            success: function(html) {
              $("#data_faktor1").html(html);
            }
          });
        });

        $("#level_faktor2").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("anjab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 2
            },
            cache: false,
            success: function(html) {
              $("#data_faktor2").html(html);
            }
          });
        });

        $("#level_faktor3").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("anjab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 3
            },
            cache: false,
            success: function(html) {
              $("#data_faktor3").html(html);
            }
          });
        });

        $("#level_faktor4A").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("anjab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 4
            },
            cache: false,
            success: function(html) {
              $("#data_faktor4A").html(html);
            }
          });
        });

        $("#level_faktor4B").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("anjab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 5
            },
            cache: false,
            success: function(html) {
              $("#data_faktor4B").html(html);
            }
          });
        });

        $("#level_faktor5").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("anjab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 6
            },
            cache: false,
            success: function(html) {
              $("#data_faktor5").html(html);
            }
          });
        });

        $("#level_faktor6").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("anjab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 7
            },
            cache: false,
            success: function(html) {
              $("#data_faktor6").html(html);
            }
          });
        });
</script>
@endsection