<input type="hidden" value="create" name="aksi">
<input type="hidden" value="{{ $id }}" name="id">

<div class="form-group">
	<label>Tugas Jabatan</label>
	<select style="width: 100%" class="form-control select2" name="tugas_jabatan" required="">
		@foreach($tugasJabatan as $itemTugasJabatan)
		<option value="{{ $itemTugasJabatan->id }}">{{ $itemTugasJabatan->uraian_jabatan }}</option>
		@endforeach
	</select>
</div>

{{-- <div class="form-group">
	<label>Uraian Hasil</label>
	<textarea required="" name="uraian_hasil" class="form-control" rows="1"></textarea> 
</div> --}}
<div class="form-group">
	<label>Hasil Kerja </label>
	<input type="text" required class="form-control" name="hasil_kerja" >
</div>
<div class="form-group">
	<label>Jumlah Hasil </label>
	<input type="number" required class="form-control" name="jumlah_hasil" >
</div>
<div class="form-group">
	<label>Waktu Penyelesaian (Jam) </label>
	<input type="number" required class="form-control" name="waktu_penyelesaian" >
</div>
<div class="form-group">
	<label>Waktu Efektif </label>
	<input value="1250" readonly="" type="number" required class="form-control" name="waktu_efektif" >
</div>


