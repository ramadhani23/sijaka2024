<input type="hidden" value="update" name="aksi">
<input type="hidden" value="{{ $abk->id }}" name="id">
<input type="hidden" value="{{ $abk->trx_anjab_id }}" name="id_anjab">

<div class="form-group">
	<label>Tugas Jabatan</label>
	<select style="width: 100%" class="form-control select2" name="tugas_jabatan" required="">
		@foreach($tugasJabatan as $itemTugasJabatan)
		<option {{ $abk->trx_uraian_jabatan_id == $itemTugasJabatan->id?'selected':'' }} value="{{ $itemTugasJabatan->id }}">{{ $itemTugasJabatan->uraian_jabatan }}</option>
		@endforeach
	</select>
</div>

{{-- <div class="form-group">
	<label>Uraian Hasil</label>
	<textarea required="" name="uraian_hasil" class="form-control" rows="10">{{ $abk->uraian_hasil }}</textarea> 
</div> --}}
<div class="form-group">
	<label>Hasil Kerja </label>
	<input type="text" required class="form-control" name="hasil_kerja" value="{{ $abk->hasil_kerja }}">
</div>
<div class="form-group">
	<label>Jumlah Hasil </label>
	<input type="number" required class="form-control" name="jumlah_hasil" value="{{ $abk->jumlah_hasil }}">
</div>
<div class="form-group">
	<label>Waktu Penyelesaian (Jam) </label>
	<input type="number" required class="form-control" name="waktu_penyelesaian" value="{{ $abk->waktu_penyelesaian }}">
</div>
<div class="form-group">
	<label>Waktu Efektif </label>
	<input type="number" readonly="" required class="form-control" name="waktu_efektif" value="{{ $abk->waktu_efektif }}">
</div>


