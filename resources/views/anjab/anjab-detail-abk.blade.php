@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                
                {{-- @include('anjab.card-anjab') --}}

                <br><br>
                <!--begin::Card-->
                <div id="analisa-card" class="card card-custom">
                  <div class="card-body">
                    <!--begin::Example-->
                        <div class="example mb-10">
                          
                          <div class="example-preview">
                            <div class="row">
                              <div class="col-4 border-right">
                                @include('anjab.step-indicator')
                              </div>
                              <div class="col-8">
                                  <div class="card card-custom">
                                      <div class="card-header">
                                        <div class="card-title">
                                          <h3 class="card-label"><i class="flaticon-analytics"></i> Analisa Beban Kerja </h3>
                                        </div>
                                        <div class="card-toolbar">
                                          <!--begin::Button-->
                                          <a href="javascript:;" class="btn btn-primary btn-sm font-weight-bolder add-abk">
                                           <i class="far fa-plus-square "></i>Analisa </a>
                                          <!--end::Button-->
                                        </div>
                                      </div>
                                      <div class="card-body">
                                        <!--begin: Datatable-->
                                        <div class="table-responsive">
                                          <table class="table" style="margin-top: 13px !important">
                                            <thead>
                                              <tr>
                                                <th class="text-center">No</th>
                                                <th class="text-center">Tugas Jabatan</th>
                                                <th class="text-center">Hasil Kerja</th>
                                                <th class="text-center">Jumlah Hasil</th>
                                                <th class="text-center">Waktu Penyelesaian</th>
                                                <th class="text-center">Total Waktu Penyelesaian</th>
                                                <th class="text-center">Waktu Efektif</th>
                                                <th class="text-center">Kebutuhan Pegawai</th>
                                                <th width="10%" class="text-center">Aksi</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              @if($abk->count() == 0)
                                                <tr>
                                                  <td colspan="8" class="text-center">Tidak Ada Data</td>
                                                </tr>
                                              @endif
                                              @php
                                                $jumlah_waktu_penyelesaian = 0;
                                                $jumlah_kebutuhan_pegawai = 0;
                                                $total_kebutuhan_pegawai = 0;
                                              @endphp
                                              @foreach($abk as $abks)
                                              @php
                                                $jumlah_waktu_penyelesaian += $abks->waktu_penyelesaian;
                                                $jumlah_kebutuhan_pegawai += $abks->kebutuhan_pegawai;
                                                $total_kebutuhan_pegawai += $jumlah_kebutuhan_pegawai;
                                              @endphp
                                                <tr>
                                                  <td class="text-center">{{ $loop->iteration + $abk->firstItem() - 1 }} </td>
                                                  <td>{{ $abks->dataTugasJabatan->uraian_jabatan }}</td>
                                                  <td class="text-center">{{ $abks->hasil_kerja }}</td>
                                                  <td class="text-center">{{ $abks->jumlah_hasil }}</td>
                                                  <td class="text-center">{{ $abks->waktu_penyelesaian }}</td>
                                                  <td class="text-center">{{ $abks->waktu_penyelesaian * $abks->jumlah_hasil }}</td>
                                                  <td class="text-center">{{ $abks->waktu_efektif }}</td>
                                                  <td class="text-center">{{ $abks->kebutuhan_pegawai }}</td>
                                                  <td width="10%">
                                                    {{-- <a data-id="{{ $abks->id }}"  href="javascript:;"  class="btn btn-icon btn-light-warning btn-circle btn-xs edit-abk">
                                                      <i class="flaticon2-edit icon-sm"></i>
                                                    </a>
                                                    <a data-id="{{ $abks->id }}" href="javascript:;" class="btn btn-icon btn-light-danger btn-circle btn-xs delete-abk">
                                                      <i class="flaticon-delete-1 icon-sm"></i>
                                                    </a> --}}

                                                    <div class="btn-group dropleft">
                                                      <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                          Menu
                                                      </button>
                                                      <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                                        <!--begin::Naviigation-->
                                                        <ul class="navi">
                                                          
                                                          <li class="navi-item">
                                                            <a data-id="{{ $abks->id }}"  href="javascript:;" class="navi-link edit-abk">
                                                              <span class="navi-icon">
                                                                <i class="navi-icon flaticon2-edit"></i>
                                                              </span>
                                                              <span class="navi-text">Edit Analisa</span>
                                                            </a>
                                                          </li>

                                                          <li class="navi-item">
                                                            <a data-id="{{ $abks->id }}"  href="javascript:;" class="navi-link delete-abk">
                                                              <span class="navi-icon">
                                                                <i class="navi-icon flaticon-delete-1"></i>
                                                              </span>
                                                              <span class="navi-text">Hapus Analisa</span>
                                                            </a>
                                                          </li>
                                                        </ul>
                                                        <!--end::Naviigation-->
                                                      </div>
                                                  </div>

                                                  </td>
                                                </tr>
                                              @endforeach
                                                <tr>
                                                  <td colspan="4" class="text-center">Jumlah</td>
                                                  <td class="text-center">{{ $jumlah_waktu_penyelesaian }}</td>
                                                  <td></td>
                                                  <td class="text-center">{{ $jumlah_kebutuhan_pegawai }}</td>
                                                  <td></td>
                                                </tr>
                                                <tr>
                                                  <td colspan="6" class="text-center">Jumlah Kebutuhan Pegawai</td>
                                                  <td></td>
                                                  <td class="text-center">{{ number_format($jumlah_kebutuhan_pegawai) }}</td>
                                                  <td></td>
                                                </tr>
                                            </tbody>
                                          </table>  
                                        </div>
                                        
                                        <!--end: Datatable-->
                                      </div>
                                    </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--end::Example-->
                  </div>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

              <div id="modal-abk" class="modal fade">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5> 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('anjab.save-abk') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                      
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="modal fade text-left" id="modal" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-xl" role="document">
                  <div class="modal-content" style="width: 1200px;">
                      <div class="modal-header">
                          <h4 class="modal-title"></h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div class="modal-body">
                          
                      </div>
                      
                  </div>
              </div>
          </div>

            <form id="form-delete-abk" method="POST" action="{{ route('anjab.delete-abk') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('js')

@endsection

@section('page-script')
<script type="text/javascript">
   $('.add-abk').on('click', function(){
    var id = $('#id').val();
    $.ajax({
      url:'{{ route("anjab.get-form-abk") }}',
      method:'GET',
      data:{aksi:'add-data',id:id },
      success:function(result){
        $('#modal-abk .modal-title').text("Tambah ABK");
        $('#modal-abk .modal-body').html(result);
        $('#modal-abk').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.edit-abk').on('click', function(){
    var id = $(this).data('id');
    $.ajax({
      url:'{{ route("anjab.get-form-abk") }}',
      method:'GET',
      data:{aksi:'edit-data',id:id },
      success:function(result){
        $('#modal-abk .modal-title').text("Edit ABK");
        $('#modal-abk .modal-body').html(result);
        $('#modal-abk').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.delete-abk').on('click', function(){
    var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin?',
            // text: "Anda tidak dapat membatalkan pengajuan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus Data',
            cancelButtonText: 'Batal',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-warning',
            buttonsStyling: false
        }).then(function (status) {
          if(status.value){
            $('form[id=form-delete-abk] input[name=id]').val(id);
                $('form[id=form-delete-abk]').submit();
          }
        });
  });

  $('#print_anjab').on('click', function(){
     var id_anjab = $('#id').val();
    $.ajax({
      type: "GET",
      url: '{{ route("anjab.view-print") }}',
      data: {
        id_anjab : id_anjab,
      },
      cache: false,
      success: function(html) {
        // $("#div_header").hide();
        // $("#div_print_card").show();
        // $("#div_print").html(html);
        $('#modal .modal-title').text("Print Anjab");
        $('#modal .modal-body').html(html);
        $('#modal ').modal('show');
      }
    });
  });
</script>
@endsection