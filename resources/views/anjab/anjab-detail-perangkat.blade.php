@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                {{-- @include('anjab.card-anjab') --}}
                <br><br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-body">
                    <!--begin::Example-->
                        <div class="example mb-10">
                          
                          <div class="example-preview">
                            <div class="row">
                              <div class="col-12">
                                @include('kunci')
                              </div>
                              <div class="col-4 border-right">
                                @include('anjab.step-indicator')
                              </div>
                              <div class="col-8">
                                  <div class="card card-custom">
                                      <div class="card-header">
                                        <div class="card-title">
                                          <h3 class="card-label"><i class="flaticon-computer"></i> Perangkat / Alat kerja</h3>
                                        </div>
                                        @if(
                                          \MojokertokabApp::allowChangingData() &&
                                          !(\MojokertokabUser::getUser()->kode_opd === '28' && $anjab->unit_kerja === '2835') &&
                                          (
                                              \MojokertokabUser::getUser()->kode_opd !== '28' ||
                                              ($anjab->parent !== null)
                                          )
                                      )
                                        <div class="card-toolbar">
                                          <!--begin::Button-->
                                          <a 
                                              data-id="{{ $anjab->id }}" 
                                              href="javascript:;" 
                                              class="btn btn-primary btn-sm add-perangkat @if($anjab->status_verification == 1) disabled-link @endif"
                                              @if($anjab->status_verification == 1) 
                                                  style="pointer-events: none; cursor: default;" 
                                              @endif
                                          >
                                              <i class="far @if($anjab->status_verification == 1) fa-check-square @else fa-plus-square @endif"></i>
                                              @if($anjab->status_verification == 1)
                                                  Sudah Verifikasi
                                              @else
                                                  Perangkat
                                              @endif
                                          </a>
                                          <!--end::Button-->
                                        </div>
                                        @endif
                                      </div>
                                      <div class="card-body">
                                        <!--begin: Datatable-->
                                        <table class="table" style="margin-top: 13px !important">
                                          <thead>
                                            <tr>
                                              <th>No</th>
                                              <th>Perangkat / ALat Kerja</th>
                                              <th>Digunakan Untuk Tugas</th>
                                              <th width="10%">Aksi</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                             @if($perangkat->count() == 0)
                                              <tr>
                                                <td colspan="3" class="text-center">Tidak Ada Data</td>
                                              </tr>
                                            @endif
                                            @foreach($perangkat as $perangkats)
                                              <tr>
                                                <td>{{ $loop->iteration + $perangkat->firstItem() - 1 }} </td>
                                                <td>{{ $perangkats->perangkat_kerja }}</td>
                                                <td>{{ $perangkats->digunakan_untuk_tugas }}</td>
                                                <td width="10%">
                                                 {{--  <a data-id="{{ $perangkats->id }}"  href="javascript:;"  class="btn btn-icon btn-light-warning btn-circle btn-xs edit-perangkat">
                                                    <i class="flaticon2-edit icon-sm"></i>
                                                  </a>
                                                  <a data-id="{{ $perangkats->id }}" href="javascript:;" class="btn btn-icon btn-light-danger btn-circle btn-xs delete-perangkat">
                                                    <i class="flaticon-delete-1 icon-sm"></i>
                                                  </a> --}}
                                                  @if(
                                                    \MojokertokabApp::allowChangingData() &&
                                                    !(\MojokertokabUser::getUser()->kode_opd === '28' && $anjab->unit_kerja === '2835') &&
                                                    (
                                                        \MojokertokabUser::getUser()->kode_opd !== '28' ||
                                                        ($anjab->parent !== null)
                                                    )
                                                )
                                                  <div class="btn-group dropleft @if($anjab->status_verification == 1) disabled-link @endif">
                                                      <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                          Menu
                                                      </button>
                                                      <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                                        <!--begin::Naviigation-->
                                                        <ul class="navi">
                                                          
                                                          <li class="navi-item">
                                                            <a data-id="{{ $perangkats->id }}"  href="javascript:;" class="navi-link edit-perangkat">
                                                              <span class="navi-icon">
                                                                <i class="navi-icon flaticon2-edit"></i>
                                                              </span>
                                                              <span class="navi-text">Edit Perangkat</span>
                                                            </a>
                                                          </li>

                                                          <li class="navi-item">
                                                            <a data-id="{{ $perangkats->id }}"  href="javascript:;" class="navi-link delete-perangkat">
                                                              <span class="navi-icon">
                                                                <i class="navi-icon flaticon-delete-1"></i>
                                                              </span>
                                                              <span class="navi-text">Hapus Perangkat</span>
                                                            </a>
                                                          </li>
                                                        </ul>
                                                        <!--end::Naviigation-->
                                                      </div>
                                                  </div>
                                                  @endif
                                                </td>
                                              </tr>
                                            @endforeach
                                          </tbody>
                                        </table>
                                        <!--end: Datatable-->
                                      </div>
                                    </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--end::Example-->
                  </div>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

              <div id="modal-perangkat" class="modal fade">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5> 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('anjab.save-perangkat') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                      
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="modal fade text-left" id="modal" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content" style="width: 1200px;">
                        <div class="modal-header">
                            <h4 class="modal-title"></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <form id="form-delete-perangkat" method="POST" action="{{ route('anjab.delete-perangkat') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('js')
{{-- js files --}}
 <script src="{{ asset('') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
   $('.add-perangkat').on('click', function(){
    var id = $('#id').val();
    $.ajax({
      url:'{{ route("anjab.get-form-perangkat") }}',
      method:'GET',
      data:{aksi:'add-data',id:id },
      success:function(result){
        $('#modal-perangkat .modal-title').text("Tambah Wewenang");
        $('#modal-perangkat .modal-body').html(result);
        $('#modal-perangkat').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.edit-perangkat').on('click', function(){
    var id = $(this).data('id');
    $.ajax({
      url:'{{ route("anjab.get-form-perangkat") }}',
      method:'GET',
      data:{aksi:'edit-data',id:id },
      success:function(result){
        $('#modal-perangkat .modal-title').text("Edit Wewenang");
        $('#modal-perangkat .modal-body').html(result);
        $('#modal-perangkat').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.delete-perangkat').on('click', function(){
    var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin?',
            // text: "Anda tidak dapat membatalkan pengajuan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus Data',
            cancelButtonText: 'Batal',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-warning',
            buttonsStyling: false
        }).then(function (status) {
          if(status.value){
            $('form[id=form-delete-perangkat] input[name=id]').val(id);
                $('form[id=form-delete-perangkat]').submit();
          }
        });
  });

  $('#print_anjab').on('click', function(){
     var id_anjab = $('#id').val();
    $.ajax({
      type: "GET",
      url: '{{ route("anjab.view-print") }}',
      data: {
        id_anjab : id_anjab,
      },
      cache: false,
      success: function(html) {
        // $("#div_header").hide();
        // $("#div_print_card").show();
        // $("#div_print").html(html);
        $('#modal .modal-title').text("Print Anjab");
        $('#modal .modal-body').html(html);
        $('#modal ').modal('show');
      }
    });
  });
</script>
@endsection