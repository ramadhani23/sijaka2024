<input type="hidden" value="update" name="aksi">
<input type="hidden" value="{{ $lingkungan_kerja->id }}" name="id">
<input type="hidden" value="{{ $lingkungan_kerja->trx_anjab_id }}" name="id_anjab">

<div class="row">
	<div class="col-6">
		<div class="form-group">
			<label class="col-sm-12 control-label">Tempat Kerja</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="tempat_kerja">
				<option value="" disabled="">-- PILIH TEMPAT KERJA --</option>
				<option {{ $lingkungan_kerja->tempat_kerja == 'luar_ruangan'?'selected':'' }} value="luar_ruangan">Luar Ruangan</option>
				<option {{ $lingkungan_kerja->tempat_kerja == 'dalam_ruangan'?'selected':'' }} value="dalam_ruangan">Dalam Ruangan</option>
				<option {{ $lingkungan_kerja->tempat_kerja == 'dalam_luar_ruangan'?'selected':'' }} value="dalam_luar_ruangan">Dalam dan Luar Ruangan</option>
				
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-12 control-label">Suhu</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="suhu">
				<option value="" disabled="" selected="">-- PILIH SUHU --</option>
				<option {{ $lingkungan_kerja->suhu == 'dingin'?'selected':'' }} value="dingin">Dingin</option>
				<option {{ $lingkungan_kerja->suhu == 'panas'?'selected':'' }} value="panas">Panas</option>
				
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-12 control-label">Udara</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="udara">
				<option value="" disabled="" selected="">-- PILIH UDARA --</option>
				<option {{ $lingkungan_kerja->udara == 'sejuk'?'selected':'' }} value="sejuk">Sejuk</option>
				<option {{ $lingkungan_kerja->udara == 'panas'?'selected':'' }} value="panas">Panas</option>
				
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-12 control-label">Keadaan Ruangan</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="keadaan_ruangan">
				<option value="" disabled="" selected="">-- PILIH KEADAAN RUANGAN --</option>
				<option {{ $lingkungan_kerja->keadaan_ruangan == 'baik'?'selected':'' }} value="baik">Baik</option>
				<option {{ $lingkungan_kerja->keadaan_ruangan == 'panas'?'selected':'' }} value="panas">Buruk</option>
				
			</select>
			</div>
		</div>
	</div>
	<div class="col-6">
		<div class="form-group">
			<label class="col-sm-12 control-label">Letak</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="letak">
				<option value="" disabled="" selected="">-- PILIH LETAK --</option>
				<option {{ $lingkungan_kerja->letak == 'strategis'?'selected':'' }} value="strategis">Strategis</option>
				<option {{ $lingkungan_kerja->letak == 'datar'?'selected':'' }} value="datar">Datar</option>
				
			</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-12 control-label">Penerangan</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="penerangan">
				<option value="" disabled="" selected="">-- PILIH PENERANGAN --</option>
				<option {{ $lingkungan_kerja->penerangan == 'terang'?'selected':'' }} value="terang">Terang</option>
				<option {{ $lingkungan_kerja->penerangan == 'gelap'?'selected':'' }} value="gelap">Gelap</option>
				
			</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-12 control-label">Suara</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="suara">
				<option value="" disabled="" >-- PILIH SUARA --</option>
				<option {{ $lingkungan_kerja->suara == 'tenang'?'selected':'' }} value="tenang">Tenang</option>
				<option {{ $lingkungan_kerja->suara == 'berisik'?'selected':'' }} value="berisik">Berisik</option>
				
			</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-12 control-label">Keadaan Tempat Kerja</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="keadaan_tempat_kerja">
				<option value="" disabled="" >-- PILIH KEADAAN TEMPAT KERJA --</option>
				<option {{ $lingkungan_kerja->bersih_dan_rapi == 'dingin'?'selected':'' }} value="bersih_dan_rapi">Bersih Dan Rapi</option>
				<option {{ $lingkungan_kerja->kotor == 'dingin'?'selected':'' }} value="kotor">Kotor</option>
				
			</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-12 control-label">Getaran</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="getaran">
				<option value="" disabled="">-- PILIH GETARAN --</option>
				<option {{ $lingkungan_kerja->getaran == 'ada'?'selected':'' }} value="ada">Ada</option>
				<option {{ $lingkungan_kerja->getaran == 'tidak_ada'?'selected':'' }} value="tidak_ada">Tidak Ada</option>
				
			</select>
			</div>
		</div>	
	</div>
</div>

