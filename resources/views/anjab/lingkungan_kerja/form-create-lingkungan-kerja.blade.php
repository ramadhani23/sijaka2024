<input type="hidden" value="create" name="aksi">
<input type="hidden" value="{{ $id }}" name="id">
	
<div class="row">
	<div class="col-6">
		<div class="form-group">
			<label class="col-sm-12 control-label">Tempat Kerja</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="tempat_kerja">
				<option value="" disabled="" selected="">-- PILIH TEMPAT KERJA --</option>
				<option value="luar_ruangan">Luar Ruangan</option>
				<option value="dalam_ruangan">Dalam Ruangan</option>
				<option value="dalam_luar_ruangan">Dalam dan Luar Ruangan</option>
				
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-12 control-label">Suhu</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="suhu">
				<option value="" disabled="" selected="">-- PILIH SUHU --</option>
				<option value="dingin">Dingin</option>
				<option value="panas">Panas</option>
				
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-12 control-label">Udara</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="udara">
				<option value="" disabled="" selected="">-- PILIH UDARA --</option>
				<option value="sejuk">Sejuk</option>
				<option value="panas">Panas</option>
				
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-12 control-label">Keadaan Ruangan</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="keadaan_ruangan">
				<option value="" disabled="" selected="">-- PILIH KEADAAN RUANGAN --</option>
				<option value="baik">Baik</option>
				<option value="panas">Buruk</option>
				
			</select>
			</div>
		</div>
	</div>

	<div class="col-6">
		<div class="form-group">
			<label class="col-sm-12 control-label">Letak</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="letak">
				<option value="" disabled="" selected="">-- PILIH LETAK --</option>
				<option value="strategis">Strategis</option>
				<option value="datar">Datar</option>
				
			</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-12 control-label">Penerangan</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="penerangan">
				<option value="" disabled="" selected="">-- PILIH PENERANGAN --</option>
				<option value="terang">Terang</option>
				<option value="gelap">Gelap</option>
				
			</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-12 control-label">Suara</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="suara">
				<option value="" disabled="" selected="">-- PILIH SUARA --</option>
				<option value="tenang">Tenang</option>
				<option value="berisik">Berisik</option>
				
			</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-12 control-label">Keadaan Tempat Kerja</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="keadaan_tempat_kerja">
				<option value="" disabled="" selected="">-- PILIH KEADAAN TEMPAT KERJA --</option>
				<option value="bersih_dan_rapi">Bersih Dan Rapi</option>
				<option value="kotor">Kotor</option>
				
			</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-12 control-label">Getaran</label>
			<div class="col-sm-12">
			<select required="" class="form-control  " name="getaran">
				<option value="" disabled="" selected="">-- PILIH GETARAN --</option>
				<option value="ada">Ada</option>
				<option value="tidak_ada">Tidak Ada</option>
			</select>
			</div>
		</div>	
	</div>
</div>


