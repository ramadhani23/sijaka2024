{{-- <style type="text/css">
	.select2-selection--single{
		min-width: 450px;
	}
</style>
<input type="hidden" value="update" name="aksi">
<input type="hidden" value="{{ $anjab->id }}" name="id">
<div class="form-group">
	<label>Nama Jabatan</label>
	<select class="form-control select2" id="jabatan" name="jabatan">
		 <option value="{{ $anjab->m_jabatan_id }}" {{ $anjab->m_jabatan_id != ''?'selected':'' }} > {{ $anjab->jabatan->nama}}</option>
	</select>
	<!-- <input type="text" class="form-control" name="nama" required="" value="{{ $anjab->nama }}"> -->
</div>

<div class="form-group">
	<label>Jenis Jabatan</label>
	<input type="text" class="form-control" name="jenis_jabatan" required="" value="{{ $anjab->jenis_jabatan }}">
</div>

<div class="form-group">
	<label>Eselon Jabatan</label>
	<br>
	<select required="" class="form-control " name="eselon_jabatan">
		<option value="" disabled="" selected="">-- PILIH ESELON JABATAN --</option>
		<option {{ $anjab->eselon_jabatan == 'Ia'?'selected':'' }} value="Ia">Ia</option>
		<option {{ $anjab->eselon_jabatan == 'Ib'?'selected':'' }} value="Ib">Ib</option>
		<option {{ $anjab->eselon_jabatan == 'IIa'?'selected':'' }} value="IIa">IIa</option>
		<option {{ $anjab->eselon_jabatan == 'IIb'?'selected':'' }} value="IIb">IIb</option>
		<option {{ $anjab->eselon_jabatan == 'IIIa'?'selected':'' }} value="IIIa">IIIa</option>
		<option {{ $anjab->eselon_jabatan == 'IIIb'?'selected':'' }} value="IIIb">IIIb</option>
		<option {{ $anjab->eselon_jabatan == 'IVa'?'selected':'' }} value="IVa">IVa</option>
		<option {{ $anjab->eselon_jabatan == 'IVb'?'selected':'' }} value="IVb">IVb</option>
	</select>
</div>

<div class="form-group">
	<label>Kode Jabatan</label>
	<input type="text" class="form-control" name="kode_jabatan" required="" value="{{ $anjab->kode_jabatan }}">
</div>



<div class="form-group">
	<label>Unit Kerja Eselon 3</label>
	<br>
	<select required="" name="unit_kerja_eselon3" class="form-control select2" id="unit_kerja_eselon3">
		<option value="" disabled="" selected="">-- PILIH UNIT KERJA ESELON 2 --</option>
		@foreach($unit_kerja_eselon3 as $unit_kerja_eselon3s)
		<option {{ $unit_kerja_eselon3s->kodeunit == $anjab->unit_kerja_eselon3?'selected':'' }} value="{{ $unit_kerja_eselon3s->kodeunit }}">{{ $unit_kerja_eselon3s->unitkerja }}</option>
		@endforeach
	</select>
	<input name="unit_kerja_eselon3" class="form-control" value="{{ $anjab->unit_kerja_eselon3 }}">
</div>

<div class="form-group">
	<label>Unit Kerja Eselon 4</label>
	<br>
	<select required="" name="unit_kerja_eselon4" class="form-control select2" id="unit_kerja_eselon4">
		<option value="" disabled="" selected="">-- PILIH UNIT KERJA ESELON 2 --</option>
		@foreach($unit_kerja_eselon4 as $unit_kerja_eselon4s)
		<option {{ $unit_kerja_eselon4s->kodeunit == $anjab->unit_kerja_eselon4?'selected':'' }} value="{{ $unit_kerja_eselon4s->kodeunit }}">{{ $unit_kerja_eselon4s->unitkerja }}</option>
		@endforeach
	</select>
	<!-- <input name="unit_kerja_eselon4" class="form-control" value="{{ $anjab->unit_kerja_eselon4 }}"> -->
</div>

<div class="form-group">
	<label>Ikhtisar Jabatan</label>
	<textarea required="" name="ikhtisar_jabatan" class="form-control">{{ $anjab->ikhtisar_jabatan }}</textarea> 
</div>

<script type="text/javascript">
	// Initialization
	jQuery(document).ready(function() {

	    function formatRepo(repo) {
	        if (repo.loading) return repo.text;

	        var markup =
	          "<div class='select2-result-repository clearfix'>" +
	            "<div class='select2-result-repository__meta'>" +
	            "<div class='select2-result-repository__title'>" +
	            "<div>"+repo.jabatan+"</div>"+
	            // "<div>Poin : "+repo.poin+"</div>"+
	            // "<div>Durasi : "+repo.durasi+"Menit </div>"+
	          '</div></div></div>';

	        return markup;
	    }

	    function formatRepoSelection(repo) {
	        return repo.full_name || repo.text;
	    }

	    $("#jabatan").select2({
	        placeholder: "Search Jabatan",
	        allowClear: true,
	        ajax: {
	            url: '{{ route("anjab.get-jabatan") }}',
	            dataType: 'json',
	            delay: 250,
	            data: function(params) {
	                return {
	                    q: params.term, // search term
	                    page: params.page
	                };
	            },
	            processResults: function(data, params) {
	                // parse the results into the format expected by Select2
	                // since we are using custom formatting functions we do not need to
	                // alter the remote JSON data, except to indicate that infinite
	                // scrolling can be used
	                params.page = params.page || 1;

	                return {
	                    results: data.items,
	                    pagination: {
	                        more: (params.page * 30) < data.total_count
	                    }
	                };
	            },
	            cache: true
	        },
	        escapeMarkup: function(markup) {
	            return markup;
	        }, // let our custom formatter work
	        minimumInputLength: 1,
	        templateResult: formatRepo, // omitted for brevity, see the source of this page
	        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
	    });
	});
</script>

 --}}

 <div class="row">
	<div class="col-4">
		<input type="hidden" value="{{ $anjab->id }}" name="id">
		<input type="hidden" value="update" name="aksi">


		<div class="form-group">
			<label>Jenis Jabatan</label>
			<select id="jenis" required="" class="form-control" name="jenis_jabatan">
				<option selected="" disabled="" value="">PILIH JENIS JABATAN</option>
				<option {{ $anjab->jenis_jabatan == '1'?'selected':'' }} value="1">Struktural</option>
				<option {{ $anjab->jenis_jabatan == '2'?'selected':'' }} value="2">Fungsional</option>
				<option {{ $anjab->jenis_jabatan == '3'?'selected':'' }} value="3">Pelaksana</option>
			</select>
		</div>

		<div class="form-group">
			<label>Nama Jabatan</label>
			<select style="width: 100%" class="form-control select2" id="jabatan" name="jabatan">
				{{-- <option label="Label"></option> --}}
				<option value="{{ $anjab->m_jabatan_id }}">{{ @$anjab->jabatan->jabatan }}</option>
			</select>
		</div>



		<div class="form-group">
			<label>Perangkat Daerah</label>
			<select style="width: 100%" required="" name="pd" class="form-control select2">
				<option value="" disabled="" selected="">-- PILIH PERANGKAT DAERAH --</option>
				@foreach($unitKerja as $unitKerjas)
				<option {{ $anjab->kode_opd == $unitKerjas->kodeunit?'selected':'' }} value="{{ $unitKerjas->kodeunit }}">{{ $unitKerjas->unitkerja }}</option>
				@endforeach
			</select>
		</div>

		

		<div class="form-group">
			<label>Eselon</label>
			<select class="form-control " name="eselon_jabatan">
				<option value="" selected="">-- TANPA ESELON --</option>
				<option {{ $anjab->eselon_jabatan == 11?'selected':'' }} value="11">Ia</option>
				<option {{ $anjab->eselon_jabatan == 12?'selected':'' }} value="12">Ib</option>
				<option {{ $anjab->eselon_jabatan == 21?'selected':'' }} value="21">IIa</option>
				<option {{ $anjab->eselon_jabatan == 22?'selected':'' }} value="22">IIb</option>
				<option {{ $anjab->eselon_jabatan == 31?'selected':'' }} value="31">IIIa</option>
				<option {{ $anjab->eselon_jabatan == 32?'selected':'' }} value="32">IIIb</option>
				<option {{ $anjab->eselon_jabatan == 41?'selected':'' }} value="41">IVa</option>
				<option {{ $anjab->eselon_jabatan == 42?'selected':'' }} value="42">IVb</option>
			</select>
		</div>

		<div class="form-group">
			<label>Kode Jabatan</label>
			<input readonly value="{{ $anjab->kode_jabatan }}" type="text" class="form-control" name="kode_jabatan">
		</div>

	</div>
	<div class="col-4">
		<div class="form-group">
			<label>Atasan Langsung</label>
			<select style="width: 100%" required="" name="atasan_langsung" class="form-control" id="atasan_langsung">
				<option value="" disabled="" selected="">-- PILIH ATASAN --</option>
				@foreach($pejabatStruktural as $itemPejabat)
				<option {{ $anjab->parent == $itemPejabat->id?'selected':'' }} value="{{ $itemPejabat->id }}">{{ @$itemPejabat->jabatan->jabatan }}</option>
				@endforeach
			</select>
		</div>

		@if(MojokertokabUser::getUser()->kode_opd >= 31 && MojokertokabUser::getUser()->kode_opd <= 48)
		<div class="form-group">
			<label>Unit Kerja Eselon 3 (Optional)</label>
			<select style="width: 100%" required name="unit_kerja_eselon3" class="form-control select2" id="unit_kerja_eselon3" >
				<option value="" disabled="" selected="">-- PILIH UNIT KERJA ESELON 3 --</option>
				@if($anjab->unit_kerja_eselon3)
				@foreach($unitKerjaEselon3 as $unitKerjas)
				<option {{ $anjab->unit_kerja_eselon3 == $unitKerjas->kodeunit?'selected':'' }} value="{{ $unitKerjas->kodeunit }}">{{ $unitKerjas->unitkerja }}</option>
				@endforeach
				@endif
			</select>
			<!-- <input name="unit_kerja_eselon3" class="form-control"> -->
		</div>

		<div class="form-group">
			<label>Unit Kerja Eselon 4 (Optional)</label>
			<select style="width: 100%" name="unit_kerja_eselon4" class="form-control select2" id="unit_kerja_eselon4" >
				<option value="" selected="">-- PILIH UNIT KERJA ESELON 4 --</option>
				@if($anjab->unit_kerja_eselon4)
				@foreach($unitKerjaEselon4 as $unitKerjas)
				<option {{ $anjab->unit_kerja_eselon4 == $unitKerjas->kodeunit?'selected':'' }} value="{{ $unitKerjas->kodeunit }}">{{ $unitKerjas->unitkerja }}</option>
				@endforeach
				@endif
			</select>
			<!-- <input name="unit_kerja_eselon4" class="form-control"> -->
		</div>
		@else
		<div class="form-group">
			<label>Unit Kerja Eselon 2</label>
			<select style="width: 100%" required="" name="unit_kerja_eselon2" class="form-control select2" id="unit_kerja_eselon2">
				<option value="" disabled="" selected="">-- PILIH UNIT KERJA ESELON 2 --</option>
				@foreach($unitKerja as $unitKerjas)
				<option {{ $unitKerjas->kodeunit == $anjab->unit_kerja_eselon2?'selected':'' }} value="{{ $unitKerjas->kodeunit }}">{{ $unitKerjas->unitkerja }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Unit Kerja Eselon 3 (Optional)</label>
			<select style="width: 100%" name="unit_kerja_eselon3" class="form-control select2" id="unit_kerja_eselon3" >
				<option value="">-- PILIH UNIT KERJA ESELON 3 --</option>
				@if($anjab->unit_kerja_eselon3)
				@foreach($unitKerjaEselon3 as $unitKerjas)
				<option {{ $anjab->unit_kerja_eselon3 == $unitKerjas->kodeunit?'selected':'' }} value="{{ $unitKerjas->kodeunit }}">{{ $unitKerjas->unitkerja }}</option>
				@endforeach
				@endif
			</select>
			<!-- <input name="unit_kerja_eselon3" class="form-control"> -->
		</div>

		<div class="form-group">
			<label>Unit Kerja Eselon 4 (Optional)</label>
			<select style="width: 100%" name="unit_kerja_eselon4" class="form-control select2" id="unit_kerja_eselon4" >
				<option value="" selected="">-- PILIH UNIT KERJA ESELON 4 --</option>
				@if($anjab->unit_kerja_eselon4)
				@foreach($unitKerjaEselon4 as $unitKerjas)
				<option {{ $anjab->unit_kerja_eselon4 == $unitKerjas->kodeunit?'selected':'' }} value="{{ $unitKerjas->kodeunit }}">{{ $unitKerjas->unitkerja }}</option>
				@endforeach
				@endif
			</select>
			<!-- <input name="unit_kerja_eselon4" class="form-control"> -->
		</div>
		@endif
	</div>

	<div class="col-4">
		<div class="form-group">
			<label>Pangkat / Golongan</label>
			<br>
			<select required="" class="form-control " name="pangkat">
				<option value="" disabled="" selected="">-- PANGKAT GOLONGAN --</option>
				<option {{ $anjab->pangkat == 11?'selected':'' }} value="11">Ia</option>
				<option {{ $anjab->pangkat == 12?'selected':'' }} value="12">Ib</option>
				<option {{ $anjab->pangkat == 13?'selected':'' }} value="13">Ic</option>
				<option {{ $anjab->pangkat == 14?'selected':'' }} value="14">Id</option>
				<option {{ $anjab->pangkat == 21?'selected':'' }} value="21">IIa</option>
				<option {{ $anjab->pangkat == 22?'selected':'' }} value="22">IIb</option>
				<option {{ $anjab->pangkat == 23?'selected':'' }} value="23">IIc</option>
				<option {{ $anjab->pangkat == 24?'selected':'' }} value="24">IId</option>
				<option {{ $anjab->pangkat == 31?'selected':'' }} value="31">IIIa</option>
				<option {{ $anjab->pangkat == 32?'selected':'' }} value="32">IIIb</option>
				<option {{ $anjab->pangkat == 33?'selected':'' }} value="33">IIIc</option>
				<option {{ $anjab->pangkat == 34?'selected':'' }} value="33">IIId</option>
				<option {{ $anjab->pangkat == 41?'selected':'' }} value="41">IVa</option>
				<option {{ $anjab->pangkat == 42?'selected':'' }} value="42">IVb</option>
				<option {{ $anjab->pangkat == 43?'selected':'' }} value="43">IVc</option>
				<option {{ $anjab->pangkat == 44?'selected':'' }} value="44">IVd</option>
				<option {{ $anjab->pangkat == 45?'selected':'' }} value="45">IVe</option>
			</select>
		</div>

		<div class="form-group">
			<label>Kualifikasi Pendidikan</label>
			<textarea required="" rows="3" required="" name="kualifikasi_pendidikan" class="form-control">{{ $anjab->kualifikasi_pendidikan }}</textarea> 
		</div>

		<div class="form-group">
			<label>Diklat Penjenjangan</label>
			<textarea required="" rows="2" required="" name="diklat_penjenjangan" class="form-control">{{ $anjab->diklat_penjenjangan }}</textarea> 
		</div>

		<div class="form-group">
			<label>Diklat Teknis</label>
			<textarea required="" rows="2" required="" name="diklat_teknis" class="form-control">{{ $anjab->diklat_teknis }}</textarea> 
		</div>

		<div class="form-group">
			<label>Pengalaman Kerja</label>
			<textarea required="" rows="2" required="" name="pengalaman_kerja" class="form-control">{{ $anjab->pengalaman_kerja }}</textarea> 
		</div>

		<div class="form-group">
			<label>Pengetahuan Kerja</label>
			<textarea required="" rows="2" required="" name="pengetahuan_kerja" class="form-control">{{ $anjab->pengetahuan_kerja }}</textarea> 
		</div>

	</div>

	<div class="col-12">
		<div class="form-group">
			<label>Ikhtisar Jabatan</label>
			<textarea required="" name="ikhtisar_jabatan" class="form-control">{{ $anjab->ikhtisar_jabatan }}</textarea> 
		</div>
	</div>

</div>

<script type="text/javascript">

	$("#unit_kerja_eselon2").change(function() {
      var dats = 'id='+ $(this).val();
      $.ajax({
        type: "GET",
        url: '{{ route("anjab.get-unit-kerja") }}',
        data: dats,
        cache: false,
        success: function(html) {
          $("#unit_kerja_eselon3").html(html);
        }
      });
    });

    $("#unit_kerja_eselon3").change(function() {
      var dats = 'id='+ $(this).val();
      $.ajax({
        type: "GET",
        url: '{{ route("anjab.get-unit-kerja") }}',
        data: dats,
        cache: false,
        success: function(html) {
          $("#unit_kerja_eselon4").html(html);
        }
      });
    });

    // Initialization
	jQuery(document).ready(function() {

			$('#jabatan').on('select2:select', function (e) {
			    var data = e.params.data;
			    console.log(data);
			    $('textarea[name=ikhtisar_jabatan]').val(data.ikhtisar_jabatan);
			    if(data.kualifikasi_pendidikan !== null){
			    	$('textarea[name=kualifikasi_pendidikan]').val(data.kualifikasi_pendidikan);
			    }
			    
			    if(data.diklat_penjenjangan !== null){
			    	$('textarea[name=diklat_penjenjangan]').val(data.diklat_penjenjangan);	
			    }
			    
			    $('input[name=kode_jabatan]').val(data.kode_jabatan);
			});

	    function formatRepo(repo) {
	        if (repo.loading) return repo.text;

	        var markup =
	          "<div class='select2-result-repository clearfix'>" +
	            "<div class='select2-result-repository__meta'>" +
	            "<div class='select2-result-repository__title'>" +
	            "<div>"+repo.jabatan+"</div>"+
	            // "<div>Poin : "+repo.poin+"</div>"+
	            // "<div>Durasi : "+repo.durasi+"Menit </div>"+
	          '</div></div></div>';

	        return markup;
	    }

	    function formatRepoSelection(repo) {
	        return repo.full_name || repo.text;
	    }

	    $("#jabatan").select2({
	        placeholder: "Cari Jabatan",
	        allowClear: true,
	        ajax: {
	            url: '{{ route("anjab.get-jabatan") }}',
	            dataType: 'json',
	            delay: 250,
	            data: function(params) {
	                return {
		                	jenis: $('#jenis').val(),
	                    q: params.term, // search term
	                    page: params.page
	                };
	            },
	            processResults: function(data, params) {
	                // parse the results into the format expected by Select2
	                // since we are using custom formatting functions we do not need to
	                // alter the remote JSON data, except to indicate that infinite
	                // scrolling can be used
	                params.page = params.page || 1;

	                return {
	                    results: data.items,
	                    pagination: {
	                        more: (params.page * 30) < data.total_count
	                    }
	                };
	            },
	            cache: true
	        },
	        escapeMarkup: function(markup) {
	            return markup;
	        }, // let our custom formatter work
	        minimumInputLength: 1,
	        templateResult: formatRepo, // omitted for brevity, see the source of this page
	        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
	    });
	});

</script>