
	<style type="text/css">
		
       body{padding:0px 50px;width:100%;border:0px solid red}
      .borders{border:0px solid black}
      .pt-15{padding-top:15px}
      .pt-7{padding-top:7px}
      .p-10{padding:5px}
      .mt-15{margin-top:15px}
      .mt-7{margin-top:7px}
      .font-size-isi{ font-size: 10pt}
      .v-align-top{vertical-align: text-top}

      table {
          width: 100%;
          margin-bottom: 1rem;
          color: #0f0f10;
      }

      .table {
        border-collapse : collapse !important;
      }

      .table {
        width : 100%;
        margin-bottom : 1rem;
        color : #0f0f10;
      }

      .table th, .table td {
        padding : 0.72rem;
        vertical-align : top;
        border-top : 1px solid #0f0f10;
      }

      .table thead th {
        vertical-align : bottom;
        border-bottom : 2px solid #0f0f10;
      }

      .table tbody + tbody {
        border-top : 2px solid #0f0f10;
      }

      .table-sm th, .table-sm td {
        padding : 0.3rem;
      }

      .table td, .table th {
        background-color : #FFFFFF !important;
      }

      .table-bordered {
        border : 1px solid #0f0f10;
      }

      .table-bordered th, .table-bordered td {
        border : 1px solid #0f0f10;
      }

      .table-bordered thead th, .table-bordered thead td {
        border-bottom-width : 2px;
      }
      .table-bordered th, .table-bordered td {
        border : 1px solid #0f0f10 !important;
      }

      .col1{
        width: 600px;
      }
	   

	</style>


	<div id="doc-header">
		<table style="width:100%">
			<tr>
	          <td  style="height:100px;text-align:center">
	            <img src="https://upload.wikimedia.org/wikipedia/commons/e/e9/Lambang_Kabupaten_Mojokerto.png" width="70px" alt="">
	          </td>
	        </tr>
	        <tr>
	          <td class="borders" style="height:auto;text-align:center;font-size:16pt">
	            <b>Informasi Anjab</b><br>
	          </td>
	        </tr>
		</table>
	</div>
	<br>
	<div id="doc-body">
		    <table class="mt-7" style="width:100%;font-size:12pt">
            <tr>
              <td class="borders font-size-isi" style="height:auto;width: 20%;text-align:left"><b>Nama Jabatan</b></td>
              <td class="borders font-size-isi" style="height:auto;width: 2%;text-align:left">:</td>
              <td class="borders font-size-isi" style="height:auto;width: 78%;text-align:left">{{ $anjab->jabatan->nama }}</td>
            </tr>
            <tr>
              <td class="borders font-size-isi" style="height:auto;width: 20%;text-align:left"><b>Unit Kerja</b></td>
              <td class="borders font-size-isi" style="height:auto;width: 2%;text-align:left">:</td>
              <td class="borders font-size-isi" style="height:auto;width: 78%;text-align:left">
               {{ $anjab->eselon2->unitkerja }}
              </td>
            </tr>
            <tr>
              <td class="borders font-size-isi" style="height:auto;width: 20%;text-align:left"><b>Intsansi</b></td>
              <td class="borders font-size-isi" style="height:auto;width: 2%;text-align:left">:</td>
              <td class="borders font-size-isi" style="height:auto;width: 78%;text-align:left">
               Pemerintah Kabupaten Mojokerto
              </td>
            </tr>
         </table>

        <div style="text-align: left">
          <b>I.&nbsp;PERAN JABATAN :</b>
        </div>
        <div style="text-align: left; margin-left: 20px; margin-top: 15px;">
          {!! $anjab->ikhtisar_jabatan !!}
        </div>
        <div style="text-align: left; margin-top: 20px">
          <b>II.&nbsp;URAIAN TUGAS :</b>
        </div>
        <div style="text-align: left; margin-left: 20px; margin-top: 15px;">
          <table style="border: none;border-collapse: separate;">
              <tbody>
                @php $a = 0; @endphp
                @foreach($anjab_uraian as $anjabs)
                @php 
                  $tahapan = $anjabs->tahapan;
                  $a++;
                @endphp
                <tr>
                  <td style="vertical-align: top;">{{ $a }}.</td>
                  <td>  {{ $anjabs->uraian_jabatan }}
                    <br>
                    @if($tahapan->count() != 0)
                    <table>
                      <tbody>
                        @php $i = 0; @endphp
                        @foreach($tahapan as $tahapans)
                        @php $i++; @endphp
                        <tr>
                          <td width="10px">{{ $a.'.'.$i }}. </td>
                          <td>  {!! $tahapans->uraian !!}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    @endif
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
        <div style="text-align: left; margin-top: 20px">
          <b>II.&nbsp;TANGGUNG JAWAB :</b>
        </div>
        <div style="text-align: left; margin-left: 20px; margin-top: 15px;">
          {!! $anjab_tanggung_jawab->tanggung_jawab !!}
        </div>
        <div style="text-align: left; margin-top: 20px">
          <b>III.&nbsp;WEWENANG :</b>
        </div>
        <div style="text-align: left; margin-left: 20px; margin-top: 15px;">
          {!! $anjab_wewenang->wewenang !!}
        </div>
        <div style="text-align: left; margin-top: 20px">
          <b>IV.&nbsp;KORELASI JABATAN :</b>
        </div>
        <div style="text-align: left; margin-left: 20px; margin-top: 15px;">
          <table class="table table-bordered">
            <thead>
              <tr>
                 <th class="text-center">No</th>
                  <th class="text-center">Nama Jabatan</th>
                  <th class="text-center">Unit Kerja / Instansi</th>
                  <th class="text-center">Hal</th>
              </tr>
            </thead>
            <tbody>
              @if($anjab_korelasi_jabatan->count() == 0)
                <tr>
                  <td colspan="3" class="text-center">Tidak Ada Data</td>
                </tr>
              @endif
              @foreach($anjab_korelasi_jabatan as $korelasi_jabatans)
              <tr>
                <td class="text-center">{{ $loop->iteration }}</td>
                <td class="text-center">{{ $korelasi_jabatans->jabatan }}</td>
                <td class="text-center">{{ $korelasi_jabatans->unit_kerja }}</td>
                <td class="text-center">{{ $korelasi_jabatans->hal }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div style="text-align: left; margin-top: 20px">
          <b>V.&nbsp;BAHAN KERJA :</b>
        </div>
        <div style="text-align: left; margin-left: 20px; margin-top: 15px;">
          <table class="table table-bordered">
            <thead>
              <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Bahan Kerja</th>
                  <th class="text-center">Penggunaan Dalam Tugas</th>
              </tr>
            </thead>
            <tbody>
              @if($anjab_bahan_kerja->count() == 0)
                <tr>
                  <td colspan="3" class="text-center">Tidak Ada Data</td>
                </tr>
              @endif
              @foreach($anjab_bahan_kerja as $bahan_kerjas)
              <tr>
                <td class="text-center">{{ $loop->iteration }}</td>
                <td>{{ $bahan_kerjas->bahan_kerja }}</td>
                <td>{{ $bahan_kerjas->penggunaan_dalam_tugas }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        
        <div style="text-align: left; margin-top: 20px;">
          <b>VI.&nbsp;PERANGKAT / ALAT KERJA :</b>
        </div>
        <div style="text-align: left; margin-left: 20px; margin-top: 15px;">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Perangkat / ALat Kerja</th>
                <th class="text-center">Digunakan Untuk Tugas</th>
              </tr>
            </thead>
            <tbody>
              @if($anjab_perangkat->count() == 0)
                <tr>
                  <td colspan="3" class="text-center">Tidak Ada Data</td>
                </tr>
              @endif
              @foreach($anjab_perangkat as $perangkats)
              <tr>
                <td class="text-center">{{ $loop->iteration }}</td>
                <td class="text-center">{{ $perangkats->perangkat_kerja }}</td>
                <td class="text-center">{{ $perangkats->digunakan_untuk_tugas }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>

        <div style="text-align: left; margin-top: 20px;">
          <b>VII.&nbsp;HASIL KERJA :</b>
        </div>
        <div style="text-align: left; margin-left: 20px; margin-top: 15px;">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Hasil Kerja</th>
                <th class="text-center">Satuan Hasil</th>
              </tr>
            </thead>
            <tbody>
              @if($anjab_hasil_kerja->count() == 0)
                <tr>
                  <td colspan="3" class="text-center">Tidak Ada Data</td>
                </tr>
              @endif
              @foreach($anjab_hasil_kerja as $hasil_kerjas)
              <tr>
                <td class="text-center">{{ $loop->iteration }}</td>
                <td class="text-center">{!! $hasil_kerjas->hasil_kerja !!}</td>
                <td class="text-center">{{ $hasil_kerjas->satuan_hasil }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>

        
        
        <div style="text-align: left; margin-top: 20px">
          <b>VIII.&nbsp;KONDISI LINGKUNGAN KERJA :</b>
        </div>
        <div style="text-align: left; margin-left: 20px; margin-top: 15px;">
          <table class="table table-striped table-bordered " >
            <tbody>
              <tr>
                <td width="5%">a.</td>
                <td width="25%">Tempat Kerja</td>
                <td width="25%">
                  @isset($anjab_lingkungan_kerja)
                    {{ ucfirst(str_replace("_"," ",$anjab_lingkungan_kerja->tempat_kerja)) }}
                  @endisset
                </td>
              </tr>
              <tr>
                <td width="5%">b.</td>
                 <td width="25%">Suhu</td>
                 <td width="25%">
                   @isset($anjab_lingkungan_kerja)
                    {{ ucfirst(str_replace("_"," ",$anjab_lingkungan_kerja->suhu)) }}
                   @endisset
                 </td>
              </tr>
              <tr>
                <td width="5%">c.</td>
                <td width="25%">Udara</td>
                <td width="25%">
                  @isset($anjab_lingkungan_kerja)
                    {{ ucfirst(str_replace("_"," ",$anjab_lingkungan_kerja->udara)) }}
                  @endisset
                </td>
              </tr>
              <tr>
                <td width="5%">d.</td>
                <td width="25%">Keadaan Ruangan</td>
                <td width="25%">
                  @isset($anjab_lingkungan_kerja)
                    {{ ucfirst(str_replace("_"," ",$anjab_lingkungan_kerja->keadaan_ruangan)) }}
                  @endisset
                </td>
              </tr>
              <tr>
                <td width="5%">e.</td>
                <td width="25%">Letak</td>
                <td width="25%">
                  @isset($anjab_lingkungan_kerja)
                    {{ ucfirst(str_replace("_"," ",$anjab_lingkungan_kerja->letak)) }}
                  @endisset
                </td>
              </tr>
              <tr>
                <td width="5%">f.</td>
                <td width="25%">Penerangan</td>
                <td width="25%">
                  @isset($anjab_lingkungan_kerja)
                    {{ ucfirst(str_replace("_"," ",$anjab_lingkungan_kerja->penerangan)) }}
                  @endisset
                 </td>
              </tr>  
              <tr>
                <td width="5%">g.</td>
                <td width="25%">Suara</td>
                <td width="25%">
                  @isset($anjab_lingkungan_kerja)
                    {{ ucfirst(str_replace("_"," ",$anjab_lingkungan_kerja->suara)) }}
                  @endisset
                </td>
              </tr>  
              <tr>
                <td width="5%">h.</td>
                <td width="25%">Keadaan Tempat Kerja</td>
                <td width="25%">
                  @isset($anjab_lingkungan_kerja)
                    {{ ucfirst(str_replace("_"," ",$anjab_lingkungan_kerja->keadaan_tempat_kerja)) }}
                  @endisset
                </td>
              </tr>
              <tr>
                <td width="5%">i.</td>
                <td width="25%">Getaran</td>
                <td width="25%">
                  @isset($anjab_lingkungan_kerja)
                    {{ ucfirst(str_replace("_"," ",$anjab_lingkungan_kerja->getaran)) }}
                  @endisset
                </td>
              </tr>
              </tr>
            </tbody>
          </table>
        </div>

        <div style="text-align: left; margin-top: 20px;">
          <b>IX.&nbsp;RESIKO BAHAYA :</b>
        </div>
        <div style="text-align: left; margin-left: 20px; margin-top: 15px;">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Resiko Bahaya</th>
                <th class="text-center">Penyebab</th>
              </tr>
            </thead>
            <tbody>
              @if($anjab_resiko_bahaya->count() == 0)
                <tr>
                  <td colspan="3" class="text-center">Tidak Ada Data</td>
                </tr>
              @endif
              @foreach($anjab_resiko_bahaya as $resiko_bahayas)
              <tr>
                <td class="text-center">{{ $loop->iteration }}</td>
                <td class="text-center">{{ $resiko_bahayas->resiko_bahaya }}</td>
                <td class="text-center">{{ $resiko_bahayas->penyebab }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>

        <div style="text-align: left; margin-top: 20px;">
          <b>X.&nbsp;SYARAT JABATAN :</b>
        </div>
        <div style="text-align: left; margin-left: 20px; margin-top: 15px;">
          <h3>Keterampilan Kerja :</h3>
          <p style="text-align: justify;">@isset($anjab_syarat_jabatan){!! $anjab_syarat_jabatan->keterampilan_kerja !!}@endisset</p>
          <h3>Bakat Kerja :</h3>
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th colspan="2" class="text-center">Bakat Kerja</th>
                <th>Ya / Tidak</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>(G) Intelegensia</td>
                <td>Kemampuan belajar secara umum</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                    {{$anjab_syarat_jabatan->intelegensia == '1'?'Ya':'Tidak'}} 
                  @endisset
                 </td>
              </tr>
              <tr>
                <td>(V) Bakat Verbal</td>
                <td>Kemampuan untuk memahami arti kata-kata dan penggunaannya secara tepat dan efektif</td>
                <td>
                    @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->bakat_verbal == '1'?'Ya':'Tidak'}} 
                    @endisset
                 </td>
              </tr>
              <tr>
                <td>(N) Bakat Numerik</td>
                <td>Kemampuan untuk melakukan operasi aritmatik secara tepat dan akurat</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                    {{$anjab_syarat_jabatan->bakat_numerik == '1'?'Ya':'Tidak'}} 
                  @endisset
                 </td>
              </tr>
              <tr>
                <td>(Q) Bakat Ketelitian</td>
                <td>berkaitan dalam bahan verbal atau dalam tabel</td>
                <td>
                    @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->bakat_ketelitian == '1'?'Ya':'Tidak'}} 
                    @endisset
                 </td>
              </tr>
            </tbody>
          </table>
          <h3>Tempramen Kerja :</h3>
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th colspan="2" class="text-center">Temperamen Kerja</th>
                <th>Ya / Tidak</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>(D) Directing Control Planning (DCP)</td>
                <td>Kemampuan menyesuaikan diri menerima tanggung jawab untuk kegiatan memimpin, mengendalikan atau merencanakan</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->directing_control_planning == '1'?'Ya':'Tidak'}} 
                  @endisset
                 </td>
              </tr>
              <tr>
                <td>(F) Feeling Idea Fact (FIF)</td>
                <td>Kemampuan menyesuaikan diri dengan kegiatan yang mengandung penafsiran perasaan, gagasan atau fakta dari sudut pandangan pribadi</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->feeling_idea_fact == '1'?'Ya':'Tidak'}} 
                  @endisset
                </td>
              </tr>
              <tr>
                <td>(I) Influencing (INFLU)</td>
                <td>Kemampuan menyesuaikan diri untuk pekerjaan-pekerjaan mempengaruhi orang lain dalam pendapat, sikap atau pertimbangan mengenai gagasan</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{ $anjab_syarat_jabatan->influencing == '1'?'Ya':'Tidak'}}  
                  @endisset
                </td>
              </tr>
              <tr>
                <td>(M) Measurable and Verifiable Creteria (MVC)</td>
                <td>Kemampuan menyesuaikan diri dengan kegiatan pengambilan peraturan, pembuatan pertimbangan, atau pembuatan peraturan berdasarkan kriteria yang diukur atau yang dapat diuji</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->measurable_verifiable_creteria == '1'?'Ya':'Tidak'}} 
                  @endisset
                </td>
              </tr>
              <tr>
                <td>(P) Dealing with People (DEPL)</td>
                <td>Kemampuan menyesuaikan diri dalam berhubungan dengan orang lain lebih dari hanya penerimaan dan pembuatan intruksi</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->dealing_with_people == '1'?'Ya':'Tidak'}} 
                   @endisset
                </td>
              </tr>
              <tr>
                <td>(R) Repetitive and Continuous (REPCON)</td>
                <td>Kemampuan menyesuaikan diri dalam kegiatan-kegiatan yang berulang, atau secara terus menerus melakukan kegiatan yang sama, sesuai dengan perangkat prosedur, urutan atau kecepatan tertentu</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                    {{$anjab_syarat_jabatan->repetitive_continuous == '1'?'Ya':'Tidak'}} 
                  @endisset
                </td>
              </tr>
            </tbody>
          </table>
          <h3>Minat Kerja :</h3>
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th colspan="2" class="text-center">Minat Kerja</th>
                <th>Ya / Tidak</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Investigatif</td>
                <td>Aktivitas yang memerlukan penyelidikan observasional, simbolik dan sistematik terhadap fenomena dan kegiatan ilmiah</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->investigatif == '1'?'Ya':'Tidak'}} 
                  @endisset
                </td>
              </tr>
              <tr>
                <td>Konvensional</td>
                <td>Aktivitas yang memerlukan manipulasi data yang eksplisit, kegiatan administrasi, rutin dan klerikal</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->konvensional == '1'?'Ya':'Tidak'}} 
                  @endisset
                </td>
              </tr>
            </tbody>
          </table>
          <h3>Kondisi Fisik :</h3>
          <table class="mt-7" style="width:100%;font-size:12pt">
            <tbody>
              <tr>
                <td>
                  Jenis Kelamin
                </td>
                <td>
                  :
                </td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{ ucfirst(str_replace("_"," ",$anjab_syarat_jabatan->jenis_kelamin)) }}
                  @endisset
                </td>
              </tr>
              <tr>
                <td>
                  Umur
                </td>
                <td>
                  :
                </td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{ $anjab_syarat_jabatan->umur }}
                  @endisset
                </td>
              </tr>
              <tr>
                <td>
                  Tinggi Badan
                </td>
                <td>
                  :
                </td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{ $anjab_syarat_jabatan->tinggi_badan }}
                  @endisset
                </td>
              </tr>
              <tr>
                <td>
                  Berat Badan
                </td>
                <td>
                  :
                </td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{ $anjab_syarat_jabatan->berat_badan }}
                  @endisset
                </td>
              </tr>
              <tr>
                <td>
                  Postur Badan
                </td>
                <td>
                  :
                </td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{ ucfirst($anjab_syarat_jabatan->postur_badan) }}
                  @endisset
                </td>
              </tr>
              <tr>
                <td>
                  Penampilan
                </td>
                <td>
                  :
                </td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{ ucfirst($anjab_syarat_jabatan->penampilan) }}
                  @endisset
                </td>
              </tr>
            </tbody>
          </table>
          <h3>Upaya Fisik  :</h3>
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th colspan="2" class="text-center">Upaya Fisik</th>
                <th>Ya / Tidak</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Duduk </td>
                <td>Berada dalam suatu tempat dalam posisi duduk biasa</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->duduk == '1'?'Ya':'Tidak'}}
                  @endisset
                </td>
              </tr>
              <tr>
                <td>Berbicara</td>
                <td>Menyatakan atau bertukar pikiran secara lisan agar dapat dipahami</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->berbicara == '1'?'Ya':'Tidak'}}
                  @endisset
                </td>
              </tr>
              <tr>
                <td>Melihat</td>
                <td>Usaha mengetahui dengan menggunakan mata</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->melihat == '1'?'Ya':'Tidak'}}
                  @endisset
                </td>
              </tr>
            </tbody>
          </table>
          <h3>Fungsi Pekerjaan  :</h3>
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th colspan="2" class="text-center">Fungsi Pekerjaan</th>
                <th>YA / Tidak</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>(D1) Mengkoordinasi data </td>
                <td>
                  Menentukan waktu, tempat atau urutan operasi yang akan dilaksanakan atau tindakan yang harus diambil berdasarkan hasil analisa data, melaksanakan ketentuan atau melaporkan kejadian dengan cara menghubung hubungkan mencari kaitan serta membandingkan data setelah data tersebut dianalisa
                </td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->mengkoordinasi_data == '1'?'Ya':'Tidak'}}
                  @endisset
                </td>
              </tr>
              <tr>
                <td>(D2) Menganalisis data</td>
                <td>Mempelajari, mengurangi, merinci dan menilai data untuk mendapatkan kejelasan,atau menyajikan tindakan alternatif</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->menganalisis_data == '1'?'Ya':'Tidak'}}
                   @endisset
                </td>
              </tr>
              <tr>
                <td>(D3) Menyusun data</td>
                <td>Mengerjakan, menghimpun atau mengelompokkan tentang data, orang atau benda</td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->menyusun_data == '1'?'Ya':'Tidak'}}
                  @endisset
                </td>
              </tr>
              <tr>
                <td>(O0) Menasehati</td>
                <td>
                  Memberi bimbingan, saran, konsultasi atau nasehat kepada perorangan atau instansi dalam pemecahan
                  masalah berdasarkan disiplin ilmu, spiritual, atau prinsip-prinsip keahlian lainnya
                </td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->menasehati == '1'?'Ya':'Tidak'}} 
                  @endisset
                </td>
              </tr>
              <tr>
                <td>(O1) Berunding</td>
                <td>
                  Menyelesaikan masalah tukar menukar dan beradu pendapat, argumen, gagasan, dengan pihak lain membuat keputusan
                </td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->berunding == '1'?'Ya':'Tidak'}}
                  @endisset
                </td>
              </tr>
              <tr>
                <td>(O2) Mengajar</td>
                <td>
                  Melatih orang lain dengan memberikan penjelasan, peragaan, bimbingan teknis, atau memberikan rekomendasi atas dasar disiplin yang bersifat teknis
                </td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->mengajar == '1'?'Ya':'Tidak'}}
                  @endisset
                </td>
              </tr>
              <tr>
                <td>(O3) Menyelia</td>
                <td>
                  Menentukan atau menafsirkan prosedur kerja, membagi tugas, menciptakan dan memelihara hubungan yang
                  harmonis diantara bawahan dan meningkatkan efisiensi
                </td>
                <td>
                  @isset($anjab_syarat_jabatan)
                      {{$anjab_syarat_jabatan->menyelia == '1'?'Ya':'Tidak'}}
                  @endisset
                </td>
              </tr>
            </tbody>
          </table>
        </div>

         
         
	</div>
