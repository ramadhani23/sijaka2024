<input type="hidden" name="id" id="id" value="{{ $anjab->id }}">
<ul class="nav flex-column nav-pills">
  <li class="nav-item mb-2">
    <a class="nav-link {{ @$pages=='detail_anjab'?'active':'' }}" href="{{ route('anjab.detail', $anjab->id) }}" >
      <span class="nav-icon">
        <i class="flaticon-signs-1"></i>
      </span>
      <span class="nav-text">Ringkasan</span>
    </a>
  </li>
  <li class="nav-item mb-2">
    <a class="nav-link {{ @$pages=='detail_uraian_tugas'?'active':'' }}" href="{{ route('anjab.uraian-tugas', $anjab->id) }}" >
      <span class="nav-icon">
        <i class="flaticon-event-calendar-symbol"></i>
      </span>
      <span class="nav-text">Uraian Tugas</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{ @$pages=='detail_hasil_kerja'?'active':'' }}" href="{{ route('anjab.browse-hasil-kerja', $anjab->id) }}">
      <span class="nav-icon">
        <i class="flaticon-presentation"></i>
      </span>
      <span class="nav-text">Hasil Kerja</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{ @$pages=='detail_bahan_kerja'?'active':'' }}" href="{{ route('anjab.browse-bahan-kerja', $anjab->id) }}">
      <span class="nav-icon">
        <i class="flaticon2-file-1"></i>
      </span>
      <span class="nav-text">Bahan Kerja</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{ @$pages=='detail_perangkat'?'active':'' }}" href="{{ route('anjab.browse-perangkat', $anjab->id) }}">
      <span class="nav-icon">
        <i class="flaticon-computer"></i>
      </span>
      <span class="nav-text">Perangkat / Alat Kerja</span>
    </a>
  </li>
  <li class="nav-item mb-2">
    <a class="nav-link {{ @$pages=='detail_tanggung_jawab'?'active':'' }}" href="{{ route('anjab.browse-tanggung-jawab', $anjab->id) }}" >
      <span class="nav-icon">
        <i class="flaticon-light"></i>
      </span>
      <span class="nav-text">Tanggung Jawab</span>
    </a>
  </li>
 
  <li class="nav-item">
    <a class="nav-link {{ @$pages=='detail_wewenang'?'active':'' }}" href="{{ route('anjab.browse-wewenang', $anjab->id) }}">
      <span class="nav-icon">
        <i class="flaticon2-group"></i>
      </span>
      <span class="nav-text">Wewenang</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{ @$pages=='detail_korelasi_jabatan'?'active':'' }}" href="{{ route('anjab.browse-korelasi-jabatan', $anjab->id) }}">
      <span class="nav-icon">
        <i class="flaticon2-reload"></i>
      </span>
      <span class="nav-text">Korelasi Jabatan</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{ @$pages=='detail_lingkungan_kerja'?'active':'' }}" href="{{ route('anjab.browse-lingkungan-kerja', $anjab->id) }}">
      <span class="nav-icon">
        <i class="flaticon-rotate"></i>
      </span>
      <span class="nav-text">Kondisi Lingkungan Kerja</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{ @$pages=='detail_resiko_bahaya'?'active':'' }}" href="{{ route('anjab.browse-resiko-bahaya', $anjab->id) }}">
      <span class="nav-icon">
        <i class="flaticon2-warning"></i>
      </span>
      <span class="nav-text">Resiko Bahaya</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{ @$pages=='detail_syarat_jabatan'?'active':'' }}" href="{{ route('anjab.browse-syarat-jabatan', $anjab->id) }}">
      <span class="nav-icon">
        <i class="flaticon-list-3"></i>
      </span>
      <span class="nav-text">Syarat Jabatan</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{ @$pages=='detail_prestasi_kerja'?'active':'' }}" href="{{ route('anjab.browse-prestasi-kerja', $anjab->id) }}">
      <span class="nav-icon">
        <i class="flaticon-confetti"></i>
      </span>
      <span class="nav-text">Prestasi Kerja</span>
    </a>
  </li>

  {{-- <li class="nav-item">
    <a class="nav-link" href="{{ route('anjab.get-verifikasi', $anjab->id) }}">
      <span class="nav-icon">
        <i class="flaticon2-check-mark"></i>
      </span>
      <span class="nav-text">Verifikasi</span>
    </a>
  </li> --}}
  

</ul>