{{-- <style type="text/css">
	.select2-selection--single{
		min-width: 450px;
	}
</style> --}}

<div class="row">
	<div style="display: none;" id="alert-anjab" class="col-12">
		<div class="alert alert-danger">

		</div>
	</div>
	<div class="col-4">
		<input type="hidden" name="id" value="{{ $anjab->id }}">
		<input type="hidden" value="update" name="aksi">
		<input type="hidden" name="pd" value="{{ $unitKerja->kodeunit }}">

		<div class="form-group">
			<label>Jenis Jabatan</label>
			<div class="form-control-plaintext">FUNGSIONAL</div>
			<input type="hidden" value="2" name="jenis">

		</div>

		<div class="form-group">
			<label>Nama Jabatan</label>
			<select style="width: 100%" class="form-control select2" id="jabatan" name="jabatan">
				<option selected value="{{ $anjab->m_jabatan_id }}">{{ @$anjab->jabatan->jabatan }}</option>
			</select>
		</div>

		<div class="form-group">
			<label>Eselon Jabatan (Optional)</label>
			<br>
			<div class="form-control-plaintext">TANPA ESELON</div>

		</div>

		<div class="form-group">
			<label>Kode Jabatan (Otomatis)</label>
			{{-- <div class="form-control-plaintext">{{ @$anjab->jabatan->_kode }}</div> --}}
			<input readonly type="text" class="form-control" name="kode_jabatan" value="{{ @$anjab->jabatan->_kode }}">

		</div>
		<div class="form-group">
			<label for="exampleFormControlSelect1">Jenjang (Otomatis)</label>
			<select class="form-control" id="replace-jenjang"  name="jenjang">
			@php

			@endphp
			{{-- <label>Jenjang Sekarang (Otomatis)</label> --}}
			{{-- <input readonly type="text" class="form-control" name="jenjang" value="{{ @$anjab->m_jabatan_fungsional_jenjang }}"> --}}
			</select>
		</div>
		{{-- {{ $anjab->m_jabatan_fungsional_jenjang }} --}}
		{{-- <div class="form-group">
			<label for="exampleFormControlSelect1">Jenjang (Otomatis){{ $anjab->m_jabatan_fungsional_jenjang }}</label>
			<select class="form-control" id="replace-jenjang" name="jenjang" value="">
		</div> --}}

		<div class="form-group">
			<label for="atasan_langsung">Atasan Langsung</label>
			<input type="text" class="form-control" id="atasan_langsung" 
				   placeholder="Atasan Langsung"
				   value="{{ @$jabatanUpt->first()->jabatan }}" readonly>
		</div>
		
	</div>
	
	{{-- <div class="col-4"> --}}


	{{-- </div> --}}

	<div class="col-4">
		{{-- <div class="form-group">
			<label>Pangkat / Golongan</label>
			<br>
			<select disabled required="" class="form-control " name="pangkat">
				<option value="" disabled="" selected="">-- PANGKAT GOLONGAN --</option>
				<option value="11">Ia</option>
				<option value="12">Ib</option>
				<option value="13">Ic</option>
				<option value="14">Id</option>
				<option value="21">IIa</option>
				<option value="22">IIb</option>
				<option value="23">IIc</option>
				<option value="24">IId</option>
				<option value="31">IIIa</option>
				<option value="32">IIIb</option>
				<option value="33">IIIc</option>
				<option value="34">IIId</option>
				<option value="41">IVa</option>
				<option value="42">IVb</option>
				<option value="43">IVc</option>
				<option value="44">IVd</option>
				<option value="45">IVe</option>
			</select>
		</div> --}}

		<div class="form-group">
			<label>Diklat Teknis</label>
			<textarea rows="4" name="diklat_teknis" class="form-control">{{ $anjab->diklat_teknis }}</textarea>
		</div>

		<div class="form-group">
			<label>Pengalaman Kerja</label>
			<textarea rows="4" name="pengalaman_kerja" class="form-control">{{ $anjab->pengalaman_kerja }}</textarea>
		</div>

		<div class="form-group">
			<label>Pengetahuan Kerja</label>
			<textarea rows="4" required="" name="pengetahuan_kerja" class="form-control">{{ $anjab->pengetahuan_kerja }}</textarea>
		</div>
	</div>
	<div class="col-4">
		<div class="form-group">
			<label>Diklat Penjenjangan</label>
			<textarea required="" rows="4" required="" name="diklat_penjenjangan" class="form-control">{{ $anjab->diklat_penjenjangan }}</textarea>
		</div>
		<div class="form-group">
			<label>Kualifikasi Pendidikan</label>
			<textarea required="" rows="4" required="" name="kualifikasi_pendidikan" class="form-control">{{ $anjab->kualifikasi_pendidikan }}</textarea>
		</div>
	</div>

</div>

<script type="text/javascript">

    // Initialization
	jQuery(document).ready(function() {
		let url = "{{ route('get-jenjang-seletced', ['id' => $anjab->m_jabatan_id, 'selected' => $anjab->m_jabatan_fungsional_jenjang ?? 0]) }}";
		$.ajax({
				type: "GET",
				url : url,
				success: function(response){
					console.log(response)

					$('#replace-jenjang').html(response);
					// $('input[name=jenjang]').val(response);
				}
			});

		$('#jabatan').on('select2:select', function (e) {
		    var data = e.params.data;
		    console.log(data, '>>> selected');
			var id_jabatan = data['id'];
			console.log(data.id);
		    $('textarea[name=ikhtisar_jabatan]').val(data.ikhtisar_jabatan);
		    $('textarea[name=kualifikasi_pendidikan]').val(data.kualifikasi_pendidikan);
		    $('textarea[name=diklat_penjenjangan]').val(data.diklat_penjenjangan);
		    $('input[name=kode_jabatan]').val(data.kode_jabatan);

		    if($('#jenis').val() == 2){
		    	$('select[name=pangkat]').val(data.pangkat);
		    }

			$.ajax({
				type: "GET",
				url: '/get-data-jenjang/' + id_jabatan,
				success: function(response){
					console.log(response)
					
					$('#replace-jenjang-edit').html(response);
					// $('input[name=jenjang]').val(response);
				}
			});

			
		});

	    function formatRepo(repo) {
	        if (repo.loading) return repo.text;

	        var markup =
	          "<div class='select2-result-repository clearfix'>" +
	            "<div class='select2-result-repository__meta'>" +
	            "<div class='select2-result-repository__title'>" +
	            "<div>"+repo.jabatan+"</div>"+
	          '</div></div></div>';

	        return markup;
	    }

	    function formatRepoSelection(repo) {
	        return repo.full_name || repo.text;
	    }

	    $("#jabatan").select2({
	        placeholder: "Cari Jabatan",
	        allowClear: true,
	        ajax: {
	            url: '{{ route("anjab.get-jabatan") }}',
	            dataType: 'json',
	            delay: 250,
	            data: function(params) {
	                return {
	                		jenis: 2,
	                    q: params.term, // search term
	                    page: params.page
	                };
	            },
	            processResults: function(data, params) {
	                // parse the results into the format expected by Select2
	                // since we are using custom formatting functions we do not need to
	                // alter the remote JSON data, except to indicate that infinite
	                // scrolling can be used
	                params.page = params.page || 1;

	                return {
	                    results: data.items,
	                    pagination: {
	                        more: (params.page * 30) < data.total_count
	                    }
	                };
	            },
	            cache: true
	        },
	        escapeMarkup: function(markup) {
	            return markup;
	        }, // let our custom formatter work
	        minimumInputLength: 1,
	        templateResult: formatRepo, // omitted for brevity, see the source of this page
	        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
	    });
	});

</script>
