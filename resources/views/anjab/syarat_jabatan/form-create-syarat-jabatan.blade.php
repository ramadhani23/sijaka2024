<style>
.modal-dialog{
	min-width:98% !important;
}
</style>
<input type="hidden" value="create" name="aksi">
<input type="hidden" value="{{ $id }}" name="id">
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<label>Keterampilan Kerja</label>
			<textarea required="" name="keterampilan_kerja" class="form-control" rows="4"></textarea> 
		</div>
		<hr>
		<h5>Bakat Kerja :</h5>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th colspan="2" class="text-center">Bakat Kerja</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach($bakatKerja as $itemBakatKerja)
				<tr>
					<td>{{ $itemBakatKerja->nama }}</td>
					<td>{{ $itemBakatKerja->keterangan }}</td>
					<td><input  type="checkbox" value="{{ $itemBakatKerja->id }}" name="bakat_kerja[]"></td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<br>
		<h5>Tempramen Kerja :</h5>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th colspan="2" class="text-center">Temperamen Kerja</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach($temperamenKerja as $itemTemperamenKerja)
				<tr>
					<td>{{ $itemTemperamenKerja->nama  }}</td>
					<td>{{ $itemTemperamenKerja->keterangan  }}</td>
					<td><input  type="checkbox" value="{{ $itemTemperamenKerja->id }}" name="temperamen_kerja[]"></td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<br>
		<h5>Minat Kerja :</h5>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th colspan="2" class="text-center">Minat Kerja</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach($minatKerja as $itemMinatKerja)
				<tr>
					<td>{{ $itemMinatKerja->nama }}</td>
					<td>{{ $itemMinatKerja->keterangan }}</td>
					<td><input  type="checkbox" value="{{ $itemMinatKerja->id }}" name="minat_kerja[]"></td>
				</tr>
				@endforeach
				
			</tbody>
		</table>
	</div>
	<div class="col-lg-6">
		<h5>Kondisi Fisik :</h5>
		<div class="form-group row">
			<div class="col-4">
				<label>Jenis Kelamin</label>
				<select required="" class="form-control  " name="jenis_kelamin">
					<option value="" disabled="" selected="">-- PILIH Jenis Kelamin --</option>
					<option value="LAKI_LAKI">LAKI-LAKI</option>
					<option value="PEREMPUAN">PEREMPUAN</option>
					<option value="LAKI_LAKI_PEREMPUAN">LAKI-LAKI/PEREMPUAN</option>
					
				</select>
			</div>
			<div class="col-4">
				<label>Umur (Minimal)</label>
				<input required="" type="number" class="form-control" name="umur">
			</div>
			<div class="col-4">
				<label>Tinggi Badan (cm) (Optional)</label>
				<input type="number" class="form-control" name="tinggi_badan">
			</div>
		</div>
		<div class="form-group row">
			<div class="col-4">
				<label>Berat Badan (kg) (Optional)</label>
				<input type="number" class="form-control" name="berat_badan">
			</div>
			<div class="col-4">
				<label>Postur Badan (Optional)</label>
				<input type="text" class="form-control" name="postur_badan">
			</div>
			<div class="col-4">
				<label>Penampilan (Optional)</label>
				<input type="text" class="form-control" name="penampilan">
			</div>
		</div>
		<hr>
		<h5>Upaya Fisik  :</h5>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th colspan="2" class="text-center">Upaya Fisik</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach($upayaFisik as $itemUpayaFisik)
				<tr>
					<td>{{ $itemUpayaFisik->nama }}</td>
					<td>{{ $itemUpayaFisik->keterangan }}.</td>
					<td><input type="checkbox" value="{{ $itemUpayaFisik->id }}" name="upaya_fisik[]"></td>
				</tr>
				@endforeach
				
			</tbody>
		</table>
		<br>
		<h5>Fungsi Pekerjaan  :</h5>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th colspan="2" class="text-center">Fungsi Pekerjaan</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach($fungsiPekerjaan as $itemFungsiPekerjaan)
				<tr>
					<td>{{ $itemFungsiPekerjaan->nama }}</td>
					<td>
						{{ $itemFungsiPekerjaan->keterangan }}
					</td>
					<td><input  type="checkbox" value="{{ $itemFungsiPekerjaan->id }}" name="fungsi_pekerjaan[]"></td>
				</tr>
				@endforeach
				
			</tbody>
		</table>
	</div>

</div>


