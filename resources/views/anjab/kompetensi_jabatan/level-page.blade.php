<div class="form-group row">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <table class="table table-bordered" id="anjab_tabel" style="margin-top: 13px !important">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Kompetensi</th>
                    <th class="text-center">Diskripsi</th>
                    <th class="text-center">Indikator Kompetensi</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$urutan}}</td>
                    <td>{{$kompetensi}}</td>
                    <td width="30%" class="text-center">{{$level->deskripsi}}</td>
                    <td>
                        <ul>
                            @foreach($levelindikator as $lev)
                                <li>
                                    {{$lev->indikator}}
                                </li>
                            @endforeach
                        </ul>
                    </td>
                        
                </tr>
            </tbody>
        </table>
    </div>
</div>