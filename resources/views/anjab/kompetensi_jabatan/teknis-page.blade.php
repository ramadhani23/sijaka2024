<div class="form-group row">
    <label class="col-lg-2 col-form-label text-right">Kompetensi</label>
    @foreach($komjabTeknis as $kom)
        <div class="col-lg-8">
            <input type="text" value="{{$kom->nama_kompetensi != null ? $kom->nama_kompetensi:''}}" name="kompetensi" id="kompetensi" >
        </div>
    @endforeach
</div>
<div class="form-group row">
    <label class="col-lg-2 col-form-label text-right">Deskripsi</label>
    @foreach($komjabTeknis as $kom)
        <div class="col-lg-8">
            <textarea  id="deskripsi_teknis" name="deskripsi_teknis" required="" class="form-control" rows="3">{{$kom->deskripsi != null ? $kom->deskripsi :''}} </textarea>
        </div>
    @endforeach
</div>
<div class="indikator_teknis">
    @if($komjabTeknisIndikator->count() != null)
        <div class="form-group row">
        <label class="col-lg-2 col-form-label text-right">Indikator Kompetensi</label>
        @foreach($komjabTeknisIndikator as $kom)
            <div class="col-lg-8">
                <textarea id="indikator_kompetensi[]" name="indikator_kompetensi[]" required="" class="form-control" rows="3">{{$kom->indikator}}</textarea>
            </div>
        @endforeach
        </div>
    @endif
</div>

<div class="row">
    <div class="col text-right">
        <button type="button" class="btn btn-primary mr-2 btn-add-indikator">Tambah</button>
    </div>
</div>