@extends('layout')

@section('content')
<!--begin::Container-->
              <div class="container">
              <form action="{{route('komjab.save')}}" enctype="multipart/form-data" method="post">
                      @csrf
                <!--begin::Dashboard-->
                  <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h3 class="card-label text-center">{{ isset($page) ? $page : 'Dashboard' }}</h3>
                        @if(isset($komjab))
                          <div class="text-right">
                            <a href="{{ route('komjab.print-word', $komjab->id) }}"  class="btn btn-success btn-sm font-weight-bolder add-anjab">
                            <i class="flaticon2-print"></i>Unduh Hasil Analisa</a>
                          </div>
                        @endif
                    </div>
                  </div>
                  <div class="card-body">
                  
                    
                      @if(isset($komjab))
                        <input type="text" hidden name="type" value="update">
                        <input type="text" hidden name="id_komjab" value="{{$komjab->id}}">
                      @else
                        <input type="text" hidden name="type" value="save">
                      @endif

                      <div class="mb-15">
                        <div class="form-group row">
                          <label class="col-lg-2 col-form-label text-right">Nama Jabatan</label>
                          <div class="col-lg-8">
                            <select required="" class="form-control" name="id_jabatan" id="jabatan">
                              <option value="" disabled="" selected="">-- PILIH JABATAN--</option>
                              @foreach($anjabAll as $anj)
                                @if(isset($komjab))
                                  <option {{$anj->id == $komjab->trx_anjab_id ? 'selected':''}} data-ikhtisar="{{$anj->ikhtisar_jabatan}}" value="{{ $anj->id }}">{{ $anj->jabatan->nama }}</option>
                                @else
                                  <option data-ikhtisar="{{$anj->ikhtisar_jabatan}}" value="{{ $anj->id }}">{{ $anj->jabatan->nama }}</option>
                                @endif
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Kelompok Jabatan :</label>
                          <div class="col-lg-8">
                            <select required="" class="form-control" name="kelompok_jabatan" id="kelompok_jabatan">
                              <option value="" disabled="" selected="">-- PILIH KELOMPOK JABATAN--</option>
                                  <option {{ isset($komjab) && $komjab->kelompok_jabatan == 'Jabatan Pimpinan Tinggi Pratama' ? 'selected':'' }} value="Jabatan Pimpinan Tinggi Pratama">Jabatan Pimpinan Tinggi Pratama</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Urusan Pemerintah :</label>
                          <div class="col-lg-8">
                            <select required="" class="form-control" name="urusan_pemerintah" id="urusan_pemerintah">
                              <option value="" disabled="" selected="">-- PILIH URUSAN PEMERINTAH--</option>
                                  <option {{ isset($komjab) && $komjab->urusan_pemerintah == 'Kesekretariatan' ? 'selected' : ''}} value="Kesekretariatan">Kesekretariatan</option>
                            </select>
                          </div>
                      </div>
                  </div>
                </div>
                <!--end::Card-->
                <br>

                <div class="card card-custom">
                  <div class="card-body">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Ikhtisar Jabatan :</label>
                        <div class="col-lg-8">
                          <textarea readonly id="ikhtisar" name="ikhtisar" required="" class="form-control" rows="10">{{ $anjab->ikhtisar_jabatan != null ? $anjab->ikhtisar_jabatan :''}}</textarea>
                        </div>
                    </div>
                  </div>
                </div>
                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Manajerial</h2>
                    </div>
                  </div>
                  <div class="card-body">
                    
                    <input type="hidden" name="id" id="id" value="">
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Integeritas</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">level</label>
                        <div class="col-lg-6">
                          <select  class="form-control" name="level_integeritas" id="level_integeritas">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_integeritas as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Integeritas" data-urutan="1" {{$level->id == $komjab->id_m_kompetensi_level_integritas? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Integeritas" data-urutan="1" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_integeritas">

                      </div>
                    </div>
                      
                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Kerjasama</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_kerjasama" id="level_kerjasama">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_kerjasama as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Kerjasama" data-urutan="2" {{$level->id == $komjab->id_m_kompetensi_level_kerjasama? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Kerjasama" data-urutan="2"  value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_kerjasama">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Komunikasi</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_komunikasi" id="level_komunikasi">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_komunikasi as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Komunikasi" data-urutan="3" {{$level->id == $komjab->id_m_kompetensi_level_komunikasi? 'selected':''}}  value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Komunikasi" data-urutan="3"  value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_komunikasi">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Orientasi Pada Hasil</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_orientasi" id="level_orientasi">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_orientasi as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Orientasi Pada Hasil" data-urutan="4" {{$level->id == $komjab->id_m_kompetensi_level_orientasi? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Orientasi Pada Hasil" data-urutan="4" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_orientasi">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pelayanan Publik</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_pelayanan" id="level_pelayanan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_pelayanan as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Pelayanan Publik" data-urutan="5" {{$level->id == $komjab->id_m_kompetensi_level_pelayanan? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Pelayanan Publik" data-urutan="5" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_pelayanan">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pengembangan Diri dan Orang Lain</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_pengembangan" id="level_pengembangan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_pengembangan as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Pengembangan Diri dan Orang Lain" data-urutan="6" {{$level->id == $komjab->id_m_kompetensi_level_pengembangan? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Pengembangan Diri dan Orang Lain" data-urutan="6" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_pengembangan">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Mengelola Perubahan</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_perubahan" id="level_perubahan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_perubahan as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Mengelola Perubahan" data-urutan="7" {{$level->id == $komjab->id_m_kompetensi_level_perubahan? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Mengelola Perubahan" data-urutan="7" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_perubahan">

                      </div>
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Pengambilan Keputusan</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label text-right">Level :</label>
                        <div class="col-lg-6">
                        <select  class="form-control" name="level_keputusan" id="level_keputusan">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_keputusan as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Pengambilan Keputusan" data-urutan="8" {{$level->id == $komjab->id_m_kompetensi_level_keputusan? 'selected':''}}  value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Pengambilan Keputusan" data-urutan="8" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_keputusan">

                      </div>
                    </div>
                    
                  </div>

                </div>

                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Sosial Kultural</h2>
                    </div>
                  </div>
                  <div class="card-body">
                    
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Perekat Bangsa</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">level</label>
                        <div class="col-lg-8">
                          <select  class="form-control" name="level_perekat" id="level_perekat">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($level_kompetensi_perekat as $level)
                              @if(isset($komjab))
                                <option data-kompetensi="Perekat Bangsa" data-urutan="9" {{$level->id == $komjab->id_m_kompetensi_level_perekat_bangsa? 'selected':''}} value="{{ $level->id }}">{{ $level->level }}</option>
                              @else
                                <option data-kompetensi="Perekat Bangsa" data-urutan="9" value="{{ $level->id }}">{{ $level->level }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_perekat">

                      </div>
                    </div>
                      
                    
                    
                  </div>
                </div>
                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Teknis</h2>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="mb-15">
                      <div class="page-teknis">
                      @php $i = 0; @endphp
                        @if(isset($komjab))
                          @if($komjab->komjabTeknis->count() > 0)
                            @foreach($komjab->komjabTeknis as $key=>$kom)
                            @php $i++ @endphp
                                

                                <div id="teknis_{{$key}}">

                                  <div class="form-group row cek-length">
                                    <label class="col-lg-2 col-form-label text-right">level</label>
                                    <div class="col-lg-8">
                                      <select required="" class="form-control" name="level_teknis_{{$key}}" id="level_teknis_{{$key}}">
                                        <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                                          <option data-id="{{ isset($komjab) ? $komjab->id :''}}" {{$kom->level = 1 ? 'selected':''}} value="1">1</option>
                                          <option data-id="{{ isset($komjab) ? $komjab->id :''}}" {{$kom->level = 2 ? 'selected':''}} value="2">2</option>
                                          <option data-id="{{ isset($komjab) ? $komjab->id :''}}" {{$kom->level = 3 ? 'selected':''}} value="3">3</option>
                                          <option data-id="{{ isset($komjab) ? $komjab->id :''}}" {{$kom->level = 4 ? 'selected':''}} value="4">4</option>
                                          <option data-id="{{ isset($komjab) ? $komjab->id :''}}" {{$kom->level = 5 ? 'selected':''}} value="5">5</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Kompetensi</label>
                                      <div class="col-lg-8">
                                        <textarea id="kompetensi-{{$key}}" name="kompetensi_{{$key}}" required="" class="form-control" rows="3">{{$kom->nama_kompetensi != null ? $kom->nama_kompetensi:''}}</textarea>
                                      </div>
                                    
                                  </div>
                                  <div class="form-group row">
                                    <label class="col-lg-2 col-form-label text-right">Deskripsi</label>
                                      <div class="col-lg-8">
                                        <textarea   name="deskripsi_teknis_{{$key}}" required="" class="form-control" rows="5">{{$kom->deskripsi != null ? $kom->deskripsi :''}} </textarea>
                                      </div>
                                  </div>
                                  <div id="indikator_teknis_{{$key}}">
                                    @if($kom->indikatorTeknis != null)
                                        @foreach($kom->indikatorTeknis as $kom)
                                          <div class="form-group row length-form" id="indikator_{{$key}}">
                                            <label class="col-lg-2 col-form-label text-right ">Indikator Kompetensi</label>
                                            <div class="col-lg-8">
                                              <textarea  name="indikator_kompetensi_{{$key}}[]" required="" class="form-control" rows="5">{{$kom->indikator}}</textarea>
                                            </div>
                                            <div class="col-lg-2">
                                                <button type="button" class="btn btn-danger mr-2" onclick="removeButtonIndikator(this)"  id="btn-delete-indikator{{$key}}" >Delete Indikator</button>
                                            </div>
                                          </div>
                                        @endforeach
                                    @endif
                                  </div>

                                  <div class="row">
                                      <div class="col text-right">
                                        <button type="button" onclick="addButton(this)" class="btn btn-warning mr-2" id="btn-add-indikator-{{$key}}" >Tambah Indikator</button>
                                        <button type="button" class="btn btn-danger mr-2" onclick="removeButtonTeknis(this)"  id="btn-add-indikator{{$key}}" >Delete Teknis</button>
                                      </div>
                                  </div>
                                  <hr>
                                </div>
                              @endforeach
                            @endif
                          @endif
                        <input type="text" value="{{$i}}" name="count_index" hidden id="count_index">
                      </div>
                    </div>

                    <div class="row">
                      <div class="col text-right">
                        <button type="button" class="btn btn-danger mr-2" id="btn-add-teknis" >Tambah Teknis</button>

                      </div>
                    </div>
                    <br><br>

                  </div>

                  
                </div>
                <!--end::Card-->

                  <div class="card-footer">
                    <div class="row">
                      <div class="col text-right">
                        @if(isset($komjab))
                          <button type="submit" class="btn btn-success mr-2">Update</button>
                        @else
                          <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        @endif
                      </div>
                    </div>
                  </div>
                
              </div>
            </form>
              <!--end::Container-->
              <!-- <input type="text" class="form-control" name="indikator_kompetensi[]" id="indikator_kompetensi[]"> -->

              
@endsection

@section('js')
{{-- js files --}}
 <script src="{{ asset('') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script src="//cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script>
  // $('.page-teknis').on('click','#btn-add-indikator', function() {
  //     $('#indikator_teknis').append(
  //         `<div class="form-group row ">
  //           <label class="col-lg-2 col-form-label text-right ">Indikator Kompetensi</label>
  //           <div class="col-lg-8">
  //             <textarea name="indikator_kompetensi[]" required="" class="form-control" rows="5" ></textarea>
  //           </div>
  //         </div>`
  //     );
  // });

  function addButton(e){
    
    var id_parent = $(e).parent().parent().parent().attr('id');
    var index = id_parent.split('_');
    console.log(index);
    console.log('index = ',index[1]);
    var counts = $('div .length-form').length;
    console.log(counts);
    

    var page= '<div class="form-group row length-form" id="indikator_'+counts+'">'+
                '<label class="col-lg-2 col-form-label text-right ">Indikator Kompetensi</label>'+
                '<div class="col-lg-8">'+
                  '<textarea name="indikator_kompetensi_'+index[1]+'[]" required="" class="form-control" rows="5" ></textarea>'+
                '</div>'+
                '<div class="col-lg-2">'+
                  '<button type="button" class="btn btn-danger mr-2" onclick="removeButtonIndikator(this)"  id="btn-delete-indikator'+counts+'" >Delete Indikator</button>'+
                '</div>'+
              '</div>';
    
    $('#indikator_teknis_'+index[1]).append(page);  
    // $(e).parent().parent().parent().append(page);
  }

  function removeButtonIndikator(e){
    
    var id_parent = $(e).parent().parent().parent().parent().attr('id');
    var index = id_parent.split('_');
    // console.log(index);
    // console.log('index_form = ',index);

    // $('#indikator_'+index[1]).remove();  
    $(e).parent().parent().remove();
    // $(e).parent().parent().parent().append(page);
  }

   function removeButtonTeknis(e){
    
 

    $(e).parent().parent().parent().remove();
    // $(e).parent().parent().parent().append(page);
  }

  $('#btn-add-teknis').on('click', function() {
    var count = $('div .cek-length').length;
    console.log('count = ', count);
    $('#count_index').val(count);
    var page = '<div id="teknis_'+count+'">'+
                '<div class="form-group row cek-length">'+
                    '<label class="col-lg-2 col-form-label text-right">level</label>'+
                    '<div class="col-lg-8">'+
                    '<select required="" class="form-control level_teknis" name="level_teknis_'+count+'" id="level_teknis_'+count+'">'+
                        '<option value="" disabled="" selected="">-- PILIH LEVEL--</option>'+
                        '<option data-id="{{ isset($komjab) ? $komjab->id :''}}" value="1">1</option>'+
                        '<option data-id="{{ isset($komjab) ? $komjab->id :''}}" value="2">2</option>'+
                        '<option data-id="{{ isset($komjab) ? $komjab->id :''}}" value="3">3</option>'+
                        '<option data-id="{{ isset($komjab) ? $komjab->id :''}}" value="4">4</option>'+
                        '<option data-id="{{ isset($komjab) ? $komjab->id :''}}" value="5">5</option>'+
                      '</select>'+
                    '</div>'+
                  '</div>'+

                  '<div class="form-group row ">'+
                      '<label class="col-lg-2 col-form-label text-right">Kompetensi</label>'+
                        '<div class="col-lg-8">'+
                          '<textarea id="kompetensi-'+count+'" name="kompetensi_'+count+'" required="" class="form-control" rows="3"></textarea>'+
                        '</div>'+

                  '</div>'+
                  '<div class="form-group row">'+
                    '<label class="col-lg-2 col-form-label text-right">Deskripsi</label>'+
                      '<div class="col-lg-8">'+
                        '<textarea name="deskripsi_teknis_'+count+'" required="" class="form-control" rows="5"> </textarea>'+
                      '</div>'+
                  '</div>'+
                  '<div id="indikator_teknis_'+count+'">'+

                    
                  '</div>'+

                  '<div class="row">'+
                    '<div class="col text-right">'+
                      '<button type="button" class="btn btn-warning mr-2" onclick="addButton(this)"  id="btn-add-indikator'+count+'" >Tambah Indikator</button>'+
                      '<button type="button" class="btn btn-danger mr-2" onclick="removeButtonTeknis(this)"  id="btn-add-indikator'+count+'" >Delete Teknis</button>'+
                    '</div>'+
                  '</div>'+
                '</div><hr>';
      $('.page-teknis').append(page);
  });
  var options = {
    filebrowserImageBrowseUrl: '{{ URL::to("laravel-filemanager?type=Images") }}',
    filebrowserImageUploadUrl: '{{ URL::to("laravel-filemanager/upload?type=Images&_token=") }}',
    filebrowserBrowseUrl: '{{ URL::to("/laravel-filemanager?type=Files") }}',
    filebrowserUploadUrl: '{{ URL::to("/laravel-filemanager/upload?type=Files&_token=") }}',
  };
</script>

<script>

        //CKEDITOR.replace( 'ruang_lingkup', options);

        // CKEDITOR.replace( 'dampak_program', options);
        // CKEDITOR.replace( 'pengaturan_organisasi', options);
        // CKEDITOR.replace( 'wewenang_penyeliaan_manajerial', options);
        // CKEDITOR.replace( 'sifat_hubungan', options);
        // CKEDITOR.replace( 'tujuan_hubungan', options);
        // CKEDITOR.replace( 'kesulitan_pengarahan', options);
        // CKEDITOR.replace( 'kondisi_lain', options);


       

        $("#nama_jabatan").change(function() {
          var ikhtisar = $(this).find(':selected').data('ikhtisar');
          $('#ikhtisar').text(ikhtisar);
          $('#id').val(id);
        });

        $("#level_integeritas").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          console.log(varkompetensi);
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_integeritas").html(html);
            }
          });
        });
        $("#level_kerjasama").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_kerjasama").html(html);
            }
          });
        });
        $("#level_komunikasi").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_komunikasi").html(html);
            }
          });
        });
        $("#level_orientasi").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_orientasi").html(html);
            }
          });
        });
        $("#level_pelayanan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_pelayanan").html(html);
            }
          });
        });
        $("#level_pengembangan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_pengembangan").html(html);
            }
          });
        });
        $("#level_perubahan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_perubahan").html(html);
            }
          });
        });
        $("#level_keputusan").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_keputusan").html(html);
            }
          });
        });
        $("#level_perekat").change(function() {
          var dats = $(this).val();
          var number = $(this).find(':selected').data('urutan');
          var varkompetensi = $(this).find(':selected').data('kompetensi');
          $.ajax({
            type: "GET",
            url: '{{ route("komjab.get-data") }}',
            data: {
                  id :  dats,
                  urutan: number,
                  kompetensi: varkompetensi,
            },
            cache: false,
            success: function(html) {
              $("#data_perekat").html(html);
            }
          });
        });
        // $("#level_teknis").change(function() {
        //   var dats = $(this).val();
        //   var id = $(this).find(':selected').data('id');
        //   $.ajax({
        //     type: "GET",
        //     url: '{{ route("komjab.get-data-teknis") }}',
        //     data: {
        //           level :  dats,
        //           id_komjab : id
        //     },
        //     cache: false,
        //     success: function(html) {
        //       $("#teknis").html(html);
        //     }
        //   });
        // });


</script>
@endsection