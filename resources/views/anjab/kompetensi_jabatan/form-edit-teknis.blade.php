<input type="hidden" value="update-teknis" name="aksi">
<input type="hidden" value="{{ $id_komjab }}" name="id_komjab">
<input type="hidden" value="{{ $id_anjab }}" name="id_anjab">	
<input type="hidden" value="{{ $id }}" name="id">

<div class="row">
	<div class="col-12">
		<div class="form-group">
			<label class="col-sm-12 control-label">Level</label>
			<div class="col-sm-12">
			<select  class="form-control" name="level_teknis" id="level_teknis">
               <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
               <option {{ $komjab_teknis->level == '1'?'selected':'' }} value="1">1</option>
               <option {{ $komjab_teknis->level == '2'?'selected':'' }} value="2">2</option>
               <option {{ $komjab_teknis->level == '3'?'selected':'' }} value="3">3</option>
               <option {{ $komjab_teknis->level == '4'?'selected':'' }} value="4">4</option>
               <option {{ $komjab_teknis->level == '5'?'selected':'' }} value="5">5</option>
            </select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-12 control-label">Nama Kompetensi</label>
			<div class="col-sm-12">
			<input type="text" class="form-control" name="nama_kompetensi" value="{{ $komjab_teknis->nama_kompetensi }}">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-12 control-label">Deskripsi</label>
			<div class="col-sm-12">
			<textarea class="form-control" name="deskripsi">{{ $komjab_teknis->deskripsi }}</textarea>
			</div>
		</div>
	</div>
</div>


