@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                {{-- @include('anjab.card-anjab') --}}
                <br><br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-body">
                    <!--begin::Example-->
                        <div class="example mb-10">
                          
                          <div class="example-preview">
                            <div class="row">
                              <div class="col-12">
                                @include('kunci')
                              </div>
                              <div class="col-4 border-right">
                                @include('anjab.step-indicator')
                              </div>
                              <div class="col-8">
                                  <div class="card card-custom">
                                      <div class="card-header">
                                        <div class="card-title">
                                          <h3 class="card-label"><i class="flaticon2-menu"></i> Syarat Jabatan</h3>
                                        </div>
                                        @if(
                                                    \MojokertokabApp::allowChangingData() &&
                                                    !(\MojokertokabUser::getUser()->kode_opd === '28' && $anjab->unit_kerja === '2835') &&
                                                    (
                                                        \MojokertokabUser::getUser()->kode_opd !== '28' ||
                                                        ($anjab->parent !== null)
                                                    )
                                                )
                                        <div class="card-toolbar">
                                          <!--begin::Button-->
                                            <a 
                                            href="javascript:;" 
                                            class="btn btn-sm font-weight-bolder 
                                            @if($anjab->status_verification == 1) disabled-link @else @if(isset($syaratJabatan)) btn-success edit-syarat-jabatan @else btn-primary add-syarat-jabatan @endif @endif"
                                            @if($anjab->status_verification == 1) 
                                                style="pointer-events: none; cursor: default;" 
                                            @endif
                                            @if(isset($syaratJabatan))
                                                data-id="{{ $anjab->id }}"
                                            @endif
                                            >
                                            <i class="@if(isset($syaratJabatan)) fas fa-pencil-alt @else far fa-plus-square @endif"></i>
                                            @if($anjab->status_verification == 1)
                                                Sudah Verifikasi
                                            @elseif(isset($syaratJabatan))
                                                Perbarui Syarat Jabatan
                                            @else
                                                Atur Syarat Jabatan
                                            @endif
                                            </a>
                                            <!--end::Button-->

                                        </div>
                                        @endif
                                      </div>
                                      <div class="card-body">
                                        <div class="form-group">
                                          <label>Keterampilan Kerja</label>
                                          <textarea disabled class="form-control" rows="4">@isset($syaratJabatan){{ $syaratJabatan->keterampilan_kerja}}@endisset
                                          </textarea> 
                                        </div>
                                        <hr>
                                          <h5>Bakat Kerja :</h5>
                                          <table class="table">
                                            <thead class="thead-light">
                                              <tr>
                                                <th colspan="2" class="text-center">Bakat Kerja</th>
                                                <th>&#10003;</th>
                                              </tr>
                                            </thead>
                                            <tbody>

                                              @if(empty($syaratJabatan->bakat_kerja) || $syaratJabatan == null)
                                              <tr>
                                                <td class="text-center" colspan="3">Tidak Ada Data</td>
                                              </tr>
                                              @else
                                                @foreach($bakatKerja as $itemBakatKerja)
                                                @if(@in_array($itemBakatKerja->id, $syaratJabatan->bakat_kerja))
                                                <tr>
                                                  <td>{{ $itemBakatKerja->nama }}</td>
                                                  <td>{{ $itemBakatKerja->keterangan }}</td>
                                                  <td>&#10003;
                                                  </td>
                                                </tr>
                                                @endif
                                                @endforeach

                                              @endif
                                              
                                            </tbody>
                                          </table>
                                          <br>
                                          <h5>Tempramen Kerja :</h5>
                                          <table class="table">
                                            <thead class="thead-light">
                                              <tr>
                                                <th colspan="2" class="text-center">Temperamen Kerja</th>
                                                <th>&#10003;</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              @if(empty($syaratJabatan->temperamen_kerja))
                                              <tr>
                                                <td class="text-center" colspan="3">Tidak Ada Data</td>
                                              </tr>
                                              @else
                                                @foreach($temperamenKerja as $itemTemperamenKerja)
                                              @if(@in_array($itemTemperamenKerja->id, $syaratJabatan->temperamen_kerja))
                                              <tr>
                                                <td>{{ $itemTemperamenKerja->nama  }}</td>
                                                <td>{{ $itemTemperamenKerja->keterangan  }}</td>
                                                <td>&#10003;</td>
                                              </tr>
                                              @endif
                                              @endforeach
                                              @endif
                                              
                                              
                                            </tbody>
                                          </table>

                                          <br>
                                          <h5>Minat Kerja :</h5>
                                          <table class="table">
                                            <thead class="thead-light">
                                              <tr>
                                                <th colspan="2" class="text-center">Minat Kerja</th>
                                                <th>&#10003;</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              @if(empty($syaratJabatan->minat_kerja))
                                              <tr>
                                                <td class="text-center" colspan="3">Tidak Ada Data</td>
                                              </tr>
                                              @else
                                                @foreach($minatKerja as $itemMinatKerja)
                                              @if(@in_array($itemMinatKerja->id, $syaratJabatan->minat_kerja))
                                              <tr>
                                                <td>{{ $itemMinatKerja->nama }}</td>
                                                <td>{{ $itemMinatKerja->keterangan }}</td>
                                                <td>&#10003;</td>
                                              </tr>
                                               @endif
                                              @endforeach
                                              @endif
                                              
                                              
                                            </tbody>
                                          </table>
                                          <h5>Kondisi Fisik :</h5>
                                          <table class="table table-striped">
                                            <tbody>
                                              <tr>
                                                <td>
                                                  Jenis Kelamin
                                                </td>
                                                <td>
                                                  :
                                                </td>
                                                <td>
                                                  @isset($syaratJabatan)
                                                  {{ $syaratJabatan->text_jenis_kelamin }}
                                                   @endisset
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  Umur
                                                </td>
                                                <td>
                                                  :
                                                </td>
                                                <td>
                                                  @isset($syaratJabatan)
                                                  {{ $syaratJabatan->text_umur }}
                                                   @endisset
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  Tinggi Badan
                                                </td>
                                                <td>
                                                  :
                                                </td>
                                                <td>
                                                  @isset($syaratJabatan)
                                                  {{ $syaratJabatan->text_tinggi_badan }}
                                                   @endisset
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  Berat Badan
                                                </td>
                                                <td>
                                                  :
                                                </td>
                                                <td>
                                                  @isset($syaratJabatan)
                                                  {{ $syaratJabatan->text_berat_badan }}
                                                   @endisset
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  Postur Badan
                                                </td>
                                                <td>
                                                  :
                                                </td>
                                                <td>
                                                  @isset($syaratJabatan)
                                                  {{ ucfirst($syaratJabatan->postur_badan) }}
                                                   @endisset
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  Penampilan
                                                </td>
                                                <td>
                                                  :
                                                </td>
                                                <td>
                                                  @isset($syaratJabatan)
                                                  {{ ucfirst($syaratJabatan->penampilan) }}
                                                   @endisset
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>

                                          <hr>
                                          <h5>Upaya Fisik  :</h5>
                                          <table class="table">
                                            <thead class="thead-light">
                                              <tr>
                                                <th colspan="2" class="text-center">Upaya Fisik</th>
                                                <th>&#10003;</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              @if(empty($syaratJabatan->upaya_fisik))
                                              <tr>
                                                <td class="text-center" colspan="3">Tidak Ada Data</td>
                                              </tr>
                                              @else
                                                @foreach($upayaFisik as $itemUpayaFisik)
                                              @if(@in_array($itemUpayaFisik->id, $syaratJabatan->upaya_fisik))
                                              <tr>
                                                <td>{{ $itemUpayaFisik->nama }}</td>
                                                <td>{{ $itemUpayaFisik->keterangan }}.</td>
                                                <td>
                                                    &#10003;
                                                </td>
                                              </tr>
                                              @endif
                                              @endforeach
                                              @endif
                                              
                                              
                                            </tbody>
                                          </table>

                                          <br>
                                          <h5>Fungsi Pekerjaan  :</h5>
                                          <table class="table">
                                            <thead class="thead-light">
                                              <tr>
                                                <th colspan="2" class="text-center">Fungsi Pekerjaan</th>
                                                <th>&#10003;</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              @if(empty($syaratJabatan->fungsi_pekerjaan))
                                              <tr>
                                                <td class="text-center" colspan="3">Tidak Ada Data</td>
                                              </tr>
                                              @else
                                                @foreach($fungsiPekerjaan as $itemFungsiPekerjaan)
                                              @if(@in_array($itemFungsiPekerjaan->id, $syaratJabatan->fungsi_pekerjaan))
                                              <tr>
                                                <td>{{ $itemFungsiPekerjaan->nama }}</td>
                                                <td>
                                                  {{ $itemFungsiPekerjaan->keterangan }}
                                                </td>
                                                <td>&#10003;</td>
                                              </tr>
                                              @endif
                                              @endforeach
                                              @endif
                                              
                                              
                                            </tbody>
                                          </table>

                                      </div>
                                    </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--end::Example-->
                  </div>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

              <div id="modal-syarat-jabatan" class="modal fade" data-backdrop="static" aria-labelledby="staticBackdrop" aria-hidden="true" role="dialog">
              <div class="modal-dialog modal-xl">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5> 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('anjab.save-syarat-jabatan') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                      
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>


            <div class="modal fade text-left" data-backdrop="static" aria-labelledby="staticBackdrop" aria-hidden="true" id="modal" role="dialog">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content" style="width: 1200px;">
                        <div class="modal-header">
                            <h4 class="modal-title"></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            
                        </div>
                        
                    </div>
                </div>
            </div>
          
            <form id="form-delete-syarat-jabatan" method="POST" action="{{ route('anjab.delete-syarat-jabatan') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('js')
{{-- js files --}}
 <script src="{{ asset('') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
   $('.add-syarat-jabatan').on('click', function(){
    var id = $('#id').val();
    $.ajax({
      url:'{{ route("anjab.get-form-syarat-jabatan") }}',
      method:'GET',
      data:{aksi:'add-data',id:id },
      success:function(result){
        $('#modal-syarat-jabatan .modal-title').text("Tambah Syarat Jabatan");
        $('#modal-syarat-jabatan .modal-body').html(result);
        $('#modal-syarat-jabatan').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.edit-syarat-jabatan').on('click', function(){
    var id = $(this).data('id');
    $.ajax({
      url:'{{ route("anjab.get-form-syarat-jabatan") }}',
      method:'GET',
      data:{aksi:'edit-data',id:id },
      success:function(result){
        $('#modal-syarat-jabatan .modal-title').text("Edit Syarat Jabatan");
        $('#modal-syarat-jabatan .modal-body').html(result);
        $('#modal-syarat-jabatan').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.delete-syarat-jabatan').on('click', function(){
    var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin?',
            // text: "Anda tidak dapat membatalkan pengajuan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus Data',
            cancelButtonText: 'Batal',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-warning',
            buttonsStyling: false
        }).then(function (status) {
          if(status.value){
            $('form[id=form-delete-syarat-jabatan] input[name=id]').val(id);
                $('form[id=form-delete-syarat-jabatan]').submit();
          }
        });
  });

  $('#print_anjab').on('click', function(){
     var id_anjab = $('#id').val();
    $.ajax({
      type: "GET",
      url: '{{ route("anjab.view-print") }}',
      data: {
        id_anjab : id_anjab,
      },
      cache: false,
      success: function(html) {
        // $("#div_header").hide();
        // $("#div_print_card").show();
        // $("#div_print").html(html);
        $('#modal .modal-title').text("Print Anjab");
        $('#modal .modal-body').html(html);
        $('#modal ').modal('show');
      }
    });
  });
</script>
@endsection