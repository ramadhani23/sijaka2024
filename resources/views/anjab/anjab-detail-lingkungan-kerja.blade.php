@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                {{-- @include('anjab.card-anjab') --}}
                <br><br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-body">
                    <!--begin::Example-->
                        <div class="example mb-10">
                          
                          <div class="example-preview">
                            <div class="row">
                              <div class="col-12">
                                @include('kunci')
                              </div>
                              <div class="col-4 border-right">
                                @include('anjab.step-indicator')
                              </div>
                              <div class="col-8">
                                  <div class="card card-custom">
                                      <div class="card-header">
                                        <div class="card-title">
                                          <h3 class="card-label"><i class="flaticon2-menu"></i> KONDISI LINGKUNGAN KERJA</h3>
                                        </div>
                                        @if(
                                                    \MojokertokabApp::allowChangingData() &&
                                                    !(\MojokertokabUser::getUser()->kode_opd === '28' && $anjab->unit_kerja === '2835') &&
                                                    (
                                                        \MojokertokabUser::getUser()->kode_opd !== '28' ||
                                                        ($anjab->parent !== null)
                                                    )
                                                )
                                        <div class="card-toolbar">
                                          <a 
                                            href="javascript:;" 
                                            class="btn btn-primary btn-sm font-weight-bolder 
                                            @if($anjab->status_verification == 1) disabled-link @endif 
                                            @if($lingkungan_kerja) edit-lingkungan-kerja @else add-lingkungan-kerja @endif"
                                            @if($anjab->status_verification == 1) 
                                                style="pointer-events: none; cursor: default;" 
                                            @endif
                                            @if($lingkungan_kerja)
                                                data-id="{{ $anjab->id }}"
                                            @endif
                                        >
                                            <i class="@if($lingkungan_kerja) fas fa-pencil-alt @else far fa-plus-square @endif"></i>
                                            @if($anjab->status_verification == 1)
                                                Sudah Verifikasi
                                            @elseif($lingkungan_kerja)
                                                Perbarui Kondisi Lingkungan Kerja
                                            @else
                                                Atur Kondisi Lingkungan Kerja
                                            @endif
                                        </a>
                                        <!--end::Button-->

                                        </div>
                                        @endif
                                      </div>
                                      <div class="card-body">
                                        <!--begin: Datatable-->
                                       
                                        <table class="table table-striped table-bordered " >
                                          <tbody>
                                            <tr>
                                              <td width="5%">a.</td>
                                              <td width="25%">Tempat Kerja</td>
                                              <td width="25%">
                                                @isset($lingkungan_kerja)
                                                  {{ ucfirst(str_replace("_"," ",$lingkungan_kerja->tempat_kerja)) }}
                                                @endisset
                                              </td>
                                            </tr>
                                            <tr>
                                              <td width="5%">b.</td>
                                               <td width="25%">Suhu</td>
                                               <td width="25%">
                                                 @isset($lingkungan_kerja)
                                                  {{ ucfirst(str_replace("_"," ",$lingkungan_kerja->suhu)) }}
                                                 @endisset
                                               </td>
                                            </tr>
                                            <tr>
                                              <td width="5%">c.</td>
                                              <td width="25%">Udara</td>
                                              <td width="25%">
                                                @isset($lingkungan_kerja)
                                                  {{ ucfirst(str_replace("_"," ",$lingkungan_kerja->udara)) }}
                                                @endisset
                                              </td>
                                            </tr>
                                            <tr>
                                              <td width="5%">d.</td>
                                              <td width="25%">Keadaan Ruangan</td>
                                              <td width="25%">
                                                @isset($lingkungan_kerja)
                                                  {{ ucfirst(str_replace("_"," ",$lingkungan_kerja->keadaan_ruangan)) }}
                                                @endisset
                                              </td>
                                            </tr>
                                            <tr>
                                              <td width="5%">e.</td>
                                              <td width="25%">Letak</td>
                                              <td width="25%">
                                                @isset($lingkungan_kerja)
                                                  {{ ucfirst(str_replace("_"," ",$lingkungan_kerja->letak)) }}
                                                @endisset
                                              </td>
                                            </tr>
                                            <tr>
                                              <td width="5%">f.</td>
                                              <td width="25%">Penerangan</td>
                                              <td width="25%">
                                                @isset($lingkungan_kerja)
                                                  {{ ucfirst(str_replace("_"," ",$lingkungan_kerja->penerangan)) }}
                                                @endisset
                                               </td>
                                            </tr>  
                                            <tr>
                                              <td width="5%">g.</td>
                                              <td width="25%">Suara</td>
                                              <td width="25%">
                                                @isset($lingkungan_kerja)
                                                  {{ ucfirst(str_replace("_"," ",$lingkungan_kerja->suara)) }}
                                                @endisset
                                              </td>
                                            </tr>  
                                            <tr>
                                              <td width="5%">h.</td>
                                              <td width="25%">Keadaan Tempat Kerja</td>
                                              <td width="25%">
                                                @isset($lingkungan_kerja)
                                                  {{ ucfirst(str_replace("_"," ",$lingkungan_kerja->keadaan_tempat_kerja)) }}
                                                @endisset
                                              </td>
                                            </tr>
                                            <tr>
                                              <td width="5%">i.</td>
                                              <td width="25%">Getaran</td>
                                              <td width="25%">
                                                @isset($lingkungan_kerja)
                                                  {{ ucfirst(str_replace("_"," ",$lingkungan_kerja->getaran)) }}
                                                @endisset
                                              </td>
                                            </tr>
                                            </tr>
                                          </tbody>
                                        </table>
                                        
                                        <!--end: Datatable-->
                                      </div>
                                    </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--end::Example-->
                  </div>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

              <div id="modal-lingkungan-kerja" class="modal fade">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title"></h5> 
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form method="POST" action="{{ route('anjab.save-lingkungan-kerja') }}">
                      {{ csrf_field() }}
                      <div class="modal-body">
                        
                      </div>

                      <div class="modal-footer">
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

            <div class="modal fade text-left" id="modal" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-xl" role="document">
                  <div class="modal-content" style="width: 1200px;">
                      <div class="modal-header">
                          <h4 class="modal-title"></h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div class="modal-body">
                          
                      </div>
                      
                  </div>
              </div>
          </div>
          
            <form id="form-delete-lingkungan-kerja" method="POST" action="{{ route('anjab.delete-lingkungan-kerja') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('js')
{{-- js files --}}
 <script src="{{ asset('') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
   $('.add-lingkungan-kerja').on('click', function(){
    var id = $('#id').val();
    $.ajax({
      url:'{{ route("anjab.get-form-lingkungan-kerja") }}',
      method:'GET',
      data:{aksi:'add-data',id:id },
      success:function(result){
        $('#modal-lingkungan-kerja .modal-title').text("Atur Kondisi Lingkungan Kerja");
        $('#modal-lingkungan-kerja .modal-body').html(result);
        $('#modal-lingkungan-kerja').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.edit-lingkungan-kerja').on('click', function(){
    var id = $(this).data('id');
    $.ajax({
      url:'{{ route("anjab.get-form-lingkungan-kerja") }}',
      method:'GET',
      data:{aksi:'edit-data',id:id },
      success:function(result){
        $('#modal-lingkungan-kerja .modal-title').text("Perbarui Kondisi Lingkungan Kerja");
        $('#modal-lingkungan-kerja .modal-body').html(result);
        $('#modal-lingkungan-kerja').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.delete-lingkungan-kerja').on('click', function(){
    var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin?',
            // text: "Anda tidak dapat membatalkan pengajuan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus Data',
            cancelButtonText: 'Batal',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-warning',
            buttonsStyling: false
        }).then(function (status) {
          if(status.value){
            $('form[id=form-delete-lingkungan-kerja] input[name=id]').val(id);
                $('form[id=form-delete-lingkungan-kerja]').submit();
          }
        });
  });

  $('#print_anjab').on('click', function(){
     var id_anjab = $('#id').val();
    $.ajax({
      type: "GET",
      url: '{{ route("anjab.view-print") }}',
      data: {
        id_anjab : id_anjab,
      },
      cache: false,
      success: function(html) {
        // $("#div_header").hide();
        // $("#div_print_card").show();
        // $("#div_print").html(html);
        $('#modal .modal-title').text("Print Anjab");
        $('#modal .modal-body').html(html);
        $('#modal ').modal('show');
      }
    });
  });
</script>
@endsection