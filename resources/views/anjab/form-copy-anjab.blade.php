<div class="row">
		
	<div class="col-12">
		<input type="hidden" value="{{ $anjab->id }}" name="id">
		<div class="form-group">
			<label>Unit Kerja Tujuan</label>
			<select style="width: 100%" required="" name="kode_opd" class="form-control select2">
				<option value="" disabled="" selected="">-- PILIH UNIT KERJA TUJUAN --</option>
				@foreach($unitKerja as $unitKerjas)
				<option value="{{ $unitKerjas->kodeunit }}">{{ $unitKerjas->unitkerja }}</option>
				@endforeach
			</select>
		</div>
	</div>

</div>