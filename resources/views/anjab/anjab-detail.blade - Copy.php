@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">       
                @include('kunci')
                @include('anjab.card-anjab')

                <br><br>
                @if(0 != 1)
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-body">
                    <!--begin::Example-->
                        <div class="example mb-10">
                          
                          <div class="example-preview">
                            <div class="row">
                              <div class="col-4 border-right">
                                @include('anjab.step-indicator')
                              </div>
                              <div class="col-8">

                                  <div class="card card-custom gutter-b">
                                      <div class="card-header">
                                        <div class="card-title">
                                          <h3 class="card-label"><i class="flaticon-event-calendar-symbol"></i> URAIAN TUGAS</h3>
                                        </div>
                                        @if(\MojokertokabApp::allowChangingData())
                                        <div class="card-toolbar">
                                          <!--begin::Button-->
                                          <a href="javascript:;" class="btn btn-primary btn-sm add-data">
                                           <i class="far fa-plus-square"></i>Uraian Tugas</a>
                                          <!--end::Button-->
                                        </div>
                                        @endif
                                      </div>
                                      <div class="card-body">
                                        <!--begin: Datatable-->
                                        <table class="table table-separate " >
                                          <thead>
                                            <tr>
                                              <th class="text-center">No</th>
                                              <th class="text-center">Uraian</th>
                                              <th></th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            @if($anjab_uraian->count() == 0)
                                              <tr>
                                                <td colspan="2" class="text-center">Tidak Ada Data</td>
                                              </tr>
                                            @endif
                                            @foreach($anjab_uraian as $anjabs)
                                            <?php $tahapan = $anjabs->tahapan ?>
                                            <tr>
                                              <td width="10%" >{{ $loop->iteration + $anjab_uraian->firstItem() - 1 }} </td>
                                              <td>
                                                {{ $anjabs->uraian_jabatan }}
                                                @if($tahapan->count() != 0)
                                                <br>
                                                <table class="table table-bordered">
                                                  <thead>
                                                    <tr>
                                                      <th class="text-center">No</th>
                                                      <th class="text-center">Uraian Tahapan</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                      @foreach($tahapan as $tahapans)
                                                      <tr>
                                                        <td class="text-center">{{ $loop->iteration }} </td>
                                                        <td class="text-center">{{ $tahapans->uraian }}</td>
                                                      </tr>
                                                      @endforeach
                                                  </tbody>
                                                </table>
                                                @endif
                                              </td>
                                              @if(\MojokertokabApp::allowChangingData())
                                              <td>
                                                
                                                <div class="btn-group dropleft">
                                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Menu
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                                      <!--begin::Naviigation-->
                                                      <ul class="navi">
                                                        <li class="navi-item">
                                                          <a data-id="{{ $anjabs->id }}"  href="javascript:;" class="navi-link add-tahapan">
                                                            <span class="navi-icon">
                                                              <i class="navi-icon flaticon2-plus-1"></i>
                                                            </span>
                                                            <span class="navi-text">Tambah Tahapan</span>
                                                          </a>
                                                        </li>
                                                        
                                                        <li class="navi-item">
                                                          <a data-id="{{ $anjabs->id }}"  href="javascript:;" class="navi-link edit-uraian">
                                                            <span class="navi-icon">
                                                              <i class="navi-icon flaticon2-edit"></i>
                                                            </span>
                                                            <span class="navi-text">Edit Uraian</span>
                                                          </a>
                                                        </li>

                                                        <li class="navi-item">
                                                          <a data-id="{{ $anjabs->id }}"  href="javascript:;" class="navi-link delete-uraian">
                                                            <span class="navi-icon">
                                                              <i class="navi-icon flaticon-delete-1"></i>
                                                            </span>
                                                            <span class="navi-text">Hapus Uraian</span>
                                                          </a>
                                                        </li>
                                                      </ul>
                                                      <!--end::Naviigation-->
                                                    </div>
                                                </div>

                                              </td>
                                              @endif
                                            </tr>
                                            @endforeach
                                          </tbody>
                                        </table>
                                        <!--end: Datatable-->
                                        {{ $anjab_uraian->links() }}
                                      </div>
                                    </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--end::Example-->
                  </div>
                </div>
                <!--end::Card-->
                @endif
              </div>
              <!--end::Container-->

              <div id="modal-uraian" class="modal fade">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5> 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('anjab.save-uraian') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                      
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div id="modal-tahapan" class="modal fade">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5> 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('anjab.save-uraian-tahapan') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                      
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>


          <form id="form-delete-uraian" method="POST" action="{{ route('anjab.delete-uraian') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>

          <div class="modal fade text-left" id="modal" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-xl" role="document">
                  <div class="modal-content" style="width: 1200px;">
                      <div class="modal-header">
                          <h4 class="modal-title"></h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div class="modal-body">
                          
                      </div>
                      
                  </div>
              </div>
          </div>
@endsection

@section('js')
{{-- js files --}}
 <script src="{{ asset('') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
  $('.add-data').on('click', function(){
    var id = $('#id').val();
    $.ajax({
      url:'{{ route("anjab.get-form-uraian") }}',
      method:'GET',
      data:{aksi:'add-data',id:id },
      success:function(result){
        $('#modal-uraian .modal-title').text("Tambah Anjab Uraian");
        $('#modal-uraian .modal-body').html(result);
        $('#modal-uraian').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.add-tahapan').on('click', function(){
    var id_anjab = $('#id').val();
    var id_uraian = $(this).data('id');
    $.ajax({
      url:'{{ route("anjab.get-form-uraian-tahapan") }}',
      method:'GET',
      data:{aksi:'add-tahapan',id_anjab:id_anjab,id_uraian:id_uraian },
      success:function(result){
        $('#modal-tahapan .modal-title').text("Tambah Anjab Tahapan ");
        $('#modal-tahapan .modal-body').html(result);
        $('#modal-tahapan').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.edit-uraian').on('click', function(){
    var id = $(this).data('id');
    $.ajax({
      url:'{{ route("anjab.get-form-uraian") }}',
      method:'GET',
      data:{aksi:'edit-uraian',id:id },
      success:function(result){
        $('#modal-uraian .modal-title').text("Edit Uraian Anjab");
        $('#modal-uraian .modal-body').html(result);
        $('#modal-uraian').modal('show');
        $('.select2').select2();
      }
    });
  });

  $('.delete-uraian').on('click', function(){
    var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin?',
            // text: "Anda tidak dapat membatalkan pengajuan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus Data',
            cancelButtonText: 'Batal',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-warning',
            buttonsStyling: false
        }).then(function (status) {
          if(status.value){
            $('form[id=form-delete-uraian] input[name=id]').val(id);
                $('form[id=form-delete-uraian]').submit();
          }
        });
  });

  $('#print_anjab').on('click', function(){
     var id_anjab = $('#id').val();
    $.ajax({
      type: "GET",
      url: '{{ route("anjab.view-print") }}',
      data: {
        id_anjab : id_anjab,
      },
      cache: false,
      success: function(html) {
        // $("#div_header").hide();
        // $("#div_print_card").show();
        // $("#div_print").html(html);
        $('#modal .modal-title').text("Print Anjab");
        $('#modal .modal-body').html(html);
        $('#modal ').modal('show');
      }
    });
  });

</script>
@endsection