<!--begin::Card-->
<div class="card card-custom">
  <div class="card-header flex-wrap py-5">
    <div class="card-title">
      <h3 class="card-label">Analisa Jabatan :  {{ @$anjab->jabatan->jabatan }}</h3>
    </div>
    <div class="card-toolbar">
      <!--begin::Button-->
      <a href="{{ route('anjab.print-word', $anjab->id) }}"  class="btn btn-success btn-sm font-weight-bolder add-anjab">
      <i class="flaticon2-print"></i>Unduh Hasil Analisa</a>
      <!--end::Button-->
    </div>
  </div>
  <div class="card-body">
    <input type="hidden" name="id" id="id" value="{{ $anjab->id }}">
    <!--begin: Datatable-->
    <table class="table table-bordered table-striped " id="anjab_tabel" style="margin-top: 13px !important">
      <tbody>
        <tr>
          <td>Nama Jabatan </td>
          <td>{{ $anjab->jabatan->jabatan }} </td>
        </tr>
        <tr>
          <td>Jenis Jabatan </td>
          <td>{{ $anjab->text_jenis_jabatan }} </td>
        </tr>
        <tr>
          <td>Eselon Jabatan </td>
          <td>{{ $anjab->text_eselon_jabatan }} </td>
        </tr>
        <tr>
          <td>Kode Jabatan </td>
          <td>{{ $anjab->jabatan->_kode }} </td>
        </tr>
        <tr>
          <td>JPT Pratama </td>
          <td>{{ @$anjab->eselon2->unitkerja }} </td>
        </tr>
        <tr>
          <td>Administrator </td>
          <td>{{ @$anjab->eselon3->unitkerja }} </td>
        </tr>
        <tr>
          <td>Pengawas </td>
          <td>{{ @$anjab->eselon4->unitkerja }} </td>
        </tr>
        <tr>
          <td>Ikhtisar Jabatan </td>
          <td width="50%" style="text-align: justify;">{{ $anjab->ikhtisar_jabatan }} </td>
        </tr>
      </tbody>
    </table>
    <!--end: Datatable-->
  </div>
</div>
<!--end::Card-->