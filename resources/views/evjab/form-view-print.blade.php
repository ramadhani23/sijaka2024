<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $anjab->jabatan->jabatan }}</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }
        h2 {
            margin-bottom: 5px;
        }
        ul {
            margin-top: 0;
            padding-left: 20px;
        }
        ul li::before {
            content: "\2022"; /* Unicode for bullet point */
            color: black;
            display: inline-block;
            width: 1em;
            margin-left: -1em;
        }
    </style>
    
</head>
<body>
    @if($anjab->jenis_jabatan == 1)
    <h1 style="text-align: center">Informasi Jabatan</h1>

    <div class="section">
        <h2>Nama Jabatan : </h2>
        <p>{{ $anjab->jabatan->jabatan }}</p>
    </div>
    <div class="section">
        <h2>I. PERAN JABATAN</h2>
        <p>{{ $anjab->ikhtisar_jabatan }}</p>
    </div>
    <div class="section">
        <h2>II. URAIAN TUGAS DAN TANGGUNG JAWAB</h2>
        <h3>A. Uraian Tugas</h3>
        @foreach ($anjab_uraian as $uraian)
        <li>{{ $uraian->uraian_jabatan }}</li>
        @endforeach
        <h3>B. Tanggung Jawab</h3>
        @foreach ($anjab_tanggung_jawab as $tanggung_jawab)
        <li>{{ $tanggung_jawab->tanggung_jawab }}</li>
        @endforeach
    </div>
    <div class="section">
        <h2>III. HASIL KERJA JABATAN</h2>
        @foreach ($anjab_hasil_kerja as $hasil_kerja)
        <li>{{ $hasil_kerja->hasil_kerja }}</li>
        @endforeach
    </div>
    <div class="section">
        <h2>IV. TINGKAT FAKTOR</h2>
        <h3>FAKTOR 1 : PENGETAHUAN YANG DIBUTUHKAN</h3>
        <p>{!! $faktor_evajab->text_faktor1 !!}</p>
        <h3>FAKTOR 2 : PENGAWASAN PENYELIA</h3>
        <p>{!! $faktor_evajab->text_faktor2 !!}</p>
        <h3>FAKTOR 3 : WEWENANG PENYELIAAN DAN MANAJERIAL</h3>
        <p>{!! $faktor_evajab->text_faktor3 !!}</p>
        <h3>FAKTOR 4 : HUBUNGAN PERSONAL</h3>
        <h4>A. Sifat Hubungan</h4>
        <p>{!! $faktor_evajab->text_faktor4A !!}</p>
        <h4>B. Tujuan Hubungan</h4>
        <p>{!! $faktor_evajab->text_faktor4B !!}</p>
        <h3>FAKTOR 5 : KESULITAN DALAM PENGARAHAN PEKERJAAN</h3>
        <p>{!! $faktor_evajab->text_faktor5 !!}</p>
        <h3>FAKTOR 6 : KONDISI LAIN</h3>
        <p>{!! $faktor_evajab->text_faktor6 !!}</p>
    </div>
    @else
    <h1 style="text-align: center">Informasi Jabatan</h1>

    <div class="section">
        <h2>Nama Jabatan : </h2>
        <p>{{ $anjab->jabatan->jabatan }}</p>
    </div>
    <div class="section">
        <h2>I. PERAN JABATAN</h2>
        <p>{{ $anjab->ikhtisar_jabatan }}</p>
    </div>
    <div class="section">
        <h2>II. URAIAN TUGAS DAN TANGGUNG JAWAB</h2>
        <h3>A. Uraian Tugas</h3>
        @foreach ($anjab_uraian as $uraian)
        <li>{{ $uraian->uraian_jabatan }}</li>
        @endforeach
        <h3>B. Tanggung Jawab</h3>
        @foreach ($anjab_tanggung_jawab as $tanggung_jawab)
        <li>{{ $tanggung_jawab->tanggung_jawab }}</li>
        @endforeach
    </div>
    <div class="section">
        <h2>III. HASIL KERJA JABATAN</h2>
        @foreach ($anjab_hasil_kerja as $hasil_kerja)
        <li>{{ $hasil_kerja->hasil_kerja }}</li>
        @endforeach
    </div>
    <div class="section">
        <h2>IV. TINGKAT FAKTOR</h2>
        <h3>FAKTOR 1 : PENGETAHUAN YANG DIBUTUHKAN</h3>
        <p>{!! $faktor_evajab->text_faktor1 !!}</p>
        <h3>FAKTOR 2 : PENGAWASAN PENYELIA</h3>
        <p>{!! $faktor_evajab->text_faktor2 !!}</p>
        <h3>FAKTOR 3 : PEDOMAN</h3>
        <p>{!! $faktor_evajab->text_faktor3 !!}</p>
        <h3>FAKTOR 4 : KOMPLEKSITAS</h3>
        <p>{!! $faktor_evajab->text_faktor4 !!}</p>
        <h3>FAKTOR 5 : RUANG LINGKUP DAN DAMPAK</h3>
        <p>{!! $faktor_evajab->text_faktor5 !!}</p>
        <h3>FAKTOR 6 : HUBUNGAN PERSONAL</h3>
        <p>{!! $faktor_evajab->text_faktor6 !!}</p>
        <h3>FAKTOR 7 : TUJUAN HUBUNGAN</h3>
        <p>{!! $faktor_evajab->text_faktor7 !!}</p>
        <h3>FAKTOR 8 : PERSYARATAN FISIK</h3>
        <p>{!! $faktor_evajab->text_faktor8 !!}</p>
        <h3>FAKTOR 9 : LINGKUNGAN PEKERJAAN</h3>
        <p>{!! $faktor_evajab->text_faktor8 !!}</p>
    </div>
    @endif
    <!-- bisa menambahkan konten lain sesuai kebutuhan -->
</body>
</html>