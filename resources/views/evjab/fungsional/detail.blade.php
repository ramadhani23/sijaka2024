@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                  <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h3 class="card-label">Faktor Jabatan :  {{ $anjab->jabatan->jabatan }}</h3>
                    </div>
                    @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                    @if (!isset($faktorEvajab->status) || $faktorEvajab->status == 'tidak')   
                    {{-- {{ $faktorEvajab->id }} --}}
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      @if (!isset($faktorEvajab->id))
                      @else
                      <a href="{{ route('evajab.verifikasi', $faktorEvajab->id )}}" class="btn btn-success font-weight-bolder">
                       <i class="fas fa-check"></i>Verifikasi</a>
                      <!--end::Button-->
                      @endif
                    </div>
                    @else
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      <a href="{{ route('evajab.batal-verifikasi', $faktorEvajab->id) }}" class="btn btn-danger font-weight-bolder">
                       <i class="fas fa-times"></i>Batal Verifikasi</a>
                      <!--end::Button-->
                    </div>
                    @endif
                    @else
                    @endif
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      <a href="{{ route('evajab.print', ['jabatan' => $anjab->jabatan, 'unitkerja' => $unit_kerja, 'anjab_id' => $anjab->id ]) }}" target="_blank" class="btn btn-warning font-weight-bolder">
                       <i class="fas fa-download"></i>Cetak</a>
                      <!--end::Button-->
                    </div>
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      <a href="{{ route('evajab.browse',['jenis' =>'fungsional', 'unit_kerja' => $unit_kerja ]) }}" class="btn btn-primary font-weight-bolder">
                       <i class="fas fa-undo"></i>Kembali</a>
                      <!--end::Button-->
                    </div>
                  </div>
                  <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table" id="anjab_tabel" style="margin-top: 13px !important">
                      <thead>
                        <tr>
                          <th class="text-center">No</th>
                          <th class="text-center">Faktor Evaluasi</th>
                          <th class="text-center">Nilai Yang Diberikan</th>
                          <th class="text-center" style="width: 20%">Standar Jabatan Fungsional Yang Digunakan (Jika Ada) </th>
                          <th class="text-center">Keterangan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Faktor 1 : Pengetahuan Yang Dibutuhkan Jabatan</td>
                          <td class="text-center">{{ isset($faktorEvajab) ? $faktorEvajab->faktorLevel1->nilai : '-' }}</td>
                          <td width="20%">-</td>
                          <td class="text-center">
                            @if($faktorEvajab)
                            Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_1 }}
                            @endif
                          </td>
                        </tr>
                      <tr>
                          <td>2</td>
                          <td>Faktor 2 : Pengawasan</td>
                          <td class="text-center">{{ isset($faktorEvajab) ? $faktorEvajab->faktorLevel2->nilai : '-' }}</td>
                          <td width="20%">-</td>
                          <td class="text-center">
                            @if($faktorEvajab)
                            Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_2 }}
                            @endif
                          </td>
                        </tr>
                      <tr>
                          <td>3</td>
                          <td>Faktor 3 : Pedoman</td>
                          <td class="text-center">{{  isset($faktorEvajab) ? $faktorEvajab->faktorLevel3->nilai : '-' }}</td>
                          <td width="20%">-</td>
                          <td class="text-center">
                            @if($faktorEvajab)
                            Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_3 }}
                            @endif
                          </td>
                        </tr>
                      <tr>
                          <td>4</td>
                          <td>Faktor 4 : Kompleksitas</td>
                          <td class="text-center"> {{ @$faktorEvajab->faktorLevel4->nilai }}</td>
                          
                          <td width="20%">
                           -
                          </td>
                          <td class="text-center">
                            @if($faktorEvajab)
                            Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_4 }}
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <td>5</td>
                          <td>Faktor 5 : Ruang Lingkup dan Dampak</td>
                          <td class="text-center">{{ isset($faktorEvajab) ? $faktorEvajab->faktorLevel5->nilai :'-' }}</td>
                          <td width="20%">
                            -
                          </td>
                          <td class="text-center">
                            @if($faktorEvajab)
                            Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_5 }}
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <td>6</td>
                          <td>Faktor 6 : Hubungan Personal</td>
                          <td class="text-center">{{ isset($faktorEvajab) ? $faktorEvajab->faktorLevel6->nilai :'-' }}</td>
                          <td width="20%">
                            -
                          </td>
                          <td class="text-center">
                            @if($faktorEvajab)
                            Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_6 }}
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <td>7</td>
                          <td>Faktor 7 : Tujuan Hubungan</td>
                          <td class="text-center">{{ isset($faktorEvajab) ? $faktorEvajab->faktorLevel7->nilai :'-' }}</td>
                          <td width="20%">
                            -
                          </td>
                          <td class="text-center">
                            @if($faktorEvajab)
                            Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_7 }}
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <td>8</td>
                          <td>Faktor 8: Persyaratan Fisik</td>
                          <td class="text-center">{{ isset($faktorEvajab) ? $faktorEvajab->faktorLevel8->nilai :'-' }}</td>
                          <td width="20%">
                            -
                          </td>
                          <td class="text-center">
                            @if($faktorEvajab)
                            Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_8 }}
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <td>9</td>
                          <td>Faktor 9 : Lingkungan Kerja</td>
                          <td class="text-center">{{ isset($faktorEvajab) ? $faktorEvajab->faktorLevel9->nilai :'-' }}</td>
                          <td width="20%">
                            -
                          </td>
                          <td class="text-center">
                            @if($faktorEvajab)
                            Tingkat Faktor {{ $faktorEvajab->tingkat_faktor_9 }}
                            @endif
                          </td>
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th class="text-center" colspan="2">Total Nilai</th>
                          <th class="text-center" >
                            @if($faktorEvajab)
                            {{ $faktorEvajab->total_nilai }}
                            @endif
                          </th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <th class="text-center" colspan="2">Kelas Jabatan</th>
                          <th class="text-center">
                            @if($faktorEvajab)
                            {{ $faktorEvajab->kelas_jabatan }}
                            @endif
                          </th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </tfoot>
                    </table>
                    <!--end: Datatable-->
                  </div>
                </div>
                <!--end::Card-->
                <br>
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header">
                    <div class="card-title">
                      <h2 class="card-label">Faktor Jabatan :  {{ $anjab->jabatan->nama }}</h2>
                    </div>
                  </div>
                  @if(isset($faktorEvajab))
                  <form id="form-input-evajab" method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('evajab.update')}}">
                  {{csrf_field()}}

                  <input type="hidden" name="id_evajab" id="id" value="{{ $faktorEvajab->id }}">
                  @else
                  <form id="form-input-evajab" method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('evajab.save')}}">
                    {{csrf_field()}}
                  @endif
                  <div class="card-body">
                    
                    <input type="hidden" name="id" id="id" value="{{ $anjab->id }}">
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 1 : Pengetahuan Yang Dibutuhkan Jabatan</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-4">
                          <select required="" class="form-control" name="level_faktor1" id="level_faktor1">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($levelFaktor1 as $levelFaktors1)
                            <option {{ @$faktorEvajab->faktor1 == $levelFaktors1->id? 'selected' : '' }}  value="{{ $levelFaktors1->id }}">{{ $levelFaktors1->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor1">
                      </div>
                      {{-- {{ isset($faktorEvajab) ? $faktorEvajab->faktorLevel1->nilai : '-' }} --}}
                      @if (isset($faktorEvajab->text_faktor1) ? $faktorEvajab->text_faktor1 : '')
                        <div class="border bg-gray-100 p-5">
                          <p>{!! isset($faktorEvajab->text_faktor1) ? $faktorEvajab->text_faktor1 : '' !!}</p>
                        </div>
                          <input type="hidden" id="text_faktor1_change" name="text_faktor1" value="{{ isset($faktorEvajab->text_faktor1) ? $faktorEvajab->text_faktor1 : '' }}">
                      @endif
                     </div>
                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 2 : Pengawasan</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                          <select required="" class="form-control" name="level_faktor2" id="level_faktor2">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($levelFaktor2 as $levelFaktors2)
                            <option {{ @$faktorEvajab->faktor2 == $levelFaktors2->id? 'selected' : '' }} value="{{ $levelFaktors2->id }}">{{ $levelFaktors2->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor2">
                      </div>
                      @if (isset($faktorEvajab->text_faktor2) ? $faktorEvajab->text_faktor2 : '')
                        <div class="border">
                          <p>{!! isset($faktorEvajab->text_faktor2) ? $faktorEvajab->text_faktor2 : '' !!}</p>
                        </div>
                          <input type="hidden" id="text_faktor2_change" name="text_faktor2" value="{{ isset($faktorEvajab->text_faktor2) ? $faktorEvajab->text_faktor2 : '' }}">
                      @endif
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 3 : Pedoman</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                          <select required="" class="form-control" name="level_faktor3" id="level_faktor3">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($levelFaktor3 as $levelFaktors3)
                            <option {{ @$faktorEvajab->faktor3 == $levelFaktors3->id? 'selected' : '' }} value="{{ $levelFaktors3->id }}">{{ $levelFaktors3->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor3">
                      </div>
                      @if (isset($faktorEvajab->text_faktor3) ? $faktorEvajab->text_faktor3 : '')
                      <div class="border">
                        <p>{!! isset($faktorEvajab->text_faktor3) ? $faktorEvajab->text_faktor3 : '' !!}</p>
                      </div>
                        <input type="hidden" id="text_faktor3_change" name="text_faktor3" value="{{ isset($faktorEvajab->text_faktor3) ? $faktorEvajab->text_faktor3 : '' }}">
                      @endif
                    </div>
                    <div class="separator separator-dashed my-5"></div>
                    <div class="form-group ">
                      <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 4 : Kompleksitas</h2>
                    </div>
                    <div class="mb-15">
                      
                      <div class="form-group row">
                        <label class="col-lg-2 text-right">Level :</label>
                        <div class="col-lg-3">
                          <select required="" class="form-control" name="level_faktor4" id="level_faktor4">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($levelFaktor4 as $levelFaktors4)
                            <option {{ @$faktorEvajab->faktor4 == $levelFaktors4->id? 'selected' : '' }} value="{{ $levelFaktors4->id }}">{{ $levelFaktors4->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor4">
                      </div>
                      @if (isset($faktorEvajab->text_faktor4) ? $faktorEvajab->text_faktor4 : '')
                      <div class="border">
                        <p>{!! isset($faktorEvajab->text_faktor4) ? $faktorEvajab->text_faktor4 : '' !!}</p>
                      </div>
                        <input type="hidden" id="text_faktor4_change" name="text_faktor4" value="{{ isset($faktorEvajab->text_faktor4) ? $faktorEvajab->text_faktor4 : '' }}">
                      @endif

                    </div>
                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 5 : Ruang Lingkup dan Dampak</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                          <select required="" class="form-control" name="level_faktor5" id="level_faktor5">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                            @foreach($levelFaktor5 as $levelFaktors5)
                            <option {{ @$faktorEvajab->faktor5 == $levelFaktors5->id? 'selected' : '' }} value="{{ $levelFaktors5->id }}">{{ $levelFaktors5->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor5">
                      </div>
                      @if (isset($faktorEvajab->text_faktor5) ? $faktorEvajab->text_faktor5 : '')
                      <div class="border">
                        <p>{!! isset($faktorEvajab->text_faktor5) ? $faktorEvajab->text_faktor5 : '' !!}</p>
                      </div>
                        <input type="hidden" id="text_faktor5_change" name="text_faktor5" value="{{ isset($faktorEvajab->text_faktor5) ? $faktorEvajab->text_faktor5 : '' }}">
                      @endif
                    </div>
                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 6 : Hubungan Personal</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                          <select required="" class="form-control" name="level_faktor6" id="level_faktor6">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                             @foreach($levelFaktor6 as $levelFaktors6)
                            <option {{ @$faktorEvajab->faktor6 == $levelFaktors6->id? 'selected' : '' }} value="{{ $levelFaktors6->id }}">{{ $levelFaktors6->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor6">
                      </div>
                      @if (isset($faktorEvajab->text_faktor6) ? $faktorEvajab->text_faktor6 : '')
                      <div class="border">
                        <p>{!! isset($faktorEvajab->text_faktor6) ? $faktorEvajab->text_faktor6 : '' !!}</p>
                      </div>
                        <input type="hidden" id="text_faktor6_change" name="text_faktor6" value="{{ isset($faktorEvajab->text_faktor6) ? $faktorEvajab->text_faktor6 : '' }}">
                      @endif
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 7 : Tujuan Hubungan</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                          <select required="" class="form-control" name="level_faktor7" id="level_faktor7">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                             @foreach($levelFaktor7 as $levelFaktors7)
                            <option {{ @$faktorEvajab->faktor7 == $levelFaktors7->id? 'selected' : '' }} value="{{ $levelFaktors7->id }}">{{ $levelFaktors7->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor7">
                      </div>
                      @if (isset($faktorEvajab->text_faktor7) ? $faktorEvajab->text_faktor7 : '')
                      <div class="border">
                        <p>{!! isset($faktorEvajab->text_faktor7) ? $faktorEvajab->text_faktor7 : '' !!}</p>
                      </div>
                        <input type="hidden" id="text_faktor7_change" name="text_faktor7" value="{{ isset($faktorEvajab->text_faktor7) ? $faktorEvajab->text_faktor7 : '' }}">
                      @endif
                    </div>

                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 8 : Persyaratan Fisik</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                          <select required="" class="form-control" name="level_faktor8" id="level_faktor8">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                             @foreach($levelFaktor8 as $levelFaktors8)
                            <option {{ @$faktorEvajab->faktor8 == $levelFaktors8->id? 'selected' : '' }} value="{{ $levelFaktors8->id }}">{{ $levelFaktors8->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor8">
                      </div>
                      @if (isset($faktorEvajab->text_faktor8) ? $faktorEvajab->text_faktor8 : '')
                      <div class="border">
                        <p>{!! isset($faktorEvajab->text_faktor8) ? $faktorEvajab->text_faktor8 : '' !!}</p>
                      </div>
                        <input type="hidden" id="text_faktor8_change" name="text_faktor8" value="{{ isset($faktorEvajab->text_faktor8) ? $faktorEvajab->text_faktor8 : '' }}">
                      @endif
                    </div>
                    
                    <div class="separator separator-dashed my-5"></div>
                    <h2 class="font-size-lg text-dark font-weight-bold mb-6">Faktor 9 : Lingkungan Kerja</h2>
                    <div class="mb-15">
                      <div class="form-group row">
                        <label class="col-lg-2 col-form-label text-right">Level :</label>
                        <div class="col-lg-3">
                          <select required="" class="form-control" name="level_faktor9" id="level_faktor9">
                            <option value="" disabled="" selected="">-- PILIH LEVEL--</option>
                             @foreach($levelFaktor9 as $levelFaktors9)
                            <option {{ @$faktorEvajab->faktor9 == $levelFaktors9->id? 'selected' : '' }} value="{{ $levelFaktors9->id }}">{{ $levelFaktors9->nama }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div id="data_faktor9">
                      </div>
                      @if (isset($faktorEvajab->text_faktor9) ? $faktorEvajab->text_faktor9 : '')
                      <div class="border">
                        <p>{!! isset($faktorEvajab->text_faktor9) ? $faktorEvajab->text_faktor9 : '' !!}</p>
                      </div>
                        <input type="hidden" id="text_faktor9_change" name="text_faktor9" value="{{ isset($faktorEvajab->text_faktor9) ? $faktorEvajab->text_faktor9 : '' }}">
                      @endif
                    </div>

                  </div>
                  <div class="card-footer">
                    <div class="row">
                      <div class="col text-right">
                        @if(isset($faktorEvajab))
                        <button type="submit" class="btn btn-success mr-2">Update</button>
                        @else
                        <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        @endif
                      </div>
                    </div>
                  </div>
                  </form>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

              
@endsection

@section('js')
{{-- js files --}}
 <script src="{{ asset('') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script src="//cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script>
  var editor_ruang_lingkup = CKEDITOR.replace( 'ruang_lingkup', options);
        // editor_ruang_lingkup.setData(text_ruang_lingkup);

        // CKEDITOR.replace( 'dampak_program', options);
        CKEDITOR.replace( 'pengaturan_organisasi', options);
        CKEDITOR.replace( 'wewenang_penyeliaan_manajerial', options);
        CKEDITOR.replace( 'sifat_hubungan', options);
        CKEDITOR.replace( 'tujuan_hubungan', options);
        CKEDITOR.replace( 'kesulitan_pengarahan', options);
        CKEDITOR.replace( 'kondisi_lain', options);
</script> 
<script>
  var options = {
    filebrowserImageBrowseUrl: '{{ URL::to("laravel-filemanager?type=Images") }}',
    filebrowserImageUploadUrl: '{{ URL::to("laravel-filemanager/upload?type=Images&_token=") }}',
    filebrowserBrowseUrl: '{{ URL::to("/laravel-filemanager?type=Files") }}',
    filebrowserUploadUrl: '{{ URL::to("/laravel-filemanager/upload?type=Files&_token=") }}',
  };
</script>

<script>

        // CKEDITOR.replace( 'text_faktor1', options);
        // CKEDITOR.replace( 'text_faktor2', options);
        // CKEDITOR.replace( 'text_faktor3', options);
        // CKEDITOR.replace( 'text_faktor4', options);
        // CKEDITOR.replace( 'text_faktor5', options);
        // CKEDITOR.replace( 'text_faktor6', options);
        // CKEDITOR.replace( 'text_faktor7', options);
        // CKEDITOR.replace( 'text_faktor8', options);
        // CKEDITOR.replace( 'text_faktor9', options);

        $("#level_faktor1").change(function() {
          var dats = $(this).val();
          // alert(dats)
          $.ajax({
            type: "GET",
            url: '{{ route("evajab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 1
            },
            cache: false,
            success: function(html) {
              $("#data_faktor1").html(html);
              $("#text_faktor1_change").remove();
            }
          });
        });

        $("#level_faktor2").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("evajab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 2
            },
            cache: false,
            success: function(html) {
              $("#data_faktor2").html(html);
              $("#text_faktor2_change").remove();
            }
          });
        });

        $("#level_faktor3").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("evajab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 3
            },
            cache: false,
            success: function(html) {
              $("#data_faktor3").html(html);
            }
          });
        });

        $("#level_faktor4").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("evajab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 4
            },
            cache: false,
            success: function(html) {
              $("#data_faktor4").html(html);
            }
          });
        });

        $("#level_faktor5").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("evajab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 5
            },
            cache: false,
            success: function(html) {
              $("#data_faktor5").html(html);
            }
          });
        });

        $("#level_faktor6").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("evajab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 6
            },
            cache: false,
            success: function(html) {
              $("#data_faktor6").html(html);
            }
          });
        });

        $("#level_faktor7").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("evajab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 7
            },
            cache: false,
            success: function(html) {
              $("#data_faktor7").html(html);
            }
          });
        });

        $("#level_faktor8").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("evajab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 8
            },
            cache: false,
            success: function(html) {
              $("#data_faktor8").html(html);
            }
          });
        });

        $("#level_faktor9").change(function() {
          var dats = $(this).val();
          $.ajax({
            type: "GET",
            url: '{{ route("evajab.get-level-faktor") }}',
            data: {
                  id :  dats,
                  faktor_id : 9
            },
            cache: false,
            success: function(html) {
              $("#data_faktor9").html(html);
            }
          });
        });
</script>
@endsection