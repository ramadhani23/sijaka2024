@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
  .table-warning {
    background-color: #fff3cd;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    {{-- <div class="card-toolbar">
                      <!--begin::Button-->
                      <a href="javascript:;" class="btn btn-primary font-weight-bolder add-evjab">
                       <i class="far fa-plus-square"></i>Tambah Evaluasi</a>
                      <!--end::Button-->
                    </div> --}}
                  </div>
                  <div class="card-body">
                    @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                    <form method="GET">
                      <div class="form-group row">
                        <label class="col-1 col-form-label">Filter</label>
                        {{-- <div class="col-4">
                          <input class="form-control" type="text" name="jabatan" placeholder="Nama Jabatan ...">
                        </div> --}}

                        <div class="col-3">
                          <select required class="form-control select2" name="unit_kerja">
                            <option selected="" disabled="">-- PILIH Unit Kerja --</option>
                            @foreach($unitKerja as $itemUnitkerja)
                            {{-- <option  value="{{ $itemUnitkerja->kodeunit }}">{{ $itemUnitkerja->unitkerja }}</option> --}}
                            <option @if(@$unit_kerja == $itemUnitkerja['kodeunit'] ) selected @endif value="{{ $itemUnitkerja['kodeunit'] }}" {{ old('unit_kerja') == $itemUnitkerja['kodeunit'] ? "selected" :""}}>{{  $itemUnitkerja['unitkerja'] }}</option>
                            @endforeach
                          </select>
                        </div>

                        <div class="col-3">
                          <select class="form-control select2" name="jenis_jabatan" disabled>
                            <option selected="" disabled="">-- PILIH Jenis Jabatan --</option>
                            <option value="1" @if ($jenis == 'struktural') selected @endif>STRUKTURAL</option>
                            <option value="2" @if ($jenis == 'fungsional') selected @endif>FUNGSIONAL</option>
                            <option value="3" @if ($jenis == 'pelaksana') selected @endif>PELAKSANA</option>
                          </select>
                        </div>

                        <div class="col-1">
                          <button class="btn btn-primary">Filter</button>
                        </div>
                      </div>
                    </form>
                    @endif
                    <!--begin: Datatable-->
                    @if((request()->unit_kerja && \MojokertokabUser::getUser()->role == 'ADMIN') or \MojokertokabUser::getUser()->role == 'PD')
                    <div class="table-responsive">
                        <table class="table table-bordered" id="anjab_tabel" style="margin-top: 13px !important">
                          <thead class="thead-light">
                            <tr>
                              <th>Nooo</th>
                              <th>Jabatan</th>
                              <th>Unit Kerja</th>
                              <th>Kelas Jabatan</th>
                              <th>Harga Jabatan</th>
                              <th colspan="2">Faktor 1</th>
                              <th colspan="2">Faktor 2</th>
                              <th colspan="2">Faktor 3</th>
                              <th colspan="2">Faktor 4</th>
                              <th colspan="2">Faktor 5</th>
                              <th colspan="2">Faktor 6</th>
                              <th colspan="2">Faktor 7</th>
                              <th colspan="2">Faktor 8</th>
                              <th colspan="2">Faktor 9</th>
                              
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>

                            @if($evjab->count() == 0)
                            
                                
                            <tr>
                              <td colspan="24" class="text-center">Tidak Ada Data</td>
                            </tr>
                            
                            @endif

                            @foreach($evjab as $itemEvjab)
                            <tr class="{{ $itemEvjab->status == 'ya' ? 'table-warning' : '' }}">
                              <td class="text-center">{{ $loop->iteration }}</td>
                              <td>
                                @if($itemEvjab->dataAnjab && $itemEvjab->dataAnjab->jabatan) 
                                {{ @$itemEvjab->dataAnjab->jabatan->jabatan }}
                                @endif
                              </td>
                              <td>{{ @$itemEvjab->dataAnjab->dataUnitKerja->unitkerja }}</td>
                              <td class="text-center">{{ $itemEvjab->kelas_jabatan }}</td>
                              <td class="text-center">{{ $itemEvjab->total_nilai }}</td>
                              {{-- {{ $itemEvjab->trx_anjab_id }} --}}
                              
                              @if (isset($itemEvjab->faktorLevel1))
                              <td>{{ explode('Level ', $itemEvjab->faktorLevel1->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel1->nilai }}</td>
                              @else
                                  <td>-</td>
                                  <td>-</td>
                              @endif

                              @if (isset($itemEvjab->faktorLevel2))
                              <td>{{ explode('Level ', $itemEvjab->faktorLevel2->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel2->nilai }}</td>
                              @else
                                  <td>-</td>
                                  <td>-</td>
                              @endif

                              @if (isset($itemEvjab->faktorLevel3))
                              <td>{{ explode('Level ', $itemEvjab->faktorLevel3->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel3->nilai }}</td>
                              @else
                                  <td>-</td>
                                  <td>-</td>
                              @endif

                              @if (isset($itemEvjab->faktorLevel4))
                              <td>{{ explode('Level ', $itemEvjab->faktorLevel4->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel4->nilai }}</td>
                              @else
                                  <td>-</td>
                                  <td>-</td>
                              @endif

                              
                              @if (isset($itemEvjab->faktorLevel1))
                              <td>{{ explode('Level ', $itemEvjab->faktorLevel5->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel5->nilai }}</td>
                              @else
                                  <td>-</td>
                                  <td>-</td>
                              @endif

                              @if (isset($itemEvjab->faktorLevel5))
                              <td>{{ explode('Level ', $itemEvjab->faktorLevel6->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel6->nilai }}</td>
                              @else
                                  <td>-</td>
                                  <td>-</td>
                              @endif

                              @if (isset($itemEvjab->faktorLevel6))
                              <td>{{ explode('Level ', $itemEvjab->faktorLevel7->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel7->nilai }}</td>
                              @else
                                  <td>-</td>
                                  <td>-</td>
                              @endif

                              @if (isset($itemEvjab->faktorLevel7))
                              <td>{{ explode('Level ', $itemEvjab->faktorLevel8->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel8->nilai }}</td>
                              @else
                                  <td>-</td>
                                  <td>-</td>
                              @endif

                              @if (isset($itemEvjab->faktorLevel8))
                              <td>{{ explode('Level ', $itemEvjab->faktorLevel9->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel9->nilai }}</td>
                              @else
                                  <td>-</td>
                                  <td>-</td>
                              @endif
                              <td>
                                <a href="{{ route('evajab.detail',['id' => $itemEvjab->trx_anjab_id, 'unit_kerja' => $unit_kerja]) }}" class="btn btn-icon btn-sm btn-warning"><i class="flaticon-edit"></i></a>
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                           
                        </table>
                    </div>
                    @endif
                    <!--end: Datatable-->
                  </div>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

             <div id="modal-evjab" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title"></h5> 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form action="{{ route('evjab.pilih-anjab') }}" method="GET">
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-12">
                        <input type="hidden" value="create" name="aksi">
                        {{-- <input type="hidden" value="{{ $itemAnjab->id }}" name="id"> --}}
                        <div class="form-group">
                          <label>Jabatan</label>
                          {{-- <select style="width: 100%" class="form-control select2" id="jabatan" name="jabatan">
                            <option value="" selected disabled>-- PILIH JABATAN --</option>
                            @foreach($anjab as $itemAnjab)
                            <option value="{{ $itemAnjab->id }}">{{ @$itemAnjab->jabatan->nama }} - {{ $itemAnjab->dataOpd->unitkerja }}</option>
                            @endforeach
                          </select> --}}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success btn-sm">Evaluasi</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          <form id="form-delete-anjab" method="POST" action="{{ route('evajab.delete') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
 $('.select2').select2();
   
  $('.add-evjab').on('click', function(){
    $('#modal-evjab .modal-title').text("Tambah Evaluasi Jabatan");
    $('#modal-evjab').modal('show');
    $('#modal-evjab .select2').select2();
  });

  function initEdit(){
    $('.edit-evjab').on('click', function(){
      var id = $(this).data('id');
      $.ajax({
        url:'{{ route("evajab.get-form") }}',
        method:'GET',
        data:{aksi:'edit-evajab',id:id },
        success:function(result){
          $('#modal-evjab .modal-title').text("Edit Anjab");
          $('#modal-evjab .modal-body').html(result);
          $('#modal-evjab').modal('show');
          $('.select2').select2();
        }
      });
    });
  }

  function initHapus(){
    $('.delete-evjab').on('click', function(){
      var id = $(this).data('id');
          swal.fire({
              title: 'Apakah anda yakin?',
              // text: "Anda tidak dapat membatalkan pengajuan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Hapus Data',
              cancelButtonText: 'Batal',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-warning',
              buttonsStyling: false
          }).then(function (status) {
            if(status.value){
              $('form[id=form-delete-evjab] input[name=id]').val(id);
                  $('form[id=form-delete-evjab').submit();
            }
          });
    });
  }

  

</script>
@endsection