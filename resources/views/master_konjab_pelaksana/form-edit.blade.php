<div class="row">
	<div class="col-12">
		<input type="hidden" value="update-konjabpelaksana" name="aksi">
		<input type="hidden" value="{{ $konjabpelaksana->id }}" name="id">

		<div class="form-group">
			<label>Jabatan Pelaksana Lama</label>
			<input value="{{ $konjabpelaksana->jabatan_lama }}" class="form-control" type="text" name="jabatan_lama" required="">
		</div>

		<div class="form-group">
			<label>Jabatan Pelaksana Lama</label>
			<input value="{{ $konjabpelaksana->jabatan_baru }}" type="text" class="form-control" name="jabatan_baru" required>
		</div>
	</div>
	
</div>