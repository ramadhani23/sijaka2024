@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>

<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    @if(\MojokertokabApp::allowChangingData())
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      {{-- <a href="javascript:;" class="btn btn-sm btn-primary font-weight-bolder add-skj">
                       <i class="far fa-plus-square"></i>Tambah Analisa Beban Kerja</a> --}}
                          {{-- @if ( $jenis == 'fungsional')
                          <a href="javascript:;" class="ml-1 btn btn-sm btn-success font-weight-bolder unggah-skj">
                            <i class="fas fa-upload"></i>Unggah Analisa Beban Kerja
                          </a>
                          @else
                              
                          @endif --}}
                        
                      <!--end::Button-->
                    </div>
                    @endif
                  </div>
                  <div class="card-body">
                    @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                    <form method="GET">
                      <div class="form-group row">
                        <label class="col-1 col-form-label">Filter</label>
                        {{-- <div class="col-4">
                          <input class="form-control" type="text" name="jabatan" placeholder="Nama Jabatan ...">
                        </div> --}}

                        <div class="col-3">
                          <select required class="form-control select2" name="unit_kerja">
                            <option selected="" disabled="">-- PILIH Unit Kerja --</option>
                            @foreach($unitKerja as $itemUnitkerja)
                            {{-- <option  value="{{ $itemUnitkerja->kodeunit }}">{{ $itemUnitkerja->unitkerja }}</option> --}}
                            <option @if(@$unit_kerja == $itemUnitkerja->kodeunit ) selected @endif value="{{ $itemUnitkerja->kodeunit }}" {{ old('unit_kerja') == $itemUnitkerja->kodeunit ? "selected" :""}}>{{  $itemUnitkerja->unitkerja }}</option>
                            @endforeach
                          </select>
                        </div>

                        <div class="col-3">
                          <select class="form-control select2" name="jenis_jabatan" disabled>
                            <option selected="" disabled="">-- PILIH Jenis Jabatan --</option>
                            <option value="1" @if ($jenis == 'struktural') selected @endif>STRUKTURAL</option>
                            <option value="2" @if ($jenis == 'fungsional') selected @endif>FUNGSIONAL</option>
                            <option value="3" @if ($jenis == 'pelaksana') selected @endif>PELAKSANA</option>
                          </select>
                        </div>

                        <div class="col-1">
                          <button class="btn btn-primary">Filter</button>
                        </div>
                      </div>
                    </form>
                    @endif

                    <!--begin: Datatable-->
                    @if((request()->unit_kerja && \MojokertokabUser::getUser()->role == 'ADMIN') or \MojokertokabUser::getUser()->role == 'PD')
                    <!-- {{ request()->unit_kerja }} -->
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-top: 13px !important">
                          <thead class="thead-light">
                            <tr>
                              <th>No</th>
                              <th>Jabatan</th>
                              <th>Unit Kerja</th>
                              <th>SKJ Terupload</th>
                              <th>Upload</th>
                              <th>Hapus SKJ</th>
                            </tr>
                          </thead>
                          <tbody>

                          
                            @if($anjab->count() == 0)
                            <tr>
                              <td class="text-center" colspan="6">Tidak ada data</td>
                            </tr>
                            @else

                            @endif
                            @php
                              $totalKebutuhan = 0;
                            @endphp
                            {{-- @foreach ($anjab->skj as $item)
                            {{ $item->id }}
                                
                            @endforeach --}}
                            @foreach($anjab as $itemAnjab)
                            {{-- {{ $itemAnjab }} --}}
                            {{-- {{ $itemAnjab->skj->id }} --}}
                            <tr>
                              <td class="text-center">{{ $loop->iteration }}</td>
                              <td>{{ @$itemAnjab->jabatan->jabatan }}  {{ strtoupper(@$itemAnjab->m_jabatan_fungsional_jenjang)  }}
                                <div>@if($itemAnjab->dataskjKhusus) <a target="_blank" href="{{ asset('skj-khusus/'.$itemAnjab->dataskjKhusus->dokumen_pendukung) }}" class="btn btn-success btn-sm">Lihat Dokumen skj</a> @endif</div>
                              </td>
                              {{-- <td>{{ @$itemAnjab->m_jabatan_fungsional_jenjang }}</td> --}}
                              <td>{{ @$itemAnjab->dataUnitKerja->unitkerja }}</td>
                              {{-- <td>
                                  <a target="_blank" href=""><i class="fas fa-download"></i></a>
                              </td> --}}
                              <td class="text-center">
                                
                                @if ( $itemAnjab->skj->dokumen_pendukung == null)
                                    Tidak Ada Data
                                @else
                                <a target="_blank" href="{{ asset('skj/'.$itemAnjab->skj->dokumen_pendukung) }}" class="btn btn-sm btn-icon btn-success"><i class="flaticon-download"></i></a>
                                @endif
                              </td>
                              <td class="text-center">
                                @if ($itemAnjab->skj->dokumen_pendukung == null)
                                <a href="javascript:;" data-id="{{ $itemAnjab->id }}" class="btn btn-sm btn-icon btn-info add-skj"><i class="flaticon-upload"></i></a>
                                @else
                                {{-- <a href="javascript:;" data-id="{{ $itemAnjab->skj->id }}" class="btn btn-sm btn-icon btn-warning edit-skj"><i class="flaticon2-writing"></i></a> --}}
                                {{-- <button data-id="{{ $itemAnjab->skj->id }}" data-toggle="modal" data-target="#edit-skj{{ $itemAnjab->skj->id }}" type="button" class="btn btn-sm btn-icon btn-warning edit-skj"><i class="flaticon2-writing"></i></button> --}}
                                -
                                @endif
                              </td>
                              <td class="text-center">
                                <a href="javascript:;" data-id="{{ $itemAnjab->skj->id }}" class="btn btn-sm btn-icon btn-danger delete-skj"><i class="
                                  flaticon2-trash"></i></a>
                                  {{-- {{ $itemAnjab->id }} --}}
                              </td>
                              
                            </tr>
                            @endforeach
                            {{-- {{ $skj->dokumen_pendukung }} --}}
                          </tbody>

                        </table>

                    </div>
                    <!--end: Datatable-->
                    @endif
                  </div>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

          <form id="form-delete-anjab" method="POST" action="{{ route('anjab.delete') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>

          <form id="form-delete-skj" method="POST" action="{{ route('skjupload.delete') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>

            <div id="modal-skj" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form action="{{ route('skjupload.save') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-12">
                        <input type="hidden" value="create-khusus" name="aksi">
                        {{-- <input type="hidden" value="{{ $itemAnjab->id }}" name="id"> --}}
                        <div class="row">
                          <div class="col-12">
                            <input type="hidden" name="txtid" id="txtid">
                            <div class="form-group">
                              <label>Dokumen SKJ</label>
                              <input type="file" class="form-control" name="dokumen">
                            </div>
                            </div>
                          </div>
                          
                        
                        </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
            

            {{-- @foreach(@$anjab as $itemAnjab)
            <div id="edit-skj{{ @$itemAnjab->skj->id }}" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form action="{{ url('mojokertoku/skjupload/edit') }}/{{ $itemAnjab->skj->id }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                  <div class="modal-body">
                        
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
            @endforeach --}}

            
            <div id="edit-skj" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form action="{{ route('skjupload.edit') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                  <div class="modal-body">
                        
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
           

            {{-- <div id="modal-skj" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form action="{{ route('skj.pilih-anjab') }}" method="GET">
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-12">
                        <input type="hidden" value="create" name="aksi">
                        <input type="hidden" value="{{ $itemAnjab->id }}" name="id">
                        <div class="form-group">
                          <label>Jabatan</label>
                          <select style="width: 100%" class="form-control select2" id="jabatan" name="jabatan">
                            <option value="" selected disabled>-- Pilih Jabatan --</option>
                            @foreach($anjab as $itemAnjab)
                            <option value="{{ $itemAnjab->id }}">{{ @$itemAnjab->jabatan->jabatan }} - {{ $itemAnjab->dataOpd->unitkerja }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success btn-sm">Analisa</button>
                  </div>
                  </form>
                </div>
              </div>
            </div> --}}


@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
   $('.select2').select2();
   

  // $('.add-skj').on('click', function(){
  //   $('#modal-skj .modal-title').text("Tambah Analisa Beban Kerja");
  //   $('#modal-skj').modal('show');
  //   $('#modal-skj .select2').select2();
  // });


    $('.add-skj').on('click', function(){
      var id = $(this).attr('data-id');
      console.log(id);
      $('#txtid').val(id);
      // $.ajax({
      //   url:'{{ route("skjupload.getForm") }}',
      //   method:'GET',
      //   data:{aksi:'add-skj'},
      //   success:function(result){
      //     $('#modal-skj .modal-title').text("Upload SKJ");
          // $('#modal-skj .modal-body').show
          
          // $('#btn-submit-skjupload').attr('disabled', true);
          // $('.select2').select2();

          // pilih_jabatan();
      //   }
      // });
      $('#modal-skj').modal('show');
    });

   

    function initEdit(){
    $('.edit-skj').on('click', function(){
      var id = $(this).data('id');
      console.log("Hello world!");
      $.ajax({
        url:'{{ route("skjupload.getForm") }}',
        method:'GET',
        data:{aksi:'edit-skj',id:id },
        success:function(result){
          $('#edit-skj .modal-title').text("Edit skj");
          $('#edit-skj .modal-body').html(result);
          $('#edit-skj').edit('show');
          $('.select2').select2();
        }
      });
    });
  }

    $('.delete-skj').on('click', function(){
    var id = $(this).data('id');
        swal.fire({
            title: 'Apakah anda yakin?',
            // text: "Anda tidak dapat membatalkan pengajuan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus Data',
            cancelButtonText: 'Batal',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-warning',
            buttonsStyling: false
        }).then(function (status) {
          if(status.value){
            $('form[id=form-delete-skj] input[name=id]').val(id);
            $('form[id=form-delete-skj').submit();
          }
        });
  });
    // $('.add-skj').on('click', function(){
    //     var id = $(this).data('id');
    //     console.log('ok');
    //     $.ajax({
    //         url:'{{ route("skjupload.getForm") }}',
    //         method:'GET',
    //         data:{aksi:'add-skj',id:id },
    //         success:function(result){
    //             // console.log(result);
    //             // $('#edit-laporan .modal-title').text("Edit laporan");
    //             $('#add-skj .modal-body').html(result);
    //             // $('#edit-proposal').modal('show');
    //             // $('.select2').select2();
    //             // CKEDITOR.replace( 'deskripsi', options);
    //         }
    //     });
    // });

 
    

    



</script>
@endsection
