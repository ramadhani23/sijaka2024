@extends('layout')

@section('content')

<link rel="stylesheet" href="{{ asset('treant-js') }}/Treant.css">
<link rel="stylesheet" href="{{ asset('treant-js') }}/examples/basic-example/basic-example.css">

<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->

                @include('kunci')

                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                          <ul class="nav nav-light-danger nav-bold nav-pills">
                            <li class="nav-item">
                              <a class="nav-link {{ $jenis == '1'?'active':'' }}" href="{{ route('skjupload.browse', ['jenis'=>'1']) }}">
                                <span class="nav-icon">
                                  <i class="flaticon2-chat-1"></i>
                                </span>
                                <span class="nav-text">Struktural</span>
                              </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link {{ $jenis == '2'?'active':'' }}" href="{{ route('skjupload.browse', ['jenis'=>'2']) }}">
                                <span class="nav-icon">
                                  <i class="flaticon2-drop"></i>
                                </span>
                                <span class="nav-text">Fungsional &nbsp<span class="badge badge-warning">Baru</span></span>
                              </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link {{ $jenis == '3'?'active':'' }}" href="{{ route('skjupload.browse', ['jenis'=>'3']) }}">
                                <span class="nav-icon">
                                  <i class="flaticon2-drop"></i>
                                </span>
                                <span class="nav-text">Pelaksana</span>
                              </a>
                            </li>
                            {{-- <li class="nav-item dropdown">
                              <a class="nav-link {{ $jenis == '2'?'active':'' }}" href="{{ route('anjab.browse', ['jenis'=>'2']) }}" role="button">
                                <span class="nav-icon">
                                  <i class="flaticon2-gear"></i>
                                </span>
                                <span class="nav-text">Fungsional</span>
                              </a>
                            </li> --}}
                          </ul>
                        </div>
                    <div class="card-toolbar">
                     
                      @if(\MojokertokabApp::allowChangingData())
                      <!--begin::Button-->
                      
                        {{-- @if($selectedUnitKerja == null)

                        @else --}}
                        <a data-jenis-jabatan="{{ $jenis }}" data-kode-opd="{{ @$pd->kodeunit }}" href="javascript:;" class="btn btn-primary btn-sm font-weight-bolder add-skj" id="btnTambahAnalis">
                          <i class="far fa-plus-square"></i>Tambah SKJ Upload
                          @if($jenis == '1') Struktural
                          @elseif($jenis == '2') Fungsional
                          @elseif($jenis == '3') Pelaksana
                          @endif</a>
  
                        {{-- @endif --}}

                      {{-- <a data-jenis-jabatan="{{ $jenis }}" data-kode-opd="{{ @$pd->kodeunit }}" href="javascript:;" class="btn btn-primary btn-sm font-weight-bolder add-anjab" id="btnTambahAnalis">
                       <i class="far fa-plus-square"></i>Tambah Analisis Jabatan
                       @if($jenis == '1') Struktural
                       @elseif($jenis == '2') Fungsional
                       @elseif($jenis == '3') Pelaksana
                       @endif</a> --}}
                          
                      @endif
                      <!--end::Button-->
                     
                      {{-- <a href="javascript:;" class="btn btn-success ml-1">Peta Jabatan</a> --}}
                    </div>
                  </div>
                  <div class="card-body">

                    {{-- <div class="row">
                      <div class="col-lg-2">
                        <div class="card card-custom bg-danger gutter-b" style="height: 130px">
                          <!--begin::Body-->
                          <div class="card-body d-flex flex-column p-0">
                            <!--begin::Stats-->
                            <div class="flex-grow-1 card-spacer-x pt-6">
                              <div class="text-inverse-danger font-weight-bold">Jabatan Struktural</div>
                              <div class="text-inverse-danger font-weight-bolder font-size-h3">3,620</div>
                            </div>
                            <!--end::Stats-->
                            <!--begin::Chart-->
                            <div id="kt_tiles_widget_2_chart" class="card-rounded-bottom" style="height: 50px"></div>
                            <!--end::Chart-->
                          </div>
                          <!--end::Body-->
                        </div>
                      </div>

                      <div class="col-lg-2">
                        <div class="card card-custom bg-danger gutter-b" style="height: 130px">
                          <!--begin::Body-->
                          <div class="card-body d-flex flex-column p-0">
                            <!--begin::Stats-->
                            <div class="flex-grow-1 card-spacer-x pt-6">
                              <div class="text-inverse-danger font-weight-bold">Jabatan Struktural</div>
                              <div class="text-inverse-danger font-weight-bolder font-size-h3">3,620</div>
                            </div>
                            <!--end::Stats-->
                            <!--begin::Chart-->
                            <div id="kt_tiles_widget_2_chart" class="card-rounded-bottom" style="height: 50px"></div>
                            <!--end::Chart-->
                          </div>
                          <!--end::Body-->
                        </div>
                      </div>

                      <div class="col-lg-2">
                        <div class="card card-custom bg-danger gutter-b" style="height: 130px">
                          <!--begin::Body-->
                          <div class="card-body d-flex flex-column p-0">
                            <!--begin::Stats-->
                            <div class="flex-grow-1 card-spacer-x pt-6">
                              <div class="text-inverse-danger font-weight-bold">Jabatan Struktural</div>
                              <div class="text-inverse-danger font-weight-bolder font-size-h3">3,620</div>
                            </div>
                            <!--end::Stats-->
                            <!--begin::Chart-->
                            <div id="kt_tiles_widget_2_chart" class="card-rounded-bottom" style="height: 50px"></div>
                            <!--end::Chart-->
                          </div>
                          <!--end::Body-->
                        </div>
                      </div>

                      <div class="col-lg-2">
                        <div class="card card-custom bg-success gutter-b" style="height: 130px">
                          <!--begin::Body-->
                          <div class="card-body d-flex flex-column p-0">
                            <!--begin::Stats-->
                            <div class="flex-grow-1 card-spacer-x pt-6">
                              <div class="text-inverse-danger font-weight-bold">Anjab Menunggu Verfikasi</div>
                              <div class="text-inverse-danger font-weight-bolder font-size-h4">3,620</div>
                            </div>
                            <!--end::Stats-->
                            <!--begin::Chart-->
                            <div id="kt_tiles_widget_2_chart" class="card-rounded-bottom" style="height: 50px"></div>
                            <!--end::Chart-->
                          </div>
                          <!--end::Body-->
                        </div>
                      </div>

                    </div> --}}

                    @if(\MojokertokabUser::getUser()->role == 'ADMIN')
                    
                    <form method="GET">
                      <input type="hidden" name="jenis" value="{{ $jenis }}">
                      <div class="form-group row">
                        <label class="col-1 col-form-label">Filter</label>
                        
                        {{-- <div class="col-2">
                          <input class="form-control" type="text" name="jabatan" placeholder="Nama Jabatan ...">
                        </div> --}}
                        <div class="col-4">
                          <select class="form-control select2" name="unit_kerja">
                            <option value="">-- PILIH UNIT KERJA --</option>
                            @foreach($unitKerja as $itemUnitkerja)
                            <option {{ $selectedUnitKerja == $itemUnitkerja->kodeunit?'selected':'' }} value="{{ $itemUnitkerja->kodeunit }}">{{ $itemUnitkerja->unitkerja }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-1">
                            <button class="btn btn-primary" id="filterunitkerja">Filter</button>
                        </div>
                    </div>
                    </form>
                    @endif

                    <div class="alert alert-danger">
                        Apabila pada daftar jabatan ada keterangan "Harus Diperbarui" maka Admin OPD harus melakukan perbaruan data dengan klik Menu > Edit > Perbaiki Data > Simpan
                    </div>

                  </div>

                  <div class="table-responsive" style="overflow-x: visible;">
                    <!--begin: Datatable-->
                    <table class="table" id="anjab_tabel" style="margin-top: 13px !important">
                      <thead class="thead-light">
                        <tr>
                          <th style="text-align: center" rowspan="4">No</th>
                          <th style="text-align: center" rowspan="8">File SKJ</th>
                          
                          {{-- <th class="text-center" colspan="11">Analisis</th> --}}
                          {{-- <th rowspan="1"></th> --}}
                        </tr>
                        {{-- <tr>
                          <th>UT</th>
                          <th>TJ</th>
                          <th>W</th>
                          <th>KJ</th>
                          <th>BK</th>
                          <th>AK</th>
                          <th>HK</th>
                          <th>KLK</th>
                          <th>RB</th>
                          <th>SJ</th>
                          <th>PK</th>
                        </tr> --}}
                      </thead>
                      <tbody>

                        @if(count($skj) > 0)
                          @foreach($skj as $itemSkj)
                            @if($itemSkj->jenis_jabatan == '2' && $itemSkj->jenjang == NULL)

                            @else

                            @endif
                            {{-- @foreach($skj as $itemSkj) --}}
                          <tr>
                            <td style="text-align: center">{{ $loop->iteration }}</td>
                            {{-- <td>{{ $itemAnjab->status_verification}}</td> --}}
                            <td style="text-align: center">
                             
                            </td>
                            
                            {{-- <td>
                              @if($itemAnjab->num_uraian_jabatan > 0)
                              <span class="badge badge-warning text-white">{{ $itemAnjab->num_uraian_jabatan }}</span>
                              @else
                              <span class="badge badge-danger text-white">{{ $itemAnjab->num_uraian_jabatan }}</span>
                              @endif
                            </td>
                            <td>
                              @if($itemAnjab->num_tanggung_jawab > 0)
                              <span class="badge badge-warning text-white">{{ $itemAnjab->num_tanggung_jawab }}</span>
                              @else
                              <span class="badge badge-danger text-white">{{ $itemAnjab->num_tanggung_jawab }}</span>
                              @endif
                            </td>
                            <td>
                              @if($itemAnjab->num_wewenang > 0)
                              <span class="badge badge-warning text-white">{{ $itemAnjab->num_wewenang }}</span>
                              @else
                              <span class="badge badge-danger text-white">{{ $itemAnjab->num_wewenang }}</span>
                              @endif
                            </td>
                            <td>
                              @if($itemAnjab->num_korelasi_jabatan > 0)
                              <span class="badge badge-warning text-white">{{ $itemAnjab->num_korelasi_jabatan }}</span>
                              @else
                              <span class="badge badge-danger text-white">{{ $itemAnjab->num_korelasi_jabatan }}</span>
                              @endif
                            </td>
                            <td>
                              @if($itemAnjab->num_bahan_kerja > 0)
                              <span class="badge badge-warning text-white">{{ $itemAnjab->num_bahan_kerja }}</span>
                              @else
                              <span class="badge badge-danger text-white">{{ $itemAnjab->num_bahan_kerja }}</span>
                              @endif
                            </td>

                            <td>
                              @if($itemAnjab->num_alat_kerja > 0)
                              <span class="badge badge-warning text-white">{{ $itemAnjab->num_alat_kerja }}</span>
                              @else
                              <span class="badge badge-danger text-white">{{ $itemAnjab->num_alat_kerja }}</span>
                              @endif
                            </td>

                            <td>
                              @if($itemAnjab->num_hasil_kerja > 0)
                              <span class="badge badge-warning text-white">{{ $itemAnjab->num_hasil_kerja }}</span>
                              @else
                              <span class="badge badge-danger text-white">{{ $itemAnjab->num_hasil_kerja }}</span>
                              @endif
                            </td>

                            <td>
                              @if($itemAnjab->kondisi_lingkungan_kerja)
                              <span class="badge badge-success text-white">OK</span>
                              @else
                              <span class="badge badge-danger text-white">BELUM</span>
                              @endif
                            </td>

                            <td>
                              @if($itemAnjab->num_resiko_bahaya > 0)
                              <span class="badge badge-warning text-white">{{ $itemAnjab->num_resiko_bahaya }}</span>
                              @else
                              <span class="badge badge-danger text-white">{{ $itemAnjab->num_resiko_bahaya }}</span>
                              @endif
                            </td>
                            <td>
                              @if($itemAnjab->syarat_jabatan)
                              <span class="badge badge-success text-white">OK</span>
                              @else
                              <span class="badge badge-danger text-white">BELUM</span>
                              @endif
                            </td>
                            <td>
                              @if($itemAnjab->num_prestasi_kerja > 0)
                              <span class="badge badge-warning text-white">{{ $itemAnjab->num_prestasi_kerja }}</span>
                              @else
                              <span class="badge badge-danger text-white">{{ $itemAnjab->num_prestasi_kerja }}</span>
                              @endif
                            </td> --}}

                          </tr>
                          @endforeach
                        @else
                          <tr>
                            <td class="text-center" colspan="19">TIDAK ADA SKJ Yang Terupload</td>
                          </tr>
                        @endif


                      </tbody>

                    </table>
                    <!--end: Datatable-->
                  </div>

                  

                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

            <div id="modal-skj" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-l">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('skjupload.save') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                      

                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button id="btn-submit-anjab" type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            @if(\MojokertokabUser::getUser()->role == 'ADMIN')
  
            <div id="modal-copy-anjab" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="">
                    {{ csrf_field() }}
                    <div class="modal-body">

                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Copy <i class="icon-copy"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            @endif

            <div id="modal-org" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-xl">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">

                  </div>

                  <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </div>

          <form id="form-delete-anjab" method="POST" action="">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('page-script')
<script type="text/javascript">
  $('.select2').select2();

  // initEdit();
  // initHapus();
  // initCopy();

  toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};

  $('.add-skj').on('click', function(){

    var jenis_jabatan = $(this).data('jenis-jabatan');
    var kode_opd = $(this).data('kode-opd');

    $.ajax({
      url:'{{ route("skjupload.get-form") }}',
      method:'GET',
      data:{aksi:'add-skj', jenis_jabatan: jenis_jabatan, kode_opd:kode_opd},
      success:function(result){
        $('#modal-skj .modal-title').text("Upload SKJ");
        $('#modal-skj .modal-body').html(result);
        $('#modal-skj').modal('show');
        // $('#btn-submit-skjupload').attr('disabled', true);
        $('.select2').select2();

        // pilih_jabatan();
      }
    });
});
</script>


@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection



