@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      <a href="javascript:;" class="btn btn-primary btn-sm font-weight-bolder add-instansi">
                       <i class="far fa-plus-square"></i>Tambah Data</a>
                      <!--end::Button-->
                    </div>
                  </div>
                  <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table" id="anjab_tabel" style="margin-top: 13px !important">
                      <thead class="thead-light">
                        <tr>
                          <th>No</th>
                          <th>Instansi Pembina</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @if($instansi->count() == 0)
                        <tr class="text-center">
                          <td colspan="3">Tidak Ada Instansi Pembina</td>
                        </tr>
                        @endif
                       @foreach($instansi as $item)
                       <tr>
                         <td>{{ $loop->iteration + $instansi->firstItem()-1 }}</td>
                         <td>{{ $item->nama }} </td>
                         <td>
                           <div class="btn-group dropleft">
                              <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Menu
                              </button>
                              <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                <!--begin::Naviigation-->
                                <ul class="navi">

                                  <li class="navi-item">
                                    <a data-id="{{ $item->id }}"  href="javascript:;" class="navi-link edit-instansi">
                                      <span class="navi-icon">
                                        <i class="navi-icon flaticon-edit-1"></i>
                                      </span>
                                      <span class="navi-text">Edit</span>
                                    </a>
                                  </li>

                                  <li class="navi-item">
                                    <a data-id="{{ $item->id }}"  href="javascript:;" class="navi-link delete-instansi">
                                      <span class="navi-icon">
                                        <i class="navi-icon flaticon-delete-1"></i>
                                      </span>
                                      <span class="navi-text">Hapus</span>
                                    </a>
                                  </li>
                                </ul>
                                <!--end::Naviigation-->
                              </div>
                          </div>
                         </td>
                       </tr>
                       @endforeach
                      </tbody>

                    </table>
                    <!--end: Datatable-->
                     {{ $instansi->links() }}
                  </div>
                </div>
                <!--end::Card-->

                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

            <div id="modal-instansi" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('instansi-pembina.save') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">

                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          <form id="form-delete-instansi" method="POST" action="{{ route('instansi-pembina.delete') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
  initEdit();
  initHapus();

  $('.add-instansi').on('click', function(){
    $.ajax({
      url:'{{ route("instansi-pembina.get-form") }}',
      method:'GET',
      data:{aksi:'create-instansi'},
      success:function(result){
        $('#modal-instansi .modal-title').text("Tambah Instansi Pembina");
        $('#modal-instansi .modal-body').html(result);
        $('#modal-instansi').modal('show');
        $('.select2').select2();
      }
    });
  });

  function initEdit(){
    $('.edit-instansi').on('click', function(){
      var id = $(this).data('id');
      $.ajax({
        url:'{{ route("instansi-pembina.get-form") }}',
        method:'GET',
        data:{aksi:'edit-instansi',id:id },
        success:function(result){
          $('#modal-instansi .modal-title').text("Edit Instansi Pembina");
          $('#modal-instansi .modal-body').html(result);
          $('#modal-instansi').modal('show');
          $('.select2').select2();
        }
      });
    });
  }

  function initHapus(){
    $('.delete-instansi').on('click', function(){
      var id = $(this).data('id');
          swal.fire({
              title: 'Apakah anda yakin?',
              text: "Data yang sudah dihapus, tidak dapat dikembalikan",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Hapus Data',
              cancelButtonText: 'Batal',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-warning',
              buttonsStyling: false
          }).then(function (status) {
            if(status.value){
              $('form[id=form-delete-instansi] input[name=id]').val(id);
                  $('form[id=form-delete-instansi').submit();
            }
          });
    });
  }



</script>
@endsection
