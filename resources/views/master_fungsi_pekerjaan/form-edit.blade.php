<div class="row">
	<div class="col-12">
		<input type="hidden" value="update-jabatan" name="aksi">
		<input type="hidden" value="{{ $jabatan->id }}" name="id">

		<div class="form-group">
			<label>Nama</label>
			<input value="{{ $jabatan->nama }}" class="form-control" type="text" name="nama" required="">
		</div>

		<div class="form-group">
			<label>Keterangan Fungsi Pekerjaan</label>
			<textarea class="form-control" name="keterangan">{{ $jabatan->keterangan }}</textarea>
		</div>


	</div>
	
</div>