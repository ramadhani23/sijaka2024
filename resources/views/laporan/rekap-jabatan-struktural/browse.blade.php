@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                      
                    </div>
                  </div>
                  <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-top: 13px !important">
                          <thead class="thead-light">
                            <tr>
                              <th>No</th>
                              <th>Jabatan</th>
                              <th>PD</th>
                              <th>Kelas Jabatan</th>
                              <th>Persediaan Pegawai</th>
                            </tr>
                          </thead>
                          <tbody>
                            @if($evjab->count() == 0 )
                            <tr>
                              <td class="text-center" colspan="6">Tidak ada data</td>
                            </tr>
                            @endif
                            @foreach($evjab as $item)
                            <tr>
                              <td>{{ $loop->iteration + $evjab->firstItem()-1 }}</td>
                              <td>{{ $item->dataAnjab->jabatan->nama }}</td>
                              <td>{{ $item->dataAnjab->dataOpd->unitkerja }}</td>
                              <td>{{ $item->kelas_jabatan }}</td>
                              <td>{{ $item->dataAnjab->total_kebutuhan_pegawai }}</td>
                            </tr>
                            @endforeach
                          </tbody>
                           
                        </table>
                        
                    </div>
                    <!--end: Datatable-->
                  </div>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

          

@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection