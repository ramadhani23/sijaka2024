@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                    </div>
                  </div>
                  <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-top: 13px !important">
                          <thead class="thead-light">
                            <tr>
                              <th>No</th>
                              <th>Jabatan</th>
                              <th>PD</th>
                              <th>Kelas Jabatan</th>
                              <th>Harga Jabatan</th>
                              <th colspan="2">Faktor 1</th>
                              <th colspan="2">Faktor 2</th>
                              <th colspan="2">Faktor 3</th>
                              <th colspan="2">Faktor 4A</th>
                              <th colspan="2">Faktor 4B</th>
                              <th colspan="2">Faktor 5</th>
                              <th colspan="2">Faktor 6</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($evjab as $itemEvjab)
                            <tr>
                              <td class="text-center">{{ $loop->iteration }}</td>
                              <td>{{ $itemEvjab->dataAnjab->jabatan->nama }}</td>
                              <td>{{ $itemEvjab->dataAnjab->dataOpd->unitkerja }}</td>
                              <td class="text-center">{{ $itemEvjab->kelas_jabatan }}</td>
                              <td class="text-center">{{ $itemEvjab->total_nilai }}</td>

                              <td>{{ explode('Level ', $itemEvjab->faktorLevel1->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel1->nilai }}</td>

                              <td>{{ explode('Level ', $itemEvjab->faktorLevel2->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel2->nilai }}</td>

                              <td>{{ explode('Level ', $itemEvjab->faktorLevel3->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel3->nilai }}</td>

                              <td>{{ explode('Level ', $itemEvjab->faktorLevel4A->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel4A->nilai }}</td>
                              <td>{{ explode('Level ', $itemEvjab->faktorLevel4B->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel4B->nilai }}</td>

                              <td>{{ explode('Level ', $itemEvjab->faktorLevel5->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel5->nilai }}</td>

                              <td>{{ explode('Level ', $itemEvjab->faktorLevel6->nama)[1] }}</td>
                              <td>{{ $itemEvjab->faktorLevel6->nilai }}</td>

                            </tr>
                            @endforeach
                          </tbody>
                           
                        </table>
                    </div>
                    <!--end: Datatable-->
                  </div>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

         
@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection