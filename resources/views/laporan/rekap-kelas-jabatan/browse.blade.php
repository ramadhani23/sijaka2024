@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                      
                    </div>
                  </div>
                  <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table class="table table-bordered" style="margin-top: 13px !important">
                          <thead class="thead-light">
                            <tr>
                              <th>No</th>
                              <th>Kelas Jabatan</th>
                              <th>jumlah Persediaan Pegawai</th>
                            </tr>
                          </thead>
                          <tbody>

                           <tr>
                             <td class="text-center">1</td>
                             <td class="text-center">15</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">2</td>
                             <td class="text-center">14</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">3</td>
                             <td class="text-center">13</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">4</td>
                             <td class="text-center">12</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">5</td>
                             <td class="text-center">11</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">6</td>
                             <td class="text-center">10</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">7</td>
                             <td class="text-center">9</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">8</td>
                             <td class="text-center">8</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">9</td>
                             <td class="text-center">7</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">10</td>
                             <td class="text-center">6</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">11</td>
                             <td class="text-center">5</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">12</td>
                             <td class="text-center">4</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">13</td>
                             <td class="text-center">3</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">14</td>
                             <td class="text-center">2</td>
                             <td class="text-center">1</td>
                           </tr>
                           <tr>
                             <td class="text-center">15</td>
                             <td class="text-center">1</td>
                             <td class="text-center">1</td>
                           </tr>
                          </tbody>
                           
                        </table>
                        
                    </div>
                    <!--end: Datatable-->
                  </div>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

          

@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection