<div class="row">
	<div class="col-12">
		<input type="hidden" value="add-jabatan" name="aksi">
		<div class="form-group">
			<label>Jabatan</label>
			<input class="form-control" type="text" name="jabatan" >
		</div>
		<div class="form-group">
			<label>Instansi</label>
			<select class="form-control" name="instansi">
				<option value="">-- Pilih Instansi --</option>
				@foreach($unitKerja as $unit)
					<option value="{{ $unit->kodeunit }}">{{ $unit->unitkerja }}</option>
				@endforeach
			</select>
		</div>
	</div>
</div>
