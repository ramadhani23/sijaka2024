@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page1) ? $page1 : 'Detail' }}
                      <div class="text-muted pt-2 font-size-sm">{{ isset($page1) ? $page1 : 'Detail' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      <a href="javascript:;" class="btn btn-primary btn-sm font-weight-bolder add-jenjang">
                       <i class="far fa-plus-square"></i>Tambah Jenjang</a>
                      <!--end::Button-->
                    </div>
                  </div>
                  <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table" id="anjab_tabel" style="margin-top: 13px !important">
                      <thead class="thead-light">
                        <tr>
                          <th>No</th>
                          <th>Jenjang</th>
                          <th>Golru</th>
                          <th>Angka Kredit</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @if($jenjang->count() == 0)
                        <tr class="text-center">
                          <td colspan="9">Tidak Ada Jabatan</td>
                        </tr>
                        @endif
                       @foreach($jenjang as $item)
                       <tr>
                         <td>{{ $loop->iteration }}</td>
                         
                         <td>@if(array_key_exists($item->jenjang, \MojokertokabApp::getJenjangFungsional()))
                                {{ @\MojokertokabApp::getJenjangFungsional()[$item->jenjang] }}    
                              @else
                                {{ $item->jenjang }} *
                              @endif
                          </td>
                         <td>{{ $item->golru }}</td>
                         <td>{{ $item->angka_kredit }}</td>
                         <td>
                           <div class="btn-group dropleft">
                              <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Menu
                              </button>
                              <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                <!--begin::Naviigation-->
                                <ul class="navi">

                                  <li class="navi-item">
                                    <a data-id="{{ $item->id }}"  href="javascript:;" class="navi-link edit-jenjang">
                                      <span class="navi-icon">
                                        <i class="navi-icon flaticon-edit-1"></i>
                                      </span>
                                      <span class="navi-text">Edit</span>
                                    </a>
                                  </li>

                                  <li class="navi-item">
                                    <a data-id="{{ $item->id }}"  href="javascript:;" class="navi-link delete-jenjang">
                                      <span class="navi-icon">
                                        <i class="navi-icon flaticon-delete-1"></i>
                                      </span>
                                      <span class="navi-text">Hapus</span>
                                    </a>
                                  </li>
                                </ul>
                                <!--end::Naviigation-->
                              </div>
                          </div>
                         </td>
                       </tr>
                       @endforeach
                      </tbody>

                    </table>
                    <!--end: Datatable-->
                     {{-- {{ $jenjang->links() }} --}}
                  </div>
                </div>
                <!--end::Card-->

                <!--end::Dashboard-->
              </div>


              <!--end::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page2) ? $page2 : 'Detail' }}
                      <div class="text-muted pt-2 font-size-sm">{{ isset($page2) ? $page2 : 'Detail' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      <a href="javascript:;" class="btn btn-primary btn-sm font-weight-bolder add-syarat">
                       <i class="far fa-plus-square"></i>Tambah Syarat</a>
                      <!--end::Button-->
                    </div>
                  </div>
                  <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table" id="anjab_tabel" style="margin-top: 13px !important">
                      <thead class="thead-light">
                        <tr>
                          <th>No</th>
                          
                          <th>Pendidikan</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @if($syarat->count() == 0)
                        <tr class="text-center">
                          <td colspan="3">Tidak Ada Jabatan</td>
                        </tr>
                        @endif
                       @foreach($syarat as $item)
                       <tr>
                         <td>{{ $loop->iteration }}</td>
                        
                         <td>{{ $item->pendidikan }}</td>
                         <td>
                           <div class="btn-group dropleft">
                              <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Menu
                              </button>
                              <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                <!--begin::Naviigation-->
                                <ul class="navi">

                                  <li class="navi-item">
                                    <a data-id="{{ $item->id }}"  href="javascript:;" class="navi-link edit-syarat">
                                      <span class="navi-icon">
                                        <i class="navi-icon flaticon-edit-1"></i>
                                      </span>
                                      <span class="navi-text">Edit</span>
                                    </a>
                                  </li>

                                  <li class="navi-item">
                                    <a data-id="{{ $item->id }}"  href="javascript:;" class="navi-link delete-syarat">
                                      <span class="navi-icon">
                                        <i class="navi-icon flaticon-delete-1"></i>
                                      </span>
                                      <span class="navi-text">Hapus</span>
                                    </a>
                                  </li>
                                </ul>
                                <!--end::Naviigation-->
                              </div>
                          </div>
                         </td>
                       </tr>
                       @endforeach
                      </tbody>

                    </table>
                    <!--end: Datatable-->
                     {{-- {{ $jenjang->links() }} --}}
                  </div>
                </div>
                <!--end::Card-->

                <!--end::Dashboard-->
              </div>
              <!--end::Container-->




            <div id="modal-jabatan" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('jabatan-fungsional.save') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

          <form id="form-delete-jenjang" method="POST" action="{{ route('jabatan-fungsional.delete-jenjang') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>

          <form id="form-delete-syarat" method="POST" action="{{ route('jabatan-fungsional.delete-syarat') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
  initEditJenjang();
  initEditSyarat();
  initHapusJenjang();
  initHapusSyarat();

  $('.add-jenjang').on('click', function(){
    $.ajax({
      url:'{{ route('jabatan-fungsional.get-form') }}',
      method:'GET',
      data:{aksi:'create-jenjang', id : '{{ request()->id }}'},
      success:function(result){
        $('#modal-jabatan .modal-title').text("Tambah Jenjang");
        $('#modal-jabatan .modal-body').html(result);
        $('#modal-jabatan').modal('show');
        $('.select2').select2();
      }
    });
  });

  function initEditJenjang(){
    $('.edit-jenjang').on('click', function(){
      var id = $(this).data('id');
      $.ajax({
        url:'{{ route('jabatan-fungsional.get-form') }}',
        method:'GET',
        data:{aksi:'edit-jenjang',id:id },
        success:function(result){
          $('#modal-jabatan .modal-title').text("Edit Jenjang");
          $('#modal-jabatan .modal-body').html(result);
          $('#modal-jabatan').modal('show');
          $('.select2').select2();
        }
      });
    });
  }

  function initHapusJenjang(){
    $('.delete-jenjang').on('click', function(){
      var id = $(this).data('id');
          swal.fire({
              title: 'Apakah anda yakin?',
              text: "Data yang sudah dihapus, tidak dapat dikembalikan",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Hapus Data',
              cancelButtonText: 'Batal',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-warning',
              buttonsStyling: false
          }).then(function (status) {
            if(status.value){
              $('form[id=form-delete-jenjang] input[name=id]').val(id);
                  $('form[id=form-delete-jenjang').submit();
            }
          });
    });
  }





  $('.add-syarat').on('click', function(){
    $.ajax({
      url:'{{ route('jabatan-fungsional.get-form') }}',
      method:'GET',
      data:{aksi:'create-syarat', id : '{{ request()->id }}'},
      success:function(result){
        $('#modal-jabatan .modal-title').text("Tambah Syarat");
        $('#modal-jabatan .modal-body').html(result);
        $('#modal-jabatan').modal('show');
        $('.select2').select2();
      }
    });
  });

  function initEditSyarat(){
    $('.edit-syarat').on('click', function(){
      var id = $(this).data('id');
      $.ajax({
        url:'{{ route('jabatan-fungsional.get-form') }}',
        method:'GET',
        data:{aksi:'edit-syarat',id:id },
        success:function(result){
          $('#modal-jabatan .modal-title').text("Edit Syarat");
          $('#modal-jabatan .modal-body').html(result);
          $('#modal-jabatan').modal('show');
          $('.select2').select2();
        }
      });
    });
  }

  function initHapusSyarat(){
    $('.delete-syarat').on('click', function(){
      var id = $(this).data('id');
          swal.fire({
              title: 'Apakah anda yakin?',
              text: "Data yang sudah dihapus, tidak dapat dikembalikan",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Hapus Data',
              cancelButtonText: 'Batal',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-warning',
              buttonsStyling: false
          }).then(function (status) {
            if(status.value){
              $('form[id=form-delete-syarat] input[name=id]').val(id);
                  $('form[id=form-delete-syarat').submit();
            }
          });
    });
  }
</script>
@endsection
