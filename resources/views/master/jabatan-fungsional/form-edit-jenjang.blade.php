<div class="row">
	<div class="col-12">
		<input type="hidden" value="update-jenjang" name="aksi">
		<input type="hidden" value="{{ $jenjang->id }}" name="id">

		<div class="form-group">
			{{-- <label>Kode Jabatan</label> --}}
			<input class="form-control" type="hidden" name="id_m_jabatan_fungsional" value="{{ $jenjang->id_m_jabatan_fungsional }}" required="">
			<input class="form-control" type="hidden" name="kode_jabatan" value="{{ $jenjang->kode_jabatan }}" required="">
		</div>

        <div class="form-group">
			<label>Jenjang</label>
			<select class="form-control" name="jenjang">
			<option selected="" disabled="">-- PILIH Jenjang --</option>
				@foreach(\MojokertokabApp::getJenjangFungsional() as $key => $itemjenjang)
				<option {{ $jenjang->jenjang == $key?'selected':'' }} value="{{ $key }}">{{ $itemjenjang }}</option>
				@endforeach
				<!-- <option selected="" disabled="">-- PILIH Jenjang --</option>
				<option value="pertama" @if($jenjang->jenjang == 'pertama') selected @endif>Pertama</option>
				<option value="pemula_pelaksana_pemula" @if($jenjang->jenjang == 'pemula_pelaksana_pemula') selected @endif>Pemula/Pelaksana Pemula</option>
				<option value="terampil_pelaksana" @if($jenjang->jenjang == 'terampil_pelaksana') selected @endif>Terampil/Pelaksana</option>
				<option value="mahir_pelaksana_lanjutan" @if($jenjang->jenjang == 'mahir_pelaksana_lanjutan') selected @endif>Mahir/Pelaksana Lanjutan</option>
				<option value="penyelia" @if($jenjang->jenjang == 'penyelia') selected @endif>Penyelia</option>
				<option value="pertama">Pertama</option>
				<option value="muda" @if($jenjang->jenjang == 'muda') selected @endif>Muda</option>
				<option value="madya" @if($jenjang->jenjang == 'madya') selected @endif>Madya</option>
				<option value="utama">Utama</option> -->
				
				{{-- <option value="pemula" @if($jenjang->jenjang == 'pemula') selected @endif>Pemula</option>
				<option value="terampil" @if($jenjang->jenjang == 'terampil') selected @endif>Terampil</option>
				<option value="mahir" @if($jenjang->jenjang == 'mahir') selected @endif>Mahir</option>
				<option value="penyelia" @if($jenjang->jenjang == 'penyelia') selected @endif>Penyelia</option>
				<option value="pertama" @if($jenjang->jenjang == 'pertama') selected @endif>Pertama</option>
				<option value="muda" @if($jenjang->jenjang == 'muda') selected @endif>Muda</option>
				<option value="madya" @if($jenjang->jenjang == 'madya') selected @endif>Madya</option> --}}
			</select>
		</div>

        <div class="form-group">
			<label>Golru</label>
			<select class="form-control" name="golru">
				<option selected="" disabled="">-- PILIH Golru --</option>
				<option value="II/A" @if($jenjang->golru == 'II/A') selected @endif>II/A</option>
				<option value="II/B" @if($jenjang->golru == 'II/B') selected @endif>II/B</option>
				<option value="II/C" @if($jenjang->golru == 'II/C') selected @endif>II/C</option>
				<option value="II/D" @if($jenjang->golru == 'II/D') selected @endif>II/D</option>
				<option value="III/A" @if($jenjang->golru == 'III/A') selected @endif>III/A</option>
				<option value="III/B" @if($jenjang->golru == 'III/B') selected @endif>III/B</option>
				<option value="III/C" @if($jenjang->golru == 'III/C') selected @endif>III/C</option>
				<option value="III/D" @if($jenjang->golru == 'III/D') selected @endif>III/D</option>
				<option value="IV/A" @if($jenjang->golru == 'IV/A') selected @endif>IV/A</option>
				<option value="IV/B" @if($jenjang->golru == 'IV/B') selected @endif>IV/B</option>
				<option value="IV/C" @if($jenjang->golru == 'IV/C') selected @endif>IV/C</option>
			</select>
		</div>

		<div class="form-group">
			<label>Angka Kredit</label>
            <input class="form-control" type="text" name="angka_kredit" value="{{ $jenjang->angka_kredit }}" required="">
		</div>

	</div>

</div>
