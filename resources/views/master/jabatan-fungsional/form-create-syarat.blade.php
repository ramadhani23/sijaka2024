<div class="row">
	<div class="col-12">
		<input type="hidden" value="add-syarat" name="aksi">

		<div class="form-group">
			{{-- <label>Kode Jabatan</label> --}}
			<input class="form-control" type="hidden" name="id_m_jabatan_fungsional" value="{{ $jabatan->id }}" required="">
			<input class="form-control" type="hidden" name="kode_jabatan" value="{{ $jabatan->kode_jabatan }}" required="">
		</div>

        <div class="form-group">
			<label>Pendidikan</label>
			{{-- <select class="form-control" name="pendidikan">
				<option selected="" disabled="">-- PILIH Pendidikan --</option>
				<option value="SMA">SMA</option>
				<option value="D1">D1</option>
				<option value="D2">D2</option>
				<option value="D3">D3</option>
				<option value="D4">D4</option>
				<option value="S1">S1</option>
				<option value="S2">S2</option>
				<option value="S3">S3</option>
				<option value="S1/D4">S1/D4</option>
			</select> --}}
            <input class="form-control" type="text" name="pendidikan" placeholder="Masukkan Pendidikan" required="">
		</div>

	</div>

</div>
