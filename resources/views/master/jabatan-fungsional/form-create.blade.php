<div class="row">
	<div class="col-12">
		<input type="hidden" value="add-jabatan" name="aksi">

		<div class="form-group">
			<label>Nama</label>
			<input class="form-control" type="text" name="nama" required="">
		</div>

		<div class="form-group">
			<label>Rumpun Jabatan</label>
			<select class="form-control" name="rumpun">
				<option selected="" disabled="">-- PILIH Rumpun --</option>
				@foreach($rumpun as $itemRumpun)
				<option value="{{ $itemRumpun->kode }}">{{ $itemRumpun->rumpun }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Ikhtisar</label>
			<textarea class="form-control" name="tugas"></textarea>
		</div>

		<div class="form-group">
			<label>Instansi Pembina</label>
			<select class="form-control" name="instansi_pembina">
				<option selected="" disabled="">-- PILIH Instansi Pembina --</option>
				@foreach($instansiPembina as $itemInstansiPembina)
				<option value="{{ $itemInstansiPembina->id }}">{{ $itemInstansiPembina->nama }}</option>
				@endforeach
			</select>
		</div>

        <div class="form-group">
			<label>Kategori</label>
			<select class="form-control" name="kategori">
				<option selected="" disabled="">-- PILIH Kategori --</option>
				<option value="keahlian">Keahlian</option>
				<option value="ketrampilan">Ketrampilan</option>
				<option value="ketrampilan_keahlian">Ketrampilan & Keahlian</option>
			</select>
		</div>

	</div>

</div>
