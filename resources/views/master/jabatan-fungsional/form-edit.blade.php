<div class="row">
	<div class="col-12">
		<input type="hidden" value="update-jabatan" name="aksi">
		<input type="hidden" value="{{ $jabatan->id }}" name="id">

		<div class="form-group">
			<label>Nama</label>
			<input class="form-control" type="text" name="nama" value="{{ $jabatan->jabatan }}" required="">
		</div>

		<div class="form-group">
			<label>Rumpun Jabatan</label>
			<select class="form-control" name="rumpun">
				<option selected="" disabled="">-- PILIH Rumpun --</option>
				@foreach($rumpun as $itemRumpun)
				<option value="{{ $itemRumpun->kode }}" @if($itemRumpun->id == $jabatan->m_jabatan_fungsional_rumpun_id) selected @endif>{{ $itemRumpun->rumpun }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Tugas</label>
			<textarea class="form-control" name="tugas" rows="5">{{ $jabatan->tugas }}</textarea>
		</div>

		<div class="form-group">
			<label>Instansi Pembina</label>
			<select class="form-control" name="instansiPembina">
				<option selected="" disabled="">-- PILIH Instansi Pembina --</option>
				@foreach($instansiPembina as $itemInstansiPembina)
				<option value="{{ $itemInstansiPembina->id }}" @if($itemInstansiPembina->id == $jabatan->m_jabatan_fungsional_instansi_pembina_id) selected @endif>{{ $itemInstansiPembina->nama }}</option>
				@endforeach
			</select>
		</div>

        <div class="form-group">
			<label>Kategori</label>
			<select class="form-control" name="kategori">
				<option value="keahlian" @if ($jabatan->kategori == "keahlian") selected @endif>Keahlian</option>
				<option value="ketrampilan" @if ($jabatan->kategori == "ketrampilan") selected @endif>Ketrampilan</option>
				<option value="ketrampilan_keahlian" @if ($jabatan->kategori == "ketrampilan_keahlian") selected @endif>Ketrampilan & Keahlian</option>
			</select>
		</div>

	</div>

</div>
