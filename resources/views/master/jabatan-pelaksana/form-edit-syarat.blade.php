<div class="row">
	<div class="col-12">
		<input type="hidden" value="update-syarat" name="aksi">
		<input type="hidden" value="{{ $syarat->id }}" name="id">

		<div class="form-group">
			<label>Kode Jabatan</label>
			<input class="form-control" type="text" name="id_m_jabatan_fungsional" value="{{ $syarat->id_m_jabatan_fungsional }}" required="">
			<input class="form-control" type="text" name="kode_jabatan" value="{{ $syarat->kode_jabatan }}" required="">
		</div>

        <div class="form-group">
			<label>Pendidikan</label>
			<select class="form-control" name="pendidikan">
				<option selected="" disabled="">-- PILIH Pendidikan --</option>
				<option value="SMA" @if($syarat->pendidikan == 'SMA') selected @endif>SMA</option>
				<option value="D1" @if($syarat->pendidikan == 'D1') selected @endif>D1</option>
				<option value="D2" @if($syarat->pendidikan == 'D2') selected @endif>D2</option>
				<option value="D3" @if($syarat->pendidikan == 'D3') selected @endif>D3</option>
				<option value="D4" @if($syarat->pendidikan == 'D4') selected @endif>D4</option>
				<option value="S1" @if($syarat->pendidikan == 'S1') selected @endif>S1</option>
				<option value="S2" @if($syarat->pendidikan == 'S2') selected @endif>S2</option>
				<option value="S3" @if($syarat->pendidikan == 'S3') selected @endif>S3</option>
			</select>
		</div>

	</div>

</div>
