@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Page Vendors Styles(used by this page)-->
<link href="{{ asset('') }}assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles-->
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      <a href="javascript:;" class="btn btn-primary btn-sm font-weight-bolder add-jabatan">
                       <i class="far fa-plus-square"></i>Tambah Jabatan</a>
                      <!--end::Button-->
                    </div>
                  </div>
                  <div class="card-body">
                    <table class="table table-separate table-head-custom table-foot-custom table-checkable" id="kt_datatable" style="margin-top: 13px !important">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                {{-- <th>Kode Urusan Pemerintahan</th> --}}
                                <th>Nama Jabatan</th>
                                <th>Instansi Teknis</th>
                                <th>Klasifikasi</th>
                                <th>Jenjang Pendidikan</th>
                                <th>Kualifikasi Pendidikan</th>
                                <th>Tugas Jabatan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($jabatan as $itemJabatan)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $itemJabatan->kode }}</td>
                                {{-- <td>{{ $itemJabatan->kode_urusan_pemerintahan }}</td> --}}
                                <td>{{ $itemJabatan->jabatan }}</td>
                                <td>{{ $itemJabatan->instansi_teknis }}</td>
                                <td>{{ $itemJabatan->klasifikasi }}</td>
                                <td>{{ $itemJabatan->jenjang_pendidikan }}</td>
                                <td>{{ $itemJabatan->kualifikasi_pendidikan }}</td>
                                <td >{{ $itemJabatan->tugas_jabatan }}</td>
                                <td style="width: 10%">
                                <div class="btn-group dropleft">
                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Menu
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                        <!--begin::Naviigation-->
                                        <ul class="navi">

                                        <li class="navi-item">
                                            <a data-id="{{ $itemJabatan->id }}"  href="javascript:;" class="navi-link edit-jabatan">
                                            <span class="navi-icon">
                                                <i class="navi-icon flaticon-edit-1"></i>
                                            </span>
                                            <span class="navi-text">Edit</span>
                                            </a>
                                        </li>

                                        <li class="navi-item">
                                            <a data-id="{{ $itemJabatan->id }}"  href="javascript:;" class="navi-link delete-jabatan">
                                            <span class="navi-icon">
                                                <i class="navi-icon flaticon-delete-1"></i>
                                            </span>
                                            <span class="navi-text">Hapus</span>
                                            </a>
                                        </li>
                                        </ul>
                                        <!--end::Naviigation-->
                                    </div>
                                </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Kode Urusan Pemerintahan</th>
                                <th>Nama Jabatan</th>
                                <th>Kualifikasi Pendidikan</th>
                                <th>Tugas Jabatan</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                    </table>
                  </div>
                </div>
                <!--end::Card-->

                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

            <div id="modal-jabatan" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('jabatan-pelaksana.save') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

          <form id="form-delete-jabatan" method="POST" action="{{ route('jabatan-pelaksana.delete') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
  initEdit();
  initHapus();

  $('.add-jabatan').on('click', function(){
    $.ajax({
      url:'{{ route('jabatan-pelaksana.get-form') }}',
      method:'GET',
      data:{aksi:'create-jabatan'},
      success:function(result){
        $('#modal-jabatan .modal-title').text("Tambah Jabatan Pelaksana");
        $('#modal-jabatan .modal-body').html(result);
        $('#modal-jabatan').modal('show');
        $('.select2').select2();
      }
    });
  });

  function initEdit(){
    $('.edit-jabatan').on('click', function(){
      var id = $(this).data('id');
      $.ajax({
        url:'{{ route('jabatan-pelaksana.get-form') }}',
        method:'GET',
        data:{aksi:'edit-jabatan',id:id },
        success:function(result){
          $('#modal-jabatan .modal-title').text("Edit Jabatan Pelaksana");
          $('#modal-jabatan .modal-body').html(result);
          $('#modal-jabatan').modal('show');
          $('.select2').select2();
        }
      });
    });
  }

  function initHapus(){
    $('.delete-jabatan').on('click', function(){
      var id = $(this).data('id');
          swal.fire({
              title: 'Apakah anda yakin?',
              text: "Data yang sudah dihapus, tidak dapat dikembalikan",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Hapus Data',
              cancelButtonText: 'Batal',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-warning',
              buttonsStyling: false
          }).then(function (status) {
            if(status.value){
              $('form[id=form-delete-jabatan] input[name=id]').val(id);
                  $('form[id=form-delete-jabatan').submit();
            }
          });
    });
  }

</script>
<!--begin::Page Vendors(used by this page)-->
<script src="{{ asset('') }}assets/plugins/custom/datatables/datatables.bundle.js"></script>
<!--end::Page Vendors-->
<!--begin::Page Scripts(used by this page)-->
<script src="{{ asset('') }}assets/js/pages/crud/datatables/advanced/row-callback.js"></script>
<!--end::Page Scripts-->
@endsection
