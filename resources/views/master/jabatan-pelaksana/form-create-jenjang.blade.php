<div class="row">
	<div class="col-12">
		<input type="hidden" value="add-jenjang" name="aksi">

		<div class="form-group">
			{{-- <label>Kode Jabatan</label> --}}
			<input class="form-control" type="hidden" name="id_m_jabatan_fungsional" value="{{ $jabatan->id }}" required="">
			<input class="form-control" type="hidden" name="kode_jabatan" value="{{ $jabatan->kode_jabatan }}" required="">
		</div>

        <div class="form-group">
			<label>Jenjang</label>
			<select class="form-control" name="jenjang">
				<option selected="" disabled="">-- PILIH Jenjang --</option>
				<option value="pertama">Pertama</option>
				<option value="pemula">Pemula</option>
				<option value="terampil">Terampil</option>
				<option value="mahir">Mahir</option>
				<option value="penyelia">Penyelia</option>
				<option value="pertama">Pertama</option>
				<option value="muda">Muda</option>
				<option value="madya">Madya</option>
			</select>
		</div>

        <div class="form-group">
			<label>Golru</label>
			<select class="form-control" name="golru">
				<option selected="" disabled="">-- PILIH Golru --</option>
				<option value="II/A">II/A</option>
				<option value="II/B">II/B</option>
				<option value="II/C">II/C</option>
				<option value="II/D">II/D</option>
				<option value="III/A">III/A</option>
				<option value="III/B">III/B</option>
				<option value="III/C">III/C</option>
				<option value="III/D">III/D</option>
				<option value="IV/A">IV/A</option>
				<option value="IV/B">IV/B</option>
				<option value="IV/C">IV/C</option>
			</select>
		</div>

		<div class="form-group">
			<label>Angka Kredit</label>
            <input class="form-control" type="text" name="angka_kredit" required="">
		</div>

	</div>

</div>
