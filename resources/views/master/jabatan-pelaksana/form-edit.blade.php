<div class="row">
	<div class="col-12">
		<input type="hidden" value="update-jabatan" name="aksi">
		<input type="hidden" value="{{ $jabatan->id }}" name="id">

		{{-- <div class="form-group">
			<label>Kode Urusan Pemerintahan</label>
			<select class="form-control" name="kode_urusan_pemerintahan">
				<option selected="" disabled="">-- PILIH Kode Urusan Pemerintahan --</option>
				@foreach($urusanPemerintahan as $item)
				<option value="{{ $item->id }}" @if($jabatan->kode_urusan_pemerintahan == $item->id) selected @endif>{{ $item->urusan }}</option>
				@endforeach
			</select>
		</div> --}}

		<div class="form-group">
			<label>Kode</label>
			<input class="form-control" type="text" name="kode" value="{{ $jabatan->kode }}" required="">
		</div>

        <div class="form-group">
			<label>Jabatan</label>
			<input class="form-control" type="text" name="jabatan" value="{{ $jabatan->jabatan }}" required="">
		</div>
		<div class="form-group">
			<label>Instansi Teknis</label>
			<input class="form-control" type="text" name="instansi_teknis" value="{{ $jabatan->instansi_teknis }}">
		</div>
		<div class="form-group">
			<label>Jenjang Pendidikan</label>
			<input class="form-control" type="text" name="jenjang_pendidikan" value="{{ $jabatan->jenjang_pendidikan }}">
		</div>
		<div class="form-group">
			<label>Kualifikasi Pendidikan</label>
            <textarea class="form-control" name="kualifikasi_pendidikan" rows="4">{{ $jabatan->kualifikasi_pendidikan }}</textarea>
		</div>
		<div class="form-group">
			<label>Klasifikasi</label>
			<input class="form-control" type="text" name="klasifikasi" value="{{ $jabatan->klasifikasi }}">
		</div>

		<div class="form-group">
			<label>Tugas</label>
			<textarea class="form-control" name="tugas" rows="4">{{ $jabatan->tugas_jabatan }}</textarea>
		</div>

		{{-- <div class="form-group">
			<label>Kode BKPP</label>
			<input class="form-control" type="text" name="kode_bkpp" value="{{ $jabatan->kode_bkpp }}" required="">
		</div> --}}

        <input class="form-control" type="hidden" name="jenis" value="{{ $jabatan->jenis }}" required="">
		<input class="form-control" type="hidden" name="dasar_hukum" value="Kepmen_1103" required="">
        <input class="form-control" type="hidden" name="_kode" value="12" required="">

	</div>

</div>
