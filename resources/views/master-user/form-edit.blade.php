<div class="row">
	<div class="col-12">
		<input type="hidden" value="edit-user" name="aksi">
		<div class="form-group">
			<label>NIP</label>
			<input readonly="" value="{{ $user->username }}" class="form-control" type="text" name="username">
		</div>

		<div class="form-group">
			<label>Nama</label>
			<input value="{{ $user->option->namalengkap }}" readonly="" class="form-control" type="text" name="nama" required="">
		</div>

		<div class="form-group">
			<label>Perangkat Daerah</label>
			<input value="{{ $user->dataOpd->unitkerja }}" readonly class="form-control" type="text" name="nama" required="">
		</div>

		{{-- <div class="form-group">
			<label>Perangkat Daerah</label>
			<select style="width: 100%" required name="kode_opd" id="kode_opd" class="form-control select2" 
				@if($isReadonly) disabled @endif>
				<option value="" disabled selected>-- PILIH PERANGKAT DAERAH --</option>
				@foreach($unitKerja as $unitKerjas)
					<option @if ($user->kode_opd == $unitKerjas->kodeunit) selected @endif 
						value="{{ $unitKerjas->kodeunit }}">{{ $unitKerjas->unitkerja }}</option>
				@endforeach
			</select>
		</div> --}}

		<div class="form-group">
			<label>Role</label>
			<select class="form-control" name="role" required="">
				<option selected="" disabled="">-- PILIH ROLE --</option>
				<option {{ $user->role == 'ADMIN'?'selected':'' }} value="ADMIN">Admin Aplikasi</option>
				<option {{ $user->role == 'PD'?'selected':'' }} value="PD">Admin OPD</option>
			</select>
		</div>

	</div>
	
</div>