<div class="row">
	<div class="col-12">
		<input type="hidden" value="add-user" name="aksi">
	
		<div class="form-group">
			<label>Perangkat Daerah</label>
			<select style="width: 100%" required="" id="kode_opd" name="kode_opd" class="form-control select2">
				<option value="" disabled="" selected="">-- PILIH PERANGKAT DAERAH --</option>
				@foreach($unitKerja as $unitKerjas)
				<option value="{{ $unitKerjas->kodeunit }}">{{ $unitKerjas->unitkerja }}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Pegawai</label>
			<select style="width: 100%" required="" id="pegawai" name="pegawai" class="form-control select2">
				<option value="" disabled="" selected="">-- PILIH PEGAWAI --</option>
			</select>
		</div>

		<div class="form-group">
			<label>Json</label>
			<input class="form-control" type="text" name="json" required="">
		</div>

		<div class="form-group">
			<label>Role</label>
			<select class="form-control" name="role" required="">
				<option selected="" disabled="">-- PILIH ROLE --</option>
				<option value="ADMIN">Admin Aplikasi</option>
				<option value="PD">Admin OPD</option>
			</select>
		</div>

	</div>
	
</div>