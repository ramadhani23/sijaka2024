@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      <a href="javascript:;" class="btn btn-primary btn-sm font-weight-bolder add-anjab">
                       <i class="far fa-plus-square"></i>Tambah Data</a>
                      <!--end::Button-->
                    </div>
                  </div>
                  <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table" id="anjab_tabel" style="margin-top: 13px !important">
                      <thead class="thead-light">
                        <tr>
                          <th>No</th>
                          <th>Nama Jabatan</th>
                          <th>Jenis</th>
                          <th>Ikhtisar</th>
                          <th>Kualifikasi Pendidikan</th>
                          <th>Kode</th>
                          
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                       
                      </tbody>
                       {{-- <tfoot>
                        <tr>
                          <th>No</th>
                          <th>Nama Jabatan</th>
                          <th>Jenis Jabatan</th>
                          <th>Eselon Jabatan</th>
                          <th>Kode Jabatan</th>
                          <th>Unit Kerja Eselon 2</th>
                          <th>Unit Kerja Eselon 3</th>
                          <th>Unit Kerja Eselon 4</th>
                          <th>Ikhtisar Jabatan</th>
                          <th>Actions</th>
                        </tr>
                      </tfoot> --}}
                    </table>
                    <!--end: Datatable-->
                  </div>
                </div>
                <!--end::Card-->
                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

            <div id="modal-anjab" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5> 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('anjab.save') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                      
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          <form id="form-delete-anjab" method="POST" action="{{ route('anjab.delete') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
 {{-- <script src="{{ asset('') }}/assets/plugins/custom/datatables/datatables.bundle.js"></script> --}}
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
  initEdit();
  initHapus();
   // $('#anjab_tabel').DataTable({
   //    responsive: true,
   //    paging: true,
   // });

  var tabel = null;
  // function generateTable() {
  //   tabel.ajax.reload();
  // }
  // $(document).ready(function(){
  //   $('#anjab_tabel tfoot th').each(function() {
  //     var title = $(this).text();
  //     $(this).html('<input type="text" class="form-control" name="search_tabel" placeholder="Search' + title + '" />');
  //   });
  //   tabel = $('#anjab_tabel').DataTable({
  //     "processing": true,
  //     "serverSide": true,
  //     // "responsive": true,
  //     "ordering": true,
  //     "order": [[ 0, 'asc' ]],
  //     "autoWidth": true,
  //     "ajax":
  //     {
  //       "url": "{{ route('anjab.table') }}",
  //       "type": "GET",
        
  //     },
  //     "deferRender": true,
  //     // "aLengthMenu": [[10, 20, 50],[ 10, 20, 50]], // Combobox Limit

  //     "columnDefs": [
  //       { "targets": [ 0 ],"searchable": true },
  //       { "targets": [ 1 ],"searchable": true, "sortable" : true },
  //       { "targets": [ 2 ],"searchable": true, "sortable" : true },
  //       { "targets": [ 5 ]},
  //       { "targets": [ 6 ]},
  //       { "targets": [ 7 ]},
  //       { "targets": [ 8 ]},
  //       { "targets": [ 9 ], className: "text-center"}
  //     ],
  //     "PaginationType": "bootstrap",
  //     dom: `<'row'<'col-sm-12'tr>>
  //     <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
  //     "lengthMenu": [[10, 25, 50, 100,200,300], [10, 25, 50, 100 ,200,300]],
  //     colReorder: true,
  //     keys: true,
  //     fixedHeader: true,
  //     scrollY: '50vh',
  //     scrollX: true,
  //     buttons: [
  //       {
  //         extend: 'copyHtml5',
  //         exportOptions: {
  //           columns: [ ':visible' ]
  //         }
  //       },{
  //         extend: 'csvHtml5',
  //         exportOptions: {
            
  //           columns: [ ':visible' ]
  //         }
  //       },{
  //         extend: 'excelHtml5',
  //         exportOptions: {
            
  //           columns: [ ':visible' ]
  //         }
  //       },{
  //         extend: 'pdfHtml5',
  //         orientation: 'landscape',
  //         pageSize: 'LEGAL',
  //         exportOptions: {
            
  //           columns: [ ':visible' ]
  //         }
  //       },{
  //         extend: 'print',
  //         exportOptions: {
            
  //           columns: [ ':visible' ]
  //         }
  //       }, 'colvis'
  //     ],

  //     drawCallback: function( settings ) {
  //       initEdit();
  //       initHapus();
  //     }

  //   });
  //   tabel.columns().every( function () {
  //     var that = this;
  //     $('input', this.footer() ).on('keyup change', function () {
  //       if ( that.search() !== this.value ) {
  //         that.search(this.value).draw();
  //       }
  //     });
  //   });
  // });

  $('.add-anjab').on('click', function(){
    $.ajax({
      url:'{{ route("anjab.get-form") }}',
      method:'GET',
      data:{aksi:'add-anjab'},
      success:function(result){
        $('#modal-anjab .modal-title').text("Tambah Anjab");
        $('#modal-anjab .modal-body').html(result);
        $('#modal-anjab').modal('show');
        $('.select2').select2();
        // pilih_jabatan();
      }
    });
  });

  // function pilih_jabatan(){
  //   $('select[name=jabatan]').on('change', function(){
  //     $.ajax({
  //       url:'',
  //       method:'get',
  //       data:'',
  //       success: function(result){
  //         $('textarea[name=ikhtisar_jabatan]').val(result);
  //       }
  //     });
  //   });
  // }
  
  function initEdit(){
    $('.edit-anjabs').on('click', function(){
      var id = $(this).data('id');
      $.ajax({
        url:'{{ route("anjab.get-form") }}',
        method:'GET',
        data:{aksi:'edit-anjab',id:id },
        success:function(result){
          $('#modal-anjab .modal-title').text("Edit Anjab");
          $('#modal-anjab .modal-body').html(result);
          $('#modal-anjab').modal('show');
          $('.select2').select2();
        }
      });
    });
  }

  function initHapus(){
    $('.delete-anjab').on('click', function(){
      var id = $(this).data('id');
          swal.fire({
              title: 'Apakah anda yakin?',
              text: "Menghapus Analisa Jabatan, akan menghapus ABK dan Evjab. Data yang sudah dihapus, tidak dapat dikembalikan",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Hapus Data',
              cancelButtonText: 'Batal',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-warning',
              buttonsStyling: false
          }).then(function (status) {
            if(status.value){
              $('form[id=form-delete-anjab] input[name=id]').val(id);
                  $('form[id=form-delete-anjab').submit();
            }
          });
    });
  }

  

</script>
@endsection