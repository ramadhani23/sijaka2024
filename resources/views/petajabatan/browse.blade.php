@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
  caption {
            font-size: 1.5em;
            margin: 10px;
  }
</style>
<!--begin::Container-->
              <div class="container">
                
                <div class="card mb-3" style="justify-content: center; display: flex">
                  
                  @if(\MojokertokabUser::getUser()->role == 'ADMIN' || 
                    (\MojokertokabUser::getUser()->role == 'PD' && \MojokertokabUser::getUser()->kode_opd === '28'))
                    
                  <div class="card-header flex-wrap py-2">
                        <form method="GET" action="{{ \MojokertokabUser::getUser()->kode_opd == '28' ? route('petajabatanupt.browse') : route('petajabatan.browse') }}">
                          <div class="form-group row align-items-center">
                            <label class="col-1 col-form-label">Filter</label>
                            {{-- <div class="col-4">
                              <input class="form-control" type="text" name="jabatan" placeholder="Nama Jabatan ...">
                            </div> --}}

                            <div class="col-3">
                              <select required class="form-control select2" name="{{ \MojokertokabUser::getUser()->kode_opd == '28' ? 'kodeopd' : 'unit_kerja' }}">>
                                <option selected="" disabled="">-- PILIH Unit Kerja --</option>
                                @foreach($unitKerja as $itemUnitkerja)
                                {{-- <option  value="{{ $itemUnitkerja->kodeunit }}">{{ $itemUnitkerja->unitkerja }}</option> --}}
                                <option @if(@$unit_kerja == $itemUnitkerja['kodeunit'] ) selected @endif value="{{ $itemUnitkerja['kodeunit'] }}" {{ old('unit_kerja') == $itemUnitkerja['kodeunit'] ? "selected" :""}}>{{  $itemUnitkerja['unitkerja'] }}</option>
                                @endforeach
                              </select>
                            </div>

                            <div class="col-1">
                              <button class="btn btn-primary">Filter</button>
                            </div>
                          </div>
                        </form>
                      </div>
                      @endif
                </div>
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="row flex-center">
                  <div class="card card-custom" style="justify-content: center; display: flex">
                    <h1 style="text-align: center">Peta Jabatan {{ $satker }}</h1>
                    <div class="card-toolbar mr-3" style="text-align: right">
                      <button class="bg-blue-500 text-black font-semibold py-2 px-4 rounded hover:bg-blue-700 transition ease-in-out duration-300 print-button" onclick="window.print()">Print Page</button>
                    </div>
                    <div class="tf-tree mt-3" style="justify-content:flex-start; display: flex">
                      @if ($data_parent == '8')
                        <ul>
                          <li>
                            <span class="tf-nc">Bupati Kabupaten Mojokerto</span>
                            <ul>
                          @foreach ($hierarchy[8] as $item)
                            <li>
                              <span class="tf-nc">{{ $item['nama'] }}</span>
                                @if (isset($hierarchy[$item['id']]))
                                    @include('petajabatan.partials_child', ['children' => $hierarchy[$item['id']]])
                                @endif
                            </li>
                          @endforeach
                            </ul>
                          </li>
                        </ul>
                      @elseif ($data_parent == '9')
                      <ul>
                        <li>
                          <span class="tf-nc">Sekretariat Daerah Kabupaten Mojokerto</span>
                          <ul>
                        @foreach ($hierarchy[9] as $item)
                          <li>
                            <span class="tf-nc">{{ $item['nama'] }}</span>
                              @if (isset($hierarchy[$item['id']]))
                                  @include('petajabatan.partials_child', ['children' => $hierarchy[$item['id']]])
                              @endif
                          </li>
                        @endforeach
                          </ul>
                        </li>
                      </ul>
                      @endif
                    </div>
                    <div class="row">
                      <div class="col-6">
                        <div>
                          <table class="table table-bordered">
                            <h3 style="text-align: center">Jabatan fungsional</h3>
                            <thead>
                              <tr>
                                <th scope="col" style="text-align: center">No</th>
                                <th scope="col" style="text-align: center">Nama Jabatan</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($anjabfungsional as $item)   
                              <tr>
                                <th scope="row" style="text-align: center">{{ $loop->iteration }}</th>
                                <td style="text-align: center">{{ @$item->jabatan->jabatan }}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                    </div>
                    <div class="col-6">
                      <div>
                        <table class="table table-bordered">
                          <h3 style="text-align: center">Jabatan Pelaksana</h3>
                          <thead>
                            <tr>
                              <th scope="col" style="text-align: center">No</th>
                              <th scope="col" style="text-align: center">Nama Jabatan</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($anjabpelaksana as $item)   
                            <tr>
                              <th scope="row" style="text-align: center">{{ $loop->iteration }}</th>
                              <td style="text-align: center">{{ $item->jabatan->jabatan }}</td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                    
                  </div>
                </div>
                <!--end::Card-->

                <!--end::Dashboard-->
              </div>
              <!--end::Container-->
@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
  $('.select2').select2();
</script>
@endsection

<style>
.form-group.row {
    display: flex;
    align-items: center; /* Memusatkan secara vertikal */
}

.form-group.row .col-form-label,
.form-group.row select,
.form-group.row button {
    margin: 0;
}

.form-group.row select {
    padding: 5px;
}

.form-group.row button {
    padding: 5px 10px;
    background-color: #007bff;
    color: white;
    border: none;
    border-radius: 3px;
}
.form-group {
    margin-bottom: 1.7rem;
    margin-top: 1.7rem;
}


</style>

