@if (isset($children) && count($children) > 0)
    <ul>
        @foreach ($children as $child)
            <li>
                <span class="tf-nc">{{ $child['nama'] }}</span>
                @if (isset($hierarchy[$child['id']]))
                    @include('petajabatan.partials_child', ['children' => $hierarchy[$child['id']]])
                @endif
             </li>
        @endforeach
    </ul>
@endif

