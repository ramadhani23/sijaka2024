<div class="row">
	<div class="col-12">
		<input type="hidden" value="update-jabatan" name="aksi">
		<input type="hidden" value="{{ $jabatan->kodeunit }}" name="id">

		<div class="form-group">
			<label>Kode Unit</label>
			<input value="{{ $jabatan->kodeunit }}" class="form-control" type="text" name="kodeunit" required="">
		</div>

		<div class="form-group">
			<label>Unit Kerja</label>
			<textarea class="form-control" name="unitkerja">{{ $jabatan->unitkerja }}</textarea>
		</div>

	</div>
	
</div>