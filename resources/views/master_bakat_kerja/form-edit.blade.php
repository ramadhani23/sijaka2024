<div class="row">
	<div class="col-12">
		<input type="hidden" value="update-bakatkerja" name="aksi">
		<input type="hidden" value="{{ $bakatkerja->id }}" name="id">

		<div class="form-group">
			<label>Nama</label>
			<input value="{{ $bakatkerja->nama }}" class="form-control" type="text" name="nama" required="">
		</div>

		<div class="form-group">
			<label>Keterangan</label>
			<textarea class="form-control" name="keterangan">{{ $bakatkerja->keterangan }}</textarea>
		</div>

	</div>
	
</div>