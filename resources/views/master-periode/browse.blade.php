@extends('layout')

@section('content')
<style type="text/css">
  #anjab_tabel_filter{
    text-align: right;
  }
</style>
<!--begin::Container-->
              <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Card-->
                <div class="card card-custom">
                  <div class="card-header flex-wrap py-5">
                    <div class="card-title">
                      <h3 class="card-label">{{ isset($page) ? $page : 'Dashboard' }}
                      <div class="text-muted pt-2 font-size-sm">Data {{ isset($page) ? $page : 'Dashboard' }}</div></h3>
                    </div>
                    <div class="card-toolbar">
                      <!--begin::Button-->
                      <a href="javascript:;" class="btn btn-primary btn-sm font-weight-bolder add-periode">
                       <i class="far fa-plus-square"></i>Tambah Data</a>
                      <!--end::Button-->
                    </div>
                  </div>
                  <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table" id="anjab_tabel" style="margin-top: 13px !important">
                      <thead class="thead-light">
                        <tr>
                          <th>No</th>
                          <th>Periode</th>
                          <th>Status</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                       @foreach($periode as $itemPeriode)
                       <tr>
                         <td>{{ $loop->iteration + $periode->firstItem()-1 }}</td>
                         <td>{{ $itemPeriode->periode }}</td>
                         <td>
                           @if($itemPeriode->is_aktif == 1)
                            <span class="badge badge-success">Aktif</span>
                           @else
                            <span class="badge badge-danger">Tidak Aktif</span>
                           @endif
                         </td>
                         <td>
                           <div class="btn-group dropleft">
                              <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Menu
                              </button>
                              <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">
                                <!--begin::Naviigation-->
                                <ul class="navi">
                                 
                                  <li class="navi-item">
                                    <a data-id="{{ $itemPeriode->id }}"  href="javascript:;" class="navi-link edit-periode">
                                      <span class="navi-icon">
                                        <i class="navi-icon flaticon-edit-1"></i>
                                      </span>
                                      <span class="navi-text">Edit</span>
                                    </a>
                                  </li>

                                  <li class="navi-item">
                                    <a data-id="{{ $itemPeriode->id }}"  href="javascript:;" class="navi-link delete-periode">
                                      <span class="navi-icon">
                                        <i class="navi-icon flaticon-delete-1"></i>
                                      </span>
                                      <span class="navi-text">Hapus</span>
                                    </a>
                                  </li>
                                </ul>
                                <!--end::Naviigation-->
                              </div>
                          </div>
                         </td>
                       </tr>
                       @endforeach
                      </tbody>
                      
                    </table>
                    <!--end: Datatable-->
                     {{ $periode->links() }}
                  </div>
                </div>
                <!--end::Card-->

                <!--end::Dashboard-->
              </div>
              <!--end::Container-->

            <div id="modal-periode" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5> 
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <form method="POST" action="{{ route('periode.save') }}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                      
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary btn-sm">Simpan <i class="icon-paperplane"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          <form id="form-delete-periode" method="POST" action="{{ route('periode.delete') }}">
            {{ csrf_field() }}
            <input type="hidden" value="" name="id">
          </form>
@endsection

@section('js')
{{-- js files --}}
<script src="{{ asset('') }}/assets/js/pages/crud/forms/widgets/select2.js"></script>
{{-- end js files --}}

@endsection

@section('page-script')
<script type="text/javascript">
  initEdit();
  initHapus();

  $('.add-periode').on('click', function(){
    $.ajax({
      url:'{{ route("periode.get-form") }}',
      method:'GET',
      data:{aksi:'create-periode'},
      success:function(result){
        $('#modal-periode .modal-title').text("Tambah periode");
        $('#modal-periode .modal-body').html(result);
        $('#modal-periode').modal('show');
        $('.select2').select2();
      }
    });
  });

  function initEdit(){
    $('.edit-periode').on('click', function(){
      var id = $(this).data('id');
      $.ajax({
        url:'{{ route("periode.get-form") }}',
        method:'GET',
        data:{aksi:'edit-periode',id:id },
        success:function(result){
          $('#modal-periode .modal-title').text("Edit periode");
          $('#modal-periode .modal-body').html(result);
          $('#modal-periode').modal('show');
          $('.select2').select2();
        }
      });
    });
  }

  function initHapus(){
    $('.delete-periode').on('click', function(){
      var id = $(this).data('id');
          swal.fire({
              title: 'Apakah anda yakin?',
              text: "Data yang sudah dihapus, tidak dapat dikembalikan",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Hapus Data',
              cancelButtonText: 'Batal',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-warning',
              buttonsStyling: false
          }).then(function (status) {
            if(status.value){
              $('form[id=form-delete-periode] input[name=id]').val(id);
                  $('form[id=form-delete-periode').submit();
            }
          });
    });
  }

  

</script>
@endsection