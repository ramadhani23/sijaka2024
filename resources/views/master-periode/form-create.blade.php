<div class="row">
	<div class="col-12">
		<input type="hidden" value="add-periode" name="aksi">

		<div class="form-group">
			<label>Periode</label>
			<input class="form-control" type="text" name="periode" required="">
		</div>

		<div class="form-group">
			<label>Status Aktif</label>
			<select class="form-control" name="is_aktif">
				<option selected="" disabled="">-- Pilih Status Aktif --</option>
				<option value="1">Aktif</option>
				<option value="0">Tidak Aktif</option>
			</select>
		</div>

	</div>
	
</div>