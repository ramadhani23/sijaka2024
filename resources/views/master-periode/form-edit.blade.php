<div class="row">
	<div class="col-12">
		<input type="hidden" value="update-periode" name="aksi">
		<input type="hidden" value="{{ $periode->id }}" name="id">

		<div class="form-group">
			<label>Periode</label>
			<input value="{{ $periode->periode }}" class="form-control" type="text" name="periode" required="">
		</div>
		
		<div class="form-group">
			<label>Status Aktif</label>
			<select class="form-control" name="is_aktif">
				<option selected="" disabled="">-- Pilih Status Aktif --</option>
				<option {{ $periode->is_aktif == '1'?'selected':'' }} value="1">Aktif</option>
				<option {{ $periode->is_aktif == '0'?'selected':'' }} value="0">Tidak AKtif</option>
			</select>
		</div>

	</div>
	
</div>